# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CCC=g++
FC=gfortran
AS=as
CXX = mpiCC
CC = gcc
MV = mv
prefix= .
TIMESTAMP = `date +%Y%m%d_%H%M%S`



# enter source file here
SRCDIR := src
BUILDDIR := obj
TARGET := bin/amody

SOURCES :=  $(SRCDIR)/main.cpp \
			$(SRCDIR)/atom.cpp \
			$(SRCDIR)/datamanager.cpp \
			$(SRCDIR)/simulation.cpp \
			$(SRCDIR)/system.cpp \
			$(SRCDIR)/substance.cpp \
			$(SRCDIR)/math/funcs.cpp \
			$(SRCDIR)/math/neural.cpp \
			$(SRCDIR)/math/vector.cpp
		
OBJECTS := $(SOURCES:.cpp=.o)
#$(BUILDDIR)/$(notdir $(SOURCES:.cpp=.o))

LDFLAGS += -fopenmp -lrt
CFLAGS += -fopenmp -std=c++17 -Werror -march=native -O3 -DPARALLEL_IO=0 -lm -pthread
#CFLAGS += -std=c++11 -lrt
CXXFLAGS = ${CFLAGS}



#  actual compile cases
all: $(SOURCES) $(TARGET)

Debug: all


$(TARGET): $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

install: $(TARGET)
	install -m 0755 $(TARGET) $(prefix)/bin
	$(MAKE) clean

clean:
	$(RM) *~ $(OBJECTS) $(TARGET)

cleanDebug:
	$(RM) *~ $(OBJECTS) $(TARGET)

distclean:
	$(MAKE) clean
	$(RM) *.png *.pdf *.dat *.fld *.eps
	$(RM) $(prefix)/bin/$(TARGET)

store:
	$(MKDIR) $(TIMESTAMP)
	$(MKDIR) $(TIMESTAMP)/source
	$(CP) $(SOURCES) $(TIMESTAMP)/source
	$(MV) *.png *.pdf *.dat *.eps *.fld $(TIMESTAMP)

test:
	$(MAKE) distclean
	$(MAKE) install
	$(prefix)/bin/$(TARGET)
