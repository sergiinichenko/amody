# AMoDy

AMoDy is a general-purpose molecular dynamics code. Initially, AMoDy was created with a goal of accurate transport properties calculation. In particular, AMoDy calculated thermal conductivity and viscosity using improved Green-Kubo algorithm providing quite accurate values of the studied properties without the need to perform multiple simulations to gather sufficient statistics.
 AMoDy is C++ base code, mainly for performance reasons. The code also implements such technologies, as MPI and OpenMP, allowing it be used on modern HPC systems and simulate big systems.

## Getting Started

To get started with AMoDy please download a copy of this repository. There are two options to do that
1. If you have git installed on your system, then downloading this repository is as easy as running the following command in a terminal:

git clone https://sergiinichenko@bitbucket.org/sergiinichenko/amody.git AMoDy

2. As a second option you can simply download zip archive of AMoDy into your local folder using the link below and unzip it.

https://bitbucket.org/sergiinichenko/amody/get/master.zip


## Installation

### Requirements

AMoDy reqieres OpenMP and MPI (OpenMPI or MPICH) to be installed on your machine before compiling

### Compilation

The supplied Makefile already contains basic setup so that you could compile AMoDy and start using it.
To compile AMoDy you have to change to the derectore, where you cloned / unziped AMoDy using the following command
```
cd /amody/installation/folder
```

After that you have to compile the project using supplied Makefile

```
make
```

This comand will create amody executable in the bin directory of the project.
If this step fails this means that, most probably, you are missing some libraries (OpenMP or MPI).

## Running the program

To run AMoDy you have to supply amody with input file and run it like this

```
./pathToAmodyExecutable PathToInputFile/inputFile.amd
```

An example of the input file (UO2.amd) can be found in the example folder. The example forlder contains the minimum configuration to run AMoDy. 
The UO2.amd contains the general setup of the project. In this file lines starting with /, //, # or @ are the comment lines and are there only for information. Files UO2.xyz and Potential.data contain information on the system structure (UO2.xyz file) and potential parameters (Potential.data) for the force field model.
To run the example project from the AMoDy folder:

```
./bin/amody example/UO2.amd
```

## Authors

* **Sergii Nichenko** - *Initial work* - [PurpleBooth](sergii.nichenko@psi.ch)

## License

AMoDy is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

AMoDy is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

