#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 10:37:34 2020

@author: nichenko_s
"""

#%%
import numpy as np

#%%
size  = 20.0
shape = 10000
x = np.random.random(shape) * size
y = np.random.random(shape) * size
z = np.random.random(shape) * size

#%%
dx = np.matrix([x,]*shape)
dy = np.matrix([y,]*shape)
dz = np.matrix([z,]*shape)

dx = dx - dx.T
dy = dy - dy.T
dz = dz - dz.T

R = np.multiply(dx,dx) + np.multiply(dy,dy) + np.multiply(dz,dz)
