# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 13:38:02 2019


@author: nichenko_s
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def preset(angl):
    global tr, itr
    cos_a = np.cos(np.pi*angl[0] / 180.0)
    cos_b = np.cos(np.pi*angl[1] / 180.0)
    cos_g = np.cos(np.pi*angl[2] / 180.0)


    sin_a = np.sin(np.pi*angl[0] / 180.0)
    sin_b = np.sin(np.pi*angl[1] / 180.0)
    sin_g = np.sin(np.pi*angl[2] / 180.0)
    
    omega = (1.0 - cos_a**2.0 - cos_b**2.0 - cos_g**2.0 + 2.0*cos_a*cos_b*cos_g)**0.5
    
    tr  = np.matrix([[1.0, cos_g, cos_b], 
                     [0.0, sin_g, (cos_a - cos_b*cos_g)/sin_g], 
                     [0.0, 0.0,   omega / sin_g]])
    
    itr = np.matrix([[1.0, -cos_g/sin_g, (cos_g*(cos_a - cos_b*cos_g)/sin_g - cos_b*sin_g)/omega], 
                     [0.0,  1.0/sin_g,   (cos_b*cos_g-cos_a)/(omega*sin_g)],
                     [0.0,  0.0,        sin_g/omega]])
    
def trans(x, y, z, ang):
    coor = np.array(list(zip(x, y, z)))
    for i in range(len(coor)):
        res = tr * coor[i].reshape(3,1)
        x[i] = res[0]
        y[i] = res[1]
        z[i] = res[2]
    return x, y, z


def itrans(x, y, z, ang):
    coor = np.array(list(zip(x, y, z)))
    for i in range(len(coor)):
        res = itr * coor[i].reshape(3,1)
        x[i] = res[0]
        y[i] = res[1]
        z[i] = res[2]
    return x, y, z


def plot(x,y,z):
    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x,y,z)
    ax.set_xlim(-2.0,2.0)
    ax.set_ylim(-1.0,2.0)

tr  = np.eye(3)
itr = np.eye(3)

a = 1.7
b = 1.2
c = 1.5
wall = [a, b, c]
angl = [90.0, 126.33, 90.0]
preset(angl)

wal_x = [0.0, a]
wal_y = [0.0, b]
wal_z = [0.0, c]

x = np.array([  0.0])#np.random.rand(10000)*2.5 - 0.5
y = np.array([  0.0])#np.random.rand(10000)*2.5 - 0.5
z = np.array([ -1.0])#np.random.rand(10000)*2.5 - 0.5


x,y,z = trans(x,y,z,angl)
plot(x,y,z)


x,y,z = itrans(x,y,z,angl)
plot(x,y,z)
for i in range(len(x)):
    if x[i] < 0.0:     x[i] = x[i] + a
    if x[i] > wall[1]: x[i] = x[i] - a
    if y[i] < 0.0:     y[i] = y[i] + b
    if y[i] > wall[1]: y[i] = y[i] - b
    if z[i] < 0.0:     z[i] = z[i] + c
    if z[i] > wall[2]: z[i] = z[i] - c
plot(x,y,z)


x,y,z = trans(x,y,z,angl)
plot(x,y,z)


x,y,z = itrans(x,y,z,angl)
plot(x,y,z)