//-------------------------------------------------------------------
// File:   amody.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "atom.h"


Atom::Atom()  {
    size = 1;
    rsize = 1.0;
    id.resize(1, 0);
    Cell.resize(1, 0);
    rmin.resize(1, 0);
    rmin2.resize(1, 0);
    EscP.resize(1, 0);
    EscS.resize(1, 0);
    FscRP.resize(1, 0);
    FscRS.resize(1, 0);
    Ep.resize(1, 0);
    Em.resize(1, 0);
    name.resize(1, "none");
    mass.resize(1, 0);
    rmass.resize(1, 0);
    x.resize(1, 0);
    y.resize(1, 0);
    z.resize(1, 0);
    x0.resize(1, 0);
    y0.resize(1, 0);
    z0.resize(1, 0);
    xav.resize(1, 0);
    yav.resize(1, 0);
    zav.resize(1, 0);
    xp.resize(1, 0);
    yp.resize(1, 0);
    zp.resize(1, 0);
    xd.resize(1, 0);
    yd.resize(1, 0);
    zd.resize(1, 0);
    vx.resize(1, 0);
    vy.resize(1, 0);
    vz.resize(1, 0);
    ax.resize(1, 0);
    ay.resize(1, 0);
    az.resize(1, 0);
    axn.resize(1, 0);
    ayn.resize(1, 0);
    azn.resize(1, 0);
    axo.resize(1, 0);
    ayo.resize(1, 0);
    azo.resize(1, 0);
    vFx.resize(1);
    vFy.resize(1);
    vFz.resize(1);
    vFx[0].clear();
    vFy[0].clear();
    vFz[0].clear();
    Fx.resize(1, 0);
    Fy.resize(1, 0);
    Fz.resize(1, 0);
    Fxp.resize(1, 0);
    Fyp.resize(1, 0);
    Fzp.resize(1, 0);
    Fxs.resize(1, 0);
    Fys.resize(1, 0);
    Fzs.resize(1, 0);
    mux.resize(1, 0);
    muy.resize(1, 0);
    muz.resize(1, 0);
    muxpr.resize(1, 0);
    muypr.resize(1, 0);
    muzpr.resize(1, 0);
    Eqx.resize(1, 0);
    Eqy.resize(1, 0);
    Eqz.resize(1, 0);
    Epx.resize(1, 0);
    Epy.resize(1, 0);
    Epz.resize(1, 0);
    Eex.resize(1, 0);
    Eey.resize(1, 0);
    Eez.resize(1, 0);
    vx_mid.resize(1, 0);
    vy_mid.resize(1, 0);
    vz_mid.resize(1, 0);
    q0.resize(1, 0);
    q.resize(1, 0);
    aff.resize(1, 0);
    ion.resize(1, 0);
    dq.resize(1, 0);
    indj.resize(1, 0);
    Eet.resize(1, 0);
    netq.resize(1, 0);
    a.resize(1, 0);
    b.resize(1, 0);
    PotEnergy.resize(1, 0);
    qs.resize(1, 0);
    ElSelf.resize(1, 0);
    PotEnergy_tau.resize(1, 0);
    ElstEn_tau.resize(1, 0);
    ElstEn.resize(1, 0);
    FscRsc.resize(1, 0);
    KinEnergy_tau.resize(1, 0);
    KinEnergy.resize(1, 0);
    Energy.resize(1, 0);
    Energy_tau.resize(1, 0);
    qi.resize(1, 0);
    condx.resize(1, 0);
    condy.resize(1, 0);
    condz.resize(1, 0);
    Ei.resize(1, 0);
    Eea.resize(1, 0);
    rn.resize(1, 0);
    n.resize(1, 0);
    polariz.resize(1, 0);
    rpolariz.resize(1, 0);
    polb.resize(1, 0);
    polc.resize(1, 0);
    TotRsc.resize(1, 0);
    neigmass.resize(1, 0);
    neigq.resize(1, 0);
    StDevN.resize(1, 0);
    CoordN.resize(1, 0);
    Displ.resize(1, 0);
    withinboundary.resize(1, 0);
    done.resize(1, false);
    pairs.resize(1);
    rho.resize(1, 0.0);
    rho0.resize(1, 0.0);
    rho1.resize(1, 0.0);
    rho2.resize(1, 0.0);
    rho3.resize(1, 0.0);
    rxfy.resize(1, 0.0);
    ryfz.resize(1, 0.0);
    rzfx.resize(1, 0.0);
    rc_meam.resize(1, 6.0);
    code.resize(1, 0);
}

void Atom::Resize(int num) {
    size = num;
    rsize = 1.0/( double(num));
    id.resize(num, 0);
    Cell.resize(num, 0);
    rmin.resize(num, 0);
    rmin2.resize(num, 0);
    EscP.resize(num, 0);
    EscS.resize(num, 0);
    FscRP.resize(num, 0);
    FscRS.resize(num, 0);
    Ep.resize(num, 0);
    Em.resize(num, 0);
    name.resize(num, "none");
    mass.resize(num, 0);
    rmass.resize(num, 0);
    x.resize(num, 0);
    y.resize(num, 0);
    z.resize(num, 0);
    x0.resize(num, 0);
    y0.resize(num, 0);
    z0.resize(num, 0);
    xav.resize(num, 0);
    yav.resize(num, 0);
    zav.resize(num, 0);
    xp.resize(num, 0);
    yp.resize(num, 0);
    zp.resize(num, 0);
    xd.resize(num, 0);
    yd.resize(num, 0);
    zd.resize(num, 0);
    vx.resize(num, 0);
    vy.resize(num, 0);
    vz.resize(num, 0);
    ax.resize(num, 0);
    ay.resize(num, 0);
    az.resize(num, 0);
    axn.resize(num, 0);
    ayn.resize(num, 0);
    azn.resize(num, 0);
    axo.resize(num, 0);
    ayo.resize(num, 0);
    azo.resize(num, 0);
    Fx.resize(num, 0);
    Fy.resize(num, 0);
    Fz.resize(num, 0);
    vFx.resize(num);
    vFy.resize(num);
    vFz.resize(num);
    Clear(vFx);
    Clear(vFy);
    Clear(vFz);
    Fxp.resize(num, 0);
    Fyp.resize(num, 0);
    Fzp.resize(num, 0);
    Fxs.resize(num, 0);
    Fys.resize(num, 0);
    Fzs.resize(num, 0);
    mux.resize(num, 0);
    muy.resize(num, 0);
    muz.resize(num, 0);
    muxpr.resize(num, 0);
    muypr.resize(num, 0);
    muzpr.resize(num, 0);
    Eqx.resize(num, 0);
    Eqy.resize(num, 0);
    Eqz.resize(num, 0);
    Epx.resize(num, 0);
    Epy.resize(num, 0);
    Epz.resize(num, 0);
    Eex.resize(num, 0);
    Eey.resize(num, 0);
    Eez.resize(num, 0);
    vx_mid.resize(num, 0);
    vy_mid.resize(num, 0);
    vz_mid.resize(num, 0);
    q0.resize(num, 0);
    q.resize(num, 0);
    aff.resize(num, 0);
    ion.resize(num, 0);
    dq.resize(num, 0);
    indj.resize(num, 0);
    Eet.resize(num, 0);
    netq.resize(num, 0);
    a.resize(num, 0);
    b.resize(num, 0);
    qs.resize(num, 0);
    ElSelf.resize(num, 0);
    PotEnergy.resize(num, 0);
    PotEnergy_tau.resize(num, 0);
    ElstEn_tau.resize(num, 0);
    ElstEn.resize(num, 0);
    FscRsc.resize(num, 0);
    KinEnergy_tau.resize(num, 0);
    KinEnergy.resize(num, 0);
    Energy.resize(num, 0);
    Energy_tau.resize(num, 0);
    qi.resize(num, 0);
    condx.resize(num, 0);
    condy.resize(num, 0);
    condz.resize(num, 0);
    Ei.resize(num, 0);
    Eea.resize(num, 0);
    rn.resize(num, 0);
    n.resize(num, 0);
    polariz.resize(num, 0);
    rpolariz.resize(num, 0);
    polb.resize(num, 0);
    polc.resize(num, 0);
    TotRsc.resize(num, 0);
    neigmass.resize(num, 0);
    neigq.resize(num, 0);
    StDevN.resize(num, 0);
    CoordN.resize(num, 0);
    Displ.resize(num, 0);
    withinboundary.resize(num, 0);
    done.resize(num, false);
    pairs.resize(num);
    rho.resize(num, 0.0);
    rho0.resize(num, 0.0);
    rho1.resize(num, 0.0);
    rho2.resize(num, 0.0);
    rho3.resize(num, 0.0);
    rxfy.resize(num, 0.0);
    ryfz.resize(num, 0.0);
    rzfx.resize(num, 0.0);
    rc_meam.resize(num, 6.0);
    code.resize(num, 0);
}
