//-------------------------------------------------------------------
// File:   amody.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#ifndef ATOM_H
#define ATOM_H

#include <iostream>
#include "globalvariables.h"
#include "math/vector.h"
#include <list>


class Atom
{
public:
    int size;
    double rsize;
    std::vector<iType> id;
    std::vector<iType> Cell;
    std::vector<dType> rmin;
    std::vector<dType> rmin2;
    std::vector<dType> EscP;
    std::vector<dType> EscS;
    std::vector<dType> FscRP;
    std::vector<dType> FscRS;
    std::vector<dType> Ep;
    std::vector<dType> Em;
    std::vector<std::string> name;
    std::vector<dType> mass;
    std::vector<dType> rmass;
    std::vector<dType> x;
    std::vector<dType> y;
    std::vector<dType> z;
    std::vector<dType> x0;
    std::vector<dType> y0;
    std::vector<dType> z0;
    std::vector<dType> xav;
    std::vector<dType> yav;
    std::vector<dType> zav;
    std::vector<dType> xp;
    std::vector<dType> yp;
    std::vector<dType> zp;
    std::vector<dType> xd;
    std::vector<dType> yd;
    std::vector<dType> zd;
    std::vector<dType> vx;
    std::vector<dType> vy;
    std::vector<dType> vz;
    std::vector<dType> ax;
    std::vector<dType> ay;
    std::vector<dType> az;
    std::vector<dType> axn;
    std::vector<dType> ayn;
    std::vector<dType> azn;
    std::vector<dType> axo;
    std::vector<dType> ayo;
    std::vector<dType> azo;
    std::vector<std::vector<dType> > vFx;
    std::vector<std::vector<dType> > vFy;
    std::vector<std::vector<dType> > vFz;
    std::vector<dType> Fx;
    std::vector<dType> Fy;
    std::vector<dType> Fz;
    std::vector<dType> Fxp;
    std::vector<dType> Fyp;
    std::vector<dType> Fzp;
    std::vector<dType> Fxs;
    std::vector<dType> Fys;
    std::vector<dType> Fzs;
    std::vector<dType> mux;
    std::vector<dType> muy;
    std::vector<dType> muz;
    std::vector<dType> muxpr;
    std::vector<dType> muypr;
    std::vector<dType> muzpr;

    std::vector<dType> Eqx;
    std::vector<dType> Eqy;
    std::vector<dType> Eqz;
    std::vector<dType> Epx;
    std::vector<dType> Epy;
    std::vector<dType> Epz;
    std::vector<dType> Eex;
    std::vector<dType> Eey;
    std::vector<dType> Eez;
    std::vector<dType> vx_mid;
    std::vector<dType> vy_mid;
    std::vector<dType> vz_mid;
    std::vector<dType> q0;
    std::vector<dType> q;
    std::vector<dType> dq;
    std::vector<dType> aff;
    std::vector<dType> ion;
    std::vector<int>   indj;
    std::vector<dType> Eet;
    std::vector<dType> netq;
    std::vector<dType> a;
    std::vector<dType> b;
    std::vector<dType> PotEnergy;
    std::vector<dType> PotEnergy_tau;
    std::vector<dType> ElstEn_tau;
    std::vector<dType> ElstEn;
    std::vector<dType> FscRsc;
    std::vector<dType> KinEnergy_tau;
    std::vector<dType> KinEnergy;
    std::vector<dType> Energy;
    std::vector<dType> Energy_tau;
    std::vector<dType> ElSelf;
    std::vector<dType> qs;
    std::vector< std::vector<pair> > pairs;


    std::vector<iType> qi;
    std::vector<dType> condx;
    std::vector<dType> condy;
    std::vector<dType> condz;
    std::vector<dType> Ei;
    std::vector<dType> Eea;

    std::vector<dType> rho;
    std::vector<dType> rho0;
    std::vector<dType> rho1;
    std::vector<dType> rho2;
    std::vector<dType> rho3;

    std::vector<dType> rn;
    std::vector<dType> n;

    // Polarization
    std::vector<dType> polariz;
    std::vector<dType> rpolariz;
    std::vector<dType> polb;
    std::vector<dType> polc;

    std::vector<dType> TotRsc;

    std::vector<dType> neigmass;
    std::vector<dType> neigq;
    std::vector<dType> StDevN;
    std::vector<dType> CoordN;
    std::vector<dType> Displ;
    std::vector<unsigned> withinboundary;
    std::vector<bool> done;
    std::vector<dType> jxy;
    std::vector<dType> jxz;
    std::vector<dType> jyz;
    std::vector<dType> rxfy;
    std::vector<dType> ryfz;
    std::vector<dType> rzfx;
    
    std::vector<dType> rc_meam;

    std::vector<iType> code;

    Atom();

    void Resize(int size);
};

#endif
