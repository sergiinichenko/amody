//-------------------------------------------------------------------
// File:   constants.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#ifndef CONSTANTS_H
#define CONSTANTS_H

static const double PI    = 3.141592653589793238462;
static const double kb    = 8.617332478e-5;  // [eV / K]
static const double h_bar = 0.658211928;  // [eV * fs]
static const double h     = 4.135667516;  // [eV * fs]
static const double Na    = 6.02214129e+23;  // [at/mol]
static const double Rgas  = 8.314462;  // [J/mol/K]
static const double aGrav = 9.80665E-20; // [A/fs^2]

#endif /* CONSTANTS_H */

