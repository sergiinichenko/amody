//-------------------------------------------------------------------
// File:   datamanager.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "datamanager.h"
//#include "simulation.h"


using std::string;
using std::endl;
using std::cout;

void DataManager::HLine(System &Sys){
    std::fstream File;
    std::stringstream stream;

    stream.str("");
    stream.clear();
    stream.fill(' ');

    stream << "----------------------------------------------------------------------------------------------------------------------------------";
    if (Sys.control.fitexp_b)
        stream << "------------";
    //stream <<  std::endl;

    if (Sys.inf.id == 0) {
        std::cout << stream.str() << std::endl;

        if (Sys.control.savetext_b)
        {
            File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
            File << stream.str() << std::endl;
            File.close();
        }
    }
}

void DataManager::HeadLine(System &Sys)
{
    std::fstream File;
    time_t timev = time(0);
    struct tm * ltm = localtime( & timev);

    std::ostringstream stream;
    string outstr;

    if (Sys.inf.id == 0) {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        HLine(Sys);

        stream.width(10);
        stream << "Started at " << funcs::timeStr();
        cout << stream.str() << std::endl;

        File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        if (!File.is_open())  std::cout << "The Output file is not fould. \n";
        File      <<  stream.str() << std::endl;
        File.close();


        HLine(Sys);

        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(10);
        stream << " Time";

        stream.width(12);
        stream << "Temper";

        stream.width(12);
        stream << "Pressure";

        stream.width(12);
        stream << "Density";

        stream.width(12);
        stream << "Energy";

        //stream.width(12);
        //stream << "Enthalpy";

        stream.width(12);
        stream << "Heat_Cap";

        stream.width(12);
        stream << "Th._Con";

        stream.width(12);
        stream << "Diffusion";

        stream.width(12);
        stream << "Viscosity";

        if (Sys.control.fitexp_b){
            stream.width(12);
            stream << "error";
        }

        stream.width(12);
        stream << "PC-Time";

        stream << std::endl;


        stream.width(10);
        stream << "ps";

        stream.width(12);
        stream << "K";

        stream.width(12);
        stream << "MPa";

        stream.width(12);
        stream << "kg/m3";

        stream.width(12);
        //stream << "kJ/mol/K";
        stream << "eV";

        //stream.width(12);
        //stream << "kJ/mol/K";

        stream.width(12);
        stream << "eV/K";

        stream.width(12);
        stream << "W/m/K";

        stream.width(12);
        stream << "A2/ps";

        stream.width(12);
        stream << "Pa s";

        if (Sys.control.fitexp_b){
            stream.width(12);
            stream << " ";
        }

        stream.width(12);


        cout << stream.str() << endl;;

        if (Sys.InfLevel == "Detailed")
        {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.width(10);
            stream << " Sys.inf.Time";

            stream.width(12);
            stream << "Temper";

            stream.width(12);
            stream << "Temper_t";

            stream.width(12);
            stream << "Pressure";

            stream.width(12);
            stream << "Pressure_t";

            stream.width(12);
            stream << "Density";

            stream.width(12);
            stream << "Density_t";

            stream.width(12);
            stream << "Energy";

            stream.width(12);
            stream << "Energy_t";

            //stream.width(12);
            //stream << "Gibbs_En";

            //stream.width(12);
            //stream << "Gibbs_En_t";

            //stream.width(12);
            //stream << "Enthalpy";

            //stream.width(12);
            //stream << "Enthalpy_t";

            //stream.width(12);
            //stream << "Entropy";

            //stream.width(12);
            //stream << "Entropy_t";

            stream.width(12);
            stream << "Heat_Cap";

            stream.width(12);
            stream << "Heat_Cap_t";

            stream.width(12);
            stream << "Th._Con";

            stream.width(12);
            stream << "Th._Con_t";

            stream.width(12);
            stream << "Diffusion";

            stream.width(12);
            stream << "Diffusion_t";

            stream.width(12);
            stream << "Viscosity";

            stream.width(12);
            stream << "Viscosity_t";

            stream.width(12);
            stream << "PC-Time" << std::endl;

            stream.width(10);
            stream << "ps";

            stream.width(12);
            stream << "K";

            stream.width(12);
            stream << "K";

            stream.width(12);
            stream << "MPa";

            stream.width(12);
            stream << "MPa";

            stream.width(12);
            stream << "kg/m3";

            stream.width(12);
            stream << "kg/m3";

            stream.width(12);
            //stream << "kJ/mol/K";
            stream << "eV";

            stream.width(12);
            stream << "eV";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";


            //stream.width(12);
            //stream << "J/mol/K";

            //stream.width(12);
            //stream << "J/mol/K";

            stream.width(12);
            stream << "eV/K";

            stream.width(12);
            stream << "eV/K";

            stream.width(12);
            stream << "W/m/K";

            stream.width(12);
            stream << "W/m/K";

            stream.width(12);
            stream << "A2/ps";

            stream.width(12);
            stream << "A2/ps";

            stream.width(12);
            stream << "Pa s";

            stream.width(12);
            stream << "Pa s";

            stream.width(12);
            stream << " Sys.inf.Time";
        }
        File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        if (!File.is_open())  std::cout << "The Output file is not fould. \n";

        File <<  stream.str() << std::endl;

        File.close();
        HLine(Sys);


        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << "----------------------------------------------------------------------------------------------------------";
        if (Sys.control.fitexp_b)
            stream << "------------";
        stream <<  std::endl;

        stream.width(10);
        stream << "Temper";

        stream.width(12);
        stream << "Pressure";

        stream.width(12);
        stream << "Density";

        stream.width(12);
        stream << "Energy";

        //stream.width(12);
        //stream << "Gibbs_En";

        //stream.width(12);
        //stream << "Enthalpy";


        //stream.width(12);
        //stream << "Entropy";

        stream.width(12);
        stream << "Heat_Cap";

        stream.width(12);
        stream << "Th._Con";

        stream.width(12);
        stream << "Diffusion";

        stream.width(12);
        stream << "Viscosity";

        if (Sys.control.fitexp_b){
            stream.width(12);
            stream << "error";
        }

        stream.width(12);
        stream << "PC-Time";

        stream << std::endl;

        stream.width(10);
        stream << "K";

        stream.width(12);
        stream << "MPa";

        stream.width(12);
        stream << "kg/m3";

        stream.width(12);
        stream << "eV";

        //stream.width(12);
        //stream << "kJ/mol/K";

        //stream.width(12);
        //stream << "eV/ion/K";


        //stream.width(12);
        //stream << "J/mol/K";

        stream.width(12);
        stream << "eV/K";

        stream.width(12);
        stream << "W/m/K";

        stream.width(12);
        stream << "A2/ps";

        stream.width(12);
        stream << "Pa s";

        if (Sys.control.fitexp_b){
            stream.width(12);
            stream << " ";
        }

        stream.width(12);
        stream << " Sys.inf.Time";
        stream <<  std::endl;

        stream << "----------------------------------------------------------------------------------------------------------";
        if (Sys.control.fitexp_b)
            stream << "------------";
        stream <<  endl;

        File.open(Sys.files.TableFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        if (!File.is_open())     std::cout << "The Table file is not fould. \n";

        if (Sys.InfLevel == "Detailed")
        {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.width(10);
            stream << "Temper";

            stream.width(12);
            stream << "Temper_t";

            stream.width(12);
            stream << "Pressure";

            stream.width(12);
            stream << "Pressure_t";

            stream.width(12);
            stream << "Density";

            stream.width(12);
            stream << "Density_t";

            stream.width(12);
            stream << "Energy";

            stream.width(12);
            stream << "Energy_t";

            //stream.width(12);
            //stream << "Gibbs_En";

            //stream.width(12);
            //stream << "Gibbs_En_t";

            //stream.width(12);
            //stream << "Enthalpy";

            //stream.width(12);
            //stream << "Enthalpy_t";

            //stream.width(12);
            //stream << "Entropy";

            //stream.width(12);
            //stream << "Entropy_t";

            stream.width(12);
            stream << "Heat_Cap";

            stream.width(12);
            stream << "Heat_Cap_t";

            stream.width(12);
            stream << "Th._Con";

            stream.width(12);
            stream << "Th._Con_t";

            stream.width(12);
            stream << "Diffusion";

            stream.width(12);
            stream << "Diffusion_t";

            stream.width(12);
            stream << "Viscosity";

            stream.width(12);
            stream << "Viscosity_t";

            stream.width(12);
            stream << "PC-Time" << std::endl;

            stream.width(10);
            stream << "K";

            stream.width(12);
            stream << "K";

            stream.width(12);
            stream << "MPa";

            stream.width(12);
            stream << "MPa";

            stream.width(12);
            stream << "kg/m3";

            stream.width(12);
            stream << "kg/m3";

            stream.width(12);
            stream << "eV";

            stream.width(12);
            stream << "eV";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";

            //stream.width(12);
            //stream << "kJ/mol/K";


            //stream.width(12);
            //stream << "J/mol/K";

            //stream.width(12);
            //stream << "J/mol/K";

            stream.width(12);
            stream << "eV/K";

            stream.width(12);
            stream << "eV/K";

            stream.width(12);
            stream << "W/m/K";

            stream.width(12);
            stream << "W/m/K";

            stream.width(12);
            stream << "A2/ps";

            stream.width(12);
            stream << "A2/ps";

            stream.width(12);
            stream << "Pa s";

            stream.width(12);
            stream << "Pa s";

            stream.width(12);
            stream << " Sys.inf.Time";
        }

        File <<  stream.str() << std::endl;
        File.close();
    }
}

void DataManager::ShowResults(System &Sys)
{
    std::fstream File;
    std::stringstream stream;
    time_t timev = time(0);
    struct tm * ltm = localtime( & timev);

    if (Sys.inf.id == 0) {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(10);
        stream.precision(3);
        stream << std::fixed <<  Sys.inf.Time*1.0e-3;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.Temperature;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.Pressure;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.Density;

        stream.width(12);
        stream.precision(4);
        //stream << std::fixed << Sys.props.Energy * Sys.At.size;
        stream << std::fixed << Sys.props.Energy;

        //stream.width(12);
        //stream.precision(3);
        //stream << std::fixed << Sys.props.Enthalpy;

        //stream.width(12);
        //stream.precision(3);
        //stream << std::fixed << Entropy;

        stream.width(12);
        stream.precision(3);
        stream << std::fixed << Sys.props.SpecificHeat;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.ThermCond;

        stream.width(12);
        stream.precision(3);
        stream << std::scientific << Sys.props.Diffusion;

        stream.width(12);
        stream.precision(3);
        stream << std::scientific << Sys.props.Viscosity;

        if (Sys.control.fitexp_b)
        {
            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.fit.error;
        }

        stream.width(10);
        stream << funcs::timeStr();;

        cout << stream.str() << endl;

        if (Sys.InfLevel == "Detailed")
        {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.width(10);
            stream.precision(3);
            stream << std::fixed <<  Sys.inf.Time*10e-3;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Temperature;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Temperature_tau;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Pressure;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Pressure_tau;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Density;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.Density_tau;

            stream.width(12);
            stream.precision(4);
            //stream << std::fixed << Sys.props.Energy * Sys.At.size;
            stream << std::fixed << Sys.props.Energy;

            stream.width(12);
            stream.precision(4);
            stream << std::fixed << Sys.props.Energy_tau;

            //stream.width(12);
            //stream.precision(3);
            //stream << std::fixed << GibbsEnergy;

            //stream.width(12);
            //stream.precision(3);
            //stream << std::fixed << GibbsEnergy_tau;

            /*
            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.props.Enthalpy;

            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.props.Enthalpy_tau;
            */

            //stream.width(12);
            //stream.precision(3);
            //stream << std::fixed << Entropy*96485;

            //stream.width(12);
            //stream.precision(3);
            //stream << std::fixed << Entropy_tau*96485;

            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.props.SpecificHeat;

            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.props.SpecificHeat_tau*96485;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.ThermCond;

            stream.width(12);
            stream.precision(2);
            stream << std::fixed << Sys.props.ThermCond_tau;

            stream.width(12);
            stream.precision(3);
            stream << std::scientific << Sys.props.Diffusion;

            stream.width(12);
            stream.precision(3);
            stream << std::scientific <<Sys.props.Diffusion_tau;

            stream.width(12);
            stream.precision(3);
            stream << std::scientific << Sys.props.Viscosity;

            stream.width(12);
            stream.precision(3);
            stream << std::scientific << Sys.props.Viscosity_tau;

            stream.width(10);
            stream << funcs::timeStr();
        }

        if (Sys.control.savetext_b)
        {
            File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
            File << stream.str() << std::endl;
            File.close();
        }
    }
}

void DataManager::SaveDataFrame(System &Sys) {
    std::stringstream stream, iss;
    std::fstream file;
    std::string name;
    std::map<std::string, std::string>::iterator miter;

    file.open(Sys.files.DataFrameFile.c_str(), std::fstream::app);

    stream.str("");
    stream.clear();

    stream.width(12);
    stream.precision(0);
    stream << std::fixed << Sys.inf.step;

    stream.width(12);
    stream.precision(2);
    stream << std::fixed << Sys.inf.Time;

    stream.width(12);
    stream.precision(2);
    stream << std::fixed << Sys.props.Temperature;

    stream.width(12);
    stream.precision(2);
    stream << std::fixed << Sys.props.Pressure;

    stream.width(12);
    stream.precision(2);
    stream << std::fixed << Sys.props.Density;

    stream.width(12);
    stream.precision(3);
    stream << std::fixed << Sys.props.Energy;
    //stream << std::fixed << Sys.props.Energy * Sys.At.size;

    /*
    stream.width(12);
    stream.precision(3);
    stream << std::fixed << Sys.props.Enthalpy;
    */

    stream.width(12);
    stream.precision(3);
    stream << std::fixed << Sys.props.SpecificHeat;

    stream.width(12);
    stream.precision(3);
    stream << std::fixed << Sys.props.ThermCond;

    stream.width(12);
    stream.precision(3);
    stream << std::scientific << Sys.props.Diffusion;

    stream.width(12);
    stream.precision(3);
    stream << std::scientific << Sys.props.Viscosity;

    stream.width(12);
    stream.precision(2);
    stream << std::fixed << Sys.fit.error;

    file << stream.str() << std::endl;
    file.close();
}

void DataManager::SaveTable(System &Sys) {
    std::fstream DataFile;
    std::stringstream stream;
    std::string text;
    time_t timev = time(0);
    struct tm * ltm = localtime( & timev);

    if (Sys.inf.id == 0) {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(10);
        stream.precision(2);
        stream << std::fixed << Sys.props.Temperature;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.Pressure;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.Density;

        stream.width(12);
        stream.precision(3);
        stream << std::fixed << Sys.props.Energy;
        //stream << std::fixed << Sys.props.Energy * Sys.At.size;

        //stream.width(12);
        //stream.precision(3);
        //stream << std::fixed << GibbsEnergy;

        /*
        stream.width(12);
        stream.precision(3);
        stream << std::fixed << Sys.props.Enthalpy;
        */

        //stream.width(12);
        //stream.precision(3);
        //stream << std::fixed << Entropy*96485;

        stream.width(12);
        stream.precision(3);
        stream << std::fixed << Sys.props.SpecificHeat;

        stream.width(12);
        stream.precision(2);
        stream << std::fixed << Sys.props.ThermCond;

        stream.width(12);
        stream.precision(3);
        stream << std::scientific << Sys.props.Diffusion;

        if (Sys.control.fitexp_b)
        {
            stream.width(12);
            stream.precision(3);
            stream << std::fixed << Sys.fit.error;
        }

        stream.width(10);
        stream <<funcs::timeStr();

        DataFile.open(Sys.files.TableFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        if (!DataFile.is_open())
            std::cout << "file is not opened" <<std::endl;
        DataFile << stream.str() << std::endl;
        DataFile.close();

        SaveDataFrame(Sys);
    }
}

void DataManager::SaveSampleData(System &Sys) {
    ParamsIter iter;
    std::fstream file;
    std::stringstream stream;
    std::string text;

    if (Sys.inf.id == 0 && Sys.control.samplefile_b)  {

        file.open(Sys.files.SampleFile.c_str(), std::fstream::app);

        if (!file.is_open())
            std::cout << "file is not opened" <<std::endl;

        for (unsigned sample = 0; sample < Sys.Coeffs.size(); ++sample) {

            stream.str("");
            stream.clear();
            stream.fill(' ');

          for (iter = Sys.Coeffs[sample].begin(); iter != Sys.Coeffs[sample].end(); ++iter) {
              stream.width(5);
              stream << iter->first << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.DM << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.alphaM << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.r << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.A << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.E << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.rho0 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.re << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.beta0 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.beta1 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.beta2 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.beta3 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.t0 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.t1 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.t2 << "\t";

              stream.width(10);
              stream.precision(4);
              stream.setf(std::ios::fixed);
              stream << iter->second.t3 << "\t";
          }
          stream << std::endl;

          stream.width(16);
          stream.precision(4);
          stream.setf(std::ios::fixed);
          stream << Sys.Coeffs[sample].begin()->second.fitres << "\t";

          stream.width(16);
          stream.precision(4);
          stream.setf(std::ios::fixed);
          stream << Sys.Coeffs[sample].begin()->second.temperature << "\t";

          stream.width(16);
          stream.precision(4);
          stream.setf(std::ios::fixed);
          stream << Sys.Coeffs[sample].begin()->second.pressure << "\t";

          stream.width(16);
          stream.precision(4);
          stream.setf(std::ios::fixed);
          stream << Sys.Coeffs[sample].begin()->second.density << "\t";

          stream.width(16);
          stream.precision(4);
          stream.setf(std::ios::fixed);
          stream << Sys.Coeffs[sample].begin()->second.energy << "\t";

          stream << std::endl;
          file << stream.str();
        }
    }
    file.close();
}

void DataManager::SaveNet(System &Sys) {
    ParamsIter iter;
    std::fstream file;
    std::stringstream stream;
    std::string text;

    if (Sys.inf.id == 0) {

        file.open(Sys.files.NNinFile.c_str(), std::fstream::out);

        if (!file.is_open())
            std::cout << "file is not opened" <<std::endl;

        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << "topology  ";
        for (unsigned i = 0; i < Sys.net.topology.size(); ++i) {
            stream << Sys.net.topology[i] << "  ";
        }
        stream << std::endl;
        stream << "data" << std::endl;

        for (unsigned layerNum = 0; layerNum < Sys.net.m_layers.size() - 1; ++ layerNum) {

            // going though all the neurons in the layer except the bias one (last one)
            for (unsigned n = 0; n < Sys.net.m_layers[layerNum].size(); ++n) {

                // going through all the output weights of the neuron
                for (unsigned w = 0; w < Sys.net.m_layers[layerNum][n].m_outputWeights.size(); ++w) {

                    stream.width(12);
                    stream.precision(8);
                    stream.setf(std::ios::fixed);
                    stream << Sys.net.m_layers[layerNum][n].m_outputWeights[w].weight;
                }
                stream << std::endl;
            }
        }

        file << stream.str();
        file.close();
    }
}


void DataManager::SaveFitPotential(System &Sys)
{
    ParamsIter iter;
    //map<std::string, Params>::const_iterator iter;
    std::fstream DataFile;
    std::stringstream stream;

    if (Sys.inf.id == 0) {
        stream.str("");
        stream.clear();
        stream.fill(' ');


        stream << "--------------------------------------------------------------------------------------------------------------" << std::endl;
        stream << "|   Standard Deviation of the results is -> " << Sys.fit.error << std::endl;
        stream << "--------------------------------------------------------------------------------------------------------------" << std::endl;
        stream << std::endl;

        stream << "/     name        DM    alphaM        rr" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "Morse";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.DM;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.alphaM;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.r << std::endl;
        }
        stream << std::endl;

        stream << "/     name     A         E        rho0      re       beta0     beta1     beta2     beta3        t0        t1        t2        t3     rc     Nz" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "MEAM";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.A;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.E;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.rho0;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.re;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.beta0;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.beta1;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.beta2;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.beta3;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.t0;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.t1;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.t2;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.t3;

            stream.setf(std::ios::fixed);
            stream.width(7);
            stream.precision(2);
            stream << iter->second.rc_meam;

            stream.setf(std::ios::fixed);
            stream.width(7);
            stream.precision(2);
            stream << iter->second.Nz << std::endl;
        }
        stream << std::endl;

        /*
        stream << "/         name     DN        alphaN   rN        rho0N    beta0N    rc_N     NN" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "Nichenko";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.DN;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.alphaN;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.rN;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.rho0N;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.beta0N;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.rc_N;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(2);
            stream << iter->second.NN << std::endl;
        }
        stream << std::endl;
        */

        stream << "/       name      pol.         b         c" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "Polariz";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.polariz;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.polb;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.polc << std::endl;
        }
        stream << std::endl;

        stream << "/      name         q" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "Charge";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.q << std::endl;
        }

        stream << std::endl;
        stream << "/    name      mass" << std::endl;
        for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
            stream << "Mass";

            stream.width(5);
            stream << iter->first;

            stream.setf(std::ios::fixed);
            stream.width(10);
            stream.precision(4);
            stream << iter->second.mass << std::endl;
        }

        stream << "--------------------------------------------------------------------------------------------------------------" << std::endl;
        stream << std::endl << std::endl;

        DataFile.open(Sys.files.TableFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
        if (!DataFile.is_open())
            std::cout << "file is not opened" <<std::endl;
        DataFile << stream.str() << std::endl;
        DataFile.close();

        DataFile.open(Sys.files.PotenFileOut.c_str(), std::fstream::out);
        if (!DataFile.is_open())
            std::cout << "file is not opened" <<std::endl;
        DataFile << stream.str() << std::endl;
        DataFile.close();

        SaveDataFrame(Sys);
    }
}

void DataManager::SaveRDF(System &Sys)
{
    if (Sys.control.rdf_b && Sys.inf.id == 0) {
        std::stringstream stream;
        std::fstream File;
        std::string pairName;
        File.open(Sys.files.RDFFile.c_str(), std::fstream::out);

        if (!File.is_open())
          {
            std::cout << "File " << Sys.files.RDFFile.c_str()<< " open" << std::endl;
          }

        stream << "Temperature = " << Sys.props.Temperature << std::endl;
        stream.width(12);
        stream << "           R";
        for (unsigned int j = 0; j < Sys.RDFfor.size(); j++) {
            pairName = Sys.RDFfor[j].At1 + Sys.RDFfor[j].At2;
            stream.width(12);
            stream << pairName;
        }

        stream << std::endl;
        File << stream.str();

        for (unsigned int i = 0; i < Sys.RDF.r.size(); i++) {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.setf(std::ios::fixed);
            stream.width(12);
            stream.precision(5);
            stream << Sys.RDF.r[i];
            for (unsigned int j = 0; j < Sys.RDFfor.size(); j++) {
                stream.width(12);
                stream.precision(5);
                stream << Sys.RDF.n[j][i];
            }
            stream << std::endl;
            File << stream.str();
        }
        File.close();
    }

}

void DataManager::SaveStateFile(System &Sys, string path) {
        std::stringstream stream, iss;
        std::fstream file;


        if (Sys.inf.id == 0) {
            file.open(path, std::fstream::out);

            stream.str("");
            stream.clear();
            stream <<  Sys.At.size << std::endl;
            stream <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c << std::endl;
            file << stream.str();

            assert(Sys.At.size == Sys.At.name.size());
            assert(Sys.At.size == Sys.At.x.size());
            assert(Sys.At.size == Sys.At.y.size());
            assert(Sys.At.size == Sys.At.z.size());
            assert(Sys.At.size == Sys.At.vx.size());
            assert(Sys.At.size == Sys.At.vy.size());
            assert(Sys.At.size == Sys.At.vz.size());
            assert(Sys.At.size == Sys.At.Fx.size());
            assert(Sys.At.size == Sys.At.Fy.size());
            assert(Sys.At.size == Sys.At.Fz.size());
            assert(Sys.At.size == Sys.At.ax.size());
            assert(Sys.At.size == Sys.At.ay.size());
            assert(Sys.At.size == Sys.At.az.size());
            assert(Sys.At.size == Sys.At.q.size());
            assert(Sys.At.size == Sys.At.qi.size());

            for (int i = 0; i< Sys.At.size; ++i) {
                stream.str("");
                stream.clear();
                stream.fill(' ');

                stream.width(5);
                stream << Sys.At.name[i];

                stream.setf(std::ios::fixed);

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.x[i];

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.y[i];

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.z[i];


                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.q[i];

                stream.width(7);
                stream.precision(0);
                stream << std::fixed << Sys.At.qi[i];

                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.vx[i];

                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.vy[i];

                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.vz[i];


                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.ax[i];

                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.ay[i];

                stream.width(14);
                stream.precision(4);
                stream << std::scientific << Sys.At.az[i] << std::endl;

                file << stream.str();
            }
            file.close();
        }
}

void DataManager::SaveXYZFile(System &Sys)
{
    std::stringstream stream, iss;
    std::fstream file;


    if (Sys.inf.id == 0) {

        file.open(Sys.files.StateFile.c_str(), std::fstream::out);

        stream.str("");
        stream.clear();
        stream <<  Sys.At.size << std::endl;
        stream <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c << std::endl;
        file << stream.str();

        assert(Sys.At.size == Sys.At.name.size());
        assert(Sys.At.size == Sys.At.x.size());
        assert(Sys.At.size == Sys.At.y.size());
        assert(Sys.At.size == Sys.At.z.size());
        assert(Sys.At.size == Sys.At.vx.size());
        assert(Sys.At.size == Sys.At.vy.size());
        assert(Sys.At.size == Sys.At.vz.size());
        assert(Sys.At.size == Sys.At.Fx.size());
        assert(Sys.At.size == Sys.At.Fy.size());
        assert(Sys.At.size == Sys.At.Fz.size());
        assert(Sys.At.size == Sys.At.ax.size());
        assert(Sys.At.size == Sys.At.ay.size());
        assert(Sys.At.size == Sys.At.az.size());
        assert(Sys.At.size == Sys.At.q.size());
        assert(Sys.At.size == Sys.At.qi.size());

        for (int i = 0; i< Sys.At.size; ++i) {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.width(5);
            stream << Sys.At.name[i];

            stream.setf(std::ios::fixed);

            stream.width(14);
            stream.precision(5);
            stream << std::fixed << Sys.At.x[i];

            stream.width(14);
            stream.precision(5);
            stream << std::fixed << Sys.At.y[i];

            stream.width(14);
            stream.precision(5);
            stream << std::fixed << Sys.At.z[i];


            stream.width(10);
            stream.precision(4);
            stream << std::fixed << Sys.At.q[i];

            stream.width(7);
            stream.precision(0);
            stream << std::fixed << Sys.At.qi[i];

            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.vx[i];

            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.vy[i];

            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.vz[i];


            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.ax[i];

            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.ay[i];

            stream.width(14);
            stream.precision(4);
            stream << std::scientific << Sys.At.az[i] << std::endl;

            file << stream.str();
        }
        file.close();



        if (Sys.control.posfile_b) {

            stream.str("");
            stream.clear();

            file.open(Sys.files.XYZFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

            stream <<  Sys.At.size << std::endl;
            stream.width(12);
            stream.precision(4);
            stream << std::fixed;
            stream << "Temperature = " << Sys.props.Temperature << "     Size = " <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c << "  Input File = " << Sys.files.XYZInFile.c_str() << std::endl;
            file << stream.str();

            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');

                stream.width(5);
                stream << Sys.At.name[i];

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.x[i];

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.y[i];

                stream.width(14);
                stream.precision(5);
                stream << std::fixed << Sys.At.z[i] << std::endl;
                file << stream.str();

            }
            file.close();
        }

        if (Sys.control.forcefile_b){
            stream.str("");
            stream.clear();

            file.open(Sys.files.ForceFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

            stream <<  Sys.At.size << std::endl;
            stream << "Temperature = " << Sys.props.Temperature << "     Size = " <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c <<std::endl;
            file << stream.str();

            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');


                stream.width(5);
                stream << Sys.At.name[i];

                stream.width(15);
                stream.precision(4);
                stream << std::scientific << Sys.At.Fx[i];

                stream.width(15);
                stream.precision(4);
                stream << std::scientific << Sys.At.Fy[i];

                stream.width(15);
                stream.precision(4);
                stream << Sys.At.Fz[i] << std::endl;
                file << std::scientific << stream.str();

            }
            file.close();
        }


        if (Sys.control.fluxfile_b){

            stream.str("");
            stream.clear();

            file.open(Sys.files.FluxFile.c_str(), std::fstream::out);

            stream.width(8);
            stream << "Num.";
            stream.width(15);
            stream << "J.x";
            stream.width(15);
            stream << "J.y";
            stream.width(15);
            stream << "J.z";
            stream.width(15);
            stream << "Rjj";
            stream.width(15);
            stream << "RjjF";
            stream.width(15);
            stream << "RjjFav";
            stream.width(15);
            stream << "Rjjint";
            stream.width(15);
            stream << "Freq";
            stream.width(15);
            stream << "Real";
            stream.width(15);
            stream << "Imag";


            stream << std::endl;


            file << stream.str();


            file.close();
        }


        if (Sys.control.vtkfile_b) {
            std::stringstream ivtk;
            std::string path;

            ivtk.str("");
            ivtk.clear();
            ivtk << Sys.inf.iVTK;
            std::string InFile = Sys.files.Path + "VTK/" + Sys.files.File + "." + ivtk.str() + ".vtk";
            Sys.inf.iVTK = Sys.inf.iVTK + 1;

            file.open(InFile.c_str(), std::fstream::out);

            stream.str("");
            stream.clear();
            stream <<  "# vtk DataFile Version 3.0" << std::endl;
            stream << "vtk output" << std::endl;
            stream << "ASCII" << std::endl;
            stream << "DATASET POLYDATA" << std::endl << std::endl;
            stream << "POINTS " << Sys.At.size << " float" << std::endl;
            file << stream.str();

            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');

                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.x[i];

                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.y[i];

                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.z[i] << std::endl;
                file << stream.str();
            }

            stream.str("");
            stream.clear();
            stream << std::endl;
            stream << "POINT_DATA " << Sys.At.size << std::endl;
            stream << "SCALARS element float" << std::endl;
            stream << "LOOKUP_TABLE default" << std::endl;
            file << stream.str();

            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');

                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.mass[i] / 103.642778314 << std::endl;
                file << stream.str();
            }

            stream.str("");
            stream.clear();
            stream << std::endl;
            stream << "SCALARS charge float" << std::endl;
            stream << "LOOKUP_TABLE default" << std::endl;
            file << stream.str();
            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');

                stream.width(10);
                stream.precision(4);
                stream << std::fixed << Sys.At.q[i] << std::endl;
                file << stream.str();
            }
            file.close();
        }

        if (Sys.control.forcefile_b) {
            stream.str("");
            stream.clear();

            file.open(Sys.files.ForceFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

            stream <<  Sys.At.size << std::endl;
            stream << "Temperature = " << Sys.props.Temperature << "     Size = " <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c <<std::endl;
            file << stream.str();

            for (int i=0; i< Sys.At.size; ++i){
                stream.str("");
                stream.clear();
                stream.fill(' ');


                stream.width(5);
                stream << Sys.At.name[i];

                stream.width(15);
                stream.precision(4);
                stream << std::scientific << Sys.At.Fx[i];

                stream.width(15);
                stream.precision(4);
                stream << std::scientific << Sys.At.Fy[i];

                stream.width(15);
                stream.precision(4);
                stream << Sys.At.Fz[i] << std::endl;
                file << std::scientific << stream.str();
            }
            file.close();
        }

    }
}

bool DataManager::copyFile(std::string src, std::string des)
{
    std::cout<<"Preparing the output file...\n";
    std::ifstream inpfile(src.c_str(), std::ios::binary);

    if (!inpfile.is_open())
        return false;

    std::ofstream outfile(des.c_str(), std::ios::binary);

    if ( !outfile.is_open())
    {
        inpfile.close();
        return false;
    }

    std::string line;
    while(!inpfile.eof())
    {
        getline(inpfile, line);
        outfile<< line;
        outfile<< '\n';
    }

    inpfile.close();
    outfile.close();

    return true;
}

void DataManager::ClearFile(std::string& file){
    std::fstream f;
    f.open(file.c_str(), std::fstream::out);
    f.close();
}

void DataManager::ClearFiles(System &Sys)
{
    std::stringstream stream;
    std::fstream file;

    if (fileExists(Sys.files.ForceFile)) {
        if (Sys.control.forcefile_b){
            file.open(Sys.files.ForceFile.c_str(), std::fstream::out);
            file.close();
        }
    }

    if (fileExists(Sys.files.FluxFile)) {
        if (Sys.control.fluxfile_b){
            file.open(Sys.files.FluxFile.c_str(), std::fstream::out);
            file.close();
        }
    }

    if (fileExists(Sys.files.XYZFile)) {
        if (Sys.control.posfile_b){
            file.open(Sys.files.XYZFile.c_str(), std::fstream::out);
            file.close();
        }
    }

    if (fileExists(Sys.files.SampleFile)) {
        if (Sys.control.samplefile_b) {
            file.open(Sys.files.SampleFile.c_str(), std::fstream::out);
            file.close();
        }
    }


    if (fileExists(Sys.files.TableFile)) {
        file.open(Sys.files.TableFile.c_str(), std::fstream::out);
        file.close();
    }

    if (fileExists(Sys.files.DataFrameFile)) {
        file.open(Sys.files.DataFrameFile.c_str(), std::fstream::out);
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(12);
        stream << "step";

        stream.width(12);
        stream << "time";

        stream.width(12);
        stream << "T";

        stream.width(12);
        stream << "P";

        stream.width(12);
        stream << "Dens";

        stream.width(12);
        stream << "E";

        stream.width(12);
        stream << "Cp";

        stream.width(12);
        stream << "ThC";

        stream.width(12);
        stream << "D";

        stream.width(12);
        stream << "v";

        stream.width(12);
        stream << "error";

        file <<  stream.str() << std::endl;
        file.close();
    }

    if (fileExists(Sys.files.FileOut)) {
        file.open(Sys.files.FileOut.c_str(), std::fstream::out);
        file.close();
    }

    if (fileExists(Sys.files.RDFFile)) {
        file.open(Sys.files.RDFFile.c_str(), std::fstream::out);
        file.close();
    }
}


void DataManager::GetLine(std::fstream& File, vector<string>& ret){
    string text;
    std::getline(File, text);
    ret = funcs::split(text);
}


void DataManager::ReadDETFile(System &Sys) {
    using std::getline;
    dType zero, val;

    std::fstream DETStrem;
    DETStrem.open(Sys.files.DETFile.c_str(), std::ios::in);

    std::string name, line;
    std::stringstream iss;

    if (!DETStrem.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The DET data file  is not fould " << Sys.files.DETFile.c_str() <<  "is not fould " << std::endl;
        return;
    }

    std::getline(DETStrem, line);

    while (!DETStrem.eof())
    {
        getline(DETStrem, line);

        if (line[0]!= '#' && line[0]!= '/' && !line.empty())
        {
            iss.clear();
            iss.str(line);
            iss >> name;

            Sys.detpar[name].name = name;

            iss >> Sys.detpar[name].Eaffin[3];
            iss >> Sys.detpar[name].Eaffin[2];
            iss >> Sys.detpar[name].Eaffin[1];

            iss >> zero;
            Sys.detpar[name].Eioniz[0] = zero;
            Sys.detpar[name].Eaffin[0] = zero;

            iss >> Sys.detpar[name].Eioniz[1];
            iss >> Sys.detpar[name].Eioniz[2];
            iss >> Sys.detpar[name].Eioniz[3];
            iss >> Sys.detpar[name].Eioniz[4];
            iss >> Sys.detpar[name].Eioniz[5];
            iss >> Sys.detpar[name].Eioniz[6];
            iss >> Sys.detpar[name].Eioniz[7];

            Sys.detpar[name].Eaffin[2] += Sys.detpar[name].Eaffin[1];
            Sys.detpar[name].Eaffin[3] += Sys.detpar[name].Eaffin[2];

            Sys.detpar[name].Eioniz[2] += Sys.detpar[name].Eioniz[1];
            Sys.detpar[name].Eioniz[3] += Sys.detpar[name].Eioniz[2];
            Sys.detpar[name].Eioniz[4] += Sys.detpar[name].Eioniz[3];
            Sys.detpar[name].Eioniz[5] += Sys.detpar[name].Eioniz[4];
            Sys.detpar[name].Eioniz[6] += Sys.detpar[name].Eioniz[5];
            Sys.detpar[name].Eioniz[7] += Sys.detpar[name].Eioniz[6];
        }
    }
    DETStrem.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the DET parameters file ... " << std::endl;
}

void DataManager::ReadNNinFile(System &Sys) {
    using std::getline;
    std::vector<unsigned> topology;
    std::fstream file;
    std::string type, name, namei, namej, pair, pairopp, line;
    std::stringstream iss;
    vector<std::string> res;
    vector<string>::iterator begin, end;

    static const std::string commb = "/*";
    static const std::string comme = "*/";


    file.open(Sys.files.NNinFile.c_str(), std::ios::in);

    if (!file.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The Neural Net input file " << Sys.files.NNinFile.c_str() <<  " is not fould " << std::endl;
        return;
    }

    std::getline(file, line);

    while (!file.eof())
    {
        std::string::const_iterator b = line.begin();
        std::string::const_iterator e = line.end();

        if (std::search(b, e, commb.begin(), commb.end()) != e){
            while (std::search(b, e, comme.begin(), comme.end()) == e && !file.eof()){
                std::getline(file, line);
                b = line.begin();
                e = line.end();
                if (file.eof()){
                    if (Sys.inf.id == 0)
                        cout <<  "Looks like you forgot to put the closing comment" << std::endl;
                    if (Sys.inf.id == 0)
                        cout <<  "Press any key to continue ...";
                    std::cin.ignore();
                }
            }
            std::getline(file, line);
        }

        res = funcs::split(line);
        begin = res.begin();
        end   = res.end();
        if (line[0]!= '#' && line[0]!= '/' && !line.empty()) {

            // Read the topology structure
            if ((begin != res.end()) && (*begin == "topology" || *begin == "Topology" || *begin == "TOPOLOGY")) {
                Sys.net.topology.clear();
                ++begin;
                while (begin < res.end()){
                    Sys.net.topology.push_back(funcs::ToInt(*begin));
                    ++begin;
                }
                Sys.net.createNet(Sys.net.topology);
            }

            if ((begin != res.end()) && (*begin == "data" || *begin == "Data" || *begin == "DATA")) {

                // Going througt all the layers of the Net except the output one
                for (unsigned layerNum = 0; layerNum < Sys.net.m_layers.size() - 1; ++layerNum) {

                    // going though all the neurons in the layer except the bias one (last one)
                    for (unsigned n = 0; n < Sys.net.m_layers[layerNum].size(); ++n) {

                        std::getline(file, line);
                        res = funcs::split(line);
                        // going through all the output weights of the neuron
                        for (unsigned w = 0; w < Sys.net.m_layers[layerNum][n].m_outputWeights.size(); ++w) {
                            Sys.net.m_layers[layerNum][n].m_outputWeights[w].weight = std::atof(res[w].c_str()); ;//funcs::ToDouble(res[w]);
                        }
                    }
                }
            }
        }
        std::getline(file, line);
    }

    file.close();

    if (Sys.inf.id == 0)
        std::cout << "Done loading the Neural Network file ... " << std::endl;
}

void DataManager::SaveNetResults(System &Sys) {
    ParamsIter iter;
    std::fstream file;
    std::stringstream stream;
    std::string text;

    if (Sys.inf.id == 0)  {

        file.open(Sys.files.NetResultsFile.c_str(), std::fstream::out);

        if (!file.is_open())
            std::cout << "NetResultsFile file is not opened" <<std::endl;

        for (unsigned sample = 0; sample < Sys.net.Sample.size(); ++sample) {

            stream.str("");
            stream.clear();
            stream.fill(' ');
            for (unsigned i = 0; i < Sys.net.Sample[sample].InData.size(); ++i) {
                stream.width(10);
                stream.precision(4);
                stream.setf(std::ios::fixed);
                stream << Sys.net.Sample[sample].InData[i] << "\t";
            }
            stream << std::endl;

            for (unsigned i = 0; i < Sys.net.Sample[sample].OutData.size(); ++i) {
                stream.width(10);
                stream.precision(4);
                stream.setf(std::ios::fixed);
                stream << Sys.net.Sample[sample].OutData[i] << "\t";
            }
            stream << std::endl;
            file << stream.str();
          }
    }
    file.close();
}

void DataManager::ReadNetTrainingFile(System &Sys) {
    using std::getline;
    std::fstream file;
    std::string type, name, namei, namej, pair, pairopp, line;
    std::stringstream iss;
    vector<std::string> res;
    vector<string>::iterator begin, end;
    SampleStruct samp;

    static const std::string commb = "/*";
    static const std::string comme = "*/";

    file.open(Sys.files.NetTrainingFile.c_str(), std::ios::in);
    Sys.net.Sample.clear();


    if (!file.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The Sample Input input file " << Sys.files.NetTrainingFile.c_str() <<  " is not fould " << std::endl;
        return;
    }

    std::getline(file, line);

    while (!file.eof())
    {
        std::string::const_iterator b = line.begin();
        std::string::const_iterator e = line.end();

        samp.InData.clear();
        samp.OutData.clear();

        if (std::search(b, e, commb.begin(), commb.end()) != e) {
            while (std::search(b, e, comme.begin(), comme.end()) == e && !file.eof()){
                std::getline(file, line);
                b = line.begin();
                e = line.end();
                if (file.eof()){
                    if (Sys.inf.id == 0)
                        cout <<  "Looks like you forgot to put the closing comment" << std::endl;
                    if (Sys.inf.id == 0)
                        cout <<  "Press any key to continue ...";
                    std::cin.ignore();
                }
            }
            std::getline(file, line);
        }


        res = funcs::split(line);
        begin = res.begin();
        end   = res.end();

        if (line[0]!= '#' && line[0]!= '/' && !line.empty()) {
            while (begin < res.end()){
                samp.InData.push_back(funcs::ToDouble(*begin));
                ++begin;
            }
        }

        std::getline(file, line);
        res = funcs::split(line);
        begin = res.begin();
        end   = res.end();

        if (line[0]!= '#' && line[0]!= '/' && !line.empty()) {
            while (begin < res.end()){
                samp.OutData.push_back(funcs::ToDouble(*begin));
                ++begin;
            }
        }
        Sys.net.Sample.push_back(samp);
        std::getline(file, line);
    }

    file.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Sample File file ... " << std::endl;
}

void DataManager::ReadSampleFile(System &Sys) {
    using std::getline;
    std::fstream file;
    std::string type, name, namei, namej, pair, pairopp, line;
    std::stringstream iss;
    vector<std::string> res;
    vector<string>::iterator begin, end;
    SampleStruct samp;

    static const std::string commb = "/*";
    static const std::string comme = "*/";

    file.open(Sys.files.SampleFile.c_str(), std::ios::in);
    Sys.net.Sample.clear();


    if (!file.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The Sample file " << Sys.files.SampleFile.c_str() <<  " is not fould " << std::endl;
        return;
    }

    std::getline(file, line);

    while (!file.eof())
    {
        std::string::const_iterator b = line.begin();
        std::string::const_iterator e = line.end();

        samp.InData.clear();
        samp.OutData.clear();

        if (std::search(b, e, commb.begin(), commb.end()) != e) {
            while (std::search(b, e, comme.begin(), comme.end()) == e && !file.eof()){
                std::getline(file, line);
                b = line.begin();
                e = line.end();
                if (file.eof()){
                    if (Sys.inf.id == 0)
                        cout <<  "Looks like you forgot to put the closing comment" << std::endl;
                    if (Sys.inf.id == 0)
                        cout <<  "Press any key to continue ...";
                    std::cin.ignore();
                }
            }
            std::getline(file, line);
        }

        res = funcs::split(line);
        begin = res.begin();
        end   = res.end();

        if (line[0]!= '#' && line[0]!= '/' && !line.empty()) {
            while (begin < res.end()){
                samp.InData.push_back(funcs::ToDouble(*begin));
                ++begin;
            }
        }

        Sys.net.Sample.push_back(samp);
        std::getline(file, line);
    }

    file.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Sample File file ... " << std::endl;
}


void DataManager::ReadPotentialFile(System &Sys){
    using std::getline;
    std::vector<std::string> res;
    std::vector<std::string>::iterator begin;
    std::vector<std::string>::iterator end;

    std::fstream PotDataFile;
    PotDataFile.open(Sys.files.PotenFile.c_str(), std::ios::in);

    std::string name, namei, namej, pair, pairopp, line, type;
    std::vector<std::string> names;
    std::stringstream iss;

    if (!PotDataFile.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The potential data file " << Sys.files.PotenFile.c_str() <<  "is not fould " << std::endl;
        return;
    }

    Sys.inf.SubsN = 0;
    while (!PotDataFile.eof()) {

        getline(PotDataFile, line);
        res = funcs::split(line);
        begin = res.begin();
        end = res.end();

        if (line[0]!= '#' && line[0]!= '/' && line[0]!= '|' && line[0]!= '-' && line[0]!= '+' && !line.empty()) {
            type = *begin;


            if (type == "Morse" || type == "morse" || type == "MORSE") {
                ++begin;
                pair = *begin;

                names = funcs::splitString(pair);

                /*
                if (names.size() > 1) {
					if (names[0] != names[1]) {

						namei = names[0] + names[1];
						namej = names[1] + names[0];
						Sys.params[namei].name = namei;
						Sys.params[namej].name = namej;
						Sys.params[namei].ni = names[0];
						Sys.params[namei].nj = names[1];
						Sys.params[namej].ni = names[1];
						Sys.params[namej].nj = names[0];
						Sys.params[namei].pair = true;
						Sys.params[namej].pair = true;

						++begin;
						Sys.params[namei].DM = funcs::ToDouble(*begin);
						Sys.params[namej].DM = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].alphaM = funcs::ToDouble(*begin);
						Sys.params[namej].alphaM = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].r = funcs::ToDouble(*begin);
						Sys.params[namej].r = funcs::ToDouble(*begin);
					} else {
						namei = names[0] + names[1];
						Sys.params[namei].name = namei;
						Sys.params[namei].pair = true;

						Sys.params[namei].ni = names[0];
						Sys.params[namei].nj = names[1];

						++begin;
						Sys.params[namei].DM = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].alphaM = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].r = funcs::ToDouble(*begin);
					}
                } else {
                    */
					namei = names[0];
	                Sys.params[namei].name = namei;
					Sys.params[namei].pair = false;
					Sys.params[namei].ni = names[0];
					Sys.params[namei].nj = names[0];

	                ++begin;
	                Sys.params[namei].DM = funcs::ToDouble(*begin);
	                ++begin;
	                Sys.params[namei].alphaM = funcs::ToDouble(*begin);
	                ++begin;
	                Sys.params[namei].r = funcs::ToDouble(*begin);
                //}
            }

            if (type == "MEAM" || type == "Meam" || type == "meam") {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
                Sys.params[namei].pair = false;
                ++begin;
                Sys.params[name].A = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].E = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].rho0 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].re = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].beta0 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].beta1 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].beta2 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].beta3 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].t0 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].t1 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].t2 = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].t3 = funcs::ToDouble(*begin);
                if ((end-begin) > 1) {
                    ++begin;
                    Sys.params[name].rc_meam = funcs::ToDouble(*begin);
                }
                if ((end-begin) > 1) {
                    ++begin;
                    Sys.params[name].Nz = funcs::ToDouble(*begin);
                }
            }


            if (type == "NICHENKO" || type == "Nichenko" || type == "nichenko") {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
                ++begin;
                Sys.params[name].DN = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].alphaN = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].rN = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].rho0N = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].beta0N = funcs::ToDouble(*begin);
                if ((end-begin) > 1) {
                    ++begin;
                    Sys.params[name].rc_N = funcs::ToDouble(*begin);
                }
                if ((end-begin) > 1) {
                    ++begin;
                    Sys.params[name].NN = funcs::ToDouble(*begin);
                }
            }

            if (type == "Mass" || type == "mass" || type == "MASS") {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
				Sys.params[name].pair = false;
				Sys.params[name].ni   = name;
                ++begin;
                Sys.params[name].mass = funcs::ToDouble(*begin);
            }

            if (type == "Charge" || type == "charge" || type == "Q" || type == "q" ) {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
				Sys.params[name].pair = false;
				Sys.params[name].ni   = name;
                ++begin;
                Sys.params[name].q = funcs::ToDouble(*begin);
            }


            if (type == "Polariz" || type == "polariz" || type == "POLARIZ") {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
                ++begin;
                Sys.params[name].polariz = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].polb = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].polc = funcs::ToDouble(*begin);
            }

            if (type == "BMH" || type == "bmh" || type == "Bmh") {
                ++begin;
                name = *begin;
                Sys.params[name].name = name;
				Sys.params[name].pair = false;
				Sys.params[name].ni   = name;
				++begin;
                Sys.params[name].a = funcs::ToDouble(*begin);
                ++begin;
                Sys.params[name].b = funcs::ToDouble(*begin);
            }

			if (type == "Buk" || type == "BUK" || type == "buk" ||
				type == "Buc" || type == "BUC" || type == "buc" ||
				type == "Buckingham" || type == "BUCKINGHAM" || type == "buckingham") {

                ++begin;
                pair = *begin;

                names = funcs::splitString(pair);

                if (names.size() > 1) {
					if (names[0] != names[1]) {

						namei = names[0] + names[1];
						namej = names[1] + names[0];
						Sys.params[namei].name = namei;
						Sys.params[namej].name = namej;
						Sys.params[namei].pair = true;
						Sys.params[namej].pair = true;
						Sys.params[namei].ni = names[0];
						Sys.params[namei].nj = names[1];
						Sys.params[namej].ni = names[1];
						Sys.params[namej].nj = names[0];

						++begin;
						Sys.params[namei].buc_Aab = funcs::ToDouble(*begin);
						Sys.params[namej].buc_Aab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_rab = funcs::ToDouble(*begin);
						Sys.params[namej].buc_rab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_Cab = funcs::ToDouble(*begin);
						Sys.params[namej].buc_Cab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_Dab = funcs::ToDouble(*begin);
						Sys.params[namej].buc_Dab = funcs::ToDouble(*begin);
					} else {
						namei = names[0] + names[1];
						Sys.params[namei].name = namei;
						Sys.params[namei].pair = true;
						Sys.params[namei].ni = names[0];
						Sys.params[namei].nj = names[1];

						++begin;
						Sys.params[namei].buc_Aab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_rab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_Cab = funcs::ToDouble(*begin);
						++begin;
						Sys.params[namei].buc_Dab = funcs::ToDouble(*begin);
					}
                } else {
					namei = names[0];
					Sys.params[namei].name = namei;
					Sys.params[namei].pair = false;
					Sys.params[namei].ni = names[0];
					Sys.params[namei].nj = names[0];

					++begin;
					Sys.params[namei].buc_Aab = funcs::ToDouble(*begin);
					++begin;
					Sys.params[namei].buc_rab = funcs::ToDouble(*begin);
					++begin;
					Sys.params[namei].buc_Cab = funcs::ToDouble(*begin);
					++begin;
					Sys.params[namei].buc_Dab = funcs::ToDouble(*begin);
                }
            }
        }
    }
    Sys.inf.SubsN = Sys.params.size();

    PotDataFile.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Potential file ... " << std::endl;
}

void DataManager::ReadFitFile(System &Sys){
    using std::getline;
    ValueUnit ValUn;

    std::fstream FitDataFile;
    vector<std::string> res;
    vector<string>::iterator begin, end;

    Sys.fit.Reset();

    FitDataFile.open(Sys.files.FitFile.c_str(), std::ios::in);

    typedef vector<double>::size_type size;

    std::string name, line;
    std::stringstream iss;

    static const std::string commb = "/*";
    static const std::string comme = "*/";

    if (!FitDataFile.is_open())
    {
        if (Sys.inf.id == 0)
            std::cout << "The fit data file " << Sys.files.FitFile.c_str() <<  "is not fould " << std::endl;
        return;
    }

    std::getline(FitDataFile, line);
    while (!FitDataFile.eof())
    {
        std::string::const_iterator b = line.begin();
        std::string::const_iterator e = line.end();

        if (std::search(b, e, commb.begin(), commb.end()) != e){
            while (std::search(b, e, comme.begin(), comme.end()) == e && !FitDataFile.eof()){
                std::getline(FitDataFile, line);
                b = line.begin();
                e = line.end();
                if (FitDataFile.eof()){
                    if (Sys.inf.id == 0)
                        cout <<  "Looks like you forgot to put the closing comment" << std::endl;
                    if (Sys.inf.id == 0)
                        cout <<  "Press any key to continue ...";
                    std::cin.ignore();
                }
            }
            std::getline(FitDataFile, line);
        }

        res = funcs::split(line);
        begin = res.begin();
        end   = res.end();
        if (line[0]!= '#' && line[0]!= '/' && !line.empty()) {

            // Read the data for the fit
            if ((begin != res.end()) && (*begin == "data" || *begin == "Data" || *begin == "DATA")) {
                ++begin;

                if ((begin != res.end()) && (*begin == "t" || *begin == "T"  || *begin == "At" || *begin == "AT" || *begin == "at")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.AtTemperature.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "density" || *begin == "Density" || *begin == "DENSITY")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Density.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "energy" || *begin == "Energy" || *begin == "ENERGY")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Energy.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "P" || *begin == "Pressure" || *begin == "pressure" || *begin == "PRESSURE")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Pressure.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "Diffusion" || *begin == "diffusion" || *begin == "DIFFUSION")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Diffusion.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "Time" || *begin == "time" || *begin == "TIME")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Time.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "Temperature" || *begin == "temperature" || *begin == "TEMPERATURE")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.Diffusion.push_back(ValUn.value);
                        ++begin;
                    }
                }

                if ((begin != res.end()) && (*begin == "Cohesive" || *begin == "CohesiveEnergy" || *begin == "CohesiveE" || *begin == "CE")) {
                    ++begin;
                    while (begin != res.end()) {
                        ValUn = Sys.getValue(*begin);
                        Sys.fit.CohesiveE.push_back(ValUn.value);
                        ++begin;
                    }
                }
            }

            if ((begin != res.end()) && (*begin == "structure" || *begin == "Structure" || *begin == "STRUCTURE"))
            {
                ++begin;
                while (begin != res.end()) {
                    if (*begin == "yes" || *begin == "true" || *begin == "on")   Sys.fit.Struct_b.push_back(true);
                    if (*begin == "no"  || *begin == "false" || *begin == "off") Sys.fit.Struct_b.push_back(false);
                    ++begin;
                }
            }

            if ((begin != res.end()) && (*begin == "premelt" || *begin == "Premelt" || *begin == "PREMELT")) {
                Sys.control.premeltR_b = true;
                ++begin;
                while (begin != res.end()) {
                    if (*begin == "yes" || *begin == "true" || *begin == "on")   Sys.fit.Premelt_b.push_back(true);
                    if (*begin == "no"  || *begin == "false" || *begin == "off") Sys.fit.Premelt_b.push_back(false);
                    ++begin;
                }
            }

            if ((begin != res.end()) && (*begin == "premelt" || *begin == "Premelt" || *begin == "PREMELT" || *begin == "premeltR" || *begin == "PremeltR" || *begin == "PREMELTR")) {
                Sys.control.premeltR_b = true;
                Sys.control.premeltL_b = false;
                ++begin;
                while (begin != res.end()) {
                    if (*begin == "yes" || *begin == "true" || *begin == "on")   Sys.fit.Premelt_b.push_back(true);
                    if (*begin == "no"  || *begin == "false" || *begin == "off") Sys.fit.Premelt_b.push_back(false);
                    ++begin;
                }
            }

            if ((begin != res.end()) && (*begin == "premeltL" || *begin == "PremeltL" || *begin == "PREMELTL")) {
                Sys.control.premeltR_b = false;
                Sys.control.premeltL_b = true;
                ++begin;
                while (begin != res.end()) {
                    if (*begin == "yes" || *begin == "true" || *begin == "on")   Sys.fit.Premelt_b.push_back(true);
                    if (*begin == "no"  || *begin == "false" || *begin == "off") Sys.fit.Premelt_b.push_back(false);
                    ++begin;
                }
            }

            if ((begin != res.end()) && (*begin == "RDF" || *begin == "rdf" || *begin == "Rdf"))  {
                int NPairs = 0;
                rdfPair rdfp;
                Sys.control.rdf_b = true;
                Sys.fit.FitRDF  = true;
                Sys.RDFfor.clear();

                if ((end-begin) > 1){
                    ++begin;
                    if (*begin == "for") {
                        ++begin;
                        while (begin != end) {
                            std::vector<std::string> out = funcs::splitString(*begin);
                            rdfp.At1 = out[0];
                            rdfp.At2 = out[1];
                            Sys.RDFfor.push_back(rdfp);
                            ++NPairs;
                            ++begin;
                        }
                    }
                }

                std::getline(FitDataFile, line);
                res = funcs::split(line);
                begin = res.begin();

                int Nint = funcs::ToInt(*begin);
                Sys.RDF.r.resize(Nint);
                Sys.RDFref.r.resize(Nint);

                std::vector<dType> empyVec;
                empyVec.resize(Nint);
                std::fill(empyVec.begin(), empyVec.end(), 0.0);
                Sys.RDF.n.clear();
                for (int i = 0; i != Nint; i++) {
                    Sys.RDF.n.push_back(empyVec);
                    Sys.RDFref.n.push_back(empyVec);
                }

                for (int i = 0; i != Nint; i++) {
                    std::getline(FitDataFile, line);
                    iss.clear();
                    iss.str(line);
                    iss >>  Sys.RDFref.r[i];
                    for (int j = 0; j != NPairs; j++) {
                        iss >>  Sys.RDFref.n[j][i];
                    }
                }
            }

        }
        std::getline(FitDataFile, line);
    }
    FitDataFile.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Fit file ... " << std::endl;
}


void DataManager::ReadPositionFile(System &Sys){
    using std::getline;
    std::fstream File;

    std::string line, name;
    std::stringstream iss;

    File.open(Sys.files.XYZInFile.c_str(), std::ios::in);

    if (!File.is_open()) {
        if (Sys.inf.id == 0)
            std::cout << "The positions file: " << Sys.files.XYZInFile.c_str()  <<" is not fould " << std::endl;
        return;
    }

    std::getline(File, line);
    iss.clear();
    iss.str(line);

    iss >>  Sys.At.size;
    Sys.At.rsize = 1.0 / ((double) Sys.At.size);
    Sys.At.Resize( Sys.At.size );

    std::getline(File, line);
    iss.clear();
    iss.str(line);
    if (funcs::split(line).size() == 3) {
        iss >>  Sys.inf.Cell.a;
        iss >>  Sys.inf.Cell.b;
        iss >>  Sys.inf.Cell.c;
    }

    if (funcs::split(line).size() == 6) {
        iss >>  Sys.inf.Cell.a;
        iss >>  Sys.inf.Cell.b;
        iss >>  Sys.inf.Cell.c;
        iss >>  Sys.inf.Cell.alpha;
        iss >>  Sys.inf.Cell.betta;
        iss >>  Sys.inf.Cell.gamma;
        Sys.inf.Cell.setTMatrix();
    }

    Sys.inf.Wall.xmin = 0;
    Sys.inf.Wall.ymin = 0;
    Sys.inf.Wall.zmin = 0;

    Sys.inf.Wall.xmax =  Sys.inf.Cell.a;
    Sys.inf.Wall.ymax =  Sys.inf.Cell.b;
    Sys.inf.Wall.zmax =  Sys.inf.Cell.c;

    for (int i=0; i< Sys.At.size; i++) {
        Sys.At.id[i] = i;

        std::getline(File, line);
        iss.clear();
        iss.str(line);

        iss >> Sys.At.name[i];
        iss >> Sys.At.x[i];
        iss >> Sys.At.y[i];
        iss >> Sys.At.z[i];
    }
    Sys.At.x0 = Sys.At.x;
    Sys.At.y0 = Sys.At.y;
    Sys.At.z0 = Sys.At.z;

    #ifdef debug 
        std::cout << "Done reading coordinates ... " << std::endl;
     #endif

    Sys.AssignParams(Sys.params);
    #ifdef debug 
        std::cout << "Done AssignParams ... " << std::endl;
     #endif

    Sys.RestoreImages();
    #ifdef debug 
        std::cout << "Done RestoreImages ... " << std::endl;
     #endif

    File.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Position file ... " << std::endl;
}

void DataManager::Restart(System &Sys) {

    std::fstream File;
    vector<string> res;

    File.open(Sys.files.XYZInFile.c_str(), std::ios::in);
    std::string name, line;
    std::stringstream iss;

    if (!File.is_open()) {
        if (Sys.inf.id == 0)
            std::cout << "The State file: " << Sys.files.XYZInFile.c_str()  <<"  is not fould " << std::endl;
    }

    GetLine(File, res);
    Sys.At.size = funcs::ToInt(res[0]);
    Sys.At.rsize = 1.0 / ((double) Sys.At.size);
    Sys.At.Resize( Sys.At.size);

    GetLine(File, res);
    Sys.inf.Cell.a = funcs::ToDouble(res[0]);
    Sys.inf.Cell.b = funcs::ToDouble(res[1]);
    Sys.inf.Cell.c = funcs::ToDouble(res[2]);

    //vector<Atom>::iterator iter;
    for (int i = 0; i < Sys.At.size; ++i) {
        GetLine(File, res);

        Sys.At.name[i] = res[0];


        Sys.At.x[i]    = funcs::ToDouble(res[1]);
        Sys.At.y[i]    = funcs::ToDouble(res[2]);
        Sys.At.z[i]    = funcs::ToDouble(res[3]);

        Sys.At.q[i]    = funcs::ToDouble(res[4]);
        Sys.At.qi[i]   = funcs::ToInt(res[5]);

        Sys.At.vx[i]   = funcs::ToDouble(res[6]);
        Sys.At.vy[i]   = funcs::ToDouble(res[7]);
        Sys.At.vz[i]   = funcs::ToDouble(res[8]);

        Sys.At.ax[i]   = funcs::ToDouble(res[9]);
        Sys.At.ay[i]   = funcs::ToDouble(res[10]);
        Sys.At.az[i]   = funcs::ToDouble(res[11]);
    }

    Sys.AssignParams(Sys.params);
    Sys.RestoreImages();

    File.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loading the Restart file ... " << std::endl;
}

void DataManager::ReadForceFile(System &Sys)
{
    std::string text, line;
    std::stringstream iss;

    std::fstream File;


    File.open(Sys.files.InForceFile.c_str(), std::ios::in);

    if (!File.is_open())
    {
        if (Sys.inf.id == 0)
        std::cout << "The force file: " << Sys.files.InForceFile.c_str()  <<"  is not fould " << std::endl;
        return;
    }

    std::getline(File, line);
    iss.clear();
    iss.str(line);

    //Resize the Force vector to the Atom number size
    Sys.Force.resize(Sys.At.size);

    // Reads the comment from the force file
    std::getline(File, line);
    iss.clear();
    iss.str(line);

    for (int i=0; i< Sys.At.size; i++)
    {
        std::getline(File, line);
        iss.clear();
        iss.str(line);
        iss >> text;
        iss >> Sys.Force[i].x;
        iss >> Sys.Force[i].y;
        iss >> Sys.Force[i].z;
    }
    File.close();
    if (Sys.inf.id == 0)
        std::cout << "Done loadiung the Force File ... \n";
}

void DataManager::SaveFitRes(System &Sys)
{
    std::fstream DataFile;
    std::stringstream stream;
    std::fstream file;

    stream.str("");
    stream.clear();

    file.open(Sys.files.FitOutFile.c_str(), std::fstream::out);

    //map<std::string, Params>::const_iterator iter;
    ParamsIterConst iter;

    //for (int i=0; i != Coeffs[0].size(); ++i){
    for (iter = Sys.Coeffs[0].begin(); iter != Sys.Coeffs[0].end(); ++iter){
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(5);
        stream << iter->first ;//(Coeffs[0].begin() + i)[i].name;

        stream.setf(std::ios::fixed);
        stream.width(10);
        stream.precision(3);
        stream << iter->second.q;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.a;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.b;


        stream.width(10);
        stream.precision(3);
        stream << iter->second.DM;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.alphaM;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.r;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.mass << std::endl;
        file << stream.str();
    }
    file.close();

    DataFile.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
    DataFile << Sys.inf.Count << " - " << Sys.Coeffs[0].begin()->second.fitres << std::endl;
    DataFile.close();


    if (Sys.control.forcefile_b)
    {
        stream.str("");
        stream.clear();

        file.open(Sys.files.ForceFile.c_str(), std::fstream::out);

        stream <<  Sys.At.size << std::endl;
        stream << "Temperature = " << Sys.props.Temperature << "     Size = " <<  Sys.inf.Cell.a << "  " <<  Sys.inf.Cell.b << "  " <<  Sys.inf.Cell.c <<std::endl;
        file << stream.str();

        for (int i=0; i< Sys.At.size; i++)
        {
            stream.str("");
            stream.clear();
            stream.fill(' ');

            stream.width(5);
            stream << Sys.At.name[i];

            stream.setf(std::ios::fixed);
            stream.width(15);
            stream.precision(4);
            stream << std::scientific << Sys.At.Fx[i];

            stream.width(15);
            stream.precision(4);
            stream << std::scientific << Sys.At.Fy[i];

            stream.width(15);
            stream.precision(4);
            stream << std::scientific << Sys.At.Fz[i] << std::endl;
            file << stream.str();

        }
        file.close();
    }

}

bool DataManager::fileExists(const std::string& filename) {
    std::ifstream infile(filename.c_str());
    return infile.good();
}
