//-------------------------------------------------------------------
// File:   datamanager.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "system.h"
//#include "simulation.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <string>
#include <iostream>
#include <time.h>
#include <vector>
#include "math/funcs.h"
#include <iomanip>      // std::setfill, std::setw
#include "datatypes.h"

#ifndef Datamanager_INCLUDED
#define Datamanager_INCLUDED

class System;

class DataManager {
public:
    time_t timev;

    void GetLine(std::fstream&, std::vector<std::string>&);

    void HLine(System &Sys);
    void HeadLine(System &Sys);
    void ShowResults(System &Sys);
    void SaveTable(System &Sys);
    void SaveRDF(System &Sys);
    void SaveXYZFile(System &Sys);
    void SaveSampleData(System &Sys);
    void SaveNet(System &Sys);
    void ClearFiles(System &Sys);
    void ClearFile(std::string&);
    bool copyFile(std::string src, std::string des);
    void ReadForceFile(System &Sys);
    void ReadPotentialFile(System &Sys);
    void ReadFitFile(System &Sys);
    void ReadDETFile(System &Sys);
    void ReadNNinFile(System &Sys);
    void ReadNetTrainingFile(System &Sys);
    void ReadSampleFile(System &Sys);
    void SaveNetResults(System &Sys);
    void ReadPositionFile(System &Sys);
    void Restart(System &Sys);
    void SaveFitRes(System &Sys);
    void SaveFitPotential(System &Sys);
    void SaveDataFrame(System &Sys);
    bool fileExists(const std::string& filename);
    void SaveStateFile(System &Sys, std::string path);
};

#endif // DATAMANAGER
