//-------------------------------------------------------------------
// File:   datatypes.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#pragma once

#include <numeric>
#include <vector>
#include <map>
#include "globalvariables.h"
#include "constants.h"
//#include "math/vector.hpp"

#ifndef DATATYPES_H
#define DATATYPES_H




struct DofS  {
    dType freq, real, imag, mag, x;
};

struct own_double_less : public std::binary_function<double,double,bool>
{
public:
  own_double_less( double arg_ = 1.0e-5 ) : epsilon(arg_) {}
  bool operator()( const double &left, const double &right  ) const
  {
    // you can choose other way to make decision
    // (The original version is: return left < right;)
    return (abs(left - right) > epsilon) && (left < right);
  }
  double epsilon;
};


struct cp_aux {
    double operator()(const DofS& dof) const {
        double exp_val;

        if (dof.x == 0.0)
            return 1.0 * dof.mag;
        if (dof.x != 0.0 && dof.x < 100){
            exp_val = std::exp(dof.x);
            return dof.x * dof.x * exp_val / ((1.0 - exp_val) * (1.0 - exp_val)) * dof.mag;
        }
        if (dof.x >= 100)
            return 0.0;
        return 0.0;
    }
};

class LUTable{
private:
    std::vector<dType> table;
    dType scale, dx;
    int size, il, ir;

public:
    LUTable()  {
        size = 10000;
        scale = 0.10;
        table.resize(size);
    }

    void defineTable(vector<dType>& X, vector<dType>& Y)  {
        dType Max = *std::max_element(X.begin(), X.end());
        dType Min = 0.0;
        int index;
        scale = size / (Max - Min);

        assert( X.size() == Y.size());

        for (int i = 0; i < Y.size(); i++) {
            index = X[i] * scale - 1;
            if (index < 0) index = 0;
            if( index >= table.size()) {
                std::cout << "index  : " << index << "   table.size : " <<  table.size() << "   Max : " << Max << "     Min : "  << Min << "    X[i] : " << X[i] << "    scale : " << scale << std::endl;
            }
            table[index] = Y[i];
        }
    }

    dType getValue(dType& X)  {
        il = X * scale;
        ir = il + 1;
        
        if (ir >= size) return table[(size-1)];
        if (il < 0)     return table[0];
        dx = (X * scale - il);
        return  table[il] + dx * (table[ir] - table[il]);
    }

    vector<dType> getValue(vector<dType>& X)  {
        std::vector<dType> res(X.size());

        for (int i = 0; i < X.size(); i++) {
            res[i] = getValue(X[i]);
        }
        return res;
    }


	void setSize(int& num)  {
        size = num;
        table.resize(size);
    }

	void setZero()  {
        std::fill(table.begin(), table.end(), 0.0);
    }
};



struct LUTstruct {
public:
    std::map<int, LUTable> table;

    vector<dType> getValue( vector<int>& key, vector<dType>& X ) {
        int size = X.size();
        vector<dType> res(size);

        //#pragma omp parallel for
        for (int i = 0; i < size; i++) {
            res[i] = table[key[i]].getValue(X[i]);
        }
        return res;
    }

    vector<dType> getValue( int& key, vector<dType>& X ) {
        int size = X.size();
        vector<dType> res(size);

        //#pragma omp parallel for
        for (int i = 0; i < size; i++) {
            res[i] = table[key].getValue(X[i]);
        }
        return res;
    }

	void setSize(int& key, int& num){
			table[key].setSize(num);
	}
};

struct ValueUnit {
    std::string unut;
    dType value, scale, shift;
};

struct ForceEnergy {
    std::vector<dType> F;
    std::vector<dType> E;
    void Clear() {
        F.clear();
        E.clear();
    }
    void Resize(int num) {
        F.resize(num);
        E.resize(num);
    }

    void Resize(int num, double val) {
        F.resize(num);
        E.resize(num);
        std::fill(F.begin(), F.end(), val);
        std::fill(E.begin(), E.end(), val);
    }

    void Fill(double num) {
        std::fill(F.begin(), F.end(), num);
        std::fill(E.begin(), E.end(), num);
    }
};

struct rdfPair {
    std::string At1, At2;
};

struct PairDist {
    iType j;
    dType R, Rx, Ry, Rz;
};

struct XYZ_vec {
    std::vector<dType> x;
    std::vector<dType> y;
    std::vector<dType> z;

    void Resize(int num) {
        x.resize(num);
        y.resize(num);
        z.resize(num);
    }

    void Clear() {
        x.clear();
        y.clear();
        z.clear();
    }
};

struct Visc_Struct {
    std::vector<dType> xx;
    std::vector<dType> xy;
    std::vector<dType> xz;
    std::vector<dType> yx;
    std::vector<dType> yy;
    std::vector<dType> yz;
    std::vector<dType> zx;
    std::vector<dType> zy;
    std::vector<dType> zz;

    std::vector<dType> Cxyxy;
    std::vector<dType> Cxzxz;
    std::vector<dType> Cyzyz;
    dType sum = 0;

    void Resize(int num) {
        xx.resize(num);
        xy.resize(num);
        xz.resize(num);
        yx.resize(num);
        yy.resize(num);
        yz.resize(num);
        zx.resize(num);
        zy.resize(num);
        zz.resize(num);
        Cxyxy.resize(num);
        Cxzxz.resize(num);
        Cyzyz.resize(num);
    }

    void Clear() {
        xx.clear();
        xy.clear();
        xz.clear();
        yx.clear();
        yy.clear();
        yz.clear();
        zx.clear();
        zy.clear();
        zz.clear();
        Cxyxy.clear();
        Cxzxz.clear();
        Cyzyz.clear();
        sum = 0;
    }
};

struct Corr_struct {

    std::vector<dType> jj;
    std::vector<dType> njj;

    std::vector<dType> xy;
    std::vector<dType> nxy;
    std::vector<dType> yz;
    std::vector<dType> nyz;
    std::vector<dType> zx;
    std::vector<dType> nzx;

    void Resize(int num) {
        jj.resize(num);
        njj.resize(num);
        xy.resize(num);
        nxy.resize(num);
        yz.resize(num);
        nyz.resize(num);
        zx.resize(num);
        nzx.resize(num);

    }
    void Push_back(dType val) {
        jj.push_back(val);
        njj.push_back(val);
        xy.push_back(val);
        nxy.push_back(val);
        yz.push_back(val);
        nyz.push_back(val);
        zx.push_back(val);
        nzx.push_back(val);
    }

    void Clear() {
        jj.clear();
        njj.clear();
        xy.clear();
        nxy.clear();
        yz.clear();
        nyz.clear();
        zx.clear();
        nzx.clear();
    }
};

struct PairDistV {

    std::vector<std::string> name;
    std::vector<iType> j;
    std::vector<iType> key;
    std::vector<iType> keyi;
    std::vector<iType> keyj;
    std::vector<dType> R;
    std::vector<dType> R2;
    std::vector<std::vector<dType> > Rvec;
    std::vector<dType> rR;
    std::vector<dType> RL;
    std::vector<dType> Rx;
    std::vector<dType> Ry;
    std::vector<dType> Rz;

    std::vector<dType> vx_mid;
    std::vector<dType> vy_mid;
    std::vector<dType> vz_mid;

    std::vector<dType> R1x;
    std::vector<dType> R1y;
    std::vector<dType> R1z;

    std::vector<dType> x;
    std::vector<dType> y;
    std::vector<dType> z;

    std::vector<dType> DM;
    std::vector<dType> alphaM;
    std::vector<dType> r;
    std::vector<dType> rc_short;

    std::vector<dType> q;
    std::vector<dType> aff;
    std::vector<dType> ion;
    std::vector<dType> Ep;
    std::vector<dType> Em;
    std::vector<dType> Rc_long;

    std::vector<dType> beta;
    std::vector<dType> rho0;
    std::vector<dType> re;
    std::vector<dType> C;
    std::vector<dType> D;
    std::vector<dType> A;
    std::vector<dType> E;
    std::vector<dType> beta0;
    std::vector<dType> beta1;
    std::vector<dType> beta2;
    std::vector<dType> beta3;
    std::vector<dType> t0;
    std::vector<dType> t1;
    std::vector<dType> t2;
    std::vector<dType> t3;
    std::vector<dType> rc_meam;

    std::vector<dType> rho;
    std::vector<dType> rrho;
    std::vector<dType> lnrho;
    std::vector<dType> sqrho;
    std::vector<dType> rsqrho;

    std::vector<dType> polb;
    std::vector<dType> polc;
    std::vector<dType> muxpr;
    std::vector<dType> muypr;
    std::vector<dType> muzpr;
    std::vector<dType> Fsc;
    std::vector<dType> DN;
    std::vector<dType> rN;
    std::vector<dType> alphaN;
    std::vector<dType> rho0N;
    std::vector<dType> beta0N;
    std::vector<dType> rc_N;
    std::vector<dType> NN;

    void Clear() {
        name.clear();
        j.clear();
        key.clear();
        keyi.clear();
        keyj.clear();
        R.clear();
        R2.clear();
        rR.clear();
        RL.clear();
        Rx.clear();
        Ry.clear();
        Rz.clear();

        vx_mid.clear();
        vy_mid.clear();
        vz_mid.clear();

        R1x.clear();
        R1y.clear();
        R1z.clear();

        DM.clear();
        alphaM.clear();
        r.clear();

        q.clear();
        aff.clear();
        ion.clear();
        Ep.clear();
        Em.clear();

        beta.clear();
        rho0.clear();
        re.clear();
        C.clear();
        D.clear();
        A.clear();
        E.clear();
        beta0.clear();
        beta1.clear();
        beta2.clear();
        beta3.clear();
        t0.clear();
        t1.clear();
        t2.clear();
        t3.clear();

        rho.clear();
        rrho.clear();
        lnrho.clear();
        sqrho.clear();
        rsqrho.clear();

        polb.clear();
        polc.clear();
        muxpr.clear();
        muypr.clear();
        muzpr.clear();

        Fsc.clear();

        DN.clear();
        rN.clear();
        alphaN.clear();
        rho0N.clear();
        beta0N.clear();
        rc_N.clear();
        NN.clear();
    }

    void Clear(int size) {
        Clear();
        Reserve(size);
    }

    void Reserve(int size){
        j.reserve(size);
        key.reserve(size);
        keyi.reserve(size);
        keyj.reserve(size);
        R.reserve(size);
        R2.reserve(size);
        rR.reserve(size);
        Rx.reserve(size);
        Ry.reserve(size);
        Rz.reserve(size);

    }

    void Resize(int size){
        j.resize(size);
        key.resize(size);
        keyi.reserve(size);
        keyj.reserve(size);
        R.resize(size);
        R2.resize(size);
        rR.resize(size);
        Rx.resize(size);
        Ry.resize(size);
        Rz.resize(size);

    }

};


struct Parameters {
    std::vector<std::string> name;
    std::vector<iType> ind;
    std::vector<dType> R;
    std::vector<dType> R2;
    std::vector<dType> rR;
    std::vector<dType> Rx;
    std::vector<dType> Ry;
    std::vector<dType> Rz;
    std::vector<dType> R1x;
    std::vector<dType> R1y;
    std::vector<dType> R1z;

    std::vector<dType> q;

    std::vector<dType> DM;
    std::vector<dType> alphaM;
    std::vector<dType> r;
    std::vector<dType> rc_short;

    std::vector<dType> beta;
    std::vector<dType> rho0;
    std::vector<dType> re;
    std::vector<dType> A;
    std::vector<dType> E;
    std::vector<dType> beta0;
    std::vector<dType> beta1;
    std::vector<dType> beta2;
    std::vector<dType> beta3;
    std::vector<dType> t0;
    std::vector<dType> t1;
    std::vector<dType> t2;
    std::vector<dType> t3;
    std::vector<dType> rc_meam;
    std::vector<dType> Z;

    std::vector<dType> polb;
    std::vector<dType> polc;
    std::vector<dType> muzpr;

    void Clear() {
        name.clear();
        ind.clear();
        R.clear();
        R2.clear();
        rR.clear();
        Rx.clear();
        Ry.clear();
        Rz.clear();
        R1x.clear();
        R1y.clear();
        R1z.clear();
        q.clear();
        DM.clear();
        alphaM.clear();
        r.clear();
        rc_short.clear();
        beta.clear();
        rho0.clear();
        re.clear();
        A.clear();
        E.clear();
        beta0.clear();
        beta1.clear();
        beta2.clear();
        beta3.clear();
        t0.clear();
        t1.clear();
        t2.clear();
        t3.clear();
        rc_meam.clear();
        Z.clear();
        polb.clear();
        polc.clear();
        muzpr.clear();
    }
    void Reserve(int size){
        name.reserve(size);
        ind.reserve(size);
        R.reserve(size);
        R2.reserve(size);
        rR.reserve(size);
        Rx.reserve(size);
        Ry.reserve(size);
        Rz.reserve(size);
        R1x.reserve(size);
        R1y.reserve(size);
        R1z.reserve(size);
        q.reserve(size);
        DM.reserve(size);
        alphaM.reserve(size);
        r.reserve(size);
        rc_short.reserve(size);
        beta.reserve(size);
        rho0.reserve(size);
        re.reserve(size);
        A.reserve(size);
        E.reserve(size);
        beta0.reserve(size);
        beta1.reserve(size);
        beta2.reserve(size);
        beta3.reserve(size);
        t0.reserve(size);
        t1.reserve(size);
        t2.reserve(size);
        t3.reserve(size);
        rc_meam.reserve(size);
        Z.reserve(size);
        polb.reserve(size);
        polc.reserve(size);
        muzpr.reserve(size);
    }

    void Resize(int size){
        name.resize(size);
        ind.resize(size);
        R.resize(size);
        R2.resize(size);
        rR.resize(size);
        Rx.resize(size);
        Ry.resize(size);
        Rz.resize(size);
        R1x.resize(size);
        R1y.resize(size);
        R1z.resize(size);
        q.resize(size);
        DM.resize(size);
        alphaM.resize(size);
        r.resize(size);
        rc_short.resize(size);
        beta.resize(size);
        rho0.resize(size);
        re.resize(size);
        A.resize(size);
        E.resize(size);
        beta0.resize(size);
        beta1.resize(size);
        beta2.resize(size);
        beta3.resize(size);
        t0.resize(size);
        t1.resize(size);
        t2.resize(size);
        t3.resize(size);
        rc_meam.resize(size);
        Z.resize(size);
        polb.resize(size);
        polc.resize(size);
        muzpr.resize(size);
    }

};


struct dEq {
    std::vector<dType> ion;
    std::vector<dType> aff;
};

struct DETparams {
    std::string name;
    std::vector<dType> Eioniz;
    std::vector<dType> Eaffin;
    dType aip[4];
    dType bip[4];
    dType aea;
    dType bea;

    DETparams() {
        Eioniz.resize(8);
        Eaffin.resize(4);
        Eioniz[0] = 0.0;
        Eioniz[1] = 10.0;
        Eioniz[2] = 25.0;
        Eioniz[3] = 40.0;
        Eioniz[4] = 60.0;
        Eioniz[5] = 90.0;
        Eioniz[6] = 130.0;
        Eioniz[7] = 170.0;

        Eaffin[0] = 0.0;
        Eaffin[1] = 0.5;
        Eaffin[2] = 5.0;
        Eaffin[3] = 15.0;

        aip[0] = 0.0;
        aip[1] = 0.0;
        aip[2] = 0.0;
        aip[3] = 0.0;

        bip[0] = 0.0;
        bip[1] = 0.0;
        bip[2] = 0.0;
        bip[3] = 0.0;
        aea    = 0.0;
        bea    = 0.0;
    }
};

struct Params {
    std::string name;
    bool pair;
    std::string ni;
    std::string nj;
    dType temperature;
    dType pressure;
    dType density;
    dType energy;
    dType fitres;
    dType q;
    dType zn;
    dType a;
    dType b;
    dType DM;
    dType alphaM;
    dType r;
    dType buc_Aab;
    dType buc_rab;
    dType buc_Cab;
    dType buc_Dab;
    dType polariz;
    dType polb;
    dType polc;
    dType mass;
    dType ro0;
    dType C6;
    dType C8;
    dType b6;
    dType b8;
    // EAM
    dType rho0;
    dType beta;
    dType re;
    dType C;
    dType D;
    dType A;
    dType E;
    dType beta0;
    dType beta1;
    dType beta2;
    dType beta3;
    dType t0;
    dType t1;
    dType t2;
    dType t3;
    dType Nz;
    char qs[65];
    char zns[65];
    char as[65];
    char bs[65];
    char Cs[65];
    char Ds[65];
    char DMs[65];
    char alphaMs[65];
    char rs[65];
    dType rc_meam;
    dType rc_short;
    dType Rc_long;
    dType DN;
    dType alphaN;
    dType rN;
    dType beta0N;
    dType rho0N;
    dType rc_N;
    dType NN;

    Params() {
    	pair = false;
    	ni   = "";
    	nj   = "";
    	ro0	= 0.0;
    	DN	= 0.0;
    	E	= 0.0;
        rN		= 0.0;
        beta0N		= 0.0;
        rho0N		= 0.0;
        rc_N		= 0.0;
        NN		= 0.0;
        beta0		= 0.0;
        beta1		= 0.0;
        beta2		= 0.0;
        beta3		= 0.0;
        t0		= 0.0;
        t1		= 0.0;
        t2		= 0.0;
        t3		= 0.0;
    	alphaN		= 0.0;
    	A			= 0.0;
        temperature = 0.0;
        pressure  = 0.0;
        density   = 0.0;
        energy    = 0.0;
        fitres    = 0.0;
        q         = 0.0;
        zn        = 0.125;
        a         = 0.0;
        b         = 0.0;
        C6        = 0.0;
        C8        = 0.0;
        DM        = 0.0;
        alphaM    = 0.0;
        r         = 0.0;
        b6        = 0.0;
        b8        = 0.0;
        mass      = 15.999;
        polariz   = 0.0;
        polb      = 1.0;
        polc      = 0.0;
        rho0      = 0.0;
        beta      = 0.0;
        re        = 0.0;
        C         = 0.0;
        D         = 0.0;
        rc_meam   = 2.0;
        rc_short  = 5.0;
        Rc_long   = 7.5;
        Nz        = 6.0;
        buc_Aab   = 0.0;
        buc_rab   = 0.0;
        buc_Cab   = 0.0;
        buc_Dab   = 0.0;
    }
};

struct CellStruct {
    std::vector<iType> atomI;
    std::vector<dType> rx;
    std::vector<dType> ry;
    std::vector<dType> rz;
    int atomN;
    int domain;
    dType x;
    dType y;
    dType z;
    dType dx;
    dType dy;
    dType dz;

    void Clear() {
    	atomI.clear();
    	rx.clear();
    	ry.clear();
    	rz.clear();
    }

    void Resize(int size) {
    	atomI.resize(size);
    	rx.resize(size);
    	ry.resize(size);
    	rz.resize(size);
    }

    void Erase(int i) {
    	int first = std::find(atomI.begin(), atomI.end(), i) - atomI.begin();
    	atomI.erase(atomI.begin()+ first);
    	rx.erase(rx.begin() + first);
    	ry.erase(ry.begin() + first);
    	rz.erase(rz.begin() + first);
    }

    void Push(int& i, dType& x, dType& y, dType& z) {
    	atomI.push_back(i);
    	rx.push_back(x);
    	ry.push_back(y);
    	rz.push_back(z);
    }

    void Update(Atom& At) {
    	for (int i = 0; i < atomI.size(); i++) {
    		rx[i] = At.x[atomI[i]];
    		ry[i] = At.y[atomI[i]];
    		rz[i] = At.z[atomI[i]];
    	}
    }

};

struct DomainStruct {
    std::vector<int> cellI;
    std::vector<int> intcellI;
    std::vector<int> extcellI;
    std::vector<int> atomI;
    std::vector<dType> rx;
    std::vector<dType> ry;
    std::vector<dType> rz;
    int cellN;
    int domain;

    void Clear() {
    	cellI.clear();
    	intcellI.clear();
    	extcellI.clear();
    	atomI.clear();
    	rx.clear();
    	ry.clear();
    	rz.clear();
    }

    void Resize(int size) {
    	atomI.resize(size);
    	rx.resize(size);
    	ry.resize(size);
    	rz.resize(size);
    }

    void Erase(int i) {
    	int first = std::find(atomI.begin(), atomI.end(), i) - atomI.begin();
    	atomI.erase(atomI.begin()+ first);
    	rx.erase(rx.begin() + first);
    	ry.erase(ry.begin() + first);
    	rz.erase(rz.begin() + first);
    }

    void Push(int i, dType& x, dType& y, dType& z) {
    	atomI.push_back(i);
    	rx.push_back(x);
    	ry.push_back(y);
    	rz.push_back(z);
    }

    void atomClear() {
    	atomI.clear();
    	rx.clear();
    	ry.clear();
    	rz.clear();
    }

    void Insert(std::vector<int>& i, std::vector<dType>& x, std::vector<dType>& y, std::vector<dType>& z) {
    	atomI.insert(atomI.begin(), i.begin(), i.end());
    	rx.insert(rx.begin(), x.begin(), x.end());
    	ry.insert(ry.begin(), y.begin(), y.end());
    	rz.insert(rz.begin(), z.begin(), z.end());
    }

};

struct wall {
    dType xmin;
    dType ymin;
    dType zmin;

    dType xmax;
    dType ymax;
    dType zmax;

    dType xmin0;
    dType ymin0;
    dType zmin0;

    dType xmax0;
    dType ymax0;
    dType zmax0;

    dType sizeX;
    dType sizeY;
    dType sizeZ;
};

struct pos {
    dType x, y, z;
};

struct XYZstruct {
    dType x, y, z;
};

struct impos{
    dType x, y, z, alpha, betta, gamma;
    dType xx, xy, xz, yx, yy, yz, zx, zy, zz;
    impos() {
        alpha = 90.0;
        betta = 90.0;
        gamma = 90.0;
        xx = 1.0; xy = 0.0; xz = 0.0;
        yx = 0.0; yy = 1.0; yz = 0.0;
        zx = 0.0; zy = 0.0; zz = 1.0;
    }
};

struct XYZMatrix {
    dType xx, xy, xz, yx, yy, yz, zx, zy, zz;
    
    XYZMatrix() {
        xx = 1.0; xy = 0.0; xz = 0.0;
        yx = 0.0; yy = 1.0; yz = 0.0;
        zx = 0.0; zy = 0.0; zz = 1.0;
    }    

    void reset() {
        xx = 1.0; xy = 0.0; xz = 0.0;
        yx = 0.0; yy = 1.0; yz = 0.0;
        zx = 0.0; zy = 0.0; zz = 1.0;
    }
};

struct CellData {
    dType a, b, c, alpha, betta, gamma;
    XYZMatrix tr;
    XYZMatrix itr;
    dType cos_a, cos_b, cos_g, sin_a, sin_b, sin_g;
    CellData() {
        a = 1.0;
        b = 1.0;
        c = 1.0;
        alpha = 90.0;
        betta = 90.0;
        gamma = 90.0;
        cos_a = std::cos(PI*alpha / 180.0);
        cos_b = std::cos(PI*betta / 180.0);
        cos_g = std::cos(PI*gamma / 180.0);

        sin_a = std::sin(PI*alpha / 180.0);
        sin_b = std::sin(PI*betta / 180.0);
        sin_g = std::sin(PI*gamma / 180.0);

        tr.reset();
        itr.reset();
    }

    void setTMatrix() {
        cos_a = std::cos(PI*alpha / 180.0);
        cos_b = std::cos(PI*betta / 180.0);
        cos_g = std::cos(PI*gamma / 180.0);

        sin_a = std::sin(PI*alpha / 180.0);
        sin_b = std::sin(PI*betta / 180.0);
        sin_g = std::sin(PI*gamma / 180.0);

        dType omega = std::sqrt(1.0 - cos_a*cos_a - cos_b*cos_b - cos_g*cos_g + 2.0*cos_a*cos_b*cos_g);

        tr.xx = 1.0;    tr.xy = cos_g;  tr.xz = cos_b;
        tr.yx = 0.0;    tr.yy = sin_g;  tr.yz = (cos_a - cos_b*cos_g)/sin_g; 
        tr.zx = 0.0;    tr.zy = 0.0;    tr.zz = omega / sin_g;
    
        itr.xx = 1.0;   itr.xy = -cos_g/sin_g;  itr.xz = (cos_a*cos_g-cos_b)/(omega*sin_g); 
        itr.yx = 0.0;   itr.yy = 1.0/sin_g;     itr.yz = (cos_b*cos_g-cos_a)/(omega*sin_g);
        itr.zx = 0.0;   itr.zy = 0.0;           itr.zz = sin_g/omega;
    }

    void printMatrix() {
        std::cout << "------- Transformation MATRIX --------" << std::endl;
        std::cout << tr.xx << "  " << tr.xy << "  " << tr.xz << std::endl;
        std::cout << tr.yx << "  " << tr.yy << "  " << tr.yz << std::endl;
        std::cout << tr.zx << "  " << tr.zy << "  " << tr.zz << std::endl;

        std::cout << std::endl;
        std::cout << "------- ITransformation MATRIX --------" << std::endl;
        std::cout << itr.xx << "  " << itr.xy << "  " << itr.xz << std::endl;
        std::cout << itr.yx << "  " << itr.yy << "  " << itr.yz << std::endl;
        std::cout << itr.zx << "  " << itr.zy << "  " << itr.zz << std::endl;
    }

    dType tr_x(dType x, dType y, dType z) {
        return x * tr.xx + y * tr.xy + z * tr.xz;
    }
    dType tr_y(dType x, dType y, dType z) {
        return x * tr.yx + y * tr.yy + z * tr.yz;
    }
    dType tr_z(dType x, dType y, dType z) {
        return x * tr.zx + y * tr.zy + z * tr.zz;
    }

    dType itr_x(dType x, dType y, dType z) {
        return x * itr.xx + y * itr.xy + z * itr.xz;
    }
    dType itr_y(dType x, dType y, dType z) {
        return x * itr.yx + y * itr.yy + z * itr.yz;
    }
    dType itr_z(dType x, dType y, dType z) {
        return x * itr.zx + y * itr.zy + z * itr.zz;
    }
};



struct rdfstruct {
	dType RDFRmax;
    std::vector<dType> r;
    std::vector< std::vector<dType> > n;

    rdfstruct() {
    	RDFRmax = 8.0;
    	r.resize(1000);
    	//n.resize(1000);
    }
};

struct StateStruct {
    Atom At;
    std::vector<pos> ImPos;
    impos Size;
    CellData Cell;
    wall Wall;
};

struct Files {
    std::string DataFrameFile;
    std::string FileIn;
    std::string FileOut;
    std::string File;
    std::string XYZFile;
    std::string PotenFile;
    std::string PotenFileOut;
    std::string FitFile;
    std::string RefFile;
    std::string DETFile;
    std::string Path;
    std::string XYZInFile;
    std::string RDFFile;
    std::string TableFile;
    std::string StateFile;
    std::string ForceFile;
    std::string FluxFile;
    std::string InForceFile;
    std::string FitOutFile;
    std::string DistFile;
    std::string VTKOutFile;
    std::string NNinFile;
    std::string SampleFile;
    std::string NetTrainingFile;
    std::string NetResultsFile;
    
    void Set() {
        FileIn   = File;
        unsigned pos  = File.find_last_of(".");
        unsigned pos1 = File.find_last_of("/");

        Path = File.substr(0, pos1+1);
        File = File.substr(pos1+1, pos-pos1-1);

        FileOut		= Path + File + ".out";
        XYZFile		= Path + File + "_pos.xyz";
        ForceFile	= Path + File + "_force.xyz";
        FluxFile	= Path + File + ".flux";
        PotenFile	= Path + "Potential.data";
        FitFile     = Path + File + ".fit";
        RefFile     = Path + File + ".ref";
        XYZInFile	= Path + "IN.XYZ";
        RDFFile		= Path + File + ".rdf";
        TableFile	= Path + File + ".table";
        StateFile   = Path + File + ".state";
        FitOutFile	= Path + File + "_out.fit";
        DistFile	= Path + File + ".dist";
        DataFrameFile = Path + File + ".df";
        PotenFileOut= Path + "Potential.out";
        //VTKOutFile  = Path + "VTK/" + File + ".0.vtk";
        VTKOutFile  = Path + "VTK/" + File;
        SampleFile    = Path + File + ".sample";
        NetTrainingFile  = Path + File + ".sample";
        NetResultsFile  = Path + "NetResults.out";
    }
};

struct Properties {
	dType Temperature, Temperature_tau, Pressure, Pressure_tau;
	dType Diffusion, Diffusion_tau, SpecificHeat, SpecificHeat_tau;
	dType Density, Density_tau, Viscosity, Viscosity_tau, ThermCond, ThermCond_tau;
	dType Volume, Volume_tau, IntEnergy, IntEnergy_tau, Energy, Energy_tau;
	dType GibbsEnergy, GibbsEnergy_tau, Enthalpy, Enthalpy_tau;
    dType PotEnergy, PotEnergy_tau, KinEnergy, KinEnergy_tau;
    dType FreeEnergy_tau, FreeEnergy, Entropy_tau, Entropy, ICompress_tau, ICompress;
    dType MSD, MSD_tau;
    dType Summ_of_PotEnergy;
    dType CohesiveEnergy, CohesiveEnergy_tau;


    Properties() { Reset(); }

    void Reset() {
    	CohesiveEnergy_tau = 0.0;
    	CohesiveEnergy = 0.0;
        Temperature = 300.0;
        Temperature_tau = 300.0;
        Pressure = 0.0;
        Pressure_tau = 0.0;
        Diffusion = 0.0;
        Diffusion_tau = 0.0;
        SpecificHeat = 0.0;
        SpecificHeat_tau = 0.0;
        Density = 5000.0;
        Density_tau = 5000.0;
        Viscosity = 0.0;
        Viscosity_tau = 0.0;
        ThermCond = 0.0;
        ThermCond_tau = 0.0;
        Volume = 1.0;
        Volume_tau = 1.0;
        IntEnergy = 0.0;
        IntEnergy_tau = 0.0;
        Energy = 0.0;
        Energy_tau = 0.0;
        GibbsEnergy = 0.0;
        GibbsEnergy_tau = 0.0;
        Enthalpy = 0.0;
        Enthalpy_tau = 0.0;
        FreeEnergy_tau = 0.0;
        FreeEnergy = 0.0;
        Entropy_tau = 0.0;
        Entropy = 0.0;
        ICompress_tau = 0.0;
        ICompress = 0.0;
        PotEnergy = 0.0;
        PotEnergy_tau = 0.0;
        KinEnergy = 0.0;
        KinEnergy_tau  = 0.0;
        MSD_tau = 0.0;
        MSD = 0.0;

    }
};

struct Controls {
    bool pressure_b, temperature_b, position_b, gravity_b, polariz_b, morse_b, coulomb_b, buk_b;
    bool meam_b, calc_Fp, calc_Fs, state_b, thermc_b, det_b, initialization_b, fitexp_b, box_b, periodic_b;
    bool findpairs_b, mpi_b, velocity_b, diffusion_b, properties_b, rdf_b;
    bool inittemp_b, posupdate_b;

    bool savetext_b, forcefile_b, posfile_b, fluxfile_b, vtkfile_b, posanalysis_b;
    bool samplefile_b;

    bool premelt_b, melt_b, premeltR_b, premeltL_b, restoreState_each_b;

    Controls() { Reset(); }

    void Reset() {
    	restoreState_each_b = false;
    	premeltL_b	 = false;
    	premeltR_b	 = false;
    	melt_b		 = false;
    	premelt_b    = false;
    	posanalysis_b= false;
    	vtkfile_b	 = false;
    	samplefile_b = false;
    	fluxfile_b	 = false;
    	posfile_b    = true;
    	forcefile_b  = false;
    	savetext_b   = true;
    	posupdate_b  = true;
    	inittemp_b   = false;
    	properties_b = true;
        velocity_b = true;
    	pressure_b = true;
        temperature_b = true;
        position_b = false;
        gravity_b = false;
        polariz_b   = false;
        initialization_b = false;
        fitexp_b    = false;
        box_b       = false;
        periodic_b  = true;
        findpairs_b = true;
        mpi_b		= true;
        meam_b      = false;
        morse_b     = false;
        buk_b		= false;
        coulomb_b   = false;
        state_b     = false;
        gravity_b   = false;
        thermc_b    = true;
        calc_Fp 	= true;
        calc_Fs 	= true;
        det_b       = false;
        diffusion_b = false;
        rdf_b	    = false;
    }
};

struct Conditions {
	dType TimeStep, TimeCalc;
    dType Temperature, Tinitial, Treg1, Treg2, Tstep, Tfinal;
    dType Preg, Pstep, P_rel, P_rel_fin, T_rel, T_rel_fin;

    dType Rc_long, rRc_long, Rc_long_Bohr, Rc_long2;
    dType Rc_short, Rc_short2, Rc_meam, alpha;

    dType Rmelt, TimePremelt, StartThC, Av_Time, Pos_Av_Time, dq, lusc_q, luin_q;
    dType RDFSmoothStep;
    dType d_tau, rd_tau, d_taus, rd_taus;
    iType lusz_q, di, Cl, ClTot;
    dType rCl;

    Conditions() {Reset ();}

    void setCl(int val) {
    	Cl = val;
    	rCl  = 1.0 / Cl;
    	ClTot = Cl * Cl * Cl;
    }

    void Reset() {
    	Cl   = 8;
    	rCl  = 1.0 / Cl;
    	ClTot = Cl * Cl * Cl;

        Tinitial    = 300;
        Tfinal      = 1500;
        Temperature = 300;
        Treg2       = 2000;
        Treg1       = 100;
        Tstep       = 0;

        Preg = 0.0;
    	Pstep = 0.0;
    	P_rel = 100.0;
    	P_rel_fin = 500.0;
        T_rel     = 50;
        T_rel_fin = 200;

        TimeStep  = 5.0e3;
		TimeCalc = 1000.0e3;

    	dq 		= 0.1;
    	di      = 1;
        lusc_q  = 1.0;
        Rmelt   = 8.0;
        TimePremelt     = 2.0e3;
        Av_Time     = 15.0e3;
        Pos_Av_Time = 2.0e3;
        StartThC    = 75.0e3;

        d_tau       = 2.0;
        rd_tau      = 1.0 / d_tau;
        d_taus      = 2.0;
        rd_taus     = 1.0 / d_taus;
        RDFSmoothStep = 25.0;

    	alpha 	= 0.12;

        Rc_long     = 16.0;
        Rc_long2    = Rc_long * Rc_long;
        rRc_long    = 1.0 / Rc_long;
        Rc_long_Bohr = 1.889725989 * Rc_long;

        Rc_meam   = 7.0;

        Rc_short  = 10.0;
        Rc_short2 = Rc_short * Rc_short;

    }
};

struct Fit {
	std::vector<dType> Density, Energy, Pressure, Diffusion, Temperature, Time, CohesiveE;
    std::vector<bool> Struct_b;
    std::vector<bool> Premelt_b;
    std::vector<dType> AtTemperature;
    std::map<std::string, std::vector<dType> > FitMap;
    std::vector<std::string> ForElement;
    std::vector<std::string> WallFor;
    bool FitRDF;

    dType HalfLife, DecayConstant, sigma, sigmacur, epsilon, rate, error, error_n, error_sum, error_prev;
    dType MutProb;
    iType iterations, population;

    std::map<std::string, bool> FitParam;

    Fit(){ Reset(); }

	void Reset() {
	    FitParam["DM"] = false;
	    FitParam["alphaM"] = false;
	    FitParam["rr"] = false;
	    FitParam["A"] = false;
	    FitParam["rho0"] = false;
	    FitParam["re"] = false;
	    FitParam["beta0"] = false;
	    FitParam["beta1"] = false;
	    FitParam["beta2"] = false;
	    FitParam["beta3"] = false;
	    FitParam["t1"] = false;
	    FitParam["t2"] = false;
	    FitParam["t3"] = false;
	    FitParam["rc"] = false;

		MutProb		= 0.25;
		iterations	= 10000;
	    HalfLife    = 500;
	    DecayConstant = 0.693147181 / HalfLife;
	    sigma       = 0.1;
	    sigmacur    = 0.1;
	    epsilon		=
	    population  = 100;

	    epsilon   = 1.0e-3;
	    rate      = 0.25;
	    error     = 0.0;
	    error_n    = 1.0;
	    error_sum  = 0.0;
	    error_prev = 0.0;

		FitRDF      = false;

		FitMap.clear();
		Struct_b.clear();
		Premelt_b.clear();
		AtTemperature.clear();
		Density.clear();
		Energy.clear();
		Pressure.clear();
		Diffusion.clear();
		Temperature.clear();
		Time.clear();
		CohesiveE.clear();
		ForElement.clear();
	}
};

struct Tables {
    LUTstruct Tab_Energy, Tab_Force, Tab_rho0_val;
    LUTstruct Tab_rho0_dx, Tab_rho1_val, Tab_rho1_dx, Tab_rho2_val,  Tab_rho2_dx, Tab_rho3_val, Tab_rho3_dx;
    LUTstruct Tab_rho_dx, Tab_rho_val;
    LUTstruct Tab_rho0N_val, Tab_rho0N_dx;
    LUTable FcutD;
};

struct Information {
	dType PosX, PosY, PosZ, VelX, VelY, VelZ, MassTot, Time;
	dType WallX, Walls;

    iType Threads, IterationsMain;
	iType num_procs, id, dID, dNP, SubsN;
    iType step, Count, iVTK;

    wall Wall;
    CellData Cell;

    std::vector<pos> ImPos;
    std::vector<pos> ImPos_init;

	Information(){ Reset(); }

	void Reset() {
	    iVTK    = 0;
	    Count   = 0;
	    step    = 0;

		ImPos.resize(27);
		ImPos_init.resize(27);
		Time  = 0.0;
	}
};

struct Maps {
public:
    std::map<char, int> char_to_int;

    Maps() {
        // convert string to int
        char_to_int['!'] = 33;
        char_to_int['"'] = 34;
        char_to_int['#'] = 35;
        char_to_int['$'] = 36;
        char_to_int['%'] = 37;
        char_to_int['&'] = 38;
        char_to_int['('] = 40;
        char_to_int[')'] = 41;
        char_to_int['*'] = 42;
        char_to_int['+'] = 43;
        char_to_int[','] = 44;
        char_to_int['-'] = 45;
        char_to_int['.'] = 46;
        char_to_int['/'] = 47;
        char_to_int['0'] = 48;
        char_to_int['1'] = 49;
        char_to_int['2'] = 50;
        char_to_int['3'] = 51;
        char_to_int['4'] = 52;
        char_to_int['5'] = 53;
        char_to_int['6'] = 54;
        char_to_int['7'] = 55;
        char_to_int['8'] = 56;
        char_to_int['9'] = 57;
        char_to_int[':'] = 58;
        char_to_int[';'] = 59;
        char_to_int['<'] = 60;
        char_to_int['='] = 61;
        char_to_int['>'] = 62;
        char_to_int['?'] = 63;
        char_to_int['@'] = 64;
        char_to_int['A'] = 65;
        char_to_int['B'] = 66;
        char_to_int['C'] = 67;
        char_to_int['D'] = 68;
        char_to_int['E'] = 69;
        char_to_int['F'] = 70;
        char_to_int['G'] = 71;
        char_to_int['H'] = 72;
        char_to_int['I'] = 73;
        char_to_int['J'] = 74;
        char_to_int['K'] = 75;
        char_to_int['L'] = 76;
        char_to_int['M'] = 77;
        char_to_int['N'] = 78;
        char_to_int['O'] = 79;
        char_to_int['P'] = 80;
        char_to_int['Q'] = 81;
        char_to_int['R'] = 82;
        char_to_int['S'] = 83;
        char_to_int['T'] = 84;
        char_to_int['U'] = 85;
        char_to_int['V'] = 86;
        char_to_int['W'] = 87;
        char_to_int['X'] = 88;
        char_to_int['Y'] = 89;
        char_to_int['Z'] = 90;
        char_to_int['['] = 91;
        char_to_int[']'] = 93;
        char_to_int['_'] = 95;
        char_to_int['`'] = 96;
        char_to_int['a'] = 97;
        char_to_int['b'] = 98;
        char_to_int['c'] = 99;
        char_to_int['d'] = 100;
        char_to_int['e'] = 101;
        char_to_int['f'] = 102;
        char_to_int['g'] = 103;
        char_to_int['h'] = 104;
        char_to_int['i'] = 105;
        char_to_int['j'] = 106;
        char_to_int['k'] = 107;
        char_to_int['l'] = 108;
        char_to_int['m'] = 109;
        char_to_int['n'] = 110;
        char_to_int['o'] = 111;
        char_to_int['p'] = 112;
        char_to_int['q'] = 113;
        char_to_int['r'] = 114;
        char_to_int['s'] = 115;
        char_to_int['t'] = 116;
        char_to_int['u'] = 117;
        char_to_int['v'] = 118;
        char_to_int['w'] = 119;
        char_to_int['x'] = 120;
        char_to_int['y'] = 121;
        char_to_int['z'] = 122;
        char_to_int['{'] = 123;
        char_to_int['|'] = 124;
        char_to_int['}'] = 125;
    }
};





typedef std::vector<dType> dVector;
typedef std::map<std::string, Params>::iterator ParamsIter;
typedef std::map<std::string, Params>::const_iterator ParamsIterConst;
typedef std::vector<Params>::size_type ParamsSize;


#endif /* DATATYPES_H */

