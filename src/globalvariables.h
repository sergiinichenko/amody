//-------------------------------------------------------------------
// File:   globalvariables.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#ifndef SRC_GLOBALVARIABLES_H_
#define SRC_GLOBALVARIABLES_H_

#include <mpi.h>

#ifdef DEF_TYPE_FLOAT
    typedef float dType;
#else
    typedef double dType;
#endif


#ifdef DEF_TYPE_INT
    typedef unsigned long int iType;
#else
    typedef int iType;
#endif


#ifdef DEF_TYPE_FLOAT
    const MPI_Datatype mpiType = MPI_FLOAT;
#else
    const MPI_Datatype mpiType = MPI_DOUBLE;
#endif


struct pair {
	iType im, cl;
	dType r;
};



#endif /* SRC_GLOBALVARIABLES_H_ */
