//-------------------------------------------------------------------
// File:   main.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include <iostream>
#include <stdio.h>  /* defines FILENAME_MAX */
#include "simulation.h"
#include "datamanager.h"
#include "mpi.h"

//#include <thread>
//#include "substance.cpp"
#undef main

#ifdef _WIN32
    #include <direct.h>
    #define GetCurrentDir _getcwd;
    #include <windows.h>

    void SetWindow(int Width, int Height)
        {
            _COORD coord;
            coord.X = Width;
            coord.Y = Height;

            _SMALL_RECT Rect;
            Rect.Top = 0;
            Rect.Left = 0;
            Rect.Bottom = Height - 1;
            Rect.Right = Width - 1;

            HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle
            SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size
            SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size
        }
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd;
 #endif


int main(int argc, char** argv)
{
    std::string text;
    int required=MPI_THREAD_FUNNELED;
    int provided;

    #ifdef _WIN32
        SetWindow(145,40);
    #endif

    /*** Select one of the following
        MPI_Init_thread( 0, 0, MPI_THREAD_SINGLE, &provided );
        MPI_Init_thread( 0, 0, MPI_THREAD_FUNNELED, &provided );
        MPI_Init_thread( 0, 0, MPI_THREAD_SERIALIZED, &provided );
        MPI_Init_thread( 0, 0, MPI_THREAD_MULTIPLE, &provided );
    ***/

    //  Create the simulatin entity
    Simulation Sim;

    //  Initialize MPI.
    MPI_Init_thread( &argc, &argv, required, &provided );

    //  Get the number of processes.
    MPI_Comm_size(MPI_COMM_WORLD,&Sim.Sys.inf.num_procs);
    //  Get the current process id.
    //Sim.Sys.id = MPI::COMM_WORLD.Get_rank ( );
    MPI_Comm_rank(MPI_COMM_WORLD, &Sim.Sys.inf.id);
    
    //  Start reading the input
    Sim.ReadTheImput(argc, argv, Sim.Sys);

    //  Terminate MPI.
    //MPI::Finalize ( );
    MPI_Finalize();

    return 0;
}
