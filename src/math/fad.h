//-------------------------------------------------------------------
// File:   fad.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#ifndef FAD_H
#define FAD_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <cmath>
#include "vector.h"

#ifdef DEF_TYPE_FLOAT
    typedef float dType;
#else
    typedef double dType;
#endif


template <class T> class Fad {
    public:
        T _val;
        T _dx;
        Fad(const T& val) {
            _val = val;
        };

        Fad() {
            _val = 0.0;
            _dx  = 1.0;
        };

        Fad & operator+=(const Fad &w);
        Fad & operator-=(const Fad &w);
        Fad<T>& operator*=(const Fad<T>& w)
        {
            _val *= w._val;
            _dx  *= w._val;
            _dx  += _val * w._dx;
            return *this;
        };
        //Fad<T> operator*(const Fad<T>& x, const Fad<T>& y);
        Fad & operator/=(const Fad &w);
};

template <class T> class FadV {
    public:
        std::vector<T> _val;
        std::vector<T> _dx;
        std::vector<T> ival{1,0.0};
        std::vector<T> idx{1,1.0};
        FadV(const std::vector<T>& val) {
            _val = val;
        };

        FadV() {
            _val = ival;
            _dx  = idx;
        };

        FadV(const int& i) {
            _val.resize(i);
            _dx.resize(i);
        };

};


// -----------------------      Multiplication operators      ------------------------
template <class T> Fad<T> operator*(const Fad<T>& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x._val * y._val;
    res._dx  = x._val * y._dx + x._dx * y._val;
    return res;
};

template <class T> Fad<T> operator*(const Fad<T>& x, const T& y) {
    Fad<T> res;
    res._val = x._val * y;
    res._dx  = y * x._dx;
    return res;
};

template <class T> Fad<T> operator*(const T& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x * y._val;
    res._dx  = x * y._dx;
    return res;
};

// -----------------------      Multiplication operators      ------------------------
template <class T> FadV<T> operator*(const FadV<T>& x, const FadV<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val * y._val;
    res._dx  = x._val * y._dx + x._dx * y._val;
    return res;
};

template <class T> FadV<T> operator*(const FadV<T>& x, const T& y) {
    FadV<T> res(x._val.size());
    res._val = x._val * y;
    res._dx  = y * x._dx;
    return res;
};

template <class T> FadV<T> operator*(const T& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x * y._val;
    res._dx  = x * y._dx;
    return res;
};

template <class T> FadV<T> operator*(const FadV<T>& x, const std::vector<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val * y;
    res._dx  = y * x._dx;
    return res;
};

template <class T> FadV<T> operator*(const std::vector<T>& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x * y._val;
    res._dx  = x * y._dx;
    return res;
};


// -----------------------      Addition operators      ------------------------
template <class T> Fad<T> operator+(const Fad<T>& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x._val + y._val;
    res._dx  = x._dx  + y._dx;
    return res;
};

template <class T> Fad<T> operator+(const Fad<T>& x, const T& y) {
    Fad<T> res;
    res._val = x._val + y;
    res._dx  = x._dx;
    return res;
};

template <class T> Fad<T> operator+(const T& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x + y._val;
    res._dx  = y._dx;
    return res;
};



// -----------------------      Addition operators      ------------------------
template <class T> FadV<T> operator+(const FadV<T>& x, const FadV<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val + y._val;
    res._dx  = x._dx  + y._dx;
    return res;
};

template <class T> FadV<T> operator+(const FadV<T>& x, const T& y) {
    FadV<T> res(x._val.size());
    res._val = x._val + y;
    res._dx  = x._dx;
    return res;
};

template <class T> FadV<T> operator+(const T& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x + y._val;
    res._dx  = y._dx;
    return res;
};

template <class T> FadV<T> operator+(const FadV<T>& x, const std::vector<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val + y;
    res._dx  = x._dx;
    return res;
};

template <class T> FadV<T> operator+(const std::vector<T>& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x + y._val;
    res._dx  = y._dx;
    return res;
};



// -----------------------      Substraction operators      ------------------------
template <class T> Fad<T> operator-(const Fad<T>& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x._val - y._val;
    res._dx  = x._dx  - y._dx;
    return res;
};

template <class T> Fad<T> operator-(const Fad<T>& x, const T& y) {
    Fad<T> res;
    res._val = x._val - y;
    res._dx  = x._dx;
    return res;
};

template <class T> Fad<T> operator-(const T& y, const Fad<T>& x) {
    Fad<T> res;
    res._val = y - x._val;
    res._dx  = - x._dx;
    return res;
};


// -----------------------      Substraction operators      ------------------------
template <class T> FadV<T> operator-(const FadV<T>& x, const FadV<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val - y._val;
    res._dx  = x._dx  - y._dx;
    return res;
};

template <class T> FadV<T> operator-(const FadV<T>& x, const T& y) {
    FadV<T> res(x._val.size());
    res._val = x._val - y;
    res._dx  = x._dx;
    return res;
};

template <class T> FadV<T> operator-(const T& y, const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = y - x._val;
    res._dx  = - x._dx;
    return res;
};

template <class T> FadV<T> operator-(const FadV<T>& x, const std::vector<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val - y;
    res._dx  = x._dx;
    return res;
};

template <class T> FadV<T> operator-(const std::vector<T>& y, const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = y - x._val;
    res._dx  = - x._dx;
    return res;
};


// -----------------------      Division operators      ------------------------
template <class T> Fad<T> operator/(const Fad<T>& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x._val / y._val;
    res._dx  = x._dx / y._val - x._val * y._dx / (y._val * y._val);
    return res;
};

template <class T> Fad<T> operator/(const Fad<T>& x, const T& y) {
    Fad<T> res;
    res._val = x._val / y;
    res._dx  = x._dx / y;
    return res;
};

template <class T> Fad<T> operator/(const T& x, const Fad<T>& y) {
    Fad<T> res;
    res._val = x / y._val;
    res._dx  = - x * y._dx / (y._val * y._val);
    return res;
};



// Vector Devide
template <class T> FadV<T> operator/(const FadV<T>& x, const FadV<T>& y) {
    FadV<T> res(x._val.size());
    //std::vector<T> rval = 1.0 / y._val;
    res._val = x._val / y._val;
    res._dx  = x._dx / y._val - x._val * y._dx / (y._val * y._val);
    return res;
};

template <class T> FadV<T> operator/(const FadV<T>& x, const T& y) {
    FadV<T> res(x._val.size());
    res._val = x._val / y;
    res._dx  = x._dx / y;
    return res;
};

template <class T> FadV<T> operator/(const T& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x / y._val;
    res._dx  = - x * y._dx / (y._val * y._val);
    return res;
};

template <class T> FadV<T> operator/(const FadV<T>& x, const std::vector<T>& y) {
    FadV<T> res(x._val.size());
    res._val = x._val / y;
    res._dx  = x._dx / y;
    return res;
};

template <class T> FadV<T> operator/(const std::vector<T>& x, const FadV<T>& y) {
    FadV<T> res(y._val.size());
    res._val = x / y._val;
    res._dx  = - x * y._dx / (y._val * y._val);
    return res;
};


template <class T> Fad<T> exp(const Fad<T>& x) {
    Fad<T> res;
    res._val = exp(x._val);
    res._dx  = x._dx * exp(x._val);
    return res;
};

template <class T> FadV<T> exp(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = exp(x._val);
    res._dx  = x._dx * exp(x._val);
    return res;
};


template <class T> Fad<T> log(const Fad<T>& x) {
    Fad<T> res;
    res._val = log(x._val);
    res._dx  = x._dx / x._val;
    return res;
};

template <class T> Fad<T> sqrt(const Fad<T>& x) {
    Fad<T> res;
    res._val = sqrt(x._val);
    res._dx  = x._dx / ( 2.0 * sqrt(x._val));
    return res;
};

template <class T> FadV<T> log(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = log(x._val);
    res._dx  = x._dx / x._val;
    return res;
};


template <class T> FadV<T> sqrt(const FadV<T>& x) {
    FadV<T> res;
    res._val = sqrt(x._val);
    res._dx  = x._dx / ( 2.0 * sqrt(x._val));
    return res;
};


template <class T> Fad<T> pow(const Fad<T>& x, const T& p) {
    Fad<T> res;
    res._val = pow(x._val, p);
    res._dx  = p * pow(x._val, (p-1.0)) * x._dx;
    return res;
};

template <class T> Fad<T> pow2(const Fad<T>& x) {
    Fad<T> res;
    res._val = x._val * x._val;
    res._dx  = 2 * x._val * x._dx;
    return res;
};

template <class T> Fad<T> pow4(const Fad<T>& x) {
    Fad<T> res;
    res._val = x._val * x._val * x._val * x._val;
    res._dx  = 3 * x._val * x._val * x._val;
    return res;
};

template <class T> Fad<T> pow22(const Fad<T>& x) {
    Fad<T> res;
    res._val = x._val * x._val;
    res._dx  = 2 * x._val * x._dx;
    return res;
};


template <class T> FadV<T> pow(const FadV<T>& x, const T& p) {
    FadV<T> res(x._val.size());
    res._val = pow(x._val, p);
    res._dx  = p * pow(x._val, (p-1.0));

    return res;
};

template <class T> FadV<T> pow2(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = x._val * x._val;
    res._dx  = 2.0 * x._val * x._dx;
    return res;
};

template <class T> FadV<T> pow4(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = x._val * x._val * x._val * x._val;
    res._dx  = 4.0 * x._val * x._val * x._val;
    return res;
};


template <class T> Fad<T> erf(const Fad<T>& x) {
    Fad<T> res;
    res._val = funcs::erf(x._val);
    res._dx  = 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};

template <class T> Fad<T> erfc(const Fad<T>& x) {
    Fad<T> res;
    res._val = 1.0 - erf(x._val);
    res._dx  = - 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};

template <class T> FadV<T> erf(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = erf(x._val);
    res._dx  = 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};

template <class T> FadV<T> erfc(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val = 1.0 - erf(x._val);
    res._dx  = - 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};


// Multiply
template <class T> std::vector<Fad<T> > operator*(const std::vector<Fad<T> >& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),        x.end(),        y.begin() ,  res.begin(),     std::multiplies<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const std::vector<Fad<T> >& x, const T& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),       x.end(),       res.begin(),     std::bind2nd(std::multiplies<Fad<T> >(), y));
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const T& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    std::transform(y.begin(),       y.end(),       res.begin(),     std::bind2nd(std::multiplies<Fad<T> >(), x));
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const std::vector<dType>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    for (int i = 0; i < y.size(); i++) {
        res[i] = x[i] * y[i];
    }
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const std::vector<Fad<T> >& x, const std::vector<dType>& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),        x.end(),        y.begin() ,  res.begin(),     std::multiplies<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const Fad<T>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    for (int i = 0; i < y.size(); i++) {
        res[i] = x * y[i];
    }
    return res;
}

template <class T> std::vector<Fad<T> > operator*(const std::vector<Fad<T> >& x, const Fad<T>& y) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = x[i] * y;
    }
    return res;
}



// Divide
template <class T> std::vector<Fad<T> > operator/(const std::vector<Fad<T> >& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),        x.end(),        y.begin() ,  res.begin(),     std::divides<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator/(const std::vector<Fad<T> >& x, const T& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),       x.end(),       res.begin(),     std::bind2nd(std::divides<Fad<T> >(), y));
    return res;
}

template <class T> std::vector<Fad<T> > operator/(const T& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    std::vector<Fad<T> > xx(y.size(), x);
    std::transform(xx.begin(),        xx.end(),        y.begin() ,  res.begin(),     std::divides<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator/(const std::vector<dType>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),        x.end(),        y.begin() ,  res.begin(),     std::divides<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator/(const std::vector<Fad<T> >& x, const std::vector<dType>& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),        x.end(),        y.begin() ,  res.begin(),     std::divides<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator/(const Fad<T>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    for (int i = 0; i < y.size(); i++) {
        res[i] = x / y[i];
    }
    return res;
}



// Plus
template <class T> std::vector<Fad<T> > operator+(const std::vector<Fad<T> >& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::plus<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator+(const std::vector<Fad<T> >& x, const T& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         res.begin(),    std::bind2nd(std::plus<Fad<T> >(), y));
    return res;
}

template <class T> std::vector<Fad<T> > operator+(const T& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    std::transform(y.begin(),         y.end(),         res.begin(),    std::bind2nd(std::plus<Fad<T> >(), x));
    return res;
}

template <class T> std::vector<Fad<T> > operator+(const std::vector<dType>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::plus<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator+(const std::vector<Fad<T> >& x, const std::vector<dType>& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::plus<Fad<T> >());
    return res;
}




// Minus
template <class T> std::vector<Fad<T> > operator-(const std::vector<Fad<T> >& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::minus<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator-(const std::vector<Fad<T> >& x, const T& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         res.begin(),    std::bind2nd(std::minus<Fad<T> >(), y));
    return res;
}

template <class T> std::vector<Fad<T> > operator-(const T& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(y.size());
    std::transform(y.begin(),       y.end(),         res.begin(),    std::bind2nd(std::minus<Fad<T> >(), x));
    std::transform(res.begin(),     res.end(),       res.begin(),    std::bind2nd(std::multiplies<Fad<T> >(), -1.0));
    return res;
}

template <class T> std::vector<Fad<T> > operator-(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),       x.end(),       res.begin(),     std::bind2nd(std::multiplies<Fad<T> >(), -1.0));
    return res;
}

template <class T> std::vector<Fad<T> > operator-(const std::vector<dType>& x, const std::vector<Fad<T> >& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::minus<Fad<T> >());
    return res;
}

template <class T> std::vector<Fad<T> > operator-(const std::vector<Fad<T> >& x, const std::vector<dType>& y) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),         x.end(),         y.begin(),     res.begin(),    std::minus<Fad<T> >());
    return res;
}


template <class T> FadV<T> operator-(const FadV<T>& x) {
    FadV<T> res(x._val.size());
    res._val.resize(x._val.size());
    res._dx.resize(x._val.size());
    std::transform(x._val.begin(),       x._val.end(),       res._val.begin(),     std::bind2nd(std::multiplies<T>(), -1.0));
    std::transform(x._dx.begin(),        x._dx.end(),        res._dx.begin(),      std::bind2nd(std::multiplies<T>(), -1.0));
    return res;
}



template <class T> std::vector<Fad<T> > exp(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = exp(x[i]);
    }
    return res;
}

template <class T> std::vector<Fad<T> > log(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = log(x[i]);
    }
    return res;
}


template <class T> std::vector<Fad<T> > abs(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    std::transform(x.begin(),       x.end(),       res.begin(),     static_cast<Fad<T> (*)(Fad<T>)>(std::fabs));
    return res;
}

template <class T> std::vector<Fad<T> > sqrt(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = sqrt(x[i]);
    }
    return res;
}

template <class T> std::vector<Fad<T> > pow(const std::vector<Fad<T> >& x, const T& p) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = pow(x[i], p);
    }
    return res;
}

template <class T> std::vector<Fad<T> > pow2(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    //res = x;
    //std::transform(x.begin(),       x.end(),       res.begin(),  pow22);
    //for_each(res.begin(), res.end(), pow22);
    for (int i = 0; i < x.size(); i++) {
        res[i] = pow2(x[i]);
    }
    return res;
}


template <class T> std::vector<Fad<T> > assignVal(std::vector<T>& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i]._val = x[i];
        res[i]._dx  = 1.0;
    }
    return res;
}

template <class T> FadV<T> assignValV(std::vector<T>& x) {
    FadV<T> res(x.size());
    res._val = x;
    res._dx.resize(x.size());
    std::fill(res._dx.begin(), res._dx.end(), 1);
    return res;
}

template <class T> std::vector<Fad<T> > assignDx(std::vector<T>& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i]._dx = x[i];
    }
    return res;
}


template <class T> std::vector<T> returnVal(std::vector<Fad<T> >& x) {
    std::vector<T> res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = x[i]._val;
    }
    return res;
}

template <class T> std::vector<T> returnDx(std::vector<Fad<T> >& x) {
    std::vector<T> res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = x[i]._dx;
    }
    return res;
}


template <class T> T sum(const std::vector<Fad<T> >& x, const T& val) {
    return std::accumulate(x.begin(), x.end(), val);;
}

template <class T> T sum(const std::vector<Fad<T> >& X, const std::vector<int>& ind) {
    double res = 0;
    int j, i;
    //#pragma omp parallel for private(i, j) reduction(+:res)
    for (i = 0; i < ind.size(); i++){
        j = ind[i];
        res += X[j];
    }
    return res;
}

template <class T> Fad<T> sum(const std::vector<Fad<T> >& X) {
    Fad<T> res;
    T val = 0.0;
    T dx  = 0.0;
    int j, i;
    //#pragma omp parallel for private(i, j) reduction(+:val, dx)
    for (i = 0; i < X.size(); i++){
        val += X[i]._val;
        dx  += X[i]._dx;
    }
    res._val = val;
    res._dx  = dx;
    return res;
}

template <class T> std::vector<Fad<T> > sumV(const std::vector<Fad<T> >& X) {
    std::vector<Fad<T> > res(X.size());
    T val = 0.0;
    int j, i;
    //#pragma omp parallel for private(i, j) reduction(+:val)
    for (i = 0; i < X.size(); i++){
        val += X[i]._val;
    }

    //#pragma omp parallel for private(i, j)
    for (i = 0; i < X.size(); i++){
        res[i]._val = val;
        res[i]._dx  = X[i]._dx;
    }
    return res;
}

template <class T> FadV<T> sumV(const FadV<T>& X) {
    FadV<T> res(X._val.size());
    res = X;
    T val = 0.0;

    val = std::accumulate(X._val.begin(), X._val.end(), 0.0);
    std::fill(res._val.begin(), res._val.end(), val);
    res._dx = X._dx;
    return res;
}

/*
template <class T> Fad<T> sumV(const FadV<T>& X) {
    Fad<T> res;
    res = 0;

    res._val = std::accumulate(X._val.begin(), X._val.end(), 0.0);
    res._dx = X._dx;
    return res;
}
*/

template <class T> FadV<T> pow2sumV(const FadV<T>& X) {
    FadV<T> res(X._val.size());
    res = X;
    T val = 0.0;

    val = std::accumulate(X._val.begin(), X._val.end(), 0.0);
    std::fill(res._val.begin(), res._val.end(), val*val);

    res._dx  = 2.0 * X._val * X._dx;

    return res;
}

template <class T> std::vector<T> pow2sumVdx(const FadV<T>& X) {
    return 2.0 * X._val * X._dx;
}


template <class T> std::vector<Fad<T> > erfc(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = erfc(x[i]);
    }
    return res;
}

template <class T> std::vector<Fad<T> > erf(const std::vector<Fad<T> >& x) {
    std::vector<Fad<T> > res(x.size());
    for (int i = 0; i < x.size(); i++) {
        res[i] = erf(x[i]);
    }
    //std::transform(x.begin(),       x.end(),       res.begin(),     static_cast<Fad<T> (*)(Fad<T>)>(erf));
    return res;
}

#endif
