//-------------------------------------------------------------------
// File:   fad.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "funcs.h"

struct timeval start_t() {
    struct timeval t;
    #ifdef _WIN32
        QueryPerformanceCounter(&t);
    #else
        //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
        gettimeofday(&t, NULL);
    #endif
    return t;
}

void stop_t( struct timeval& t) {
    #ifdef _WIN32
        QueryPerformanceCounter(&t);
    #else
        //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
        gettimeofday(&t, NULL);
    #endif
}

double elapsed_t( struct timeval& t1) {
    struct timeval t2;
    double time;

    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
    gettimeofday(&t2, NULL);

    time = (double)(t2.tv_sec - t1.tv_sec) * 1.0e6;      // sec to ms
    time += (double)(t2.tv_usec - t1.tv_usec);   // us to ms

    gettimeofday(&t1, NULL);

    return time;
}

void printMatrix(vector<dType>& X, std::string str) {
    std::stringstream stream;

    std::cout << str << std::endl;
    for ( int i = 0; i < X.size(); ++i ) {
        stream.str("");
        stream.clear();
        stream.width(14);
        stream.precision(12);

        stream  << X[i] << "   ";
	    std::cout << stream.str() <<std::endl;
	}
	std::cout <<std::endl;
}

void printMatrix(vector<vector<dType> >& X, std::string str) {
    std::stringstream stream;

    std::cout << str << std::endl;
    for (int  i = 0; i < X.size(); ++i ) {
        stream.str("");
        stream.clear();
        for (int j = 0; j < X[i].size(); ++j ) {
            stream.width(14);
            stream.precision(12);
            stream  << X[i][j] << "   ";
        }
        std::cout << stream.str() <<std::endl;
    }
	std::cout <<std::endl;
}

void Spline::LUDeco(vector<vector<dType> >& X, vector<vector<dType> >& L, vector<vector<dType> >& U)  {
    int i = 0, j = 0, k = 0;

    size = X.size();
    L = X;
    U = X;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++)         {
            L[i][j] = 0.0;
            U[i][j] = 0.0;
        }
    }
    for (i = 0; i < size; i++)     {
        for (j = 0; j < size; j++)         {
            if (j < i)
                L[j][i] = 0;
            else   {
                L[j][i] = X[j][i];
                for (k = 0; k < i; k++)  {
                    L[j][i] = L[j][i] - L[j][k] * U[k][i];
                }
            }
        }
        for (j = 0; j < size; j++) {
            if (j < i)
                U[i][j] = 0;
            else if (j == i)
                U[i][j] = 1;
            else  {
                U[i][j] = X[i][j] / L[i][i];
                for (k = 0; k < i; k++)  {
                    U[i][j] = U[i][j] - ((L[i][k] * U[k][j]) / L[i][i]);
                }
            }
        }
    }
}

void Spline::defineSpline(vector<dType>& X, vector<dType>& Y) {
    int n, i, j;
    struct timeval t1, t2;
    deg = 11;
    dType p, val;
    dType D;
    vector< vector<dType> > A;
    vector< dType> B;
    vector< dType> temp;
    vector< vector<vector<dType> > > Avec;
    vector< dType> Dvec;
    vector< vector<dType> > powX;
    vector< vector<dType> > L;
    vector< vector<dType> > U;

    size = X.size();
    B.resize(deg);
    powX.resize(size);
    A.resize(deg);
    Avec.resize(deg);
    Dvec.resize(deg);
    coeffs.resize(deg);
    powVal.resize(size);

    t1 = start_t();

    for (int s = 0; s < size; ++s ) {
        powX[s].resize(deg);

        powX[s][0] = 1.0;
        powX[s][1] = X[s];
        powX[s][2] = 1.0/X[s];
        powX[s][3] = powX[s][1] * powX[s][1];
        powX[s][4] = powX[s][2] * powX[s][2];
        powX[s][5] = powX[s][3] * powX[s][3];
        powX[s][6] = powX[s][4] * powX[s][4];
        powX[s][7] = powX[s][5] * powX[s][5];
        powX[s][8] = powX[s][6] * powX[s][6];
        powX[s][9] = powX[s][7] * powX[s][7];
        powX[s][10] = powX[s][8] * powX[s][8];
    }
    /*
    for ( i = 0; i < deg; ++i ) {
        std::cout << powX[0][i] << std::endl;
    }
    */

    for ( i = 0; i < deg; ++i ) {
        A[i].resize(deg);
        Avec[i].resize(deg);
        for ( j = 0; j < deg; ++j ) {
            Avec[i][j].resize(deg);
        }
    }

    for (i = 0; i < deg; ++i) {

        val = 0.0;
        for (int s = 0; s < size; ++s ) {
            val += Y[s] * powX[s][i];
        }
        B[i] = val;

        for (j = 0; j < deg; ++j) {
            val = 0.0;
            for (int s = 0; s < size; ++s ) {
                val += powX[s][i] * powX[s][j];
            }
            A[i][j] = val;
        }
    }

    for ( n = 0; n < deg; ++n ) {
        Avec[n] = A;
        for (j = 0; j < deg; ++j) {
            Avec[n][j][n] = B[j];
        }
    }

    D  = detmnt(A);
    /*
    std::cout << "D  : " << D << std::endl;
    printMatrix(A, "A");
    printMatrix(B, "B");
    */

    for (i = 0; i < deg; ++i) {
        Dvec[i] = detmnt(Avec[i]);
        //std::cout << "Dvec[" << i << "]  : " << Dvec[i] << std::endl;
        //printMatrix(Avec[i], "Avec");
    }

    coeffs = Dvec / D;

    vector<dType> Diff;
    vector<dType> Calc;

    Calc  = getValue(X);
    Diff = (Calc - Y);
    std::cout << "Error " << sum(pow(Diff, 2.0)) << std::endl;
}


dType Spline::getValue(dType X) {
    dType res;
    std::vector<dType> powX;
    powX.resize(deg);

    powX[0] = 1.0;
    powX[1] = X;
    powX[2] = 1.0/X;
    powX[3] = powX[1] * powX[1];
    powX[4] = powX[2] * powX[2];
    powX[5] = powX[3] * powX[3];
    powX[6] = powX[4] * powX[4];
    powX[7] = powX[5] * powX[5];
    powX[8] = powX[6] * powX[6];
    powX[9] = powX[7] * powX[7];
    powX[10] = powX[8] * powX[8];

    return sum (powX * coeffs);
}

std::vector<dType> Spline::getValue(std::vector<dType>& X) {
    dType p;
    std::vector<std::vector<dType> > powX;
    std::vector<dType> res;
    powX.resize(X.size());
    res.resize(X.size());

    for (int s = 0; s < X.size(); ++s ) {
        powX[s].resize(deg);

        powX[s][0] = 1.0;
        powX[s][1] = X[s];
        powX[s][2] = 1.0/X[s];
        powX[s][3] = powX[s][1] * powX[s][1];
        powX[s][4] = powX[s][2] * powX[s][2];
        powX[s][5] = powX[s][3] * powX[s][3];
        powX[s][6] = powX[s][4] * powX[s][4];
        powX[s][7] = powX[s][5] * powX[s][5];
        powX[s][8] = powX[s][6] * powX[s][6];
        powX[s][9] = powX[s][7] * powX[s][7];
        powX[s][10] = powX[s][8] * powX[s][8];
    }

    for (int s = 0; s < X.size(); ++s ) {
        res[s] =  sum (powX[s] * coeffs);
    }
    return res;
}

dType Spline::getValueV(std::vector<dType>& X) {
    return sum (X * coeffs);
}

std::vector<dType> Spline::getCoeffs() {
    return coeffs;
}

dType Spline::detmnt(vector<vector<dType> >& X, int n) {
    dType res = 0.0;
    int size = X.size();
    int c, subi, i, j, subj;
    vector<vector<dType> > submat = X;

    if (n == 2)    {
        return( (X[0][0] * X[1][1]) - (X[1][0] * X[0][1]));
    } else {
        for(c = 0; c < n; c++)  {
            subi = 0;
            for(i = 1; i < n; i++) {
                subj = 0;
                for(j = 0; j < n; j++)  {
                    if (j == c)  {
                        continue;
                    }
                    submat[subi][subj] = X[i][j];
                    subj++;
                }
                subi++;
            }
        res = res + (pow(-1.0 ,c) * X[0][c] * detmnt(submat, n-1));
        }
    }
    return res;
}

/*
dType Spline::detmnt(vector<vector<dType> >& X) {
    dType res = 0.0;
    int size = X.size();
    int c, subi, i, j, subj;
    vector<vector<dType> > submat = X;

    submat.resize(size);
    for (i = 0; i < size; i++)
        submat[i].resize(size);

    if (size == 2)    {
        return( (X[0][0] * X[1][1]) - (X[1][0] * X[0][1]));
    } else {
        for(c = 0; c < size; c++)  {
            subi = 0;
            for(i = 1; i < size; i++) {
                subj = 0;
                for(j = 0; j < size; j++)  {
                    if (j == c)  {
                        continue;
                    }
                    submat[subi][subj] = X[i][j];
                    subj++;
                }
                subi++;
            }
        res = res + (pow(-1 ,c) * X[0][c] * detmnt(submat, size-1));
        }
    }
    return res;
}
*/

dType Spline::detmnt(vector<vector<dType> >& X) {
    struct timeval t1, t2;
    std::stringstream str;
    dType res = 0.0;
    dType coeff = 1.0, det;
    int size = X.size();
    int c, subi, i, j, subj;
    vector<vector<dType> > submat;
    vector<vector<dType> > L;
    vector<vector<dType> > U;

    det = 1.0;

    LUDeco(X, L, U);

    for (int i = 0; i < size; i++) {
        det *= L[i][i];
    }
    for (int i = 0; i < size; i++) {
        det *= U[i][i];
    }
    res = det;

/*
    if (size == 2)    {
        return( (X[0][0] * X[1][1]) - (X[1][0] * X[0][1]));
    } else {
        for(c = 0; c < size; c++)  {
            submat = X;
            submat.erase( submat.begin());

            for (unsigned i = 0; i < submat.size(); ++i) {
                submat[i].erase(submat[i].begin() + c);
            }
            det = detmnt(submat);
            //str << coeff * X[0][c] << "   :  " << det;
            //printMatrix(submat, str.str());
            res = res + coeff * X[0][c] * det;
            coeff *= -1.0;
        }
    }
    */
    return res;
}


void checkcond(bool &val, std::string &message) {
  if (!val)
    std::cout << message << std::endl;
  //assert(bool);
}
void checkcond(bool &val) {};


funcs::funcs() {
    srand(static_cast<int>(time(0)));
}

double funcs::Dirac(double R, double a) {
    return 1.0 / (a * 1.772453851) * std::exp(-R * R / (a * a));
}

/*
template<typename T>
dType funcs::KDelta(const T &x, const T &y) {
    if (x == y) return 1.0;
    else return 0.0;
}
*/
void funcs::SWAP(dType& a, dType& b)
{
    dType tempr = a;
    a = b;
    b = tempr;
}

dType funcs::Gaussian(dType mu = 0, dType sigma = 1)
{
    dType polar;

    if (sigma <= 0)
        std::cout << "sigma Must be greater than zero.";

    dType v1, v2, rSquared;
    do
    {
        // two random values between -1.0 and 1.0
        v1 = 2.0 * ( (dType)rand() / (dType)RAND_MAX ) - 1.0;
        v2 = 2.0 * ( (dType)rand() / (dType)RAND_MAX ) - 1.0;
        rSquared = v1 * v1 + v2 * v2;
        // ensure within the unit circle
    } while (rSquared >= 1 || rSquared == 0);

    // calculate polar tranformation for each deviate
    polar = std::sqrt(-2.0 * std::log(rSquared) / rSquared);
    return v1 * polar * sigma + mu;
}

void funcs::FFT2(dType data[], unsigned long nn, int isign)
{
    unsigned long n,mmax,m,j,istep,i;
    dType wtemp,wr,wpr,wpi,wi,theta;    // Double precision for the trigonometric recurrences.
    dType tempr,tempi;

    n=nn << 1;
    j=1;
    for (i=1;i<n;i+=2)
    {                 // This is the bit-reversal section of the routine.
        if (j > i)
        {
            SWAP(data[j],  data[i]);       // Exchange the two complex numbers.
            SWAP(data[j+1],data[i+1]);
        }
        m=nn;
        while (m >= 2 && j > m)
        {
            j -=  m;
            m >>= 1;
        }
        j +=  m;
    }

    //Here begins the Danielson-Lanczos section of the routine.

    mmax=2;
    while (n > mmax)
    {
        // Outer loop executed log2nn  Sys.Times.
        istep   = mmax << 1;
        theta   = isign*(6.28318530717959/mmax);     // Initialize the trigonometric recurrence.
        wtemp   = sin(0.5*theta);
        wpr     = -2.0*wtemp*wtemp;
        wpi     = sin(theta);
        wr      = 1.0;
        wi      = 0.0;

        for (m=1;m<mmax;m+=2)
        {          // Here are the two nested inner loops.
            for (i=m;i<=n;i+=istep)
            {
                j          = i + mmax;                // This is the Danielson-Lanczos formula:
                tempr      = wr*data[j]   - wi*data[j+1];
                tempi      = wr*data[j+1] + wi*data[j];
                data[j]    = data[i]   - tempr;
                data[j+1]  = data[i+1] - tempi;
                data[i]   += tempr;
                data[i+1] += tempi;
            }
            wr = (wtemp=wr)*wpr - wi*wpi + wr;  // Trigonometric recurrence.
            wi = wi*wpr + wtemp*wpi + wi;
        }
        mmax = istep;
    }
}

void funcs::FFT(vector<dType>& data)
{
    int n = data.size();
    dType PI    = 3.141592653589793238462;
    // checks n is a power of 2 in 2's complement format
    if ((n & (n - 1)) != 0)
        throw "data length in FFT is not a power of 2!!!!";
    n /= 2;    // n is the number of samples

    Reverse(data, n); // bit index data reversal

    // do transform: so single point transforms, then dTypes, etc.
    dType sign = 1.0; //dType sign = forward ? B : -B;       B=1
    int mmax = 1;
    while (n > mmax)
    {
        int istep = 2 * mmax;
        dType theta = sign * PI / mmax;
        dType wr = 1, wi = 0;
        dType wpr = std::cos(theta);
        dType wpi = std::sin(theta);
        for (int m = 0; m < istep; m += 2)
        {
            for (int k = m; k < 2 * n; k += 2 * istep)
            {
                int j = k + istep;
                dType tempr = wr * data[j] - wi * data[j + 1];
                dType tempi = wi * data[j] + wr * data[j + 1];
                data[j] = data[k] - tempr;
                data[j + 1] = data[k + 1] - tempi;
                data[k] = data[k] + tempr;
                data[k + 1] = data[k + 1] + tempi;
            }
            dType t = wr; // trig recurrence
            wr = wr * wpr - wi * wpi;
            wi = wi * wpr + t * wpi;
        }
        mmax = istep;
    }

    // perform data scaling as needed
    //Scale(ref data);
}

void funcs::rFFT(vector<dType>& data)
{
    int n = data.size();
    dType PI    = 3.141592653589793238462;

    // checks n is a power of 2 in 2's complement format
    if ((n & (n - 1)) != 0)
        throw "data length in FFT is not a power of 2!!!";
    n /= 2;    // n is the number of samples

    Reverse(data, n); // bit index data reversal

    // do transform: so single point transforms, then dTypes, etc.
    dType sign = -1.0; //dType sign = forward ? B : -B;
    int mmax = 1;
    while (n > mmax)
    {
        int istep = 2 * mmax;
        dType theta = sign * PI / mmax;
        dType wr = 1, wi = 0;
        dType wpr = std::cos(theta);
        dType wpi = std::sin(theta);
        for (int m = 0; m < istep; m += 2)
        {
            for (int k = m; k < 2 * n; k += 2 * istep)
            {
                int j = k + istep;
                dType tempr = wr * data[j] - wi * data[j + 1];
                dType tempi = wi * data[j] + wr * data[j + 1];
                data[j] = data[k] - tempr;
                data[j + 1] = data[k + 1] - tempi;
                data[k] = data[k] + tempr;
                data[k + 1] = data[k + 1] + tempi;
            }
            dType t = wr; // trig recurrence
            wr = wr * wpr - wi * wpi;
            wi = wi * wpr + t * wpi;
        }
        mmax = istep;
    }

    // perform data scaling as needed
    //rScale(ref data);
}

void funcs::Scale(vector<dType>& data, int n)
{
    // forward scaling if needed
    dType scale = std::pow(n, (0 - 1) / 2.0);

    for (int i = 0; i < n; i++)
        data[i] *= scale;
}

void funcs::rScale(vector<dType>& data, int n)
{
    // forward scaling if needed
    dType scale = std::pow(n, -(0 + 1) / 2.0);

    for (int i = 0; i < n; ++i)
        data[i] *= scale;
}

void funcs::Reverse(vector<dType>& data, int n)
{
    // bit reverse the indices. This is exercise 5 in section
    // 7.2.1.1 of Knuth's TAOCP the idea is a binary counter
    // in k and one with bits reversed in j

    int j = 0, k = 0; // Knuth R1: initialize
    int top = n / 2;  // this is Knuth's 2^(n-1)

    while (true)
    {
        // Knuth R2: swap - swap j+1 and k+2^(n-1), 2 entries each
        dType t = data[j + 2];
        data[j + 2] = data[k + n];
        data[k + n] = t;
        t = data[j + 3];
        data[j + 3] = data[k + n + 1];
        data[k + n + 1] = t;
        if (j > k)
        { // swap two more
            // j and k
            t = data[j];
            data[j] = data[k];
            data[k] = t;
            t = data[j + 1];
            data[j + 1] = data[k + 1];
            data[k + 1] = t;
            // j + top + 1 and k+top + 1
            t = data[j + n + 2];
            data[j + n + 2] = data[k + n + 2];
            data[k + n + 2] = t;
            t = data[j + n + 3];
            data[j + n + 3] = data[k + n + 3];
            data[k + n + 3] = t;
        }
        // Knuth R3: advance k
        k += 4;
        if (k >= n)
            break;
        // Knuth R4: advance j
        int h = top;
        while (j >= h)
        {
            j -= h;
            h /= 2;
        }
        j += h;
    } // bit reverse loop
}

std::vector<dType> funcs::Regres3(vector<dType>& val, dType d_tau, int res, int step)
{
    std::vector<dType> valav;
    valav.resize(val.size());

    int ind = val.size();

    int i, j, n, up, dw, st, lim;
    dType a, b, c, d;
    dType x, x2, x3, x4, x5, x6, y, yx, yx2, yx3, D, D1, D2, D3, D4;
    dType tau = d_tau;
    dType tau2, tau3, tau4, tau5, tau6;
    dType s0, s1, s2, s3, s4, s5, c5, c4, c3, c2, c1, c0;
    tau2 = tau * tau;
    tau3 = tau * tau * tau;
    tau4 = tau * tau * tau * tau;
    tau5 = tau * tau * tau * tau * tau;
    tau6 = tau * tau * tau * tau * tau * tau;

    lim = (int)(res / 2);

    x = 0.0;
    x2 = 0.0;
    x3 = 0.0;
    x4 = 0.0;
    x5 = 0.0;
    x6 = 0.0;

    y = 0.0;
    yx = 0.0;
    yx2 = 0.0;
    yx3 = 0.0;
    n = 0;
    st = 0;

    for (j = 0; j < res; j++)
    {
        n += 1;
        x += tau * j;
        x2 += tau2 * j * j;
        x3 += tau3 * j * j * j;
        x4 += tau4 * j * j * j * j;
        x5 += tau5 * j * j * j * j * j;
        x6 += tau6 * j * j * j * j * j * j;

        y += val[j];
        yx += val[j] * tau * j;
        yx2 += val[j] * tau2 * j * j;
        yx3 += val[j] * tau3 * j * j * j;
    }


    s0 = (n * x2 - x * x);
    s1 = (n * x3 - x2 * x);
    s2 = (n * x4 - x3 * x);
    s3 = (x * x3 - x2 * x2);
    s4 = (x * x4 - x3 * x2);
    s5 = (x2 * x4 - x3 * x3);

    c5 = (x4 * x6 - x5 * x5);
    c4 = (x3 * x6 - x5 * x4);
    c3 = (x3 * x5 - x4 * x4);
    c2 = (x2 * x6 - x5 * x3);
    c1 = (x2 * x5 - x4 * x3);
    c0 = (x2 * x4 - x3 * x3);
    D = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


    s0 = (y * x2 - x * yx);
    s1 = (y * x3 - x2 * yx);
    s2 = (y * x4 - x3 * yx);
    s3 = (x * x3 - x2 * x2);
    s4 = (x * x4 - x3 * x2);
    s5 = (x2 * x4 - x3 * x3);

    c5 = (x4 * x6 - x5 * x5);
    c4 = (x3 * x6 - x5 * x4);
    c3 = (x3 * x5 - x4 * x4);
    c2 = (yx2 * x6 - x5 * yx3);
    c1 = (yx2 * x5 - x4 * yx3);
    c0 = (yx2 * x4 - x3 * yx3);
    D1 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


    s0 = (n * yx - y * x);
    s1 = (n * x3 - x2 * x);
    s2 = (n * x4 - x3 * x);
    s3 = (y * x3 - x2 * yx);
    s4 = (y * x4 - x3 * yx);
    s5 = (x2 * x4 - x3 * x3);

    c5 = (x4 * x6 - x5 * x5);
    c4 = (yx2 * x6 - x5 * yx3);
    c3 = (yx2 * x5 - x4 * yx3);
    c2 = (x2 * x6 - x5 * x3);
    c1 = (x2 * x5 - x4 * x3);
    c0 = (x2 * yx3 - yx2 * x3);
    D2 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


    s0 = (n * x2 - x * x);
    s1 = (n * yx - y * x);
    s2 = (n * x4 - x3 * x);
    s3 = (x * yx - y * x2);
    s4 = (x * x4 - x3 * x2);
    s5 = (y * x4 - x3 * yx);

    c5 = (yx2 * x6 - x5 * yx3);
    c4 = (x3 * x6 - x5 * x4);
    c3 = (x3 * yx3 - yx2 * x4);
    c2 = (x2 * x6 - x5 * x3);
    c1 = (x2 * yx3 - yx2 * x3);
    c0 = (x2 * x4 - x3 * x3);
    D3 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


    s0 = (n * x2 - x * x);
    s1 = (n * x3 - x2 * x);
    s2 = (n * yx - y * x);
    s3 = (x * x3 - x2 * x2);
    s4 = (x * yx - y * x2);
    s5 = (x2 * yx - y * x3);

    c5 = (x4 * yx3 - yx2 * x5);
    c4 = (x3 * yx3 - yx2 * x4);
    c3 = (x3 * x5 - x4 * x4);
    c2 = (x2 * yx3 - yx2 * x3);
    c1 = (x2 * x5 - x4 * x3);
    c0 = (x2 * x4 - x3 * x3);
    D4 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


    a = D1 / D;
    b = D2 / D;
    c = D3 / D;
    d = D4 / D;

    for (i = 0; i < ind; i++)
    {
        if (i < lim)
        {
            valav[i] = a + b * (i * tau) + c * (i * tau) * (i * tau) + d * (i * tau) * (i * tau) * (i * tau);
        }

        else
        {
            if (st < step)
            {
                up = (i + lim);
                dw = (i - lim);

                x -= tau * dw;
                x += tau * up;
                x2 -= tau2 * dw * dw;
                x2 += tau2 * up * up;
                x3 -= tau3 * dw * dw * dw;
                x3 += tau3 * up * up * up;
                x4 -= tau4 * dw * dw * dw * dw;
                x4 += tau4 * up * up * up * up;
                x5 -= tau5 * dw * dw * dw * dw * dw;
                x5 += tau5 * up * up * up * up * up;
                x6 -= tau6 * dw * dw * dw * dw * dw * dw;
                x6 += tau6 * up * up * up * up * up * up;

                y -= val[dw];
                y += val[up];
                yx -= val[dw] * tau * dw;
                yx += val[up] * tau * up;
                yx2 -= val[dw] * tau2 * dw * dw;
                yx2 += val[up] * tau2 * up * up;
                yx3 -= val[dw] * tau3 * dw * dw * dw;
                yx3 += val[up] * tau3 * up * up * up;

                st += 1;
            }

            else
            {
                up = (i + lim);

                n += 1;
                x += tau * up;
                x2 += tau2 * up * up;
                x3 += tau3 * up * up * up;
                x4 += tau4 * up * up * up * up;
                x5 += tau5 * up * up * up * up * up;
                x6 += tau6 * up * up * up * up * up * up;
                y += val[up];
                yx += val[up] * tau * up;
                yx2 += val[up] * tau2 * up * up;
                yx3 += val[up] * tau3 * up * up * up;
                lim += 1;
                up = (i + lim);

                n += 1;
                x += tau * up;
                x2 += tau2 * up * up;
                x3 += tau3 * up * up * up;
                x4 += tau4 * up * up * up * up;
                x5 += tau5 * up * up * up * up * up;
                x6 += tau6 * up * up * up * up * up * up;
                y += val[up];
                yx += val[up] * tau * up;
                yx2 += val[up] * tau2 * up * up;
                yx3 += val[up] * tau3 * up * up * up;

                st = 0;
            }

            s0 = (n * x2 - x * x);
            s1 = (n * x3 - x2 * x);
            s2 = (n * x4 - x3 * x);
            s3 = (x * x3 - x2 * x2);
            s4 = (x * x4 - x3 * x2);
            s5 = (x2 * x4 - x3 * x3);

            c5 = (x4 * x6 - x5 * x5);
            c4 = (x3 * x6 - x5 * x4);
            c3 = (x3 * x5 - x4 * x4);
            c2 = (x2 * x6 - x5 * x3);
            c1 = (x2 * x5 - x4 * x3);
            c0 = (x2 * x4 - x3 * x3);
            D = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


            s0 = (y * x2 - x * yx);
            s1 = (y * x3 - x2 * yx);
            s2 = (y * x4 - x3 * yx);
            s3 = (x * x3 - x2 * x2);
            s4 = (x * x4 - x3 * x2);
            s5 = (x2 * x4 - x3 * x3);

            c5 = (x4 * x6 - x5 * x5);
            c4 = (x3 * x6 - x5 * x4);
            c3 = (x3 * x5 - x4 * x4);
            c2 = (yx2 * x6 - x5 * yx3);
            c1 = (yx2 * x5 - x4 * yx3);
            c0 = (yx2 * x4 - x3 * yx3);
            D1 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


            s0 = (n * yx - y * x);
            s1 = (n * x3 - x2 * x);
            s2 = (n * x4 - x3 * x);
            s3 = (y * x3 - x2 * yx);
            s4 = (y * x4 - x3 * yx);
            s5 = (x2 * x4 - x3 * x3);

            c5 = (x4 * x6 - x5 * x5);
            c4 = (yx2 * x6 - x5 * yx3);
            c3 = (yx2 * x5 - x4 * yx3);
            c2 = (x2 * x6 - x5 * x3);
            c1 = (x2 * x5 - x4 * x3);
            c0 = (x2 * yx3 - yx2 * x3);
            D2 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


            s0 = (n * x2 - x * x);
            s1 = (n * yx - y * x);
            s2 = (n * x4 - x3 * x);
            s3 = (x * yx - y * x2);
            s4 = (x * x4 - x3 * x2);
            s5 = (y * x4 - x3 * yx);

            c5 = (yx2 * x6 - x5 * yx3);
            c4 = (x3 * x6 - x5 * x4);
            c3 = (x3 * yx3 - yx2 * x4);
            c2 = (x2 * x6 - x5 * x3);
            c1 = (x2 * yx3 - yx2 * x3);
            c0 = (x2 * x4 - x3 * x3);
            D3 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;


            s0 = (n * x2 - x * x);
            s1 = (n * x3 - x2 * x);
            s2 = (n * yx - y * x);
            s3 = (x * x3 - x2 * x2);
            s4 = (x * yx - y * x2);
            s5 = (x2 * yx - y * x3);

            c5 = (x4 * yx3 - yx2 * x5);
            c4 = (x3 * yx3 - yx2 * x4);
            c3 = (x3 * x5 - x4 * x4);
            c2 = (x2 * yx3 - yx2 * x3);
            c1 = (x2 * x5 - x4 * x3);
            c0 = (x2 * x4 - x3 * x3);
            D4 = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;

            a = D1 / D;
            b = D2 / D;
            c = D3 / D;
            d = D4 / D;

            valav[i] = a + b * (i * tau) + c * (i * tau) * (i * tau) + d * (i * tau) * (i * tau) * (i * tau);
        }
    }
    return valav;
}

std::vector<dType> funcs::Regress2(vector<dType>& data, int step) {
    int n, i, size, pos, limstep;
    dType x1, x2, x3, x4, y, yx, yx2;
    dType D, D1, D2, D3, a, b, c, DX;

    size = data.size();

    std::vector<dType> outdata;
    outdata.resize(size);

    limstep = (int) (step * 0.5);

    n  = step;
    for (pos = 0; pos != size; pos++) {

        x1 = 0;
        x2 = 0;
        x3 = 0;
        x4 = 0;

        y   = 0;
        yx  = 0;
        yx2 = 0;

        if (pos >= limstep && pos < (size-limstep)) {
            for (i = -limstep; i != limstep; i++) {
                DX = (pos+i) * 0.1;

                x1 += DX;
                x2 += DX*DX;
                x3 += DX*DX*DX;
                x4 += DX*DX*DX*DX;

                y   += data[pos+i];
                yx  += data[pos+i]*DX;
                yx2 += data[pos+i]*DX*DX;
            }
        }

        if (pos < step) {
            for (i = 0; i != step; i++) {
                DX = i * 0.1;

                x1 += DX;
                x2 += DX*DX;
                x3 += DX*DX*DX;
                x4 += DX*DX*DX*DX;

                y   += data[i];
                yx  += data[i]*DX;
                yx2 += data[i]*DX*DX;
            }
        }

        if (pos >= (size-step)) {
            for (i = (size-step); i != size; i++) {
                DX = i * 0.1;

                x1 += DX;
                x2 += DX*DX;
                x3 += DX*DX*DX;
                x4 += DX*DX*DX*DX;

                y   += data[i];
                yx  += data[i]*DX;
                yx2 += data[i]*DX*DX;
            }
        }


        //--- define the determinants of the matrix
        D  = n*(x2*x4 - x3*x3) + x1*(x3*x2  - x1*x4) + x2*(x1*x3 - x2*x2);
        D1 = y*(x2*x4 - x3*x3) + x1*(x3*yx2 - yx*x4) + x2*(yx*x3 - x2*yx2);
        D2 = n*(yx*x4 - x3*yx2)+ y *(x3*x2  - x1*x4) + x2*(x1*yx2 - yx*x2);
        D3 = n*(x2*yx2 - yx*x3)+ x1*(yx*x2 - x1*yx2) + y *(x1*x3 - x2*x2);

        //--- define the coefficients of the quadratic equation
        a = D1/D;
        b = D2/D;
        c = D3/D;
        DX = pos * 0.1;
        outdata[pos] = a + b * DX + c * DX * DX;
    }
    return outdata;
}

dType funcs::AverageW(vector<dType>& val, int ind0, int ind1)
{
    int i;
    dType ValSum, DevSum, AvVal, Sigma;
    vector<dType> Dev;
    Dev.resize(ind1);

    // Define the average value
    AvVal = std::accumulate(val.begin(), val.end(), 0) / val.size();

    // Define Deviation
    for (i = ind0; i < ind1; i += 1)
    {
        Sigma = (val[i] - AvVal) * (val[i] - AvVal);
        Dev[i] = 1.0 / Sigma;
    }

    // Define the weightened value
    ValSum = 0.0;
    DevSum = 0.0;

    for (i = ind0; i < ind1; i += 1)
    {
        ValSum += val[i] * Dev[i];
        DevSum += Dev[i];
    }
    return ValSum / DevSum;
}

dType funcs::Average(vector<dType>& val, int ind0, int ind1)
{
    int i;
    dType Val;

    // Define the average value
    Val = std::accumulate(val.begin(), val.end(), 0) / val.size();

    return Val;
}

void funcs::Integral(vector<dType>& val, vector<dType>& valint, long ind)
{
    valint[0] = val[0];
    for (int i = 1; i < ind; i += 1)
    {
        valint[i] = valint[i - 1] + val[i];
    }
}

dType funcs::ToDouble(const string& s){
    std::istringstream i(s);
    dType x;
    if (!(i >> x))
        throw std::runtime_error("convertToDouble(\"" + s + "\")");
    return x;
}

int funcs::ToInt(const string& s){
    int r = 0;
    string::const_iterator p = s.begin();
    bool neg = false;
    if (*p == '-') {
        neg = true;
        ++p;
    }
    while (p != s.end() && *p >= '0' && *p <= '9') {
        r = (r*10) + (*p - '0');
        ++p;
    }
    if (neg) {
        r = -r;
    }
    return r;
}

vector<string> funcs::split(const string& str){
    std::vector<std::string> result;
    std::istringstream iss(str);
    for(std::string s; iss >> s; )
        result.push_back(s);
/*
    typedef string::const_iterator iter;

    iter i = s.begin();

    while (i != s.end()){

        i = std::find_if(i, s.end(), not_Space());

        iter j = std::find_if(i, s.end(), Space());

        if (i != j)
            ret.push_back(std::string(i, j));
        i = j;
    }
    */
    return result;
}

vector<string> funcs::splitString(const string& s){
    vector<string> ret;
    string symbol = "+-_:/\0";
    typedef string::const_iterator iter;

    iter i = s.begin();

    while (i < s.end()) {

        iter j = i;
        while (std::find(symbol.begin(), symbol.end(), *j) == symbol.end() && j < s.end()) {
            j++;
        }

        if (i != j)
            ret.push_back(std::string(i, j));
        i = j+1;
    }
    return ret;
}


void funcs::DoubleToBinary(dType in, char out[65])
{
    int trunc;
    int denom;
    int next;
    int prev;

    trunc = (int)(std::abs(in));
    denom = (int)(std::abs((in - trunc)*100000000));
    if (in<0)
        out[0] = '1';
    if (in>0)
        out[0] = '0';

    prev = trunc;
    for (int i=32; i>0; i--)
    {
        next = (int)(prev * 0.5);
        if ((prev % 2) == 0)
            out[i] = '0';
        else
            out[i] = '1';
        prev = next;
    }

    prev = denom;
    for (int i=63; i>32; i--)
    {
        next = (int)(prev * 0.5);
        if ((prev % 2) == 0)
            out[i] = '0';
        else
            out[i] = '1';
        prev = next;
    }
}

void funcs::BinaryToDouble(char in[65], dType& out)
{
    int val;
    int trunc = 0;
    int denom = 0;

    val = 1;
    for (int i=63; i>32; i--)
    {
        if (in[i] == '1')
            denom += val;
        val *=2;
    }

    val = 1;
    for (int i=32; i>0; i--)
    {
        if (in[i] == '1')
            trunc += val;
        val *=2;
    }

    if (in[0] == '0')
        out = trunc + denom*1.0e-8;

    if (in[0] == '1')
        out = -(trunc + denom*1.0e-8);
}

std::valarray<dType> funcs::erfc(std::valarray<dType> x)
{
    return 1.0 - erf(x);
}

std::valarray<dType> funcs::erf(std::valarray<dType> x)
{
    // constants
    dType a1 = 0.254829592;
    dType a2 = -0.284496736;
    dType a3 = 1.421413741;
    dType a4 = -1.453152027;
    dType a5 = 1.061405429;
    dType p = 0.3275911;

    // Save the sign of x
    std::valarray<dType> sign(1, x.size());

    sign[x < 0.0] = -1;
    x = abs(x);

    // A&S formula 7.1.26
    std::valarray<dType> t = 1.0 / (1.0 + p*x);
    std::valarray<dType> y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return sign*y;
}

dType funcs::pow2(dType x)
{
    return x*x;
}

dType funcs::erfc(dType x)
{
    return 1.0 - erf(x);
}

dType funcs::erf(dType x)
{
    // constants
    dType a1 = 0.254829592;
    dType a2 = -0.284496736;
    dType a3 = 1.421413741;
    dType a4 = -1.453152027;
    dType a5 = 1.061405429;
    dType p = 0.3275911;

    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x);

    // A&S formula 7.1.26
    dType t = 1.0 / (1.0 + p*x);
    dType y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return sign*y;
}

dType funcs::rsqrt(dType x) {
    float xhalf = 0.5f*x;
    union
    {
        float x;
        int i;
    } u;
    u.x = x;
    u.i = 0x5f3759df - (u.i >> 1);
    /* The next line can be repeated any number of times to increase accuracy */
    u.x = u.x * (1.5f - xhalf * u.x * u.x);
    return u.x;
}

dType funcs::sqrt(dType x) {
    return x * rsqrt(x);
}

std::vector<dType> funcs::KDelta(const std::vector<dType> &x, const std::vector<dType> &y) {
    std::vector<dType> res(0.0, x.size());
    for (int i = 0; i < x.size(); i++) {
        if (x[i] == y[i]) res[i] = 1.0;
        else  res[i] = 0.0;
    }
    return res;
}

std::string funcs::timeStr(int prec) {
    struct timeval tv;
    time_t timev = time(0);
    std::stringstream buffer;
    std::stringstream val;

    struct tm * ltm = localtime( & timev);
    gettimeofday(&tv, NULL);

    buffer.setf(std::ios::fixed);
    buffer.width(6);
    buffer << ltm->tm_hour << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_min << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_sec<<".";
    buffer.width(2);

    val.clear();
    val.width(prec);
    val.fill('0');
    val <<  tv.tv_usec;
    buffer << val.str().substr (0, prec);
    return buffer.str();
};

std::string funcs::timeStr() {
    struct timeval tv;
    time_t timev = time(0);
    std::stringstream buffer;
    std::stringstream val;

    struct tm * ltm = localtime( & timev);
    gettimeofday(&tv, NULL);

    buffer.setf(std::ios::fixed);
    buffer.width(6);
    buffer << ltm->tm_hour << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_min << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_sec;
    return buffer.str();
};
