//-------------------------------------------------------------------
// File:   funcs.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <string>
#include <iostream>
#include <time.h>
#include <vector>
#include <map>
#include <cmath>
#include <valarray>
#include "vector.h"
#include <iomanip>      // std::setfill, std::setw
#include <time.h>
#include <cassert>

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/time.h>
#endif


#ifdef DEF_TYPE_FLOAT
    typedef float dType;
#else
    typedef double dType;
#endif

#ifdef DEF_TYPE_INT
    typedef unsigned long int iType;
#else
    typedef int iType;
#endif


#ifndef Funcs_INCLUDED
#define Funcs_INCLUDED

//static const dType PI    = 3.141592653589793238462;

using std::vector;
using std::map;
using std::string;

void checkcond(bool &val, std::string &message);
void checkcond(bool &val);

struct timeval start_t();
void stop_t( struct timeval& t);
double elapsed_t( struct timeval& t1);


class Spline{
public:
    void defineSpline(vector<dType>& X, vector<dType>& Y);
    void LUDeco(vector<vector<dType> >& X, vector<vector<dType> >& L, vector<vector<dType> >& U);
    dType getValue(dType val);
    vector<dType> getValue(vector<dType>& val);
    dType getValueV(vector<dType>& val);
    static dType detmnt(vector<vector<dType> >& X, int n);
    dType detmnt(vector<vector<dType> >& X);
    std::vector<dType> getCoeffs();
    vector<dType> coeffs;
private:
    vector<dType> powVal;
    int size;
    int deg;
};


class funcs {
    public:

        struct not_Space{
            bool operator()(const char& c){
                return !isspace(c);
            }
        };

        struct Space{
            bool operator()(const char& c){
                return isspace(c);
            }
        };

        static double Dirac(double R, double a);
        static void SWAP(dType& a, dType& b);
        static dType Gaussian(dType mu, dType sigma);
        static void FFT(std::vector<dType>& data);
        static void FFT2(dType data[], unsigned long nn, int isign);
        static void rFFT(std::vector<dType>& data);
        static void Scale(std::vector<dType>& data, int n);
        static void rScale(std::vector<dType>& data, int n);
        static void Reverse(std::vector<dType>& data, int n);
        static vector<dType> Regres3(std::vector<dType>& val, dType d_tau, int res, int step);
        static vector<dType> Regress2(std::vector<dType>& data, int step);
        static dType Regress2(vector<dType>& X, vector<dType>& Y, dType val);
        static dType Regress3(vector<dType>& X, vector<dType>& Y, dType val);
        static dType Regress4(vector<dType>& X, vector<dType>& Y, dType val);
        static dType Spline(vector<dType>& X, vector<dType>& Y, dType val);
        static dType AverageW(std::vector<dType>& val, int ind0, int ind1);
        static dType Average(std::vector<dType>& val, int ind0, int ind1);
        static void Integral(std::vector<dType>& val, std::vector<dType>& RJJFint, long ind);
        static dType ToDouble(const std::string&);
        static int ToInt(const std::string&);
        static void DoubleToBinary(dType in, char out[65]);
        static void BinaryToDouble(char in[65], dType& out);
        static dType erfc(dType x);
        static dType erf(dType x);
        static dType rsqrt(dType x);
        static dType sqrt(dType x);
        static dType pow2(dType x);
        static std::valarray<dType> erfc(std::valarray<dType> x);
        static std::valarray<dType> erf(std::valarray<dType> x);
        static std::vector<std::string> split(const std::string&);
        static std::vector<std::string> splitString(const std::string&);
        static dType detmnt(vector<vector<dType> >& X, int n);
        static dType detmnt(vector<vector<dType> >& X);
        static std::string timeStr(int prec);
        static std::string timeStr();


        std::vector<dType> KDelta(const std::vector<dType> &x, const std::vector<dType> &y);

        funcs();
};


#endif
