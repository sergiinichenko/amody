//-------------------------------------------------------------------
// File:   neural.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------



#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include "neural.h"

// using namespace std;

//
//

void TrainingData::getTopology(std::vector<unsigned> &topology)
{
    std::string line;
    std::string label;

    getline(m_trainingDataFile, line);
    std::stringstream ss(line);
    ss >> label;
    if (this->isEof() || label.compare("topology:") != 0) {
        abort();
    }

    while (!ss.eof()) {
        unsigned n;
        ss >> n;
        topology.push_back(n);
    }

    return;
}

TrainingData::TrainingData(const std::string filename)
{
    m_trainingDataFile.open(filename.c_str());
}

unsigned TrainingData::getNextInputs(t_vals &inputVals)
{
    inputVals.clear();

    std::string line;
    getline(m_trainingDataFile, line);
    std::stringstream ss(line);

    std::string label;
    ss>> label;
    if (label.compare("in:") == 0)
    {
        double oneValue;

        while (ss >> oneValue)
            inputVals.push_back(oneValue);
    }

    return inputVals.size();
}

unsigned TrainingData::getTargetOutputs(t_vals &targetOutputVals)
{
    targetOutputVals.clear();

    std::string line;
    getline(m_trainingDataFile, line);
    std::stringstream ss(line);

    std::string label;
    ss >> label;
    if (label.compare("out:") == 0)
    {
        double oneValue;

        while (ss >> oneValue)
            targetOutputVals.push_back(oneValue);
    }

    return targetOutputVals.size();
}

// TrainingData
//
//














double Neuron::eta = 0.10;    // overall net learning rate, [0.0..1.0]
double Neuron::alpha = 0.25;   // momentum, multiplier of last deltaWeight, [0.0..1.0]

Neuron::Neuron(unsigned numOutputs, unsigned myIndex)
    : m_myIndex(myIndex)
{
    for (unsigned i = 0; i < numOutputs; ++i)
    {
        m_outputWeights.push_back(Connection());
        m_outputWeights.back().weight = randomWeight();
    }
}

void Neuron::feedForward(const Layer &prevLayer) {
    unsigned n;
    double sum = 0.0;

    // Sum the previous layer's outputs (which are our inputs)
    // Include the bias node from the previous layer.
    //#pragma omp parallel for private(n) reduction(+:sum)
    for (n = 0; n < prevLayer.size(); ++n)
        sum += prevLayer[n].getOutputVal() * prevLayer[n].m_outputWeights[m_myIndex].weight;

    m_outputVal = Neuron::transferFunction(sum);
}

void Neuron::calcOutputGradients(double targetVal)
{
    double delta = targetVal - m_outputVal;
    m_gradient = delta * Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::calcHiddenGradients(const Layer &nextLayer)
{
    double dow = sumDOW(nextLayer);
    m_gradient = dow * Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::updateInputWeights(Layer &prevLayer)
{
    // The weights to be updated are in the Connection container
    // in the neurons in the preceding layer

    for (unsigned n = 0; n < prevLayer.size(); ++n)
    {
        Neuron &neuron = prevLayer[n];
        double oldDeltaWeight = neuron.m_outputWeights[m_myIndex].deltaWeight;

        double newDeltaWeight =
                // Individual input, magnified by the gradient and train rate:
                eta
                * neuron.getOutputVal()
                * m_gradient
                // Also add momentum = a fraction of the previous delta weight;
                + alpha
                * oldDeltaWeight;

        neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
        neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
    }
}

double Neuron::transferFunction(double x)
{
    // tanh - output range [-1.0..1.0]
    return tanh(x);
}

double Neuron::transferFunctionDerivative(double x)
{
    // tanh derivative
    return (1.0 - x * x);
}

double Neuron::sumDOW(const Layer &nextLayer) const
{
    double sum = 0.0;

    // Sum our contributions of the errors at the nodes we feed.

    unsigned num_neuron = (nextLayer.size() - 1); // exclude bias neuron

    for (unsigned n = 0; n < num_neuron; ++n)
        sum += m_outputWeights[n].weight * nextLayer[n].m_gradient;

    return sum;
}

// NEURON
//
//










double Net::k_recentAvgSmoothingFactor = 100.0; // Number of training samples to average over

Net::Net()
    :   m_error(0.0),
        m_recentAvgError(0.0)
{
}


Net::Net(const std::vector<unsigned>& topology)
    :   m_error(0.0),
        m_recentAvgError(0.0)
{
    assert( !topology.empty() ); // no empty topology

    for (unsigned i = 0; i < topology.size(); ++i)
    {
        unsigned num_neuron = topology[i];

        assert( num_neuron > 0 ); // no empty layer

        m_layers.push_back(Layer());

        Layer& new_layer = m_layers.back();

        bool is_last_layer = (i == (topology.size() - 1));

        // 0 output if on the last layer
        unsigned numOutputs = ((is_last_layer) ? (0) : (topology[i + 1]));

        // We have a new layer, now fill it with neurons, and
        // add a bias neuron in each layer.
        for (unsigned j = 0; j < (num_neuron + 1); ++j) // add a bias neuron
            new_layer.push_back( Neuron(numOutputs, j) );

        // Force the bias node's output to 1.0 (it was the last neuron pushed in this layer):
        Neuron& bias_neuron = new_layer.back();
        bias_neuron.setOutputVal(1.0);
    }
}

void Net::createNet(const std::vector<unsigned>& topology) {
    assert( !topology.empty() ); // no empty topology

    for (unsigned i = 0; i < topology.size(); ++i)
    {
        unsigned num_neuron = topology[i];

        assert( num_neuron > 0 ); // no empty layer

        m_layers.push_back(Layer());

        Layer& new_layer = m_layers.back();

        bool is_last_layer = (i == (topology.size() - 1));

        // 0 output if on the last layer
        unsigned numOutputs = ((is_last_layer) ? (0) : (topology[i + 1]));

        // We have a new layer, now fill it with neurons, and
        // add a bias neuron in each layer.
        for (unsigned j = 0; j < (num_neuron + 1); ++j) // add a bias neuron
            new_layer.push_back( Neuron(numOutputs, j) );

        // Force the bias node's output to 1.0 (it was the last neuron pushed in this layer):
        Neuron& bias_neuron = new_layer.back();
        bias_neuron.setOutputVal(1.0);
    }
}

void Net::feedForward(const t_vals &inputVals) {
    unsigned i, n;
    assert( inputVals.size() == (m_layers[0].size() - 1) ); // exclude bias neuron

    // Assign (latch) the input values into the input neurons
    //#pragma omp parallel for private(i)
    for (i = 0; i < inputVals.size(); ++i)
        m_layers[0][i].setOutputVal(inputVals[i]);

    // forward propagate
    for (unsigned i = 1; i < m_layers.size(); ++i) { // exclude input layer
        Layer& prevLayer = m_layers[i - 1];
        Layer& currLayer = m_layers[i];

        unsigned num_neuron = (currLayer.size() - 1); // exclude bias neuron
        //#pragma omp parallel for private(n)
        for (n = 0; n < num_neuron; ++n)
            currLayer[n].feedForward(prevLayer);
    }
}

void Net::backProp(const t_vals &targetVals)
{
    unsigned n, i;
    double delta;
    //
    // error

    // Calculate overall net error (RMS of output neuron errors)

    Layer &outputLayer = m_layers.back();
    m_error = 0.0;

    for (n = 0; n < outputLayer.size() - 1; ++n) {
        delta = (targetVals[n] - outputLayer[n].getOutputVal());
        //if (targetVals[n] != 0.0)
        //  delta = (targetVals[n] - outputLayer[n].getOutputVal()) / targetVals[n];
        //else
        //  delta = (targetVals[n] - outputLayer[n].getOutputVal());

        m_error += delta * delta;
    }
    m_error /= (outputLayer.size() - 1); // get average error squared
    m_error = sqrt(m_error); // RMS

    // Implement a recent average measurement

    m_recentAvgError =
            (m_recentAvgError * k_recentAvgSmoothingFactor + m_error)
            / (k_recentAvgSmoothingFactor + 1.0);

    // error
    //


    //
    // Gradients

    // Calculate output layer gradients

    for (n = 0; n < (outputLayer.size() - 1); ++n)
        outputLayer[n].calcOutputGradients(targetVals[n]);

    // Calculate hidden layer gradients

    for (i = (m_layers.size() - 2); i > 0; --i)
    {
        Layer &hiddenLayer = m_layers[i];
        Layer &nextLayer = m_layers[i + 1];

        //#pragma omp parallel for private(n)
        for (n = 0; n < hiddenLayer.size(); ++n)
            hiddenLayer[n].calcHiddenGradients(nextLayer);
    }

    // Gradients
    //


    // For all layers from outputs to first hidden layer,
    // update connection weights

    for (unsigned i = (m_layers.size() - 1); i > 0; --i)
    {
        Layer &currLayer = m_layers[i];
        Layer &prevLayer = m_layers[i - 1];

        //#pragma omp parallel for private(n)
        for (n = 0; n < (currLayer.size() - 1); ++n) // exclude bias
            currLayer[n].updateInputWeights(prevLayer);
    }
}

void Net::getResults(t_vals &resultVals) const
{
    unsigned n;
    resultVals.clear();

    const Layer& outputLayer = m_layers.back();

    // exclude last neuron (bias neuron)
    unsigned total_neuron = (outputLayer.size() - 1);

    //#pragma omp parallel for private(n)
    for (n = 0; n < total_neuron; ++n)
        resultVals.push_back(outputLayer[n].getOutputVal());
}

// NET
//
//









//
//
// MAIN

void Net::showVectorVals(std::string label, t_vals &v)
{
    std::cout << label << " ";
    for (unsigned i = 0; i < v.size(); ++i)
        std::cout << v[i] << " ";

    std::cout << std::endl;
}

/*
int main()
{
    TrainingData trainData("trainsample/out_xor.txt");
    // TrainingData trainData("trainsample/out_and.txt");
    // TrainingData trainData("trainsample/out_or.txt");
    // TrainingData trainData("trainsample/out_no.txt");

    // e.g., { 3, 2, 1 }
    std::vector<unsigned> topology;
    trainData.getTopology(topology);

    Net myNet(topology);

    t_vals inputVals, targetVals, resultVals;
    int trainingPass = 0;

    while (!trainData.isEof())
    {
        ++trainingPass;
        std::cout << std::endl << "Pass " << trainingPass << std::endl;

        // Get new input data and feed it forward:
        if (trainData.getNextInputs(inputVals) != topology[0])
            break;

        showVectorVals("Inputs:", inputVals);
        myNet.feedForward(inputVals);

        // Collect the net's actual output results:
        myNet.getResults(resultVals);
        showVectorVals("Outputs:", resultVals);

        // Train the net what the outputs should have been:
        trainData.getTargetOutputs(targetVals);
        showVectorVals("Targets:", targetVals);
        assert(targetVals.size() == topology.back());

        myNet.backProp(targetVals);

        // Report how well the training is working, average over recent samples:
        std::cout << "Net current error: " << myNet.getError() << std::endl;
        std::cout << "Net recent average error: " << myNet.getRecentAverageError() << std::endl;

        if (trainingPass > 100 && myNet.getRecentAverageError() < 0.05)
        {
            std::cout << std::endl << "average error acceptable -> break" << std::endl;
            break;
        }
    }

    std::cout << std::endl << "Done" << std::endl;

    if (topology[0] == 2)
    {
        std::cout << "TEST" << std::endl;
        std::cout << std::endl;

        unsigned dblarr_test[4][2] = { {0,0}, {0,1}, {1,0}, {1,1} };

        for (unsigned i = 0; i < 4; ++i)
        {
            inputVals.clear();
            inputVals.push_back(dblarr_test[i][0]);
            inputVals.push_back(dblarr_test[i][1]);

            myNet.feedForward(inputVals);
            myNet.getResults(resultVals);

            showVectorVals("Inputs:", inputVals);
            showVectorVals("Outputs:", resultVals);

            std::cout << std::endl;
        }

        std::cout << "/TEST" << std::endl;
    }
}

// MAIN
*/




/*
// Neural net
#include <iostream>
#include "neural.h"

double Neuron::eta = 0.05;   // [0.0, 1.0] overall training rate
double Neuron::alpha = 0.20;  // [0.0, n] multiplier of the last weight change [momentum]


Neuron::Neuron(unsigned numOutputs, unsigned Index) {
  for (unsigned c = 0; c < numOutputs; ++c){
    outputWeights.push_back(Connection());
    outputWeights.back().weight = randomWeight();
  }
  myIndex = Index;
}

double Neuron::transferFunction(double x) {
  // tanh  - output range [-1.0, 1.0]
  return tanh(x);
  //return x;
}

double Neuron::transferFunctionDerivative(double x) {
  // tanh derivative d/dx tanh x = 1.0 - tanh^2 x
  return 1.0 - x * x;
  //return 1.0;
}

void Neuron::calcOutputGradients(double targetVal){
  double delta = targetVal - outputValue;
  gradient = delta * Neuron::transferFunctionDerivative(outputValue);
}

double Neuron::sumDOW(const Layer& nextLayer) const {
  double sum = 0.0;

  // Sum our contributions of the errors at the nodes we feed
  for (unsigned n = 0; n < nextLayer.size() - 1; ++ n){
    sum += outputWeights[n].weight * nextLayer[n].gradient;
  }
  return sum;
}

void Neuron::calcHiddenGradients(const Layer& nextLayer){
  double dow = sumDOW(nextLayer);
  gradient = dow * Neuron::transferFunctionDerivative(outputValue);
}


void Neuron::feedForward(const Layer& prevLayer) {
  double sum = 0.0;

  // Sum the previuos layer's outputs (which are our inputs)
  // Include the bias node from the previous layer
  for (unsigned n = 0; n < prevLayer.size(); ++n){
    sum += prevLayer[n].getOutputValue() *
           prevLayer[n].outputWeights[myIndex].weight;
  }
  outputValue = Neuron::transferFunction(sum / prevLayer.size());
}

void Neuron::updateInputWeights(Layer& prevLayer){
  // The weights to be updated are in the Connection container
  // in the neurons in the preceding layer

  for (unsigned n = 0; n < prevLayer.size(); ++n) {
    Neuron& neuron = prevLayer[n];
    double oldDeltaWeight = neuron.outputWeights[myIndex].deltaWeight;
    double newDeltaWeight =
          // Individual input magnified by the gradient and learning rate
          eta
          * neuron.getOutputValue()
          * gradient
          // Also add momentum = a fraction of the previous delta weight
          + alpha
          * oldDeltaWeight;
    neuron.outputWeights[myIndex].deltaWeight = newDeltaWeight;
    neuron.outputWeights[myIndex].weight += newDeltaWeight;

  }
}

Net::Net() {
}


Net::Net(const std::vector<unsigned>& topology) {
// Number of layers is thes defined by the size of topology vector
  unsigned numLayer = topology.size();

  for (unsigned layerNum = 0; layerNum < numLayer; ++layerNum){

    // Creating the new layer of the Net
    layer.push_back(Layer());
    unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

    // Polulation the newly created layer with neurons
    for (unsigned neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum){
      layer.back().push_back(Neuron(numOutputs, neuronNum));
    }

    // Force the bias node's output value to be equal 1.0.
    // It is the last neuron created above
    layer.back().back().setOutputValue(1.0);
  }
}

void Net::createNet(const std::vector<unsigned>& topology) {
// Number of layers is thes defined by the size of topology vector
  unsigned numLayer = topology.size();
  layer.clear();

  for (unsigned layerNum = 0; layerNum < numLayer; ++layerNum){

    // Creating the new layer of the Net
    layer.push_back(Layer());
    unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

    // Polulation the newly created layer with neurons
    for (unsigned neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum){
      layer.back().push_back(Neuron(numOutputs, neuronNum));
    }

    // Force the bias node's output value to be equal 1.0.
    // It is the last neuron created above
    layer.back().back().setOutputValue(1.0);
  }
}

void Net::feedForward(const std::vector<double>& inputValues) {

  assert(inputValues.size() == layer[0].size() - 1);

  // Assign (latch) the inputValues to the input neurons
  for (unsigned i = 0; i < inputValues.size(); ++i){
    layer[0][i].setOutputValue(inputValues[i]);
  }

  // Forward propogation through all the layers to the output layer
  for (unsigned layerNum = 1; layerNum < layer.size(); ++ layerNum){

    Layer &prevLayer = layer[layerNum - 1];
    // going though all the neurons in the layer except the bias one (last one)
    for (unsigned n = 0; n < layer[layerNum].size() - 1; ++n) {
      layer[layerNum][n].feedForward(prevLayer);
    }
  }
}

void Net::backProp(const std::vector<double>& targetValues) {
  // Canlulate the overal ner error (RMS of the output layer)
  Layer& outpulLayer = layer.back();
  error = 0.0;

  for (unsigned n = 0; n < outpulLayer.size(); ++n) {
    double delta = targetValues[n] - outpulLayer[n].getOutputValue();
    error += delta * delta;
  }

  error /= outpulLayer.size() - 1;
  error = sqrt(error);


  // Implement a recent evarage error
  recentAvarageError =
      ( recentAvarageError * recentAvarageSmoothingFactor + error)
      / (recentAvarageSmoothingFactor + 1.0);


  // Calculate output layer gradient

  for (unsigned n = 0; n < outpulLayer.size() - 1; ++n) {
    outpulLayer[n].calcOutputGradients(targetValues[n]);
  }

  // Calculate gradient of hidden layers
  for (unsigned layerNum = layer.size() - 2; layerNum > 0; --layerNum) {
    Layer& hiddenLayer = layer[layerNum];
    Layer& nextLayer   = layer[layerNum + 1];
    for (unsigned n = 0; n < hiddenLayer.size(); ++n) {
      hiddenLayer[n].calcHiddenGradients(nextLayer);
    }
  }

  // For all layers from the output to the first layer
  // update connection weights
  for (unsigned layerNum = layer.size() - 1; layerNum > 0; --layerNum) {
    Layer& clayer    = layer[layerNum];
    Layer& prevLayer = layer[layerNum - 1];

    for (unsigned n = 0; n < clayer.size() - 1; ++n) {
      clayer[n].updateInputWeights(prevLayer);
    }
  }
}

void Net::getResults(std::vector<double>& resultValue) const {
  resultValue.clear();
  for (unsigned n = 0; n < layer.back().size() - 1; ++n) {
    resultValue.push_back(layer.back()[n].getOutputValue());
  }
}
*/
