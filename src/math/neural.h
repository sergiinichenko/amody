//-------------------------------------------------------------------
// File:   neural.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#pragma once
// Neural net
#ifndef NEURAL_H
#define NEURAL_H

#include "vector.h"
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>



// TrainingData

// Silly class to read training data from a text file -- Replace This.
// Replace class TrainingData with whatever you need to get input data into the
// program, e.g., connect to a database, or take a stream of data from stdin, or
// from a file specified by a command line argument, etc.

typedef std::vector<double> t_vals;

class TrainingData
{
public:
    TrainingData(const std::string filename);
    bool isEof(void) { return m_trainingDataFile.eof(); }
    void getTopology(std::vector<unsigned> &topology);

    // Returns the number of input values read from the file:
    unsigned getNextInputs(t_vals &inputVals);
    unsigned getTargetOutputs(t_vals &targetOutputVals);

private:
    std::ifstream m_trainingDataFile;
};





struct Connection
{
    double weight;
    double deltaWeight;
};
typedef std::vector<Connection> Connections;


class Neuron;

typedef std::vector<Neuron> Layer;




//
//
// NEURON

class Neuron
{
public:
    Neuron(unsigned numOutputs, unsigned myIndex);

    inline void setOutputVal(double val) { m_outputVal = val; }
    inline double getOutputVal(void) const { return m_outputVal; }

    void feedForward(const Layer& prevLayer);
    void calcOutputGradients(double targetVal);
    void calcHiddenGradients(const Layer& nextLayer);
    void updateInputWeights(Layer& prevLayer);
    Connections m_outputWeights;

private:
    static double eta;   // [0.0..1.0] overall net training rate
    static double alpha; // [0.0..n] multiplier of last weight change (momentum)
    static double transferFunction(double x);
    static double transferFunctionDerivative(double x);
    static double randomWeight(void) { return 0.1 * rand() / double(RAND_MAX); }
    double sumDOW(const Layer &nextLayer) const;
    double m_outputVal;
    unsigned m_myIndex;
    double m_gradient; // used by the backpropagation
};







//
//
// NET
struct SampleStruct{
    std::vector<dType> InData;
    std::vector<dType> OutData;
};


class Net
{
public:

	std::vector<unsigned> topology;
	std::vector<SampleStruct> Sample;

    Net();
    Net(const std::vector<unsigned> &topology);
    void createNet(const std::vector<unsigned>& topology);
    void feedForward(const t_vals &inputVals);
    void backProp(const t_vals &targetVals);
    void getResults(t_vals &resultVals) const;
    void showVectorVals(std::string label, t_vals &v);

public: // error
    void setIterations(const int& val) {iterations = val; }
    unsigned getIterations(void) {return iterations ;}
    void setScale(const double& val) {scale = val; }
    double getScale(void) {return scale ;}
    double getError(void) const { return m_error; }
    double getRecentAverageError(void) const { return m_recentAvgError; }
    std::vector<Layer> m_layers; // m_layers[layerNum][neuronNum]

private:

    // error
    unsigned iterations = 1000;
    double scale = 1.0;
    double m_error;
    double m_recentAvgError;
    static double k_recentAvgSmoothingFactor;
    // /error
};




/*

struct Connection
{
  double weight;
  double deltaWeight;
};

typedef std::vector<Connection> Connections;


class Neuron;

typedef std::vector<Neuron> Layer;



class Neuron
{
public:
  Neuron(unsigned numOutputs, unsigned myIndex);
  void setOutputValue(double val) { outputValue = val; }
  double getOutputValue(void) const {return outputValue; }
  void feedForward(const Layer& prevLayer);
  void calcOutputGradients(double targetVal);
  void calcHiddenGradients(const Layer& nextLayer);
  void updateInputWeights(Layer& prevLayer);
  std::vector<Connection> outputWeights;

private:
  static double eta;    // [0.0, 1.0] overall training rate
  static double alpha;  // [0.0, n] multiplier of the last weight change [momentum]
  static double transferFunction(double x);
  static double transferFunctionDerivative(double x);
  static double randomWeight(void) {return rand() / double(RAND_MAX) ;}
  double sumDOW(const Layer& nextLayer) const;
  double outputValue;
  double gradient;
  unsigned myIndex;
};


class Net
{
public:
    Net();
    Net(const std::vector<unsigned>& topology);
    void createNet(const std::vector<unsigned>& topology);
    void feedForward(const std::vector<double>& inputValues);
    void backProp(const std::vector<double>& targetValues);
    void getResults(std::vector<double>& resultValue) const;
    double getError(void) const {return error; }
    void setScale(const double& val) {scale = val; }
    double getScale(void) {return scale ;}
    std::vector<Layer> layer; // layer[layer][neuron]

private:
  double scale;
  double error;
  double recentAvarageError;
  double recentAvarageSmoothingFactor;
};
*/

#endif
