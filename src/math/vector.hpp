// -*- lsst-c++ -*-
/*
 * ThermoSol is a module for calculating bulc properties of chemical systems,
 * activities of species and mixing effects
 *
 * Copyright (C) 2018-2019 Sergii Nichenko
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FVECTOR_HPP
#define FVECTOR_HPP

#include <functional>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <string.h>
#include <iostream>

template<typename T>
inline constexpr T erf(T x)
{
    // constants
    T a1 = 0.254829592;
    T a2 = -0.284496736;
    T a3 = 1.421413741;
    T a4 = -1.453152027;
    T a5 = 1.061405429;
    T p = 0.3275911;

    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x);

    // A&S formula 7.1.26
    T t = 1.0 / (1.0 + p*x);
    T y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return sign*y;
}







template <typename Type>
class TypeIterator {
public:
    // Iterator tags here...
    using iterator_category = std::forward_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = Type;
    using pointer           = Type*;  // or also value_type*
    using reference         = Type&;  // or also value_type&

private:
    Type* m_ptr;

public:

    // Iterator constructors here...
    TypeIterator<Type>() = default;
    TypeIterator<Type>(Type* ptr) : m_ptr(ptr) {}

    reference operator*() const { return *m_ptr; }
    Type* operator->() { return m_ptr; }

    TypeIterator<Type>& operator+=(const difference_type& movement){m_ptr += movement;return (*this);}
    TypeIterator<Type>& operator-=(const difference_type& movement){m_ptr -= movement;return (*this);}

    // Prefix increment
    TypeIterator<Type>&  operator++() { ++m_ptr; return *this; }  
    TypeIterator<Type>&  operator--() { --m_ptr; return *this; }  

    // Postfix increment
    TypeIterator<Type> operator++(int) { TypeIterator tmp = *this; ++(*this); return tmp; }

    friend bool operator== (const TypeIterator& a, const TypeIterator& b) { return a.m_ptr == b.m_ptr; };
    friend bool operator!= (const TypeIterator& a, const TypeIterator& b) { return a.m_ptr != b.m_ptr; };     
};



/// Template expression for basic math functions
/// +, -, *, /
template <class LExpr, class Op, class RExpr>
class BinExpr {
public:
    BinExpr(const LExpr& x, const RExpr& y) : left_(x), right_(y) {}
    auto operator[](int i) const -> decltype(auto) { 
        if constexpr (std::is_same<LExpr, double>::value && !std::is_same<RExpr, double>::value)
            return Op::apply(left_, right_[i]); 
        else if constexpr (!std::is_same<LExpr, double>::value && std::is_same<RExpr, double>::value)
            return Op::apply(left_[i], right_); 
        else 
            return Op::apply(left_[i], right_[i]); 
    }
private:
 const LExpr& left_;
 const RExpr& right_;
};


/// Function expression template for such functions like
/// sqrt, exp ...
template <class Expr, class Op>
class FunExpr {
public:
    FunExpr(const Expr& x) : val_(x) {}
    auto operator[](int i) const -> decltype(auto) { 
        if constexpr (std::is_same<Expr, double>::value)
            return Op::apply(val_); 
        else 
            return Op::apply(val_[i]); 
    }
private:
 const Expr& val_;
};




template <class T> 
class fVal {
public:
    T _val;
    T _dx;
    fVal(const T& val) {
        _val = val;
        _dx  = 1.0;
    };

    fVal() {
        _val = 0.0;
        _dx  = 1.0;
    };

    fVal& operator=(const double& w) {
        _val = w;
        _dx  = 1.0;
        return *this;
    }

    //fVal<T> operator*(const fVal<T>& x, const fVal<T>& y);
    fVal & operator/=(const fVal &w);
    
    friend std::ostream& operator<<(std::ostream& os, const fVal<T>& vec) {
        os << vec._val << " : " << vec._dx << std::endl;
        return os;
    }

};






template <class Type>
class fVector {
public:
    // Iterator tags here...
    using iterator_category = std::forward_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = Type;
    using size_type         = size_t;
    using pointer           = Type*;  // or also value_type*
    using reference         = Type&;  // or also value_type&

    //typedef typename iterator::pointer			pointer;
    typedef TypeIterator<Type>        iterator;
    typedef TypeIterator<const Type>  const_iterator;


private:
    size_type   sz_;

    /// The beginning of the array
    Type* m_begin   = nullptr;
    /// The current end of the array
    Type* m_end     = nullptr;
    /// The total size of the array
    Type* m_finish  = nullptr;
    /// Total size of the array
    size_t m_size  = 0;
    /// Is vector
    bool is_vector = true;

public:

    bool is_vec() { return is_vector; }

    fVector<Type>() {}
    fVector<Type>(size_t n) {resize(n) ;}
    fVector<Type>(size_t n, Type x) {
        resize(n) ;
        fill(x);
    }
    ~fVector<Type>() {free(m_begin);}

    fVector<Type> (const fVector<Type> &other) {
        // Guard self assignment
        *this = other;
    }

    size_t   size()   {return m_size; }

    iterator begin()  {return iterator(m_begin); }
    iterator end()    {return iterator(m_end); }

    const size_t size()    const {return m_size; }
    const iterator begin() const {return iterator(m_begin); }
    const iterator end()   const {return iterator(m_end); }

    Type& operator[](size_t ind)       { return *(m_begin + ind); }
    Type& operator[](size_t ind) const { return *(m_begin + ind); }




    void resize(size_t s) {
        m_size = s;

        pointer old_begin = m_begin;
        pointer old_end   = m_end;

        // allocate memory
        m_begin   = (Type*)malloc(m_size * sizeof(Type));
        m_end     = m_begin + m_size;
        m_finish  = m_end;

        // free memory
        free(old_begin);
    }

    void resize(size_t s, Type x) {
        m_size = s;

        pointer old_begin = m_begin;
        pointer old_end   = m_end;

        // allocate memory
        m_begin   = (Type*)malloc(m_size * sizeof(Type));
        m_end     = m_begin + m_size;
        m_finish  = m_end;
        fill(x);

        // free memory
        free(old_begin);
    }

    void reserve(size_t s) {
        if (this->m_begin != nullptr) {
            free(this->m_begin);
            this->m_begin = nullptr;
            this->m_end   = nullptr;
            this->m_size  = 0;
        }

        // allocate memory
        m_size = 0;
        m_begin   = (Type*)malloc(s * sizeof(Type));
        m_finish  = m_begin + s;
        m_end     = m_begin;
        fill(0.0);
    }

    void clear() {
        m_size = 0;
        
        // reset to nullptr
        m_begin   = nullptr;
        m_end     = nullptr;
        m_finish  = nullptr;
        free(m_begin);
    }


    void push_back(Type x) {
        m_size++;

        if ( (m_end - m_begin) < (m_finish - m_begin)) {
            *m_end = x;
            m_end++;
        } else {
            pointer old_begin = m_begin;
            pointer old_end   = m_end;
            // allocate memory
            m_begin = (Type*)malloc(m_size * sizeof(Type));
            m_end   = m_begin + m_size;
            m_finish= m_end;

            memcpy(m_begin, old_begin, old_end - old_begin);
            free(old_begin);
        }
    }


    void fill(Type x) {
        std::fill(m_begin, m_end, x);
    }

public:
    fVector<Type>& operator=(const fVector<Type>& other) {
        // Guard self assignment
        if (this == &other)
            return *this;
    
        if (this->m_begin != nullptr) {
            free(this->m_begin);
            this->m_begin  = nullptr;
            this->m_end    = nullptr;
            this->m_finish = nullptr;
            this->m_size   = 0;
        }

        this->m_size   = other.m_size;
        this->m_begin  = (Type*)malloc(this->m_size * sizeof(Type));
        this->m_end    = this->m_begin + this->m_size;
        this->m_finish = other.m_finish;

        memcpy(m_begin, other.m_begin, this->m_size * sizeof(Type));

        return *this;
    }


    template <class L, class Op, class R>
    fVector<Type>& operator=(const BinExpr< L, Op, R >& v) {
        for (int i=0;i<size();++i)
            m_begin[i] = v[i]; 
        return *this;
    }


    template <class Expr, class Op>
    fVector<Type>& operator=(const FunExpr< Expr, Op >& v) {
        for (int i=0;i<size();++i)
            m_begin[i] = v[i]; 
        return *this;
    }
};

template <typename Type>
std::ostream& operator<<(std::ostream& os, const fVector<Type>& vec)
{
    for (auto it : vec)
        os << it << " ";
    return os;
}

//------------ Type traits -------------------------------------
struct true_type {
  static constexpr bool value = true;
};

struct false_type {
  static constexpr bool value = false;
};


template<typename T>
struct __is_diff_type : public false_type { };


template<class LExpr, class Op>
struct __is_diff_type<FunExpr<LExpr, Op>> : public true_type { };


template<class LExpr, class Op, class RExpr>
struct __is_diff_type<BinExpr<LExpr, Op, RExpr>> : public true_type { };

template<class T>
struct __is_diff_type<fVal<T>> : public true_type { };

template<class T>
struct __is_diff_type<fVector<T>> : public true_type { };

template<>
struct __is_diff_type<double> : public true_type { };

template<>
struct __is_diff_type<int> : public true_type { };

/// is an appropriate type
template<typename T>
struct is_diff_type : public __is_diff_type<T> { };

template<bool B, class T = void>
struct enable_return {};
 
template<class T>
struct enable_return<true, T> { typedef T type; };







template<class LExpr, class Op, class RExpr>
struct binexpr_type {
  typedef BinExpr< LExpr, Op, RExpr > type;
};

template<bool A, bool B, class LExpr, class Op, class RExpr>
struct type_if {};



template<bool A, bool B, class LExpr, class Op, class RExpr>
struct return_type
{ using type = BinExpr<LExpr, Op, RExpr>; };

template<class LExpr, class Op, class RExpr>
struct return_type<true, true , LExpr, Op, RExpr> 
{ using type = BinExpr<LExpr, Op, RExpr>; };

template<class LExpr, class Op, class RExpr>
struct return_type<true, false , LExpr, Op, RExpr> 
{ using type = BinExpr<LExpr, Op, RExpr>; };

template<class LExpr, class Op, class RExpr>
struct return_type<false, true , LExpr, Op, RExpr> 
{ using type = BinExpr<LExpr, Op, RExpr>; };

template<class LExpr, class Op, class RExpr>
struct return_type<false, false , LExpr, Op, RExpr> 
{ using type = LExpr; };





//---------- Automatic derivation methods ---------------------------

// -----------------------      Multiplication operators      ------------------------
template <class T> fVal<T> operator*(const fVal<T>& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x._val * y._val;
    res._dx  = x._val * y._dx + x._dx * y._val;
    return res;
};

template <class T> fVal<T> operator*(const fVal<T>& x, const T& y) {
    fVal<T> res;
    res._val = x._val * y;
    res._dx  = y * x._dx;
    return res;
};

template <class T> fVal<T> operator*(const T& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x * y._val;
    res._dx  = x * y._dx;
    return res;
};


// -----------------------      Addition operators      ------------------------
template <class T> fVal<T> operator+(const fVal<T>& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x._val + y._val;
    res._dx  = x._dx  + y._dx;
    return res;
};

template <class T> fVal<T> operator+(const fVal<T>& x, const T& y) {
    fVal<T> res;
    res._val = x._val + y;
    res._dx  = x._dx;
    return res;
};

template <class T> fVal<T> operator+(const T& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x + y._val;
    res._dx  = y._dx;
    return res;
};


// -----------------------      Substraction operators      ------------------------
template <class T> fVal<T> operator-(const fVal<T>& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x._val - y._val;
    res._dx  = x._dx  - y._dx;
    return res;
};

template <class T> fVal<T> operator-(const fVal<T>& x, const T& y) {
    fVal<T> res;
    res._val = x._val - y;
    res._dx  = x._dx;
    return res;
};

template <class T> fVal<T> operator-(const T& y, const fVal<T>& x) {
    fVal<T> res;
    res._val = y - x._val;
    res._dx  = - x._dx;
    return res;
};



// -----------------------      Division operators      ------------------------
template <class T> fVal<T> operator/(const fVal<T>& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x._val / y._val;
    res._dx  = x._dx / y._val - x._val * y._dx / (y._val * y._val);
    return res;
};

template <class T> fVal<T> operator/(const fVal<T>& x, const T& y) {
    fVal<T> res;
    res._val = x._val / y;
    res._dx  = x._dx / y;
    return res;
};

template <class T> fVal<T> operator/(const T& x, const fVal<T>& y) {
    fVal<T> res;
    res._val = x / y._val;
    res._dx  = - x * y._dx / (y._val * y._val);
    return res;
};




template <class T> fVal<T> exp(const fVal<T>& x) {
    fVal<T> res;
    res._val = exp(x._val);
    res._dx  = x._dx * exp(x._val);
    return res;
};

template <class T> fVal<T> log(const fVal<T>& x) {
    fVal<T> res;
    res._val = log(x._val);
    res._dx  = x._dx / x._val;
    return res;
};

template <class T> fVal<T> sqrt(const fVal<T>& x) {
    fVal<T> res;
    res._val = std::sqrt(x._val);
    res._dx  = x._dx / ( 2.0 * std::sqrt(x._val));
    return res;
};

template <class T> fVal<T> pow(const fVal<T>& x, const T& p) {
    fVal<T> res;
    res._val = pow(x._val, p);
    res._dx  = p * pow(x._val, (p-1.0)) * x._dx;
    return res;
};

template <class T> fVal<T> pow(const fVal<T>& x, const fVal<T>& p) {
    fVal<T> res;
    res._val = pow(x._val, p._val);
    res._dx  = p._val * pow(x._val, (p._val-1.0)) * x._dx;
    return res;
};

template <class T> fVal<T> pow2(const fVal<T>& x) {
    fVal<T> res;
    res._val = x._val * x._val;
    res._dx  = 2 * x._val * x._dx;
    return res;
};

template <class T> fVal<T> pow4(const fVal<T>& x) {
    fVal<T> res;
    res._val = x._val * x._val * x._val * x._val;
    res._dx  = 3 * x._val * x._val * x._val;
    return res;
};

template <class T> fVal<T> pow22(const fVal<T>& x) {
    fVal<T> res;
    res._val = x._val * x._val;
    res._dx  = 2 * x._val * x._dx;
    return res;
};

template <class T> fVal<T> erf(const fVal<T>& x) {
    fVal<T> res;
    res._val = erf(x._val);
    res._dx  = 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};

template <class T> fVal<T> erfc(const fVal<T>& x) {
    fVal<T> res;
    res._val = 1.0 - erf(x._val);
    res._dx  = - 2.0 * 0.5641895835 *  x._dx * exp( - x._val * x._val); // 0.5641895835 = 1 / sqrt(pi)
    return res;
};



struct plus  {    
    static inline std::vector<int>::iterator   apply(std::vector<int>::iterator a, const int& b) { return a + b;} 
    
    static inline double       apply(const double& a, const double& b) { return a+b;} 
    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return a + b;}    
};

struct minus {    
    static inline double apply(const double& a, const double& b) { return a-b;} 
    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return a-b;}
};

struct multiply { 
    static inline double apply(const double& a, const double& b) { return a*b;}
    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return a * b;} 
};

struct devide {   
    static inline double apply(const double& a, const double& b) { return a/b;} 
    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return a/b;}    
};

struct power {    
    static inline double apply(const double& a, const double& b) { return std::pow(a, b);} 
    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return pow(a, b);} 
};

struct sqroot {   
    static inline double apply(const double& a) { return std::sqrt(a);} 
    static inline fVal<double> apply(const fVal<double>& a) { return sqrt(a);} 
};

//struct power {    static inline fVal<double> apply(const fVal<double>& a, const fVal<double>& b) { return std::pow(a, b);} };
//struct sqroot {   static inline fVal<double> apply(const fVal<double>& a) { return std::sqrt(a);} };


/// Plus, minus, multiply, devide
template <class LExpr, class RExpr>
typename enable_return<is_diff_type<LExpr>::value, BinExpr< LExpr,plus, RExpr >>::type 
operator+(const LExpr& self, const RExpr& other) {
    return BinExpr< LExpr,plus, RExpr >(self, other);
}

template <class LExpr, class RExpr>
typename enable_return<is_diff_type<LExpr>::value, BinExpr< LExpr,minus, RExpr >>::type 
operator-(const LExpr& self, const RExpr& other) {
    return BinExpr< LExpr,minus, RExpr >(self, other);
}

template <class LExpr, class RExpr>
typename enable_return<is_diff_type<LExpr>::value, BinExpr< LExpr,multiply, RExpr >>::type 
operator*(const LExpr& self, const RExpr& other) {
    return BinExpr< LExpr,multiply, RExpr >(self, other);
}

template <class LExpr, class RExpr>
typename enable_return<is_diff_type<LExpr>::value, BinExpr< LExpr,devide, RExpr >>::type 
operator/(const LExpr& self, const RExpr& other) {
    return BinExpr< LExpr,devide, RExpr >(self, other);
}


/// Pow, sqrt etc...
template <class LExpr, class RExpr>
typename enable_return<is_diff_type<LExpr>::value, BinExpr< LExpr,power, RExpr >>::type 
pow(const LExpr& self, const RExpr& other) {
    return BinExpr< LExpr,power, RExpr >(self, other);
}

template <class Expr>
inline FunExpr< Expr,sqroot > sqrt(const Expr& expr) {
    return FunExpr< Expr,sqroot >(expr);
}



template < class T >
inline T sum(const fVector<T>& x) {
    return std::accumulate(x.begin(), x.end(), 0.0);
}

template < class LExpr, class Op, class RExpr >
inline double sum(const BinExpr<LExpr, Op, RExpr >& x) {
    fVector<double> res;
    res = x;
    return std::accumulate(res.begin(), res.end(), 0.0);
}



/*

/// Plus, minus, multiply, devide
template <class LExpr, class RExpr>
inline BinExpr< fVal<double>, LExpr, plus< fVal<double> >, RExpr > operator+(const LExpr& self, const RExpr& other) {
    return BinExpr< fVal<double>, LExpr, plus< fVal<double> >, RExpr >(self, other);
}

/// Plus, minus, multiply, devide
template <class LExpr, class RExpr>
inline BinExpr<fVal<double>, LExpr,plus<fVal<double>>, RExpr > operator+(const LExpr& self, const RExpr& other) {
    return BinExpr<fVal<double>, LExpr,plus<fVal<double>>, RExpr >(self, other);
}

template <class LExpr, class RExpr>
inline BinExpr<double, LExpr,minus<double>, RExpr > operator-(const LExpr& self, const RExpr& other) {
    return BinExpr<double, LExpr,minus<double>, RExpr >(self, other);
}

template <class LExpr, class RExpr>
inline BinExpr<double, LExpr,multiply<double>, RExpr > operator*(const LExpr& self, const RExpr& other) {
    return BinExpr<double, LExpr,multiply<double>, RExpr >(self, other);
}

template <class LExpr, class RExpr>
inline BinExpr<double, LExpr,devide<double>, RExpr > operator/(const LExpr& self, const RExpr& other) {
    return BinExpr<double, LExpr,devide<double>, RExpr >(self, other);
}


/// Pow, sqrt etc...
template <class LExpr, class RExpr>
inline BinExpr<double, LExpr,power<double>, RExpr > pow(const LExpr& self, const RExpr& other) {
    return BinExpr<double, LExpr,power<double>, RExpr >(self, other);
}

template <class Expr>
inline FunExpr< double, Expr,sqroot<double> > sqrt(const Expr& expr) {
    return FunExpr< double, Expr,sqroot<double> >(expr);
}



template < class T >
inline T sum(const fVector<T>& x) {
    return std::accumulate(x.begin(), x.end(), 0.0);
}

template < class LExpr, class Op, class RExpr >
inline double sum(const BinExpr<double, LExpr, Op, RExpr >& x) {
    fVector<double> res;
    res = x;
    return std::accumulate(res.begin(), res.end(), 0.0);
}

template < class T >
inline fVector<T> sqrt(const fVector<T>& x) {
    fVector<T> res;
    res = x;
    std::transform(x.begin(),       x.end(),       res.begin(),     static_cast<double (*)(double)>(std::sqrt));
    return res;
}
*/




#endif
