// -*- lsst-c++ -*-
/*
 * ThermoSol is a module for calculating bulc properties of chemical systems,
 * activities of species and mixing effects
 *
 * Copyright (C) 2018-2019 Sergii Nichenko
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "basePotential.h"

Potential::Potential() {
}

Potential::Potential(const Potential& other) {
    this = other;
}

Potential& Potential::operator=(Potential other) {
    this = other;
    return *this;
}

Potential::~Potential() {
}

ForceEnergy Potential::operator()( int i, PairDistV& PairL ) {
}



