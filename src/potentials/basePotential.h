// -*- lsst-c++ -*-
/*
 * AMoDy is a general purpose molecular dynamics package
 *
 * Copyright (C) 2018-2019 Sergii Nichenko
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BasePotential_H
#define BasePotential_H
// C++ includes
#include "../datatypes.h"
#include "../system.h"
#include "../math/vector.h"
#include "../math/fad.h"


class Potential {
public:
    /// Constrcut a default Potential instance
    Potential();

    /// Construct a copy of an ModelBase instance
    Potential(const basePotential& other);

    /// Assign an Potential instance to this instance
    Potential& operator=(Potential other);

    /// Destroy this Potential instance
    virtual ~Potential();

    /// functor that returns the calculations
    virtual ForceEnergy operator()( int i, PairDistV& PairL );

};

#endif // BasePotential_H
