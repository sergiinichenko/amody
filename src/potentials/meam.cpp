/*
 * morse.cpp
 *
 *  Created on: Dec 19, 2017
 *      Author: nichenko_s
 */

#include "meam.h"

class meamFunction : public basePotential {
	ForceEnergy operator()(int i, PairDistV& PairL) {

	    ForceEnergy res;
	    int s = R.size();
	    dType d, Dab, alphaab, rab,  qq, rRc_long_Bohr, ExpRc, erfcRC;

	    std:vector<dType> F(s);

	    FadV<dType> ExpVal(s);
	    FadV<dType> Energy(s);
	    FadV<dType> erfFunc(s);
	    FadV<dType> XBohr(s);
	    FadV<dType> XCBohr(s);
	    FadV<dType> X(s);
	    std::vector<dType> erfcR_Sc;
	    std::vector<dType> R_sc_Bohr;
	    std::vector<dType> rR_sc_Bohr;

	    X = assignValV(R);

	    std::fill(F.begin(), F.end(), 0.0);

        Dab     = params[namei].DM * params[namej].DM;
        alphaab = params[namei].alphaM + params[namej].alphaM;
        rab     = params[namei].r + params[namej].r;

        ExpVal = exp( - alphaab * (X - rab));
        ExpRc  = exp( - alphaab * (cond.Rc_short - rab));

        Energy = Dab * ((ExpVal * ExpVal - 2.0 * ExpVal) - ((ExpRc * ExpRc - 2.0 * ExpRc)));
        res.F  = res.F + (-Energy._dx);
        res.E  = res.E + (Energy._val);
        
        return res;
    }
};

