/*
 * morse.h
 *
 *  Created on: Dec 19, 2017
 *      Author: nichenko_s
 */


#ifndef SRC_POTENTIALS_MEAM_H_
#define SRC_POTENTIALS_MEAM_H_


#include "../datatypes.h"
#include "../system.h"
#include "../math/vector.h"
#include "../math/fad.h"

class meamFunction;

#endif /* SRC_POTENTIALS_MEAM_H_ */
