/*
 * morse.h
 *
 *  Created on: Dec 19, 2017
 *      Author: nichenko_s
 */


#ifndef SRC_POTENTIALS_MORSE_H_
#define SRC_POTENTIALS_MORSE_H_


#include "../datatypes.h"
#include "../system.h"
#include "../math/vector.h"
#include "../math/fad.h"

#endif /* SRC_POTENTIALS_MORSE_H_ */
