//-------------------------------------------------------------------
// File:   properties.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "properties.h"
#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>

#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <cmath>
#include <algorithm>

bool IsNan (double x)  { return std::isnan(x); }
bool IsInf (double x)  { return std::isinf(x); }
bool IsnFVal(double x) {return !(x < 1.0e20 && x > -1.0e20); }
bool IsLow(double x)   {return x < -1.0e8; }
bool IsHihg(double x)  {return x >  1.0e8; }
bool IsNeg (double x)  { return x < 0.0; }


Properties::Properties() {
    Reset();
}

void Properties::Reset() {
    CohesiveEnergy_tau = 0.0;
    CohesiveEnergy = 0.0;
    Temperature = 300.0;
    Temperature_tau = 300.0;
    Pressure = 0.0;
    Pressure_tau = 0.0;
    Diffusion = 0.0;
    Diffusion_tau = 0.0;
    SpecificHeat = 0.0;
    SpecificHeat_tau = 0.0;
    Density = 5000.0;
    Density_tau = 5000.0;
    Viscosity = 0.0;
    Viscosity_tau = 0.0;
    ThermCond = 0.0;
    ThermCond_tau = 0.0;
    Volume = 1.0;
    Volume_tau = 1.0;
    IntEnergy = 0.0;
    IntEnergy_tau = 0.0;
    Energy = 0.0;
    Energy_tau = 0.0;
    GibbsEnergy = 0.0;
    GibbsEnergy_tau = 0.0;
    Enthalpy = 0.0;
    Enthalpy_tau = 0.0;
    FreeEnergy_tau = 0.0;
    FreeEnergy = 0.0;
    Entropy_tau = 0.0;
    Entropy = 0.0;
    ICompress_tau = 0.0;
    ICompress = 0.0;
    PotEnergy = 0.0;
    PotEnergy_tau = 0.0;
    KinEnergy = 0.0;
    KinEnergy_tau  = 0.0;
    MSD_tau = 0.0;
    MSD = 0.0;
}

void Properties::RegVelocity() {
    double TVelX, TVelY, TVelZ;
    double dt_t, sc;
    int i, in, icl, cl;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    TVelX = 0.0;
    TVelY = 0.0;
    TVelZ = 0.0;

    #pragma omp parallel for private(i, in) reduction(+: TVelX, TVelY, TVelZ)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        assert( i < At.vx.size());
        TVelX += At.vx[i];
        TVelY += At.vy[i];
        TVelZ += At.vz[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&TVelX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelX = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&TVelY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelY = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&TVelZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelZ = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    sc = inf.Time < 0.25e3 ? 1.0 / (inf.Time) : 1.0 / (0.25 * 1000);
    sc = sc > 1.0 ? 0.1 : sc;

    inf.VelX += sc * (TVelX * At.rsize - inf.VelX);
    inf.VelY += sc * (TVelY * At.rsize - inf.VelY);
    inf.VelZ += sc * (TVelZ * At.rsize - inf.VelZ);

    dt_t =  inf.Time < 50.0e3 ? 50.0 / ( inf.Time) : 1.0 / (50.0 * 1000);
    dt_t = dt_t > 1.0 ? 0.1 : std::sqrt(dt_t);

    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        At.vx[i] -=  inf.VelX * dt_t;
        At.vy[i] -=  inf.VelY * dt_t;
        At.vz[i] -=  inf.VelZ * dt_t;
    }
}

void Properties::RegPosition() {
    double TotPosX, TotPosY, TotPosZ;
    double Dx, Dy, Dz, sc;
    int i, in, icl, cl;

    TotPosX = 0.0;
    TotPosY = 0.0;
    TotPosZ = 0.0;

    #pragma omp parallel for private(i, in) reduction(+: TotPosX, TotPosY, TotPosZ)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        TotPosX += At.x[i];
        TotPosY += At.y[i];
        TotPosZ += At.z[i];
    }

    TotPosX *= At.rsize;
    TotPosY *= At.rsize;
    TotPosZ *= At.rsize;

    sc = 0.002 * (0.25 + 0.75 * std::exp(- 1.0e-3 * inf.Time * 0.05));

    inf.PosX = 0.5 * ( inf.Wall.xmax -  inf.Wall.xmin);
    inf.PosY = 0.5 * ( inf.Wall.ymax -  inf.Wall.ymin);
    inf.PosZ = 0.5 * ( inf.Wall.zmax -  inf.Wall.zmin);

    Dx = (TotPosX -  inf.PosX) * sc;
    Dy = (TotPosY -  inf.PosY) * sc;
    Dz = (TotPosZ -  inf.PosZ) * sc;


    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        At.x[i] -= Dx;
        At.y[i] -= Dy;
        At.z[i] -= Dz;

        At.x0[i] -= Dx;
        At.y0[i] -= Dy;
        At.z0[i] -= Dz;

        At.xp[i] -= Dx;
        At.yp[i] -= Dy;
        At.zp[i] -= Dz;

        At.xd[i] -= Dx;
        At.yd[i] -= Dy;
        At.zd[i] -= Dz;

        At.xav[i] -= Dx;
        At.yav[i] -= Dy;
        At.zav[i] -= Dz;
    }
}

void Properties::RegTemperature() {
    double dt_tT, ScT, E_kin;
    std::vector<dType> vel;
    E_kin = 0;

    E_kin = 0;

    vel = At.vx * At.vx + At.vy * At.vy + At.vz * At.vz;
    E_kin = 0.5 * sum((At.mass * vel));
    props.Temperature_tau = 2.0 * E_kin / (kb * 3 *  At.size);

    // --------------------   Temperature regulation   --------------
    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Temperature / props.Temperature_tau - 1.0));

    At.vx = At.vx * ScT;
    At.vy = At.vy * ScT;
    At.vz = At.vz * ScT;
}

void Properties::RegPressure() {
    double dt_tP, ScP, Dfin;
    int im, i, in, icl, cl;

    // --------------------   props.Pressure regulation   --------------
    dt_tP = cond.P_rel + 1.0  / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.P_rel_fin - cond.P_rel) - (1.0 / (1.0 + 148.41316) * (cond.P_rel_fin - cond.P_rel));
    ScP = std::pow(1.0 - 1.0 / (dt_tP * 100000) * (cond.Preg - props.Pressure_tau), 1.0 / 3.0);

    if (IsNan(ScP)) ScP = 1.0;
    while (ScP > 1.05 || ScP < 0.95) {
        ScP = std::pow(ScP, 0.95);
    }
    assert( ScP < 1.05 && ScP > 0.95 );

    #pragma omp parallel for private(im)
    for (im = 0; im < inf.ImPos.size(); im++) {
        inf.ImPos[im].x *= ScP;
        inf.ImPos[im].y *= ScP;
        inf.ImPos[im].z *= ScP;
    }

     inf.Cell.a *= ScP;
     inf.Cell.b *= ScP;
     inf.Cell.c *= ScP;

     inf.Wall.xmin *= ScP;
     inf.Wall.ymin *= ScP;
     inf.Wall.zmin *= ScP;

     inf.Wall.xmax *= ScP;
     inf.Wall.ymax *= ScP;
     inf.Wall.zmax *= ScP;

     inf.PosX *= ScP;
     inf.PosY *= ScP;
     inf.PosZ *= ScP;

     At.xp = At.xp * ScP;
     At.yp = At.yp * ScP;
     At.zp = At.zp * ScP;

     At.xd = At.xd * ScP;
     At.yd = At.yd * ScP;
     At.zd = At.zd * ScP;

     At.xav = At.xav * ScP;
     At.yav = At.yav * ScP;
     At.zav = At.zav * ScP;

     At.x = At.x * ScP;
     At.y = At.y * ScP;
     At.z = At.z * ScP;

     At.x0 = At.x0 * ScP;
     At.y0 = At.y0 * ScP;
     At.z0 = At.z0 * ScP;
}

void Properties::PremeltR() {
    double dt_tT, ScT1, ScT2;
    double Rlim2, Rsc2, zmin, zmax;
    double Z, Y, X;
    int i, in;

    zmin = 0.0;
    zmax =  inf.Cell.c;
    X = 0.5 * ( inf.Wall.xmax +  inf.Wall.xmin);
    Y = 0.5 * ( inf.Wall.ymax +  inf.Wall.ymin);
    Z = 0.5 * ( inf.Wall.zmax +  inf.Wall.zmin);

    Rlim2  = cond.Rmelt * cond.Rmelt;

    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT1 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg1 / props.Temperature_tau - 1.0));
    ScT2 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg2 / props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i, in, Rsc2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        Rsc2 = (At.x[i] - X) * (At.x[i] - X) + (At.y[i] - Y) * (At.y[i] - Y) + (At.z[i] - Z) * (At.z[i] - Z);
        if (Rsc2 <= Rlim2) {
            At.vx[i] *= ScT2;
            At.vy[i] *= ScT2;
            At.vz[i] *= ScT2;
        } else {
            At.vx[i] *= ScT1;
            At.vy[i] *= ScT1;
            At.vz[i] *= ScT1;
        }
    }
}

void Properties::PremeltL() {
    double dt_tT, ScT1, ScT2;
    double Rlim2, Rsc2, zmin, zmax;
    double Z, Y, X;
    int i, in;

    zmin = 0.0;
    zmax =  inf.Cell.c;
    X = 0.5 * ( inf.Wall.xmax +  inf.Wall.xmin);
    Y = 0.5 * ( inf.Wall.ymax +  inf.Wall.ymin);
    Z = 0.5 * ( inf.Wall.zmax +  inf.Wall.zmin);

    Rlim2  = cond.Rmelt * cond.Rmelt;

    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT1 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg1 / props.Temperature_tau - 1.0));
    ScT2 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg2 / props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i, in, Rsc2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        if (At.x[i] < X) {
            At.vx[i] *= ScT2;
            At.vy[i] *= ScT2;
            At.vz[i] *= ScT2;
        } else {
            At.vx[i] *= ScT1;
            At.vy[i] *= ScT1;
            At.vz[i] *= ScT1;
        }
    }
}

void Properties::GetProperties() {
    double R, R_sc, R_sc2, masstot;
    double RminAv, val, sum, X, Y, Z;
    double xmax, ymax, zmax, xmin, ymin, zmin;
    double PTerm1, PTerm2, IntEn, E_pot;
    double E_kin, vel;
    double aver, b, sc;
    int i, j, in, icl, cl;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    double tv, tt, tp, ten, td, tsh;
    tv = 0.0;
    tt = 0.0;
    tp = 0.0;
    ten = 0.0;
    td = 0.0;
    tsh = 0.0;

    //start_t(tv1);
    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    // SIMPLE CUBIC VOLUME CALCULATION
    props.Volume_tau = ( inf.Wall.xmax -  inf.Wall.xmin) * ( inf.Wall.ymax -  inf.Wall.ymin) * ( inf.Wall.zmax -  inf.Wall.zmin) * inf.Cell.sin_b;


    //--------------------------   props.Density mesurement   ---------------------------
    props.Density_tau = inf.MassTot / props.Volume_tau;



    //-------------------------  Internal Kinetic temperature #1   --------------------
    E_kin = 0;

    #pragma omp parallel for private(i, in, vel) reduction(+:E_kin)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        vel = (At.vx[i] * At.vx[i] + At.vy[i] * At.vy[i] + At.vz[i] * At.vz[i]);
        At.KinEnergy_tau[i] = 0.5 * At.mass[i] * vel;
        E_kin += At.KinEnergy_tau[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&E_kin,    1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_kin = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.Temperature_tau = 2.0 * E_kin / (kb * 3 *  At.size);

    //------------------------   props.Pressure mesurement   -------------------------
    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&props.Summ_of_PotEnergy, 1, mpiType,
                                  &idData.front(),    1, mpiType, MPI_COMM_WORLD);
        props.Summ_of_PotEnergy = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    PTerm1 = kb * props.Temperature_tau *  At.size / props.Volume_tau;
    PTerm2 = 0.1666667 / props.Volume_tau * props.Summ_of_PotEnergy;
    props.Pressure_tau = (PTerm1 + PTerm2) * 160218.0; // MPa


    //----------------------   Internal energy mesurement   -----------------------
    IntEn = 0.0;
    E_pot = 0.0;
    E_kin = 0.0;

    #pragma omp parallel for private(i, in) reduction(+:E_pot, E_kin)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        E_kin = E_kin + At.KinEnergy_tau[i];
        E_pot = E_pot + At.PotEnergy_tau[i];
        At.Energy_tau[i] = At.KinEnergy_tau[i] + 0.5 * At.PotEnergy_tau[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&E_kin,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_kin = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&E_pot,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_pot = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.KinEnergy_tau = E_kin;
    props.PotEnergy_tau = 0.5 * E_pot;

    props.Energy_tau = (props.KinEnergy_tau + props.PotEnergy_tau) *  At.rsize;

    props.IntEnergy_tau = 0.5 * E_pot;

    props.Enthalpy_tau = props.Energy_tau + cond.Preg / 160218.0 * props.Volume;//props.Pressure / 160218.0 * props.Volume;
    props.CohesiveEnergy_tau = (E_pot + E_kin) * At.rsize;

    // ---------------------------   DIFFUSION ----------------------------------------
    double R_sc2_sum;
    R_sc2       = 0;
    R_sc2_sum   = 0;
    //props.Diffusion_tau =  MSD_tau * 1.0/(2.0*3.0) * cond.rd_tau;

    #pragma omp parallel for private(i, in, R_sc2) reduction(+: R_sc2_sum)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        R_sc2 = ((At.xd[i] - At.x[i]) * (At.xd[i] - At.x[i]) +
                 (At.yd[i] - At.y[i]) * (At.yd[i] - At.y[i]) +
                 (At.zd[i] - At.z[i]) * (At.zd[i] - At.z[i]));
        R_sc2_sum += R_sc2;
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&R_sc2_sum,       1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        R_sc2_sum = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.MSD_tau = R_sc2_sum *  At.rsize;
    props.Diffusion_tau       = CalcDiffusion();
    props.ThermCond_tau  	  = CalcThermalConductivity();
    props.ICompress_tau    	  = CalcIsotermalCompressibility();
    props.SpecificHeat_tau 	  = CalcSpecificHeat();
    props.Viscosity_tau       = CalcViscosity();

}

void Properties::AverageProperties() {
    double sc, b;
    int i, in;

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;


    props.Temperature += sc * (props.Temperature_tau - props.Temperature);
    props.Pressure += sc * (props.Pressure_tau - props.Pressure);
    props.Density += sc * (props.Density_tau - props.Density);
    props.IntEnergy += sc * (props.IntEnergy_tau - props.IntEnergy);
    props.Energy += sc * (props.Energy_tau - props.Energy);
    props.Diffusion += sc * (props.Diffusion_tau - props.Diffusion);
    props.Volume += sc *  (props.Volume_tau - props.Volume);
    props.MSD    += sc * (props.MSD_tau - props.MSD);
    props.SpecificHeat += sc * (props.SpecificHeat_tau - props.SpecificHeat);
    props.PotEnergy += sc * (props.PotEnergy_tau - props.PotEnergy);
    props.KinEnergy += sc * (props.KinEnergy_tau - props.KinEnergy);

    props.Enthalpy += sc * (props.Enthalpy_tau - props.Enthalpy);
    props.GibbsEnergy += sc * (props.GibbsEnergy_tau - props.GibbsEnergy);
    props.ThermCond += sc * (props.ThermCond_tau - props.ThermCond);
    props.ICompress  += sc * (props.ICompress_tau - props.ICompress);
    props.Viscosity     += sc * (props.Viscosity_tau - props.Viscosity);
    props.CohesiveEnergy += sc * (props.CohesiveEnergy_tau - props.CohesiveEnergy);

    #pragma omp parallel for private(i)
    for (i=0; i< At.size; i++)
    {
        At.KinEnergy[i] += sc * (At.KinEnergy_tau[i] - At.KinEnergy[i]);
        At.PotEnergy[i] += sc * (At.PotEnergy_tau[i] - At.PotEnergy[i]);
        At.Energy[i]       += sc * (At.Energy_tau[i] - At.Energy[i]);
    }

    b = -std::log(1.0 / (cond.Pos_Av_Time))/(cond.Pos_Av_Time);
    sc =  inf.Time < cond.Pos_Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Pos_Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        At.xav[i]       += sc * (At.x[i] - At.xav[i]);
        At.yav[i]       += sc * (At.y[i] - At.yav[i]);
        At.zav[i]       += sc * (At.z[i] - At.zav[i]);
    }
}

void Properties::RadialDistFunc() {
    int nj, Ncl, j, cl, i, n, in, k, cl2, Ncl2, im, nint, atNum, pairNum;
    double DR = RDF.RDFRmax / RDF.r.size();
    double R_sc, R_sc2, R, Rmax2, Rx, Ry, Rz;
    Rmax2 = RDF.RDFRmax * RDF.RDFRmax;

    std::string namePair;
    std::vector<rdfPair>::const_iterator rdfp;
    std::vector<std::string> RDFpairs;
    std::vector<dType> RDFuni;

    RDFuni.resize(RDF.r.size());

    for (i = 0; i < RDF.r.size(); i++) {
        RDF.r[i] = i * DR + 0.5 * DR;
        for (j = 0; j < RDFfor.size(); j++) {
            RDF.n[j][i] = 0.0;
        }
    }

    pairNum = 0;


    RDFpairs.clear();
    for (rdfp = RDFfor.begin(); rdfp != RDFfor.end(); ++rdfp) {
        RDFpairs.push_back((*rdfp).At1 + (*rdfp).At2);
        RDFpairs.push_back((*rdfp).At2 + (*rdfp).At1);
    }


    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        cl = At.Cell[i];
        Ncl = Pairs[cl].size();
        for (k = 0; k < Ncl; k++)   {

            im      = Pairs[cl][k].im;
            cl2     = Pairs[cl][k].cl;
            Ncl2    = Cell[cl2].atomI.size();

            for (n = 0; n < Ncl2; n++) {

                j = Cell[cl2].atomI[n];
                if (j == i) continue;

                namePair = At.name[i] + At.name[j];
                // Check if the name of atom exists in the DRFfor vector
                if (std::find(RDFpairs.begin(), RDFpairs.end(), namePair) == RDFpairs.end()) continue;
                pairNum = int((find(RDFpairs.begin(), RDFpairs.end(), namePair) - RDFpairs.begin()) / 2.0);

                Rx = (At.x[i] - (At.x[j] + inf.ImPos[im].x));
                if (Rx >  cond.Rc_long) continue;
                Ry = (At.y[i] - (At.y[j] + inf.ImPos[im].y));
                if (Ry >  cond.Rc_long) continue;
                Rz = (At.z[i] - (At.z[j] + inf.ImPos[im].z));
                if (Rz >  cond.Rc_long) continue;

                R_sc2 = (Rx * Rx + Ry * Ry + Rz * Rz);

                if (R_sc2 <=  Rmax2 && R_sc2 > 0.0) {
                    R_sc    = std::sqrt(R_sc2);
                    nint = (int) (R_sc / DR);
                    RDF.n[pairNum][nint] += 1.0;//funcs.Dirac(0 * DR);
                }
            }
        }
    }

    for (pairNum = 0; pairNum != RDFfor.size(); ++pairNum) {
        atNum = std::accumulate(RDF.n[pairNum].begin(), RDF.n[pairNum].end(), 0.0);

        for (i = 0; i < RDFuni.size(); i++) {
            RDFuni[i] = atNum / (4.0/3.0 * PI * RDF.RDFRmax * RDF.RDFRmax * RDF.RDFRmax) * 4 * PI *RDF.r[i] * RDF.r[i] * DR;
        }

        for (i = 0; i < RDF.r.size(); i++) {
            RDF.n[pairNum][i] = RDF.n[pairNum][i] / RDFuni[i];
        }
    }

    vector<double> data;
    vector<double> dataout;
    data.resize(RDF.r.size());
    dataout.resize(RDF.r.size());

    pairNum = 0;
    for (rdfp = RDFfor.begin(); rdfp != RDFfor.end(); ++rdfp) {
        for (i = 0; i < RDF.r.size(); i++) {
            data[i] = RDF.n[pairNum][i];
        }

        dataout = funcs::Regress2(data, cond.RDFSmoothStep);

        for (int i = 0; i < RDF.r.size(); i++)
            if (dataout[i] > 0.0)
                RDF.n[pairNum][i] = dataout[i];
            else
                RDF.n[pairNum][i] = 0.0;

        pairNum++;
    }
}

dType Properties::CalcSpecificHeat() {
    dType res, EnFl, En, En2, b, sc;
    iType in, i;
    dType DEn, DEn_tau, DEn2, DEn2_tau;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    En = 0.0;
    En2 = 0.0;

    #pragma omp parallel for private(i, in) reduction(+:En, En2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        En   = En + (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume);
        En2 = En2 + (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume) * (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume);
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&En,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        En = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&En2,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        En2 = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    DEn_tau = En * At.rsize;
    DEn2_tau = En2 * At.rsize;

    DEn += sc * (DEn_tau - DEn);
    DEn2 += sc * (DEn2_tau - DEn2);

    EnFl = (DEn2  - DEn * DEn);

    res = 1.0 / (kb * props.Temperature * props.Temperature)  *  EnFl * 96.48535 * 1000.0;
    return res;
}

dType Properties::CalcViscosity() {
    
    double Term1XY, Term2XY, TermXY, Term1YZ, Term2YZ, TermYZ, Term1ZX, Term2ZX, TermZX, coef;
    double w1, val, E, sum, res;
    int ind, i, m, in, icl, cli, cl;
    int n, k, t, size;
    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    res = 0.0;

    if (( inf.Time >= 2.0)) {
        coef = cond.d_tau / ( 3.0 * props.Volume * kb * props.Temperature ) * 1.60218e-4;
        //coef = cond.d_tau / ( props.Volume * kb * props.Temperature ) * 1.60218e-4;

        Term1XY = 0.0;
        Term2XY = 0.0;
        Term1YZ = 0.0;
        Term2YZ = 0.0;
        Term1ZX = 0.0;
        Term2ZX = 0.0;

        #pragma omp parallel for private(i, in, icl, cl, E)  \
                                 reduction(+:Term1XY, Term2XY, Term1YZ, Term2YZ, Term1ZX, Term2ZX)
        for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
            cl = Domain[inf.dID].cellI[icl];
            for (in = 0; in < Cell[cl].atomI.size(); in++) {
                i = Cell[cl].atomI[in];

                Term1XY += At.mass[i]*At.vx[i]*At.vy[i];
                Term1YZ += At.mass[i]*At.vy[i]*At.vz[i];
                Term1ZX += At.mass[i]*At.vz[i]*At.vx[i];
                Term2XY += 0.5 * At.rxfy[i];
                Term2YZ += 0.5 * At.ryfz[i];
                Term2ZX += 0.5 * At.rzfx[i];
            }
        }

        TermXY = Term1XY + Term2XY;
        TermYZ = Term1YZ + Term2YZ;
        TermZX = Term1ZX + Term2ZX;

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermXY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermXY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermYZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermYZ = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZX = std::accumulate(idData.begin(), idData.end(), 0.0);
        }


        if ( P_vec.xy.size() < (int)(100000/cond.d_tau) ) {
            P_vec.xy.push_back(TermXY);
            P_vec.yz.push_back(TermYZ);
            P_vec.zx.push_back(TermZX);
            Corr_P_vec.Push_back(0.0);

            size = P_vec.xy.size();
            t = size - 1;

            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                Corr_P_vec.xy[k] += (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] += (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] += (P_vec.zx[n] * P_vec.zx[t]);
                Corr_P_vec.nxy[k] = Corr_P_vec.xy[k]  / (size - k);
                Corr_P_vec.nyz[k] = Corr_P_vec.yz[k]  / (size - k);
                Corr_P_vec.nzx[k] = Corr_P_vec.zx[k]  / (size - k);
            }
        } else {
            size = P_vec.xy.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                Corr_P_vec.xy[k] -= (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] -= (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] -= (P_vec.zx[n] * P_vec.zx[t]);
            }

            P_vec.xy.erase(P_vec.xy.begin());
            P_vec.yz.erase(P_vec.yz.begin());
            P_vec.zx.erase(P_vec.zx.begin());
            P_vec.xy.push_back(TermXY);
            P_vec.yz.push_back(TermYZ);
            P_vec.zx.push_back(TermZX);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                Corr_P_vec.xy[k] += (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] += (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] += (P_vec.zx[n] * P_vec.zx[t]);
                Corr_P_vec.nxy[k] = Corr_P_vec.xy[k]  / (size - k);
                Corr_P_vec.nyz[k] = Corr_P_vec.yz[k]  / (size - k);
                Corr_P_vec.nzx[k] = Corr_P_vec.zx[k]  / (size - k);
            }
        }

        if (Corr_P_vec.nxy.size() < 10) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(J_current_Vec, RJJ, J_current_Vec.x.size());

            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            //RJJ.njj = FFFilter(RJJ.njj, FFstart);
            if (Corr_P_vec.nxy.size() > (500+25*25)) {
                for (int j=0; j<25; j++)
                    res += coef * 1.0/25 * (std::accumulate(Corr_P_vec.nxy.begin(), Corr_P_vec.nxy.begin()+(500+25*j), 0.0) + 
                                            std::accumulate(Corr_P_vec.nyz.begin(), Corr_P_vec.nyz.begin()+(500+25*j), 0.0) + 
                                            std::accumulate(Corr_P_vec.nzx.begin(), Corr_P_vec.nzx.begin()+(500+25*j), 0.0) );
            } else {
                res = coef * ( std::accumulate(Corr_P_vec.nxy.begin(), Corr_P_vec.nxy.end(), 0.0) +
                               std::accumulate(Corr_P_vec.nyz.begin(), Corr_P_vec.nyz.end(), 0.0) +
                               std::accumulate(Corr_P_vec.nzx.begin(), Corr_P_vec.nzx.end(), 0.0) );
            }
        }
    }

    return res;

    /*
    //Attention!! The correction term below is not correct for grand canonical ensembles, see Annu. Rev. Phys. Chem. 1965. 16:67-102. p.  92
    //Jab_av = props.Volume_tau*(props.Pressure_tau +
    //compute tensors
    //At.jxx = At.vx*At.vx*At.mass + At.x*At.Fx;
    //std::cout<< kb <<" kb"<<"\n";
    //jxx = sum(At.jxx);
    At.jxy = At.vx*At.vy*At.mass + At.x*At.Fy;
    jxy = sum(At.jxy);
    At.jxz = At.vx*At.vz*At.mass + At.x*At.Fz;
    jxz = sum(At.jxz);
    //At.jyy = At.vy*At.vy*At.mass + At.y*At.Fy;
    //jyy = sum(At.jyy);
    //At.jyx = At.vy*At.vx*At.mass + At.y*At.Fx;
    //jyx = sum(At.jyx);
    At.jyz = At.vy*At.vz*At.mass + At.y*At.Fz;
    jyz = sum(At.jyz);
    //At.jzz = At.vz*At.vz*At.mass + At.z*At.Fz;
    //jzz = sum(At.jzz);
    //At.jzx = At.vz*At.vx*At.mass + At.z*At.Fx;
    //jzx = sum(At.jzx);
    //At.jzy = At.vz*At.vy*At.mass + At.z*At.Fy;
    //jzy = sum(At.jzy);
    //At.rf_y = At.y*At.Fy;
    //At.rf_z = At.z*At.Fz;


            //different autocorrelation functions
            //bulk viscosity
            //shear viscosity
            //Cxyxy.push_back(0.0);
            //Cxzxz.push_back(0.0);
            //Cyzyz.push_back(0.0);

        //} else {
            //J_vec.xx.erase(J_vec.xx.begin());
            //J_vec.xy.erase(J_vec.xy.begin());
            //J_vec.xz.erase(J_vec.xz.begin());
            //J_vec.yx.erase(J_vec.yx.begin());
            //J_vec.yy.erase(J_vec.yy.begin());
            //J_vec.yz.erase(J_vec.yz.begin());
            //J_vec.zx.erase(J_vec.zx.begin());
            //J_vec.zy.erase(J_vec.zy.begin());
            //J_vec.zz.erase(J_vec.zz.begin());


            //J_vec.xx.push_back(jxx);
            //J_vec.xy.push_back(jxy);
            //J_vec.xz.push_back(jxz);
            //J_vec.yx.push_back(jyx);
            //J_vec.yy.push_back(jyy);
            //J_vec.yz.push_back(jyz);
            //J_vec.zx.push_back(jzx);
            //J_vec.zy.push_back(jzy);
            //J_vec.zz.push_back(jzz);
        //}

        //clean the autocorr functions
        //start summing up terms
        double tau_max;
        //std::cout<< tau_max<< "  !"<<"\n";


        //dType bulk_vis = 0; //bulk viscosity
        double res = 0.0;
        int n,k,t;
        int size;
        //dType count = 0;
        //dType vbulk_running_integral;
        //std::cout << vbulk_running_integral << "old nu" <<"\n";

    if ( J_vec.xy.size() < (int)(100000/cond.d_tau))
    {
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            J_vec.yz.push_back(jyz);
            Corr_J_vec.Push_back(0.0);
            Cxyxy.push_back(0.0);
            Cxzxz.push_back(0.0);
            Cyzyz.push_back(0.0);

            size = J_vec.xy.size();


    }

    else //erase first member, add a new last member
     {
    J_vec.xy.erase(J_vec.xy.begin());
    J_vec.xz.erase(J_vec.xz.begin());
    J_vec.yz.erase(J_vec.yz.begin());


    J_vec.xy.push_back(jxy);
    J_vec.xz.push_back(jxz);
    J_vec.yz.push_back(jyz);
    }


    std::fill(Cxyxy.begin(), Cxyxy.end(), 0.0);
    std::fill(Cxzxz.begin(), Cxzxz.end(), 0.0);
    std::fill(Cyzyz.begin(), Cyzyz.end(), 0.0);

    //calculate the autocorr functn
    #pragma omp parallel for private(n, k, t)
    for (k = 0; k < size; k++)
     {

        for (t = 0; t < (size - k); t++)
         {
            n = t + k;
            Cxyxy[k]+= (J_vec.xy[n] * J_vec.xy[t]);
            Cxzxz[k]+= (J_vec.xz[n] * J_vec.xz[t]);
            Cyzyz[k] += (J_vec.yz[n] * J_vec.yz[t]);
         }
        //normalize
        Cxyxy[k] = Cxyxy[k] / (size - k);
        Cxzxz[k] = Cxzxz[k] / (size - k);
        Cyzyz[k] = Cyzyz[k] / (size - k);
     }

    std::ofstream write_output("correl_out.dat");
    assert(write_output.is_open());
    for (int n=0; n<J_vec.xy.size(); n=n+10)
    {
        write_output << n << " "<< Cxyxy[n] <<" " <<Cxzxz[n]<<" "<<Cyzyz[n]<<"\n";
    }
    write_output.close();
    */


    //this viscosity script is based on the perturbation method, see J. Chem. Phys. 116, 209 (2002) Berk Hess.
    /*
    //dType jxx;
    dType jxy;
    dType jxz;
    //dType jyy;
    //dType jyx;
    dType jyz;
    //dType jzz;
    //dType jzx;
    //dType jzy;

    //add the perturbation acceleration to each of the particles: a_{x}(z) = Acos(kz) ; A denoted as Amp below:
    dType Amp = 0.1; //nm ps^-2, can be chosen

    //At.ax = At.ax + Amp*cos()

    std::cout<< cos(2*pi)<<"\n";
    At.jxy = At.vx*At.vy*At.mass + At.x*At.Fy;
    jxy = sum(At.jxy);
    At.jxz = At.vx*At.vz*At.mass + At.x*At.Fz;
    jxz = sum(At.jxz);
    //At.jyy = At.vy*At.vy*At.mass + At.y*At.Fy;
    //jyy = sum(At.jyy);
    //At.jyx = At.vy*At.vx*At.mass + At.y*At.Fx;
    //jyx = sum(At.jyx);
    At.jyz = At.vy*At.vz*At.mass + At.y*At.Fz;
    jyz = sum(At.jyz);
    //At.jzz = At.vz*At.vz*At.mass + At.z*At.Fz;
    //jzz = sum(At.jzz);
    //At.jzx = At.vz*At.vx*At.mass + At.z*At.Fx;
    //jzx = sum(At.jzx);
    //At.jzy = At.vz*At.vy*At.mass + At.z*At.Fy;
    //jzy = sum(At.jzy);
    //At.rf_y = At.y*At.Fy;
    //At.rf_z = At.z*At.Fz;

        if ( J_vec.xy.size() < (int)(100000/cond.d_tau)) {
            //J_vec.xx.push_back(jxx);
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            //J_vec.yx.push_back(jyx);
            //J_vec.yy.push_back(jyy);
            J_vec.yz.push_back(jyz);
            //J_vec.zx.push_back(jzx);
            //J_vec.zy.push_back(jzy);
            //J_vec.zz.push_back(jzz);
            Corr_J_vec.Push_back(0.0);

            //different autocorrelation functions
            //bulk viscosity

            //shear viscosity
            Cxyxy.push_back(0.0);
            Cxzxz.push_back(0.0);
            Cyzyz.push_back(0.0);

        } else {
            //J_vec.xx.erase(J_vec.xx.begin());
            J_vec.xy.erase(J_vec.xy.begin());
            J_vec.xz.erase(J_vec.xz.begin());
            //J_vec.yx.erase(J_vec.yx.begin());
            //J_vec.yy.erase(J_vec.yy.begin());
            J_vec.yz.erase(J_vec.yz.begin());
            //J_vec.zx.erase(J_vec.zx.begin());
            //J_vec.zy.erase(J_vec.zy.begin());
            //J_vec.zz.erase(J_vec.zz.begin());


            //J_vec.xx.push_back(jxx);
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            //J_vec.yx.push_back(jyx);
            //J_vec.yy.push_back(jyy);
            J_vec.yz.push_back(jyz);
            //J_vec.zx.push_back(jzx);
            //J_vec.zy.push_back(jzy);
            //J_vec.zz.push_back(jzz);
        }

        //clean the autocorr functions

        //same for shear viscosity autocorr fnctns.
        std::fill(Cxyxy.begin(), Cxyxy.end(), 0.0);
        std::fill(Cxzxz.begin(), Cxzxz.end(), 0.0);
        std::fill(Cyzyz.begin(), Cyzyz.end(), 0.0);


        //start summing up terms
        double tau_max;
        //std::cout<< tau_max<< "  !"<<"\n";


        //dType bulk_vis = 0; //bulk viscosity
        double res = 0.0;
        //dType count = 0;
        //dType vbulk_running_integral;
        //std::cout << vbulk_running_integral << "old nu" <<"\n";
        for (int n=0; n<J_vec.xy.size() ; n++)
        {
            tau_max = 0.0;
            for (int i=0; i<(J_vec.xy.size()-n) ; i++)
            {
                //bulk autocoor. fnctns.

                //shear autocoor. fnctns.
                Cxyxy[n] += J_vec.xy[i]*J_vec.xy[i+n];
                Cxzxz[n] += J_vec.xz[i]*J_vec.xz[i+n];
                Cyzyz[n] += J_vec.yz[i]*J_vec.yz[i+n];
                tau_max++;
            }

            Cxyxy[n] /= tau_max;
            Cxzxz[n] /= tau_max;
            Cyzyz[n] /= tau_max;
        }

        //correl_intgrnd = cond.d_tau*(Cxxxx + Cxxyy + Cxxzz + Cyyxx + Cyyyy + Cyyzz + Czzxx + Czzyy + Czzzz); //value of integrand at time tau_max
        shear_intgrnd = cond.d_tau*(Cxyxy + Cxzxz + Cyzyz);

        for (int n=0; n< shear_intgrnd.size(); n++)
        {
            //bulk_vis += correl_intgrnd[n]; // ev^2/fs
            res += shear_intgrnd[n];

        }
        //final integrals:
        //bulk_vis = 1.0/(9.0*kb*props.Volume_tau*props.Temperature)*bulk_vis;
        res = 1.0/(3.0*kb*props.Volume_tau*props.Temperature)*res;
        //switch units
        //bulk_vis = bulk_vis*1.60218e-4;
        res = res*1.60218e-4;

        //J_vec.bv.push_back(bulk_vis);
        //vbulk_running_integral = sum(J_vec.zz);
        //std:: cout << shear_vis << "\n";
        //std:: cout << Czzzz.size() << "\n";
        */



    /*
    dType jxx;
    dType jxy;
    dType jxz;
    dType jyy;
    dType jyx;
    dType jyz;
    dType jzz;
    dType jzx;
    dType jzy;

    //Einstein relation approach
    At.jxy = At.x*At.vy*At.mass;
    jxy = sum(At.jxy);
    At.jxz = At.x*At.vz*At.mass;
    jxz = sum(At.jxz);
    At.jyz = At.y*At.vz*At.mass;
    jyz = sum(At.jyz);

    int lag=50000;

    if ( P_vec.xy.size() < lag ) {
        P_vec.xy.push_back(jxy);
        P_vec.xz.push_back(jxz);
        P_vec.yz.push_back(jyz);
        //Corr_P_vec.Push_back(0.0);
        P_vec.Cxyxy.push_back(0.0);
        P_vec.Cxzxz.push_back(0.0);
        P_vec.Cyzyz.push_back(0.0);
    } else {
        P_vec.xy.erase(P_vec.xy.begin());
        P_vec.xz.erase(P_vec.xz.begin());
        P_vec.yz.erase(P_vec.yz.begin());
        P_vec.xy.push_back(jxy);
        P_vec.xz.push_back(jxz);
        P_vec.yz.push_back(jyz);
    }

    //clean vector that will hold Pxy(t) values
    std::fill(P_vec.Cxyxy.begin(), P_vec.Cxyxy.end(), 0.0);
    std::fill(P_vec.Cxzxz.begin(), P_vec.Cxzxz.end(), 0.0);
    std::fill(P_vec.Cyzyz.begin(), P_vec.Cyzyz.end(), 0.0);
    P_vec.sum =0.0;
    double obs_time = P_vec.xy.size()*cond.d_tau;
    //if (J_vec.xy.size() >= (int)(1000/cond.d_tau)){ //make sure enough time origins are collected
    if (P_vec.xy.size() >= 20) { //make sure enough time origins are collected
        P_vec.Cxyxy = pow2d(P_vec.xy - P_vec.xy[0]);
        P_vec.Cxzxz = pow2d(P_vec.xz - P_vec.xz[0]);
        P_vec.Cyzyz = pow2d(P_vec.yz - P_vec.yz[0]);
        P_vec.sum = sum(P_vec.Cxyxy + P_vec.Cxzxz + P_vec.Cyzyz);

        //res=res/P_vec.xy.size(); //final average over nt0 time steps
        P_vec.sum = P_vec.sum/At.size; //final average over nt0 time steps
        P_vec.sum = 1.0/(6.0 * kb * props.Volume_tau * props.Temperature * obs_time) * P_vec.sum;
        //switch units
        P_vec.sum = P_vec.sum*1.60218e-4;
    }
    return P_vec.sum;
    */
}

dType Properties::CalcIsotermalCompressibility() {
    dType res, VFl, Vol2tau, b, sc, Volume2;
    iType in, i;
    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    Vol2tau = props.Volume_tau * props.Volume_tau;
    Volume2 += sc * (Vol2tau - Volume2);

    VFl = (Volume2 - props.Volume * props.Volume);

    res =  - 1.0 / (kb * props.Temperature * props.Temperature * props.Volume)  *  VFl;
    return res;
}

dType Properties::CalcThermalConductivity() {
    double Cond_TermX, Cond_TermY, Cond_TermZ, Conv_TermX, Conv_TermY, Conv_TermZ;
    double wn, res, TermX, TermY, TermZ;

    double w1, val, coef, E, sum;
    int J_i, Nm, ind, i, m, in, icl, cli, cl;
    int n, k, t, size;

    std::vector<dType> idData, njj;
    idData.resize(inf.dNP, 0);
    res = 0.0;

    if (( inf.Time >= 5.0) && control.thermc_b) {
        coef = cond.d_tau / (3 * props.Volume * kb * props.Temperature * props.Temperature) * 1602176;

        Conv_TermX = 0.0;
        Conv_TermY = 0.0;
        Conv_TermZ = 0.0;
        Cond_TermX = 0.0;
        Cond_TermY = 0.0;
        Cond_TermZ = 0.0;

        #pragma omp parallel for private(i, in, icl, cl, E)  \
                                 reduction(+:Conv_TermX, Conv_TermY, Conv_TermZ) \
                                 reduction(+:Cond_TermX, Cond_TermY, Cond_TermZ)
        for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
            cl = Domain[inf.dID].cellI[icl];
            for (in = 0; in < Cell[cl].atomI.size(); in++) {
                i = Cell[cl].atomI[in];

                Cond_TermX += At.condx[i];
                Cond_TermY += At.condy[i];
                Cond_TermZ += At.condz[i];

                E = At.KinEnergy_tau[i] + 0.5 * At.PotEnergy_tau[i];

                Conv_TermX += E * At.vx[i];
                Conv_TermY += E * At.vy[i];
                Conv_TermZ += E * At.vz[i];
            }
        }

        TermX = Conv_TermX + 0.5 * Cond_TermX;
        TermY = Conv_TermY + 0.5 * Cond_TermY;
        TermZ = Conv_TermZ + 0.5 * Cond_TermZ;

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermX = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZ = std::accumulate(idData.begin(), idData.end(), 0.0);
        }

        if ( J_current_Vec.x.size() < (int)(100000/cond.d_tau) ) {
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);
            RJJ.Push_back(0.0);

            size = J_current_Vec.x.size();
            t = size - 1;

            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        } else {
            size = J_current_Vec.x.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                RJJ.jj[k] -= (J_current_Vec.x[n] * J_current_Vec.x[t] +
                                    J_current_Vec.y[n] * J_current_Vec.y[t] +
                                    J_current_Vec.z[n] * J_current_Vec.z[t]);
            }

            J_current_Vec.x.erase(J_current_Vec.x.begin());
            J_current_Vec.y.erase(J_current_Vec.y.begin());
            J_current_Vec.z.erase(J_current_Vec.z.begin());
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        }

        if (RJJ.njj.size() < 200) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(J_current_Vec, RJJ, J_current_Vec.x.size());
            /*
            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            //njj = FFFilter(RJJ.njj, FFstart);
            if (RJJ.njj.size() > (500+25*25)) {
                for (int j=0; j<25; j++)
                    res += coef * 1.0/25 * std::accumulate(RJJ.njj.begin(), RJJ.njj.begin()+(500+25*j), 0.0);
            } else {
                res = coef * std::accumulate(RJJ.njj.begin(), RJJ.njj.end(), 0.0);
            }
            */
            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            njj  = RJJ.njj; //FFFilter(RJJ.njj, 1.0);
            size = njj.size();

            int from, to, step;
            if (njj.size() > (100000)) {
                from = int(100000*0.25);
                to   = int(100000*0.50);
                step = int((to - from) / 25);
            } else {
                from = int(size*0.25);
                to   = int(size*0.50);
                step = int((to - from) / 25);
            }

            for (int j=0; j<25; j++)
                res += coef * 1.0/25 * std::accumulate(njj.begin(), njj.begin()+(from+step*j), 0.0);
        }
    }
    return res;
}

dType Properties::CalcDiffusion(Atom& At) {
    double TermX, TermY, TermZ;
    double wn;
    double res;

    double w1, val, coef, E, sum;
    int J_i, Nm, ind, i, m, in, icl, cli, cl;
    int n, k, t, size, FFcount = 25;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);
    res = 0.0;

    // 1.0e-1 is a convertion coefficient from A2/fs to cm2/s
    coef = 1.0e-1; //kb * props.Temperature cond.d_tau / (3 * props.Volume * kb * props.Temperature * props.Temperature) * 1602176;
    if (( inf.Time >= cond.StartThC) && (control.thermc_b)) {

        TermX = std::accumulate(At.vx.begin(), At.vx.end(), 0.0 );
        TermY = std::accumulate(At.vy.begin(), At.vy.end(), 0.0 );
        TermZ = std::accumulate(At.vz.begin(), At.vz.end(), 0.0 );

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermX = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZ = std::accumulate(idData.begin(), idData.end(), 0.0);
        }

        if ( Velocity_Vec.x.size() < (int)(250000/cond.d_tau)) {
            Velocity_Vec.x.push_back(TermX);
            Velocity_Vec.y.push_back(TermY);
            Velocity_Vec.z.push_back(TermZ);
            Rvv.Push_back(0.0);

            size = Velocity_Vec.x.size();

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                Rvv.jj[k] += (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                     Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                     Velocity_Vec.z[n] * Velocity_Vec.z[t]);
                Rvv.njj[k] = Rvv.jj[k]  / (size - k);
            }
        } else {

            size = Velocity_Vec.x.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                Rvv.jj[k] -= (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                    Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                    Velocity_Vec.z[n] * Velocity_Vec.z[t]);
            }

            Velocity_Vec.x.erase(Velocity_Vec.x.begin());
            Velocity_Vec.y.erase(Velocity_Vec.y.begin());
            Velocity_Vec.z.erase(Velocity_Vec.z.begin());
            Velocity_Vec.x.push_back(TermX);
            Velocity_Vec.y.push_back(TermY);
            Velocity_Vec.z.push_back(TermZ);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                Rvv.jj[k] += (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                      Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                      Velocity_Vec.z[n] * Velocity_Vec.z[t]);
                Rvv.njj[k] = Rvv.jj[k]  / (size - k);
            }
        }

        if (Rvv.njj.size() < 10) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(Velocity_Vec, Rvv, Velocity_Vec.x.size());
            if (Rvv.njj.size() > (500+25*FFcount)) {
                for (int j=0; j<FFcount; j++)
                    res += coef * 1.0/FFcount * std::accumulate(Rvv.njj.begin(), Rvv.njj.begin()+(500+25*j), 0.0);
            } else {
                res = coef * std::accumulate(Rvv.njj.begin(), Rvv.njj.end(), 0.0);
            }
        }
    }
    return res;
}

std::vector<dType> Properties::Autocorrelation(XYZ_vec& jvec, int size) {
    int n, m, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    #pragma omp parallel for private(n, m, k, t, val)
    for (k = 0; k < size; k++) {
        val = 0.0;

        for (t = 0; t < (size - k); t++) {
            n = t + k;
            val += (jvec.x[t] * jvec.x[n] +
                         jvec.y[t] * jvec.y[n] +
                         jvec.z[t] * jvec.z[n]);
        }
        res[k] = val / (size - k);
    }
    return res;
}

std::vector<dType> Properties::FFFilter(std::vector<dType>& InVec, double freq) {
    int i, Nm;
    vector<dType> data;
    int nn = 1;
    int nn2 = nn * 2;

    nn2 = 65536;
    nn  = 32768;
    vector<dType> dataout(nn);

    data.resize(nn2);
    //#pragma omp parallel for private(i)
    for ( i=0; i<nn; i++ ) {
        if (i < InVec.size()) {
            data[i * 2]     = InVec[i];
            data[i * 2 + 1] = 0.0;
        } else {
            data[i * 2] = 0.0;
            data[i * 2 + 1] = 0.0;
        }
    }

    funcs::FFT( data );

    Nm = (int)(freq * (nn * cond.d_tau) + 1);
    for (i = 0; i < nn; i++)  {
        if ((i >= (Nm + 1)) && (i <= (nn - Nm))) {
            data[i * 2] = 0.0;
            data[i * 2 + 1] = 0.0;
        }
    }

    funcs::rFFT( data );

    for ( i = 0; i < nn; i++ ) {
        dataout[i]  = data[i * 2] / nn;
    }
    return dataout;
}

void Properties::AutocorrelationCumm(XYZ_vec& jvec, Corr_struct& RJJ, int size) {
    int n, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    t = size - 1;

    #pragma omp parallel for private(n, k)
    for (k = 0; k < size; k++) {
        n = size - k - 1;
        RJJ.jj[k] += (jvec.x[n] * jvec.x[t] +
                             jvec.y[n] * jvec.y[t] +
                             jvec.z[n] * jvec.z[t]);
        RJJ.njj[k] = RJJ.jj[k]  / (size - k);
    }
}

void Properties::AutocorrelationMov(XYZ_vec& jvec, Corr_struct& RJJ, int size) {
    int n, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    t = size - 1;

    #pragma omp parallel for private(n, k)
    for (k = 0; k < size; k++) {
        n = size - k - 1;
        RJJ.jj[k] += (jvec.x[n] * jvec.x[t] +
                             jvec.y[n] * jvec.y[t] +
                             jvec.z[n] * jvec.z[t]);
        RJJ.njj[k] = RJJ.jj[k]  / (size - k);
    }
}