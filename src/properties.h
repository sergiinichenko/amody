//-------------------------------------------------------------------
// File:   properties.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#pragma once

#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <ctime>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>
#include <map>
#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <cmath>
#include <algorithm>
#include <numeric>
#include <valarray>
#include "math/neural.h"
#include "math/fad.h"
#include "math/funcs.h"
#include "math/vector.h"
#include "datatypes.h"
#include "constants.h"


class Properties {
public:
	dType Temperature, Temperature_tau, Pressure, Pressure_tau;
	dType Diffusion, Diffusion_tau, SpecificHeat, SpecificHeat_tau;
	dType Density, Density_tau, Viscosity, Viscosity_tau, ThermCond, ThermCond_tau;
	dType Volume, Volume_tau, IntEnergy, IntEnergy_tau, Energy, Energy_tau;
	dType GibbsEnergy, GibbsEnergy_tau, Enthalpy, Enthalpy_tau;
    dType PotEnergy, PotEnergy_tau, KinEnergy, KinEnergy_tau;
    dType FreeEnergy_tau, FreeEnergy, Entropy_tau, Entropy, ICompress_tau, ICompress;
    dType MSD, MSD_tau;
    dType Summ_of_PotEnergy;
    dType CohesiveEnergy, CohesiveEnergy_tau;

    // -----------------  Neural Network data set        --------------------
    Fit fit;
    Properties props;
    Controls control;
    Conditions cond;
    Files files;
    Tables tab;
    Information inf;
    Net net;

    std::map<std::string, Params> params;
    std::map<std::string, DETparams> detpar;

    //Diffusion Data
    Corr_struct Rvv;
    XYZ_vec Velocity_Vec;


    // Thermal Conductivity Data
    Corr_struct RJJ;
    XYZ_vec J_current_Vec;

    // Viscosity calculation
    Visc_Struct P_vec;
    Corr_struct Corr_P_vec;

    std::vector<rdfPair> RDFfor;
    std::string InfLevel;

    Atom At;

    rdfstruct RDF;
    rdfstruct RDFref;

    std::map<std::string, dEq > lup_dEq;
    std::map< std::string, StateStruct > States;

    Properties();
    void Reset();

    void RegVelocity();
    void RegPosition();
    void RegTemperature();
    void RegPressure();
    void PremeltR();
    void PremeltL();

    void GetProperties();
    void RadialDistFunc();
    dType CalcViscosity();
    dType CalcThermalConductivity();
    dType CalcDiffusion();
    dType CalcSpecificHeat();
    dType CalcIsotermalCompressibility();
    void CalcThCData();
    void PhonoDensityOfSate();
    void AverageProperties();
    std::vector<dType> Autocorrelation(XYZ_vec& jvec, int end);
    void AutocorrelationCumm(XYZ_vec& jvec, Corr_struct& RJJ, int size);
    void AutocorrelationMov(XYZ_vec& jvec, Corr_struct& RJJ, int size);
    std::vector<dType> FFFilter(std::vector<dType>& jvec, double freq);
};

#endif