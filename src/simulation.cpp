//-------------------------------------------------------------------
// File:   simulation.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

//#include <chrono>
#include "math/fad.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include "datamanager.h"
#include "simulation.h"
#include "system.h"
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <ctime>
#ifdef _WIN32
    #include <windows.h>
 #endif

using namespace std;

using std::map;
using std::string;
using std::endl;
using std::cout;

Simulation::Simulation() {
    Sys.props.Reset();
    Sys.fit.Reset();
    Sys.inf.Time        = 0;
}

void Simulation::TestScript() {



}

void Simulation::BCastParams(System &Sys) {
    int i, j;
    const int root=0;

    vector<map<std::string, Params> >::iterator iter_pop;
    map<std::string, Params>::iterator iter;

    if (Sys.inf.num_procs > 1) {

        // The Main process: id = 0
        if (Sys.inf.id == 0) {
            // Copy the data to AtData array
            i = 0;
            for (iter_pop = Sys.Coeffs.begin(); iter_pop != Sys.Coeffs.end(); ++iter_pop){
                j = 0;
                for (iter = (*iter_pop).begin(); iter != (*iter_pop).end(); ++iter){

                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 0] = (iter->second).q;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 2] = (iter->second).a;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 3] = (iter->second).b;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 6] = (iter->second).DM;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 7] = (iter->second).alphaM;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 8] = (iter->second).r;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 11]= (iter->second).mass;

                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 12]= (iter->second).rho0;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 13]= (iter->second).beta;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 14]= (iter->second).re;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 15]= (iter->second).C;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 16]= (iter->second).D;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 17]= (iter->second).polariz;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 18]= (iter->second).polb;
                    Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 19]= (iter->second).polc;
                    j++;
                }
                i++;
            }
        }

        MPI_Bcast(&Sys.ParData.front(), Sys.ParData.size(), mpiType, root, MPI_COMM_WORLD);

        // The daughter processes
        if (Sys.inf.id != 0) {
            i = 0;
            for (iter_pop = Sys.Coeffs.begin(); iter_pop != Sys.Coeffs.end(); ++iter_pop) {
                j = 0;
                for (iter = (*iter_pop).begin(); iter != (*iter_pop).end(); ++iter){
                    (iter->second).q    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 0];
                    (iter->second).zn   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 1];
                    (iter->second).a    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 2];
                    (iter->second).b    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 3];
                    (iter->second).C6   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 4];
                    (iter->second).C8   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 5];
                    (iter->second).DM   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 6];
                    (iter->second).alphaM = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 7];
                    (iter->second).r    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 8];
                    (iter->second).b6   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 9];
                    (iter->second).b8   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 10];
                    (iter->second).mass = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 11];

                    (iter->second).rho0 = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 12];
                    (iter->second).beta = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 13];
                    (iter->second).re   = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 14];
                    (iter->second).C    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 15];
                    (iter->second).D    = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 16];
                    (iter->second).polariz = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 17];
                    (iter->second).polb = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 18];
                    (iter->second).polc = Sys.ParData[i*Sys.inf.SubsN*20 + j*20 + 19];

                    j++;
                }
                i++;
            }
        }
    }
}

void Simulation::GetParams(System &Sys) {
    dType fit, data;
    MPI_Status status;

    std::vector<dType> idData;
    idData.resize(Sys.inf.num_procs, 0);

    if (Sys.inf.num_procs > 1) {
      assert(Sys.inf.num_procs == Sys.Coeffs.size() && "The total number of processes should be equal to the Sys.Coeffs size");
      data = Sys.Coeffs[Sys.inf.id].begin()->second.fitres;

      MPI_Allgather(&data, 1, mpiType,
                    &idData.front(), 1, mpiType, MPI_COMM_WORLD);

      for (int np = 0; np!= Sys.inf.num_procs; np++) {
          Sys.Coeffs[np].begin()->second.fitres = idData[np];
      }

      MPI_Allgather(&Sys.props.Temperature, 1, mpiType,
                    &idData.front(), 1, mpiType, MPI_COMM_WORLD);

      for (int np = 0; np!= Sys.inf.num_procs; np++) {
          Sys.Coeffs[np].begin()->second.temperature = idData[np];
      }

      MPI_Allgather(&Sys.props.Pressure, 1, mpiType,
                    &idData.front(), 1, mpiType, MPI_COMM_WORLD);

      for (int np = 0; np!= Sys.inf.num_procs; np++) {
          Sys.Coeffs[np].begin()->second.pressure = idData[np];
      }

      MPI_Allgather(&Sys.props.Density, 1, mpiType,
                    &idData.front(), 1, mpiType, MPI_COMM_WORLD);

      for (int np = 0; np!= Sys.inf.num_procs; np++) {
          Sys.Coeffs[np].begin()->second.density = idData[np];
      }

      MPI_Allgather(&Sys.props.Energy, 1, mpiType,
                    &idData.front(), 1, mpiType, MPI_COMM_WORLD);

      for (int np = 0; np!= Sys.inf.num_procs; np++) {
          Sys.Coeffs[np].begin()->second.energy = idData[np];
      }
    }

}

void Simulation::MDStep(System &Sys) {
    double TimeSt = 0.0;
    double TimeSts = 0.0;
    struct timeval t1, t2;
    double t_el;
    t1 = start_t();
    Sys.FindPairs();

    #if(debug)
        std::cout << "id : " << Sys.inf.id << "    MDStep  FindPairs : " << std::endl;
    #endif
    #if(timing)
        std::cout << "id : " << Sys.inf.id << "    FindPairs : " << elapsed_t(t1) << std::endl;
    #endif

    if (Sys.control.meam_b && Sys.cond.d_tau != Sys.cond.d_taus)  {
        while (TimeSt < Sys.cond.TimeStep)  {
          TimeSts = 0.0;
          Sys.ResetForces("p");
            while (TimeSts < Sys.cond.d_tau) {
                Sys.ResetForces("s");
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    ResetForces : " << elapsed_t(t1) << std::endl;
                #endif
                if (Sys.control.posupdate_b)  Sys.NewPositions("s");
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    NewPositions : " << elapsed_t(t1) << std::endl;
                #endif
                if (Sys.control.periodic_b)  Sys.PeriodicConditions();
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    PeriodicConditions : " << elapsed_t(t1) << std::endl;
                #endif

                Sys.ExchangeData();
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    ExchangeData : " << elapsed_t(t1) << std::endl;
                #endif

                Sys.UpdateCells();
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    UpdateCells : " << elapsed_t(t1) << std::endl;
                #endif

                if (Sys.control.box_b)      Sys.BoxConditions();
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    BoxConditions : " << elapsed_t(t1) << std::endl;
                #endif

                Sys.ForceField();
                #if(timing)
                    std::cout << "id : " << Sys.inf.id << "    ForceField : " << elapsed_t(t1) << std::endl;
                #endif

                Sys.UpdateVelocity("s");
                if (Sys.control.temperature_b)       Sys.RegTemperature();

                TimeSts  += Sys.cond.d_taus;
                Sys.inf.Time += Sys.cond.d_taus;
                TimeSt   += Sys.cond.d_taus;
            }
            if (Sys.control.velocity_b)     Sys.RegVelocity();
            if (Sys.control.position_b)     Sys.RegPosition();
            if (Sys.control.properties_b)    Sys.GetProperties();
            if (Sys.control.pressure_b)       Sys.RegPressure();
            Sys.AverageProperties();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    Reg + Prop : " << elapsed_t(t1) << std::endl;
            #endif

            t1 = start_t();
            if (Sys.control.det_b)      Sys.DETStep();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    DETStep : " << elapsed_t(t1) << std::endl;
            #endif

        }
    } else {

        while (TimeSt < Sys.cond.TimeStep) {
            t1 = start_t();
            Sys.ResetForces("n");
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    ResetForces : " << elapsed_t(t1) << std::endl;
            #endif
            if (Sys.control.posupdate_b)  Sys.NewPositions("n");
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    NewPositions : " << elapsed_t(t1) << std::endl;
            #endif
            if (Sys.control.periodic_b)  Sys.PeriodicConditions();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    PeriodicConditions : " << elapsed_t(t1) << std::endl;
            #endif

            Sys.ExchangeData();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    ExchangeData : " << elapsed_t(t1) << std::endl;
            #endif

            Sys.UpdateCells();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    UpdateCells : " << elapsed_t(t1) << std::endl;
            #endif

            if (Sys.control.box_b)      Sys.BoxConditions();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    BoxConditions : " << elapsed_t(t1) << std::endl;
            #endif


            Sys.ForceField();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    ForceField : " << elapsed_t(t1) << std::endl;
            #endif

            Sys.UpdateVelocity("n");
            if (Sys.control.velocity_b)     Sys.RegVelocity();
            if (Sys.control.position_b)     Sys.RegPosition();
            if (Sys.control.properties_b)   Sys.GetProperties();
            if (Sys.control.temperature_b)  Sys.RegTemperature();
            if (Sys.control.pressure_b)     Sys.RegPressure();
            Sys.AverageProperties();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    Reg + Prop : " << elapsed_t(t1) << std::endl;
            #endif

            if (Sys.control.det_b)      Sys.DETStep();
            #if(timing)
                std::cout << "id : " << Sys.inf.id << "    DETStep : " << elapsed_t(t1) << std::endl;
            #endif

            Sys.inf.Time += Sys.cond.d_tau;
            TimeSt   += Sys.cond.d_tau;
        }
    }
    t1 = start_t();
    if (Sys.control.rdf_b)    Sys.RadialDistFunc();
    #if(timing)
        std::cout << "id : " << Sys.inf.id << "    RadialDistFunc : " << elapsed_t(t1) << std::endl;
    #endif

    t1 = start_t();
    if (Sys.control.posanalysis_b)  PositionAnalysis(Sys);
    #if(timing)
        std::cout << "id : " <<Sys.inf.id << "    PositionAnalysis : " << elapsed_t(t1) << std::endl;
    #endif
}

void Simulation::PremeltRegion(System &Sys) {
    double TimeSt = 0.0;
    struct timeval t1, t2;

    double a,b;

    a = Sys.cond.T_rel;
    b = Sys.cond.T_rel_fin;
    Sys.cond.T_rel = 2.0;
    Sys.cond.T_rel_fin = 3.0;

    Sys.inf.Time = 0.0;

    t1 = start_t();
    Sys.FindPairs();
    #if(debug)
        std::cout << "id : " << Sys.inf.id << "  PremeltRegion  FindPairs : " << Sys.timeStr() << std::endl;
    #endif
    #if(timing)
        std::cout << "id : " << Sys.inf.id << "  PremeltRegion    FindPairs : " << elapsed_t(t1) << std::endl;
    #endif

    while (TimeSt < Sys.cond.TimePremelt) {
        t1 = start_t();
        Sys.ResetForces("n");
        if (Sys.control.posupdate_b)  Sys.NewPositions("n");
        if (Sys.control.periodic_b)  Sys.PeriodicConditions();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    PeriodicConditions : " << elapsed_t(t1) << std::endl;
        #endif

        t1 = start_t();
        Sys.ExchangeData();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    ExchangeData : " << elapsed_t(t1) << std::endl;
        #endif

        t1 = start_t();
        Sys.UpdateCells();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    UpdateCells : " << elapsed_t(t1) << std::endl;
        #endif

        t1 = start_t();
        if (Sys.control.box_b)      Sys.BoxConditions();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    BoxConditions : " << elapsed_t(t1) << std::endl;
        #endif

        t1 = start_t();
        Sys.ForceField();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    ForceField : " << elapsed_t(t1) << std::endl;
        #endif

        t1 = start_t();
        Sys.UpdateVelocity("n");
        if (Sys.control.velocity_b)     Sys.RegVelocity();
        if (Sys.control.position_b)     Sys.RegPosition();
        if (Sys.control.properties_b)    Sys.GetProperties();
        if (Sys.control.premeltR_b)  Sys.PremeltR();
        if (Sys.control.premeltL_b)  Sys.PremeltL();
        if (Sys.control.pressure_b)       Sys.RegPressure();
        Sys.AverageProperties();
        #if(timing)
            std::cout << "id : " << Sys.inf.id << "  PremeltRegion    Reg + Prop : " << elapsed_t(t1) << std::endl;
        #endif

        Sys.inf.Time += Sys.cond.d_tau;
        TimeSt    += Sys.cond.d_tau;
    }

    Dat.ShowResults(Sys);
    Dat.SaveXYZFile(Sys);

    Sys.inf.Time = 0;
    Sys.cond.T_rel = a;
    Sys.cond.T_rel_fin = b;
}

void Simulation::Premelt(System &Sys) {
    int cnt;
    Sys.inf.Time = 0.0;
    cnt  = (int)( Sys.cond.TimePremelt / Sys.cond.d_tau );
    for (int i=0; i < cnt; i++)
    {
        if (Sys.control.posupdate_b)      Sys.NewPositions("n");
;
        if (Sys.control.periodic_b)     Sys.PeriodicConditions();
        if (Sys.control.box_b)          Sys.BoxConditions();

        Sys.UpdateCells();
        Sys.ResetForces("n");
        Sys.ForceField();
        Sys.UpdateVelocity("n");

        if (Sys.control.velocity_b)     Sys.RegVelocity();
        if (Sys.control.position_b)     Sys.RegPosition();
        if (Sys.control.properties_b)    Sys.GetProperties();
        Premelt10(Sys);
        if (Sys.control.pressure_b)       Sys.RegPressure();
        Sys.AverageProperties();
        Sys.inf.Time += Sys.cond.d_tau;
    }
    Dat.ShowResults(Sys);
    Dat.SaveXYZFile(Sys);

     Sys.inf.Time = 0;
}

void Simulation::Premelt05(System &Sys) {
    double dt_tT, ScT;
    double Z, zmin, zmax;
    int i;

    zmin = 0.0;
    zmax =  Sys.inf.Cell.c;

    Z  = (zmin + zmax)*0.5;

    // --------------------   Temperature regulation   --------------
    dt_tT = Sys.cond.T_rel + 1.0 / (1.0 + std::exp(-0.2 *  Sys.inf.Time*1.0e3 + 5.0)) * (Sys.cond.T_rel_fin - Sys.cond.T_rel) - (1.0 / (1.0 + 148.41316) * (Sys.cond.T_rel_fin - Sys.cond.T_rel));
    ScT = std::sqrt(1.0 + 1.0 / dt_tT * (Sys.cond.Treg1 / Sys.props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i)
    for (i=0; i< Sys.At.size; i++) {
        if (Sys.At.z[i] <= Z) {
            Sys.At.vx[i] *= ScT;
            Sys.At.vy[i] *= ScT;
            Sys.At.vz[i] *= ScT;
        }
    }

    ScT = std::sqrt(1.0 + 1.0 / dt_tT * (Sys.cond.Treg2 / Sys.props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i)
    for (i=0; i< Sys.At.size; i++)
    {
        if (Sys.At.z[i] > Z){
            Sys.At.vx[i] *= ScT;
            Sys.At.vy[i] *= ScT;
            Sys.At.vz[i] *= ScT;
        }
    }
}

void Simulation::Premelt10(System &Sys) {
    double dt_tT, ScT;
    int i;
    // --------------------   Temperature regulation   --------------
    dt_tT = Sys.cond.T_rel + 1.0 / (1.0 + std::exp(-0.2 *  Sys.inf.Time*1.0e3 + 5.0)) * (Sys.cond.T_rel_fin - Sys.cond.T_rel) - (1.0 / (1.0 + 148.41316) * (Sys.cond.T_rel_fin - Sys.cond.T_rel));
    ScT = std::sqrt(1.0 + 1.0 / dt_tT * (Sys.cond.Treg2 / Sys.props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i)
    for (i = 0; i <  Sys.At.size; i++)
    {
        // --- Temperature regulation ----
        Sys.At.vx[i] *= ScT;
        Sys.At.vy[i] *= ScT;
        Sys.At.vz[i] *= ScT;
    }
}


void Simulation::PositionAnalysis(System &Sys) {
    int cl, k, im, cl2, n, j, Ncl, Ncl2, indi, indj, i, iran;
    dType R_sc2, rr, dev;
    dType R_sc, rR_sc;
    dType Rc_short2;
    dType Rx, Ry, Rz, totN, rrlim, devN, neigx, neigy, neigz;

    cond.Rc_short2 = Sys.cond.Rc_short * Sys.cond.Rc_short;

    #pragma omp parallel for schedule(dynamic, 1) private(i, iran, cl, k, im, cl2, n, j, R_sc2, Ncl, Ncl2)\
                             private(R_sc, rR_sc, Rx, Ry, Rz, indi, indj, totN, rr, rrlim, devN, neigx, neigy, neigz)
    for (i = 0; i < Sys.At.size; ++i) {

        neigx    = 0.0;
        neigy    = 0.0;
        neigz    = 0.0;

        Sys.At.neigmass[i] = 0.0;
        Sys.At.neigq[i]    = 0.0;
        Sys.At.StDevN[i]   = 0.0;
        Sys.At.CoordN[i]   = 0.0;
        Sys.At.Displ[i]    = 0.0;

        totN     = 0.0;
        devN     = 0.0;
        dev      = 0.0;

        cl  = Sys.At.Cell[i];
        Ncl = Sys.Pairs[cl].size();
        rR_sc  = 0.0;
        for (k = 0; k < Ncl; k++)  {
            im      = Sys.Pairs[cl][k].im;
            cl2     = Sys.Pairs[cl][k].cl;
            Ncl2    = Sys.Cell[cl2].atomI.size(); //Sys.NofAtomsinCell[cl2];

            for (n = 0; n < Ncl2; n++)  {
                j = Sys.Cell[cl2].atomI[n];

                Rx = (Sys.At.x[i] - (Sys.At.x[j] + Sys.inf.ImPos[im].x));
                if (Rx >  Sys.cond.Rc_long) continue;
                Ry = (Sys.At.y[i] - (Sys.At.y[j] + Sys.inf.ImPos[im].y));
                if (Ry >  Sys.cond.Rc_long) continue;
                Rz = (Sys.At.z[i] - (Sys.At.z[j] + Sys.inf.ImPos[im].z));
                if (Rz >  Sys.cond.Rc_long) continue;

                R_sc2 = (Rx * Rx + Ry * Ry + Rz * Rz);

                if (R_sc2 <=  cond.Rc_short2 && R_sc2 > 0.0)  {
                    totN += 1.0;
                    neigx += Rx;
                    neigy += Ry;
                    neigz += Rz;

                    Sys.At.neigmass[i] += Sys.At.mass[i];
                    Sys.At.neigq[i]    += Sys.At.q[i];

                    rr = Sys.At.rmin[i]; //Sys.At.r + (*Atj).r;

                    R_sc  = std::sqrt(R_sc2);
                    rrlim = 1.25 * Sys.At.rmin[i];

                    if (R_sc <= rrlim){
                        dev  += (rr - R_sc) * (rr - R_sc);
                        devN += 1.0;
                        Sys.At.CoordN[i]  += 1.0;
                    }

                }
            }
        }

        Sys.At.Displ[i]  = std::sqrt((Sys.At.x[i] - Sys.At.xav[i]) * (Sys.At.x[i] - Sys.At.xav[i]) +
                                     (Sys.At.y[i] - Sys.At.yav[i]) * (Sys.At.y[i] - Sys.At.yav[i]) +
                                     (Sys.At.z[i] - Sys.At.zav[i]) * (Sys.At.z[i] - Sys.At.zav[i]));
        Sys.At.TotRsc[i] = std::sqrt(neigx * neigx + neigy * neigy + neigz * neigz);

        if (devN > 0) {
            Sys.At.StDevN[i]   = dev / devN;
        }
    }
}

void Simulation::RunNeuralNet(System &Sys) {
    std::vector<double> inputdata;
    std::vector<double> outputdata;
    std::vector<double> resultVals;

    for (unsigned i = 0; i < Sys.net.Sample.size(); ++i) {
        inputdata  = Sys.net.Sample[i].InData  / Sys.net.getScale();

        Sys.net.showVectorVals("Inputs:", Sys.net.Sample[i].InData);
        Sys.net.feedForward(inputdata);

        // Collect the net's actual output results:
        Sys.net.getResults(resultVals);
        resultVals = resultVals * Sys.net.getScale();
        Sys.net.Sample[i].OutData = resultVals;
        Sys.net.showVectorVals("Outputs:", resultVals);
    }
    Dat.SaveNetResults(Sys);
}


void Simulation::RunNeuralNetFit(System &Sys) {
    unsigned iterNum = Sys.net.getIterations();
    std::vector<double> inputdata;
    std::vector<double> resultVals;
    std::vector<double> outputdata;
    double error;
    #if(debug)
        std::cout << "id : " << Sys.inf.id << "    Define input / output data : " << Sys.timeStr() << std::endl;
    #endif

    for (unsigned n = 0; n < iterNum; ++n) {
        for (unsigned step = 0; step < 100 ; ++step) {
            error = 0.0;
            for (unsigned i = 0; i < Sys.net.Sample.size(); ++i) {

                inputdata  = Sys.net.Sample[i].InData  / Sys.net.getScale();
                outputdata = Sys.net.Sample[i].OutData / Sys.net.getScale();
                #if(debug)
                    std::cout << "id : " << Sys.inf.id << "    topology : " << Sys.net.m_layers.size() <<  std::endl;
                    std::cout << "id : " << Sys.inf.id << "    inputdata : " << inputdata.size() << std::endl;
                    std::cout << "id : " << Sys.inf.id << "    outputdata : " << outputdata.size() << std::endl;
                #endif

                Sys.net.feedForward(inputdata);
                #if(debug)
                    std::cout << "id : " << Sys.inf.id << "    feedForward : " << i << std::endl;
                #endif
                Sys.net.backProp(outputdata);
                #if(debug)
                    std::cout << "id : " << Sys.inf.id << "   backPropa : " << i << std::endl;
                #endif
                error += Sys.net.getError();
            }
        }

        unsigned sam = (int) (Sys.net.Sample.size() * rand() / double(RAND_MAX));
        std::cout << Sys.timeStr() << std::endl;
        Sys.net.showVectorVals("Inputs:", Sys.net.Sample[sam].InData);
        inputdata  = Sys.net.Sample[sam].InData  / Sys.net.getScale();
        Sys.net.feedForward(inputdata);

        // Collect the net's actual output results:
        Sys.net.getResults(resultVals);
        resultVals = resultVals * Sys.net.getScale();
        Sys.net.showVectorVals("Outputs:", resultVals);

        // Train the net what the outputs should have been:
        //outputdata[0] = Sys.net.Sample[i].OutData[0];
        Sys.net.showVectorVals("Targets:", Sys.net.Sample[sam].OutData);

        // Report how well the training is working, average over recent samples:
        std::cout << "Net current error: " << Sys.net.getError() << std::endl;
        std::cout << "Net recent average error: " << Sys.net.getRecentAverageError() << std::endl;
        std::cout << "step : " << n << "   error : " << error << std::endl;
        Dat.SaveNet(Sys);
        //Dat.SaveSampleData(Sys);
    }
    Dat.SaveNet(Sys);
}

void Simulation::RunMD(System &Sys) {
    dType MainTime;
    Sys.Initialization();
    Sys.SetInitPositions();
    Sys.AssignParams(Sys.params);

    int cnt;
    cnt = 1 + (int)((Sys.cond.Tfinal - Sys.cond.Temperature)/Sys.cond.Tstep);

    if (Sys.control.melt_b)  Premelt( Sys );

    for (int i=0; i<cnt; ++i) {
        Dat.HeadLine(Sys);
        if (Sys.control.restoreState_each_b) Sys.RestoreState("default");

        if (Sys.control.inittemp_b) Sys.InitialTemperature();
        if (Sys.control.premelt_b)  PremeltRegion( Sys);

        Sys.InitializeData();

        MainTime = 0.0;
        while (Sys.inf.Time < Sys.cond.TimeCalc) {
            MDStep(Sys);

            Dat.ShowResults(Sys);
            Dat.SaveXYZFile(Sys);
            Dat.SaveRDF(Sys);
            Dat.SaveDataFrame(Sys);
            Sys.inf.step++;
        }

        Dat.HLine(Sys);
        Dat.SaveTable(Sys);
        Sys.cond.Temperature += Sys.cond.Tstep;
        Sys.cond.Preg += Sys.cond.Pstep;

        if (Sys.inf.id == 0)
            cout << "Next temperature...\n";
    }
    if (Sys.inf.id == 0)
        cout << "Done MD...\n" << std::endl;

}


void Simulation::RunPremelt(System &Sys) {
    Sys.Initialization();
    Sys.control.savetext_b = true;
    Sys.SetInitPositions();
    Sys.AssignParams(Sys.params);

    Dat.HeadLine(Sys);
    PremeltRegion( Sys );
    if (Sys.inf.id == 0)
        cout << "Done Premelt...\n";
}

void Simulation::RunGen(System &Sys) {
    if (Sys.inf.id == 0)
        cout << "Initialization of the Genetic Algorithm .... \n";
    if (!Sys.control.initialization_b)
    {
        Sys.Initialization();
	    Sys.control.savetext_b = true;
        Sys.control.initialization_b = true;
    }
    Sys.FindPairs();
    Sys.UpdateCells();

    Populate(Sys);
    for (int i = 0; i < Sys.fit.iterations; i++)
    {
        CrossoverDouble(Sys);
        MutationDouble(Sys);
        FitnessFunctionForceFit(Sys);
        CloneTheBest(Sys);
        Damp(Sys);
        if (Sys.inf.id == 0)
            cout << Sys.inf.Count << " - " << (Sys.Coeffs[0].begin()->second).fitres << std::endl;
        Dat.SaveFitRes(Sys);
        Sys.inf.Count += 1;
    }
    if (Sys.inf.id == 0)
        cout << "Done...\n";
}

void Simulation::RunFitSeq(System &Sys) {
    vector< map<std::string, Params> >::iterator iter_pop;
    int Tnum, ti, i, j, pop;
    double val;

    Sys.Initialization();

    Tnum = Sys.fit.AtTemperature.size();
    Populate(Sys);

    Dat.ClearFile(Sys.files.SampleFile);

    for (i = 0; i < Sys.fit.iterations; i++) {
        pop       = 0;

        Dat.HeadLine(Sys);
        CrossoverDouble(Sys);
        MutationDouble(Sys);

        BCastParams(Sys);

        for (iter_pop = Sys.Coeffs.begin(); iter_pop != Sys.Coeffs.end(); ++iter_pop) {
            Sys.fit.error     = 0.0;
            Sys.fit.error_n   = 1.0;
            Sys.fit.error_sum = 0.0;

            Sys.params  = *iter_pop;

            for (ti = 0; ti < Tnum ; ti++) {
                Sys.RestoreState("default");
                Sys.AssignParams(Sys.params);

                Sys.cond.Temperature = Sys.fit.AtTemperature[ti];
                Sys.cond.Tstep = 50.0;
                Sys.cond.Tfinal = Sys.cond.Temperature;
                if (Sys.control.inittemp_b) Sys.InitialTemperature();

                if (Sys.control.premelt_b) PremeltRegion( Sys);

                // ---------------------  Start Function ------------------------------
                Sys.InitializeData();

                while (Sys.inf.Time < Sys.cond.TimeCalc) {
                    MDStep(Sys);
                    FitnessFunctionExperimentCurrent(Sys, ti);

                    Dat.ShowResults(Sys);
                }

                Dat.HLine(Sys);
                FitnessFunctionExperiment(Sys, ti);
                if (Sys.inf.id == 0) {
                    cout << "Run : " << i << "     Set : " << pop << " | Error : " << Sys.fit.error << " | "  << std::endl;
                    cout << "--------------------------------------" << std::endl;
                    Dat.HLine(Sys);
                }

                Sys.fit.error_n   += 1.0;
            }
            // Save only the best fit data
            if (pop == 0) {
                Dat.SaveXYZFile(Sys);
                Dat.SaveRDF(Sys);
            }

            Sys.Coeffs[pop].begin()->second.fitres = Sys.fit.error;
            Sys.Coeffs[pop].begin()->second.fitres   = Sys.fit.error;
            Sys.Coeffs[pop].begin()->second.temperature = Sys.props.Temperature;
            Sys.Coeffs[pop].begin()->second.pressure = Sys.props.Pressure;
            Sys.Coeffs[pop].begin()->second.density  = Sys.props.Density;
            Sys.Coeffs[pop].begin()->second.energy   = Sys.props.Energy;
            pop++;
        }


        //Dat.SaveSampleData(Sys);
        //GetParams();
        SortCoeffs(Sys);

        if (Sys.inf.id == 0) {
            cout << "----------------------------------------------------------------------------------------------------------------------";
            if (Sys.control.fitexp_b)
                cout << "------------";
            cout <<  "\n";

            cout << "Best Set Deviation     Run : " << i << "    Error : " << Sys.Coeffs[0].begin()->second.fitres << std::endl;

            cout << "----------------------------------------------------------------------------------------------------------------------";
            if (Sys.control.fitexp_b)
                cout << "------------";
            cout <<  "\n";
            cout <<  "\n";
        }

        CloneTheBest(Sys);
        Damp(Sys);

        Dat.SaveFitPotential(Sys);
    }
    if (Sys.inf.id == 0)
        cout << "Done Fit...\n" << std::endl;

    Sys.inf.step++;
}

void Simulation::RunFitPar(System &Sys) {
    std::stringstream stream, iss;
    int Tnum, ti, i;
    Sys.Initialization();
    Sys.SetInitPositions();

    #if(debug)
        std::cout << "id : " << Sys.inf.id << "    BackUpState : " << Sys.timeStr() << std::endl;
    #endif

    Sys.control.mpi_b = false;
    Sys.inf.dID      = 0;
    Sys.inf.dNP     = 1;

    Tnum = Sys.fit.AtTemperature.size();
    Populate(Sys);
    #if(debug)
        std::cout << "id : " << Sys.inf.id << "    Populate : " << Sys.timeStr() << std::endl;
    #endif

    Dat.ClearFile(Sys.files.SampleFile);

    for (i = 0; i < Sys.fit.iterations; i++) {
        Dat.HeadLine(Sys);
        CrossoverDouble(Sys);
        MutationDouble(Sys);
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    Cross+Mut : " << Sys.timeStr() << std::endl;
        #endif

        BCastParams(Sys);
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    BCastParams : " << Sys.timeStr() << std::endl;
        #endif

        // Each process gets its own set of parameters
        Sys.params  = Sys.Coeffs[Sys.inf.id];

        Sys.fit.error     = 0.0;
        Sys.fit.error_n   = 1.0;
        Sys.fit.error_sum = 0.0;

        for (ti = 0; ti < Tnum ; ti++) {

            Sys.RestoreState("default");
            Sys.AssignParams(Sys.params);
            #if(debug)
                std::cout << "id : " << Sys.inf.id << "    AssignParams : " << Sys.timeStr() << std::endl;
            #endif

            Sys.cond.Temperature    = Sys.fit.AtTemperature[ti];
            Sys.cond.Tstep   = 50.0;
            Sys.cond.Tfinal  = Sys.cond.Temperature;
            
            if (Sys.fit.Pressure.size() > ti) 
                Sys.cond.Preg = Sys.fit.Pressure[ti];

            if (Sys.fit.Time.size() != 0) {
                Sys.cond.TimeCalc = Sys.fit.Time[ti];
                Sys.cond.TimeStep = Sys.fit.Time[ti];
            }

            if (Sys.control.inittemp_b) Sys.InitialTemperature();
            #if(debug)
                std::cout << "id : " << Sys.inf.id << "    InitialTemperature : " << Sys.timeStr() << std::endl;
            #endif


            if (Sys.control.premelt_b)  PremeltRegion( Sys);
            if (Sys.fit.Premelt_b.size() != 0)
                if (Sys.fit.Premelt_b[ti]) PremeltRegion( Sys);

            #if(debug)
                std::cout << "id : " << Sys.inf.id << "    PremeltRegion : " << Sys.timeStr() << std::endl;
            #endif

            Sys.InitializeData();
            #if(debug)
                std::cout << "id : " << Sys.inf.id << "    InitializeData : " << Sys.timeStr() << std::endl;
            #endif
            while (Sys.inf.Time < Sys.cond.TimeCalc) {
                try {
                    MDStep(Sys);
                    #if(debug)
                        std::cout << "id : " << Sys.inf.id << "    MDStep : " << Sys.timeStr() << std::endl;
                    #endif
                    FitnessFunctionExperimentCurrent(Sys, ti);
                    #if(debug)
                        std::cout << "id : " << Sys.inf.id << "    FitnessFunctionExperimentCurrent : " << Sys.timeStr() << std::endl;
                    #endif
                } catch (std::exception& e) {
                    Sys.fit.error = 1.0e5;
                    std::cerr << "Exception catched : " << e.what() << std::endl;
                }

                Dat.ShowResults(Sys);
                Dat.SaveXYZFile(Sys);
                Dat.SaveRDF(Sys);
                #if(debug)
                    std::cout << "id : " << Sys.inf.id << "    SaveRDF : " << Sys.timeStr() << std::endl;
                #endif
            }

            Dat.HLine(Sys);
            FitnessFunctionExperiment(Sys, ti);
            #if(debug)
                std::cout << "id : " << Sys.inf.id << "    FitnessFunctionExperiment : " << Sys.timeStr() << std::endl;
            #endif

            Sys.fit.error_n   += 1.0;
        }

        assert(Sys.inf.id < Sys.Coeffs.size() && "The size of the Sys.Coeffs vector is smaller then the number of processes");
        Sys.Coeffs[Sys.inf.id].begin()->second.fitres   = Sys.fit.error;
        Sys.Coeffs[Sys.inf.id].begin()->second.temperature = Sys.props.Temperature;
        Sys.Coeffs[Sys.inf.id].begin()->second.pressure = Sys.props.Pressure;
        Sys.Coeffs[Sys.inf.id].begin()->second.density  = Sys.props.Density;
        Sys.Coeffs[Sys.inf.id].begin()->second.energy   = Sys.props.Energy;

        GetParams(Sys);

        Dat.SaveSampleData(Sys);

        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    GetParams : " << Sys.timeStr() << std::endl;
        #endif
        SortCoeffs(Sys);
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    SortCoeffs : " << Sys.timeStr() << std::endl;
        #endif

        if (Sys.inf.id == 0) {
            stream.str("");
            stream.clear();
            stream << "----------------------------------------------------------------------------------------------------------------------";
            if (Sys.control.fitexp_b)
                stream << "------------";
            stream <<  "\n";
            stream << "Best Set Deviation     Run : " << i << "    Error : " << Sys.Coeffs[0].begin()->second.fitres << std::endl;

            stream << "----------------------------------------------------------------------------------------------------------------------";
            if (Sys.control.fitexp_b)
                stream << "------------";
            stream <<  "\n" << "\n";
            std::cout << stream.str();
            if (Sys.control.savetext_b)  {
                std::fstream File;
                File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
                File << stream.str() << std::endl;
                File.close();
            }

        }

        CloneTheBest(Sys);
        Damp(Sys);

        Dat.SaveFitPotential(Sys);
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    SaveFitPotential : " << Sys.timeStr() << std::endl;
        #endif
        Sys.inf.step++;
    }
    if (Sys.inf.id == 0)
        cout << "Done Fit...\n" << std::endl;
    Sys.control.mpi_b = true;
}

void Simulation::RunGrad(System &Sys) {
    //ReadForceFile(InForceFile, params);
    Sys.RestoreImages();
    Sys.Initialization();
    Sys.control.savetext_b = true;

    Sys.SetInitPositions();
    Sys.FindPairs();
    Sys.UpdateCells();

    for (int i = 0; i < Sys.fit.iterations; i++)
    {
        CalcGradient(Sys);
        Damp(Sys);
        if (Sys.inf.id == 0)
            cout << Sys.inf.Count << " - " << (Sys.Coeffs[0].begin()->second).fitres << "\n";
        Dat.SaveFitRes(Sys);
        Sys.inf.Count += 1;
    }
    if (Sys.inf.id == 0)
        cout << "Done...\n";
}

void Simulation::ParametersMutaion(System &Sys) {
    double val, Sigma;
    vector<std::string>::iterator iter;
    std::string name;

    for (iter = Sys.fit.ForElement.begin(); iter != Sys.fit.ForElement.end(); ++iter)
    //for (i = 0; i != ForElement.size(); ++i)
    {
        val = ( (double)rand() / (double)RAND_MAX );

        name = *iter;

        if (Sys.fit.FitParam["q"]) {
            if (Sys.params[name].q != 0.0)
                Sigma = std::abs(Sys.params[name].q) * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].q *= funcs::Gaussian(1.0, Sigma);
        }

        if (Sys.fit.FitParam["a"]){
            if (Sys.params[name].a != 0.0)
                Sigma = Sys.params[name].a * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].a += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["b"]){
            if (Sys.params[name].b != 0.0)
                Sigma = Sys.params[name].b * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].b += funcs::Gaussian(0.0, Sigma);
        }



        if (Sys.fit.FitParam["DM"]){
            if (Sys.params[name].DM != 0.0)
                Sigma = Sys.params[name].DM * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].DM += funcs::Gaussian(0.0, Sigma);
        }


        if (Sys.fit.FitParam["alphaM"]) {
            if (Sys.params[name].alphaM != 0.0)
                Sigma = Sys.params[name].alphaM * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].alphaM += funcs::Gaussian(0.0, Sigma);
        }


        if (Sys.fit.FitParam["rr"]){
            if (Sys.params[name].r != 0.0)
                Sigma = Sys.params[name].r * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].r += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["polariz"]){
            if (Sys.params[name].polariz != 0.0)
                Sigma = Sys.params[name].polariz * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].polariz += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["rho0"]) {
            if (Sys.params[name].rho0 != 0.0)
                Sigma = Sys.params[name].rho0 * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].rho0 += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["beta"]) {
            if (Sys.params[name].beta != 0.0)
                Sigma = Sys.params[name].beta * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].beta += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["re"]) {
            if (Sys.params[name].re != 0.0)
                Sigma = Sys.params[name].re * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].re += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["C"]) {
            if (Sys.params[name].C != 0.0)
                Sigma = Sys.params[name].C * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].C += funcs::Gaussian(0.0, Sigma);
        }

        if (Sys.fit.FitParam["D"]) {
            if (Sys.params[name].D != 0.0)
                Sigma = Sys.params[name].D * Sys.fit.sigmacur;
            else
                Sigma = Sys.fit.sigmacur;
            if (Sigma < 0)      Sigma = -Sigma;
            Sys.params[name].D += funcs::Gaussian(0.0, Sigma);
        }

    }
    if (Sys.inf.id == 0)
        cout << "---- Mutation -----" << std::endl;
}

void Simulation::RunParamsGauss(System &Sys) {
    Dat.ReadPotentialFile(Sys);

    ParamsIter iter;

    double val, Sigma;

    for (iter = Sys.params.begin(); iter != Sys.params.end(); ++iter){
        val = ( (double)rand() / (double)RAND_MAX );

        if ((iter->first)=="Mo"){
			if (Sys.fit.FitParam["q"]){
				if ((iter->second).q != 0.0)
					Sigma = (iter->second).q * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).q *= funcs::Gaussian(1.0, Sigma);
			}

			if (Sys.fit.FitParam["a"]){
				if ((iter->second).a != 0.0)
					Sigma = (iter->second).a * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).a += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["b"]){
				if ((iter->second).b != 0.0)
					Sigma = (iter->second).b * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).b += funcs::Gaussian(0.0, Sigma);
			}



			if (Sys.fit.FitParam["DM"]){
				if ((iter->second).DM != 0.0)
					Sigma = (iter->second).DM * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).DM += funcs::Gaussian(0.0, Sigma);
			}


			if (Sys.fit.FitParam["alphaM"]){
				if ((iter->second).alphaM != 0.0)
					Sigma = (iter->second).alphaM * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).alphaM += funcs::Gaussian(0.0, Sigma);
			}


			if (Sys.fit.FitParam["rr"]){
				if ((iter->second).r != 0.0)
					Sigma = (iter->second).r * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).r += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["polariz"]){
				if ((iter->second).polariz != 0.0)
					Sigma = (iter->second).polariz * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).polariz += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["rho0"]){
				if ((iter->second).rho0 != 0.0)
					Sigma = (iter->second).rho0 * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).rho0 += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["beta"]){
				if ((iter->second).beta != 0.0)
					Sigma = (iter->second).beta * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).beta += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["re"]){
				if ((iter->second).re != 0.0)
					Sigma = (iter->second).re * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).re += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["C"]){
				if ((iter->second).C != 0.0)
					Sigma = (iter->second).C * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).C += funcs::Gaussian(0.0, Sigma);
			}

			if (Sys.fit.FitParam["D"]){
				if ((iter->second).D != 0.0)
					Sigma = (iter->second).D * Sys.fit.sigmacur;
				else
					Sigma = Sys.fit.sigmacur;
				if (Sigma < 0)      Sigma = -Sigma;
				(iter->second).D += funcs::Gaussian(0.0, Sigma);
			}
        }
    }


    std::fstream File;
    std::stringstream stream;

    File.open(Sys.files.FileOut.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
    for (iter = Sys.params.begin(); iter != Sys.params.end(); ++iter){
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream.width(5);
        stream << iter->first;

        stream.setf(std::ios::fixed);
        stream.width(10);
        stream.precision(3);
        stream << iter->second.q;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.a;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.b;


        stream.width(10);
        stream.precision(4);
        stream << iter->second.DM;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.alphaM;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.r;

        stream.width(10);
        stream.precision(3);
        stream << iter->second.mass << "\n";
        File << stream.str();
    }
    File.close();
}

void Simulation::Populate(System &Sys) {
    int ind;
    //double min = 0.001, max = 1.0;
    typedef vector<vector<Params> >::size_type size;
    vector<map<std::string, Params> >::iterator iter_pop;
    map<std::string, Params>::iterator iter;
    map<std::string, Params>::iterator iter0;

    // Checks if the Coeffs vector already full is.
    if (Sys.Coeffs.size() != Sys.fit.population) {
        Sys.Coeffs.resize(Sys.fit.population);
        //map<std::string, Params>::const_iterator iter;
        for (ind = 0; ind != Sys.fit.population; ++ind)
            Sys.Coeffs[ind] = Sys.params;
    }

    Sys.ParData.resize( Sys.fit.population * Sys.inf.SubsN * 20, 0.0 );

    Sys.fit.sigmacur = Sys.fit.sigma;

    if ( Sys.inf.id == 0 )
        cout << "Done Populate ... \n";
}

double Simulation::CalcFitRes(System &Sys) {
    int i;
    double ValSum, DevSum, AvVal, Sigma, Dev, val;
    Sys.ResetForces("n");
    Sys.ForceField();

    // Define the average value
    ValSum = 0.0;
    DevSum = 0.0;
    for (i = 0; i <  Sys.At.size; i++)
    {
        ValSum += ((Sys.Force[i].x - Sys.At.Fx[i]) * (Sys.Force[i].x - Sys.At.Fx[i]) +
                   (Sys.Force[i].y - Sys.At.Fy[i]) * (Sys.Force[i].y - Sys.At.Fy[i]) +
                   (Sys.Force[i].z - Sys.At.Fz[i]) * (Sys.Force[i].z - Sys.At.Fz[i]));
        DevSum += 1.0;
    }
    AvVal = ValSum / DevSum;

    // Define Deviation
    for (i = 0; i <  Sys.At.size; i++)
    {
        val     = ((Sys.Force[i].x - Sys.At.Fx[i]) * (Sys.Force[i].x - Sys.At.Fx[i]) +
                   (Sys.Force[i].y - Sys.At.Fy[i]) * (Sys.Force[i].y - Sys.At.Fy[i]) +
                   (Sys.Force[i].z - Sys.At.Fz[i]) * (Sys.Force[i].z - Sys.At.Fz[i]));

        Sigma = (val - AvVal) * (val - AvVal);
        Dev  = 1.0 / Sigma;

        //ValSum += val * Dev;
        //DevSum += Dev;
        ValSum += val;
        DevSum += 1.0;
    }
    // Define the weightened value
    return ValSum / DevSum;
}

void Simulation::FitnessFunctionExperiment(System &Sys, const int& it) {
    map<std::string, vector<double> >::iterator iter;

    if (Sys.fit.Density.size() > 0 && it < Sys.fit.Density.size()) {
      assert(it < Sys.fit.Density.size() && "Looks like the it parameter is bigger then the size of Sys.Density");
      if (Sys.fit.Density[it] != 0.0) {
    	  Sys.fit.error_sum += std::sqrt(( Sys.props.Density - Sys.fit.Density[it] ) * ( Sys.props.Density - Sys.fit.Density[it]));
      }
    }

    if (Sys.fit.Energy.size() > 0 && it < Sys.fit.Energy.size()) {
      assert(it < Sys.fit.Energy.size() && "Looks like the it parameter is bigger then the size of Sys.Energy");
      if (Sys.fit.Energy[it] != 0.0) {
    	  Sys.fit.error_sum += 1000.0 * std::sqrt(( Sys.props.Energy - Sys.fit.Energy[it] ) * ( Sys.props.Energy - Sys.fit.Energy[it]));
      }
    }

    if (Sys.fit.Pressure.size() > 0 && it < Sys.fit.Pressure.size()) {
      assert(it < Sys.fit.Pressure.size() && "Looks like the it parameter is bigger then the size of Sys.Pressure");
      if (Sys.fit.Pressure[it] != 0.0) {
    	  Sys.fit.error_sum += std::sqrt(( Sys.props.Pressure - Sys.fit.Pressure[it] ) * ( Sys.props.Pressure - Sys.fit.Pressure[it]));
      }
    }

    if (Sys.fit.Temperature.size() > 0 && it < Sys.fit.Temperature.size()) {
      assert(it < Sys.fit.Temperature.size() && "Looks like the it parameter is bigger then the size of Sys.Temperature");
      if (Sys.fit.Temperature[it] != 0.0) {
    	  Sys.fit.error_sum += std::sqrt(( Sys.props.Temperature - Sys.fit.Temperature[it] ) * ( Sys.props.Temperature - Sys.fit.Temperature[it]));
      }
    }

    if (Sys.fit.Diffusion.size() > 0 && it < Sys.fit.Diffusion.size()) {
      assert(it < Sys.fit.Diffusion.size() && "Looks like the it parameter is bigger then the size of Sys.Diffusion");
      if (Sys.fit.Diffusion[it] != 0.0) {
    	  Sys.fit.error_sum += std::max(( Sys.props.Diffusion / Sys.fit.Diffusion[it] )*( Sys.props.Diffusion / Sys.fit.Diffusion[it] ), ( Sys.fit.Diffusion[it] / Sys.props.Diffusion )*( Sys.fit.Diffusion[it] / Sys.props.Diffusion));
      }
    }

    if (Sys.fit.Struct_b.size() > 0  && it < Sys.fit.Struct_b.size()) {
        assert(it < Sys.fit.Struct_b.size() && "Looks like the it parameter is bigger then the size of Sys.Struct_b");
        if (Sys.fit.Struct_b[it]) {
            Sys.fit.error_sum += CheckStructure(Sys);
        }
    }

    if (Sys.fit.CohesiveE.size() > 0 && it < Sys.fit.CohesiveE.size()) {
      assert(it < Sys.fit.CohesiveE.size() && "Looks like the it parameter is bigger then the size of Sys.Energy");
      if (Sys.fit.CohesiveE[it] != 0.0) {
    	  Sys.fit.error_sum += std::sqrt(( Sys.props.CohesiveEnergy - Sys.fit.CohesiveE[it] ) * ( Sys.props.CohesiveEnergy - Sys.fit.CohesiveE[it]));
      }
    }

    if (Sys.fit.FitRDF) {
        Sys.fit.error_sum += CompareRDFs(Sys);
    }

    Sys.fit.error = Sys.fit.error_sum/Sys.fit.error_n;
}

void Simulation::FitnessFunctionExperimentCurrent(System &Sys, const int& it) {
    map<std::string, vector<double> >::iterator iter;
    Sys.fit.error = 0.0;
    #if(debug)
        std::cout << "id : " << Sys.inf.id << "    Enter  FitnessFunctionExperimentCurrent with step " << it << std::endl;
    #endif


    if (Sys.fit.Density.size() > 0 && it < Sys.fit.Density.size()) {
        assert(it < Sys.fit.Density.size() && "Looks like the it parameter is bigger then the size of Sys.Density");
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    Sys.fit.Density " << Sys.fit.Density[it] << "    Sys.props.Density " << Sys.props.Density << "     Sys.fit.error "  << Sys.fit.error << std::endl;
        #endif
        if (Sys.fit.Density[it] != 0.0) {
            #if(debug)
                std::cout << "id : " << Sys.inf.id << "         Sys.fit.Density " << Sys.fit.Density[it]  << std::endl;
                std::cout << "id : " << Sys.inf.id << "         Sys.fit.Density test " << (( Sys.props.Density - Sys.fit.Density[it] ) * ( Sys.props.Density - Sys.fit.Density[it]))  << std::endl;
            #endif
            Sys.fit.error +=  sqrt(( Sys.props.Density - Sys.fit.Density[it] ) * ( Sys.props.Density - Sys.fit.Density[it]));
            #if(debug)
                std::cout << "Density system" << Sys.props.Density << "   Density fit " << Sys.fit.Density[it] << "   at " << Sys.fit.Temperature[it] << "   error "<< sqrt(( Sys.props.Density - Sys.fit.Density[it] ) * ( Sys.props.Density - Sys.fit.Density[it])) << std::endl;
            #endif
      }
    }

    if (Sys.fit.Energy.size() > 0 && it < Sys.fit.Energy.size()) {
      assert(it < Sys.fit.Energy.size() && "Looks like the it parameter is bigger then the size of Sys.Energy");
        #if(debug)
            std::cout << "id : " << Sys.inf.id << "    Sys.fit.Energy " << Sys.fit.Energy[it] << it << std::endl;
        #endif
      if (Sys.fit.Energy[it] != 0.0) {
    	  Sys.fit.error += 1000.0 * sqrt(( Sys.props.Energy - Sys.fit.Energy[it] ) * ( Sys.props.Energy - Sys.fit.Energy[it]));
            #if(debug)
              std::cout << "Energy system" << Sys.props.Energy << "   Energy fit " << Sys.fit.Energy[it] << "   at " << Sys.fit.Temperature[it] << "   error "<< sqrt(( Sys.props.Energy - Sys.fit.Energy[it] ) * ( Sys.props.Energy - Sys.fit.Energy[it])) << std::endl;
            #endif
      }
    }

    if (Sys.fit.Pressure.size() > 0 && it < Sys.fit.Pressure.size()) {
      assert(it < Sys.fit.Pressure.size() && "Looks like the it parameter is bigger then the size of Sys.Pressure");
      if (Sys.fit.Pressure[it] != 0.0) {
    	  Sys.fit.error += sqrt(( Sys.props.Pressure - Sys.fit.Pressure[it] ) * ( Sys.props.Pressure - Sys.fit.Pressure[it]));
      }
    }

    if (Sys.fit.Temperature.size() > 0 && it < Sys.fit.Temperature.size()) {
      assert(it < Sys.fit.Temperature.size() && "Looks like the it parameter is bigger then the size of Sys.Temperature");
      if (Sys.fit.Temperature[it] != 0.0) {
    	  Sys.fit.error += sqrt(( Sys.props.Temperature - Sys.fit.Temperature[it] ) * ( Sys.props.Temperature - Sys.fit.Temperature[it]));
      }
    }

    if (Sys.fit.Diffusion.size() > 0 && it < Sys.fit.Diffusion.size()) {
      assert(it < Sys.fit.Diffusion.size() && "Looks like the it parameter is bigger then the size of Sys.Diffusion");
      if (Sys.fit.Diffusion[it] != 0.0) {
    	  Sys.fit.error += max(( Sys.props.Diffusion / Sys.fit.Diffusion[it] )*( Sys.props.Diffusion / Sys.fit.Diffusion[it] ), ( Sys.fit.Diffusion[it] / Sys.props.Diffusion )*( Sys.fit.Diffusion[it] / Sys.props.Diffusion));
      }
    }

    if (Sys.fit.Struct_b.size() > 0 && it < Sys.fit.Struct_b.size()) {
        assert(it < Sys.fit.Struct_b.size() && "Looks like the it parameter is bigger then the size of Sys.Struct_b");
        if (Sys.fit.Struct_b[it]) {
            Sys.fit.error += CheckStructure(Sys);
        }
    }

    if (Sys.fit.CohesiveE.size() > 0 && it < Sys.fit.CohesiveE.size()) {
      assert(it < Sys.fit.CohesiveE.size() && "Looks like the it parameter is bigger then the size of Sys.Energy");
      if (Sys.fit.CohesiveE[it] != 0.0) {
    	  Sys.fit.error += sqrt(( Sys.props.CohesiveEnergy - Sys.fit.CohesiveE[it] ) * ( Sys.props.CohesiveEnergy - Sys.fit.CohesiveE[it]));
      }
    }

    if (Sys.fit.FitRDF) {
        Sys.fit.error += CompareRDFs(Sys);
    }
}


dType Simulation::CompareRDFs(System &Sys) {
    int i, j;
    dType err, DArea, RefArea, DR;

    DR = Sys.RDF.r[1] - Sys.RDF.r[0];

    Sys.RadialDistFunc();

    err = 0.0;

    for (j = 0; j < Sys.RDFfor.size(); j++) {
        RefArea = 0.0;
        DArea   = 0.0;
        for (i = 0; i != Sys.RDF.r.size(); i++) {
            RefArea += DR * Sys.RDFref.n[j][i];
            DArea   += std::sqrt((DR * Sys.RDFref.n[j][i] - DR * Sys.RDF.n[j][i]) * (DR * Sys.RDFref.n[j][i] - DR * Sys.RDF.n[j][i]));
            //if (Sys.RDFref.n[j][i] > 0.0) {
            //}
        }
        err += DArea / RefArea;
    }

    return err * 100.0;
}

dType Simulation::CheckStructure(System &Sys) {
    dType err;
    dType Rmin, R_sc, Rx, Ry, Rz, Tx, Ty, Tz, Rxm, Rym, Rzm;
    iType i, n, j, Ncl, cl, in, im;
    std::stringstream stream;
    std::string str;
    std::vector<dType> Dx = Sys.At.x;
    std::vector<dType> Dy = Sys.At.y;
    std::vector<dType> Dz = Sys.At.z;
    err = 0.0;

    Tx = 0.0;
    Ty = 0.0;
    Tz = 0.0;

    #pragma omp parallel for private(i, j, n, Rmin, R_sc, cl, Ncl, stream, im, Rxm, Rym, Rzm) reduction(+:err, Tx, Ty, Tz)
    for (i = 0; i < Sys.At.size; ++i) {
        Rmin = 1.0e6;
        Rxm = 0.0;
        Rym = 0.0;
        Rzm = 0.0;

        cl = Sys.At.Cell[i];

        try {
            Ncl = Sys.Cell[cl].atomI.size();
        } catch (const std::out_of_range& oor) {
            std::cerr << Sys.inf.id << "   3038    Out of range error: " << oor.what() << "\n";
        }

        for (n = 0; n < Ncl; n++)  {
            try {
                j = Sys.Cell[cl].atomI[n];
            }catch (const std::out_of_range& oor) {
                std::cerr << Sys.inf.id << "   3045    Out of range error: " << oor.what() << "\n";
            }

            if (Sys.At.name[i] == Sys.At.name[j])  {
                for (im = 0; im < Sys.inf.ImPos.size(); im ++) {
                    Rx = (Sys.At.xav[i] - (Sys.At.x0[j] + Sys.inf.ImPos[im].x));
                    if (Rx >  Sys.cond.Rc_long) continue;
                    Ry = (Sys.At.yav[i] - (Sys.At.y0[j] + Sys.inf.ImPos[im].y));
                    if (Ry >  Sys.cond.Rc_long) continue;
                    Rz = (Sys.At.zav[i] - (Sys.At.z0[j] + Sys.inf.ImPos[im].z));
                    if (Rz >  Sys.cond.Rc_long) continue;

                    R_sc = (Rx * Rx + Ry * Ry + Rz * Rz);

                    if (R_sc < Rmin) {
                        Rxm = Rx;
                        Rym = Ry;
                        Rzm = Rz;
                        Rmin = R_sc;
                    }
                }
            }
        }
        Dx[i] = Rxm;
        Dy[i] = Rym;
        Dz[i] = Rzm;
        Tx += Rxm;
        Ty += Rym;
        Tz += Rzm;

        err += Rmin;
    }

    Tx *= Sys.At.rsize;
    Ty *= Sys.At.rsize;
    Tz *= Sys.At.rsize;

    err = 0.0;
    for (i = 0; i < Sys.At.size; ++i) {
        Dx[i] -= Tx;
        Dy[i] -= Ty;
        Dz[i] -= Tz;

        err += (Dx[i] * Dx[i] + Dy[i] * Dy[i] + Dz[i] * Dz[i]);
    }

    err *= Sys.At.rsize;

    return 100 * std::sqrt(err);
}

void Simulation::SortCoeffs(System &Sys) {
    std::sort(Sys.Coeffs.begin(), Sys.Coeffs.end(), Compare());
    Sys.params = Sys.Coeffs[0];
    Sys.fit.error  = Sys.Coeffs[0].begin()->second.fitres;
}

void Simulation::FitnessFunctionForceFit(System &Sys) {
    int i;
    std::string name;

    vector< map<std::string, Params> >::iterator iter;

    for (iter = Sys.Coeffs.begin(); iter != Sys.Coeffs.end(); ++iter){
        ((*iter).begin()->second).fitres = 0.0;

        for (i=0; i< Sys.At.size; i++)
        {
            name = Sys.At.name[i];
            Sys.At.q[i]     = (*iter)[name].q;

            Sys.At.a[i]     = (*iter)[name].a;
            Sys.At.b[i]     = (*iter)[name].b;
        }
        ((*iter).begin()->second).fitres = CalcFitRes(Sys);
    }

    std::sort(Sys.Coeffs.begin(), Sys.Coeffs.end(), Compare());

    for (i=0; i< Sys.At.size; i++)
    {
        name = Sys.At.name[i];
        Sys.At.q[i]     = Sys.Coeffs[0][name].q;

        Sys.At.a[i]     = Sys.Coeffs[0][name].a;
        Sys.At.b[i]     = Sys.Coeffs[0][name].b;

    }

    Sys.ResetForces("n");
    Sys.ForceField();
}

bool FitCondition(double a, double b) {
    return a < b;
}

void Simulation::CloneTheBest(System &Sys) {
    int start, n;

    start = (int) (Sys.fit.rate * Sys.fit.population);

    for (int pop = start; pop < Sys.fit.population; pop++){

        n = (int)(start * ( (double)rand() / (double)RAND_MAX ));

        Sys.Coeffs[pop] = Sys.Coeffs[n];
    }
}

void Simulation::CrossoverDouble(System &Sys) {
    int k, n;
    double X;
    int start;

    start = (int) (Sys.fit.rate * Sys.fit.population);

    vector< map<std::string, Params> >::iterator iter_pop;
    map<std::string, Params>::iterator iterm;
    map<std::string, Params>::iterator itervk;
    map<std::string, Params>::iterator itervn;

    for (iter_pop = (Sys.Coeffs.begin() + start); iter_pop != Sys.Coeffs.end(); ++iter_pop){

        k = (int)(start * ( (double)rand() / (double)RAND_MAX ));
        n = (int)(start * ( (double)rand() / (double)RAND_MAX ));
        X = ( (double)rand() / (double)RAND_MAX );

        iterm  = (*iter_pop).begin();

        itervk = (*(Sys.Coeffs.begin() + k)).begin();
        itervn = (*(Sys.Coeffs.begin() + n)).begin();

        //if (Sys.fit.FitParam["q"])      (iterm->second).q       =  X * (itervk->second).q       + (1.0 - X) * (itervn->second).q;
        if (Sys.fit.FitParam["a"])      (iterm->second).a       =  X * (itervk->second).a       + (1.0 - X) * (itervn->second).a;
        if (Sys.fit.FitParam["b"])      (iterm->second).b       =  X * (itervk->second).b       + (1.0 - X) * (itervn->second).b;

        if (Sys.fit.FitParam["DM"])     (iterm->second).DM      =  X * (itervk->second).DM      + (1.0 - X) * (itervn->second).DM;
        if (Sys.fit.FitParam["alphaM"]) (iterm->second).alphaM  =  X * (itervk->second).alphaM  + (1.0 - X) * (itervn->second).alphaM;
        if (Sys.fit.FitParam["rr"])     (iterm->second).r       =  X * (itervk->second).r       + (1.0 - X) * (itervn->second).r;

        if (Sys.fit.FitParam["rho0"])   (iterm->second).rho0    =  X * (itervk->second).rho0        + (1.0 - X) * (itervn->second).rho0;
        if (Sys.fit.FitParam["beta"])   (iterm->second).beta    =  X * (itervk->second).beta        + (1.0 - X) * (itervn->second).beta;
        if (Sys.fit.FitParam["re"])     (iterm->second).re      =  X * (itervk->second).re      + (1.0 - X) * (itervn->second).re;
        if (Sys.fit.FitParam["C"])      (iterm->second).C       =  X * (itervk->second).C       + (1.0 - X) * (itervn->second).C;
        if (Sys.fit.FitParam["D"])      (iterm->second).D       =  X * (itervk->second).D       + (1.0 - X) * (itervn->second).D;

        if (Sys.fit.FitParam["A"])      (iterm->second).A       =  X * (itervk->second).A       + (1.0 - X) * (itervn->second).A;
        if (Sys.fit.FitParam["E"])      (iterm->second).E       =  X * (itervk->second).E       + (1.0 - X) * (itervn->second).E;
        if (Sys.fit.FitParam["rho0"])   (iterm->second).rho0    =  X * (itervk->second).rho0        + (1.0 - X) * (itervn->second).rho0;
        if (Sys.fit.FitParam["re"])     (iterm->second).re      =  X * (itervk->second).re      + (1.0 - X) * (itervn->second).re;
        if (Sys.fit.FitParam["beta0"])  (iterm->second).beta0   =  X * (itervk->second).beta0       + (1.0 - X) * (itervn->second).beta0;
        if (Sys.fit.FitParam["beta1"])  (iterm->second).beta1   =  X * (itervk->second).beta1       + (1.0 - X) * (itervn->second).beta1;
        if (Sys.fit.FitParam["beta2"])  (iterm->second).beta2   =  X * (itervk->second).beta2       + (1.0 - X) * (itervn->second).beta2;
        if (Sys.fit.FitParam["beta3"])  (iterm->second).beta3   =  X * (itervk->second).beta3       + (1.0 - X) * (itervn->second).beta3;
        if (Sys.fit.FitParam["t0"])     (iterm->second).t0      =  X * (itervk->second).t0      + (1.0 - X) * (itervn->second).t0;
        if (Sys.fit.FitParam["t1"])     (iterm->second).t1      =  X * (itervk->second).t1      + (1.0 - X) * (itervn->second).t1;
        if (Sys.fit.FitParam["t2"])     (iterm->second).t2      =  X * (itervk->second).t2      + (1.0 - X) * (itervn->second).t2;
        if (Sys.fit.FitParam["t3"])     (iterm->second).t3      =  X * (itervk->second).t3      + (1.0 - X) * (itervn->second).t3;
        if (Sys.fit.FitParam["rc"])     (iterm->second).rc_meam      =  X * (itervk->second).rc_meam      + (1.0 - X) * (itervn->second).rc_meam;

        if (Sys.fit.FitParam["DN"])     (iterm->second).DN       =  X * (itervk->second).DN       + (1.0 - X) * (itervn->second).DN;
        if (Sys.fit.FitParam["alphaN"]) (iterm->second).alphaN   =  X * (itervk->second).alphaN       + (1.0 - X) * (itervn->second).alphaN;
        if (Sys.fit.FitParam["rN"])     (iterm->second).rN       =  X * (itervk->second).rN       + (1.0 - X) * (itervn->second).rN;
        if (Sys.fit.FitParam["beta0N"]) (iterm->second).beta0N   =  X * (itervk->second).beta0N       + (1.0 - X) * (itervn->second).beta0N;
        if (Sys.fit.FitParam["rho0N"])  (iterm->second).rho0N    =  X * (itervk->second).rho0N       + (1.0 - X) * (itervn->second).rho0N;
        if (Sys.fit.FitParam["NN"])     (iterm->second).NN       =  X * (itervk->second).NN         + (1.0 - X) * (itervn->second).NN;

    }
}

void Simulation::MutationDouble(System &Sys) {
    int start;
    double val, Sigma;

    vector<std::string>::iterator iter_s;
    std::string name;
    start = (int) (Sys.fit.rate * Sys.fit.population);


    for (int i = start; i != Sys.Coeffs.size(); ++i) {

        if (Sys.fit.FitParam["q"]) {
            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {

                Sigma = (1.0 + funcs::Gaussian(0.0, Sys.fit.sigmacur));
                for (iter_s = Sys.fit.ForElement.begin(); iter_s != Sys.fit.ForElement.end(); ++iter_s)  {
                    name = *iter_s;
                    Sys.Coeffs[i][name].q *= Sigma;
                }

            }
        }

        for (iter_s = Sys.fit.ForElement.begin(); iter_s != Sys.fit.ForElement.end(); ++iter_s) {
            name = *iter_s;

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["a"]){
                    if (std::abs((Sys.Coeffs[i])[name].a) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].a * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    (Sys.Coeffs[i])[name].a += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["b"]){
                    if (std::abs((Sys.Coeffs[i])[name].b) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].b * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].b += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );

            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["DM"]) {
                	if (std::abs(Sys.Coeffs[i][name].DM) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].DM * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].DM += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["alphaM"]){
                    if (std::abs(Sys.Coeffs[i][name].alphaM) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].alphaM * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].alphaM += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["rr"]){
                    if (std::abs(Sys.Coeffs[i][name].r) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].r * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].r += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["rho0"]){
                    if (std::abs(Sys.Coeffs[i][name].rho0) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].rho0 * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].rho0 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta"]){
                    if (std::abs(Sys.Coeffs[i][name].beta) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].beta * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["re"]){
                    if (std::abs(Sys.Coeffs[i][name].re) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].re * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].re += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["C"]){
                    if (std::abs(Sys.Coeffs[i][name].C) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].C * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].C += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["D"]){
                    if (std::abs(Sys.Coeffs[i][name].D) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].D * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].D += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["A"]){
                    if (std::abs(Sys.Coeffs[i][name].A) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].A * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].A += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["E"]){
                    if (std::abs(Sys.Coeffs[i][name].E) > Sys.fit.sigmacur)
                        Sigma = std::abs( Sys.Coeffs[i][name].E * Sys.fit.sigmacur );
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].E += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta0"]){
                    if (std::abs(Sys.Coeffs[i][name].beta0) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].beta0 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta0 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta1"]){
                    if (std::abs(Sys.Coeffs[i][name].beta1) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].beta1 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta1 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta2"]){
                    if (std::abs(Sys.Coeffs[i][name].beta2) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].beta2 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta2 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta3"]){
                    if (std::abs(Sys.Coeffs[i][name].beta3) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].beta3 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta3 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["t0"]){
                    if (std::abs(Sys.Coeffs[i][name].t0) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].t0 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].t0 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["t1"]){
                    if (std::abs(Sys.Coeffs[i][name].t1) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].t1 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].t1 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["t2"]) {
                    if (std::abs(Sys.Coeffs[i][name].t2) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].t2 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].t2 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["t3"]){
                    if (std::abs(Sys.Coeffs[i][name].t3) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].t3 * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].t3 += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["rc"]){
                    if (std::abs(Sys.Coeffs[i][name].rc_meam) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].rc_meam * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].rc_meam += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["DN"]){
                    if (std::abs(Sys.Coeffs[i][name].DN) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].DN * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].DN += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["alphaN"]){
                    if (std::abs(Sys.Coeffs[i][name].alphaN) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].alphaN * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].alphaN += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["rN"]){
                    if (std::abs(Sys.Coeffs[i][name].rN) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].rN * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].rN += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["NN"]){
                    if (std::abs(Sys.Coeffs[i][name].NN) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].NN * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].NN += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["beta0N"]){
                    if (std::abs(Sys.Coeffs[i][name].beta0N) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].beta0N * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].beta0N += funcs::Gaussian(0.0, Sigma);
                }
            }

            val = ( (double)rand() / (double)RAND_MAX );
            if (val <= Sys.fit.MutProb) {
                if (Sys.fit.FitParam["rho0N"]){
                    if (std::abs(Sys.Coeffs[i][name].rho0N) > Sys.fit.sigmacur)
                        Sigma = std::abs(Sys.Coeffs[i][name].rho0N * Sys.fit.sigmacur);
                    else
                        Sigma = Sys.fit.sigmacur;
                    Sys.Coeffs[i][name].rho0N += funcs::Gaussian(0.0, Sigma);
                }
            }





        }
    }
}

void Simulation::Damp(System &Sys) {
    Sys.fit.sigmacur *= std::exp(-Sys.fit.DecayConstant);
    Sys.fit.epsilon *= std::exp(-Sys.fit.DecayConstant);;
}

void Simulation::CalcGradient(System &Sys) {
    int i;
    double val, FitRes1;
    std::string name;


    for (i=0; i< Sys.At.size; i++){
        name = Sys.At.name[i];
        Sys.At.q[i]     = Sys.Coeffs[0][name].q;
        Sys.At.a[i]     = Sys.Coeffs[0][name].a;
        Sys.At.b[i]     = Sys.Coeffs[0][name].b;
    }

    Sys.Coeffs[0].begin()->second.fitres = CalcFitRes(Sys);

    for (int j=0; j != Sys.Coeffs[0].size(); ++j){

        /*
        if (FitParam["q"]){
            val = Coeffs[0][j].q;
            Coeffs[0][j].q += sigma;
            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].q     = Coeffs[0][j].q;
                }
            }
            FitRes1 = CalcFitRes();
            Coeffs[0][j].q = val - epsilon / sigma * (FitRes1 - Coeffs[0][0].fitres);

            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].q     = Coeffs[0][j].q;
                }
            }
            Coeffs[0][0].fitres = CalcFitRes();
        }



        if (FitParam["a"]){
            val = Coeffs[0][j].a;
            Coeffs[0][j].a += sigma;
            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].a     = Coeffs[0][j].a;
                }
            }
            FitRes1 = CalcFitRes();
            Coeffs[0][j].a = val - epsilon / sigma * (FitRes1 - Coeffs[0][0].fitres);

            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].a     = Coeffs[0][j].a;
                }
            }
            Coeffs[0][0].fitres = CalcFitRes();
        }


        if (FitParam["b"]){
            val = Coeffs[0][j].b;
            Coeffs[0][j].b += sigma;
            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].b     = Coeffs[0][j].b;
                }
            }
            FitRes1 = CalcFitRes();
            Coeffs[0][j].b = val - epsilon / sigma * (FitRes1 - Coeffs[0][0].fitres);

            for (i=0; i< Sys.At.seze; i++)    {
                if (Coeffs[0][j].name == Sys.At[i].name){
                    Sys.At[i].b     = Coeffs[0][j].b;
                }
            }
            Coeffs[0][0].fitres = CalcFitRes();
        }


        */
    }
    Sys.Coeffs[0][0].fitres = CalcFitRes(Sys);
}


void Simulation::ReadTheImput(int argc, char** argv, System &Sys) {
    Substance Sub;
    std::string text, name1, name2, file, line;
    int latt[3];
    double size;
    std::stringstream iss;
    
    if (argc) {
        
        if (Sys.inf.id == 0)
            cout << argv[1];
        Sys.files.File = argv[1];
        unsigned pos = Sys.files.File.find_last_of("/");
        Sys.files.Path = Sys.files.File.substr(0, pos+1);
        Sys.files.Set();
        ReadFile(argc, argv, Sys);
    
    } else {
        
        if (Sys.inf.id == 0)
            cout << "Please enter the taks: ";
        
        std::getline (std::cin,line);

        iss.str("");
        iss.clear();
        iss.str(line);

        iss >> text;

        if (text == "Run"|| text=="run" || text == "RUN")
        {
            iss >> Sys.files.File;

            unsigned pos = Sys.files.File.find_last_of("/");
            Sys.files.Path = Sys.files.File.substr(0, pos+1);
            
            Sys.files.Set();
            ReadFile(argc, argv, Sys);
        }

        else if (text == "create" || text == "Create" || text == "CREATE")
        {
            if (Sys.inf.id == 0)
                cout << "Enter the structure [NaF]: ";
            std::cin >> text;
            if (text == "NaF")
            {
                if (Sys.inf.id == 0)
                    cout << "Enter element 1: ";
                std::cin >> name1;

                if (Sys.inf.id == 0)
                    cout << "Enter element 2: ";
                std::cin >> name2;

                if (Sys.inf.id == 0)
                    cout << "Enter size of the lattice: ";
                std::cin >> latt[0];
                std::cin >> latt[1];
                std::cin >> latt[2];

                if (Sys.inf.id == 0)
                    cout << "Enter lattice constant: ";
                std::cin >> size;

                if (Sys.inf.id == 0)
                    cout << "Enter the output file: ";
                std::cin >> file;

                Sub.CreateFluoride(name1, name2, latt, size, file);

                ReadTheImput(argc, argv, Sys);
            }

            if (text == "SC" || text == "sc" || text == "cP")
            {
                if (Sys.inf.id == 0)
                    cout << "Enter element: ";
                std::cin >> name1;

                if (Sys.inf.id == 0)
                    cout << "Enter size of the lattice: ";
                std::cin >> latt[0];

                if (Sys.inf.id == 0)
                    cout << "Enter lattice constant: ";
                std::cin >> size;

                if (Sys.inf.id == 0)
                    cout << "Enter the output file: ";
                std::cin >> file;

                Sub.CreateSC(name1, latt[0], size, file);

                ReadTheImput(argc, argv, Sys);
            }


            if (text == "BCC" || text == "bcc" || text == "cI")
            {
                if (Sys.inf.id == 0)
                    cout << "Enter element: ";
                std::cin >> name1;

                if (Sys.inf.id == 0)
                    cout << "Enter size of the lattice: ";
                std::cin >> latt[0];

                if (Sys.inf.id == 0)
                    cout << "Enter lattice constant: ";
                std::cin >> size;

                if (Sys.inf.id == 0)
                    cout << "Enter the output file: ";
                std::cin >> file;

                Sub.CreateBCC(name1, latt[0], size, file);

                ReadTheImput(argc, argv, Sys);
            }

            if (text == "FCC" || text == "fcc")
            {
                if (Sys.inf.id == 0)
                    cout << "Enter element 1: ";
                std::cin >> name1;

                if (Sys.inf.id == 0)
                    cout << "Enter element 2: ";
                std::cin >> name2;

                if (Sys.inf.id == 0)
                    cout << "Enter size of the lattice: ";
                std::cin >> latt[0];

                if (Sys.inf.id == 0)
                    cout << "Enter lattice constant: ";
                std::cin >> size;

                if (Sys.inf.id == 0)
                    cout << "Enter the output file: ";
                std::cin >> file;

                Sub.CreateFCC(name1, name2, latt[0], size, file);

                ReadTheImput(argc, argv, Sys);
            }
        }

        else {
            Sys.files.File = text;

            unsigned pos = Sys.files.File.find_last_of("/");
            Sys.files.Path = Sys.files.File.substr(0, pos+1);

            Sys.files.Set();
            ReadFile(argc, argv, Sys);
        }

    }
}

void Simulation::ReadFile(int argc, char** argv, System &Sys) {
    params.clear();
    detpar.clear();

    std::string  line;
    std::fstream DataFile;

    static const std::string commb = "/*";
    static const std::string comme = "*/";

    vector<string> res;

    DataFile.open(Sys.files.FileIn.c_str(), std::ios::in);

    if (!DataFile.is_open()) {
        if (Sys.inf.id == 0)
            cout << "The input file is not fould. Try again ... " << "\n";
        ReadTheImput(argc, argv, Sys);
    }

    Dat.ClearFiles(Sys);
    while (!DataFile.eof()) {
        std::getline(DataFile, line);
        res = funcs::split(line);
        
        std::string::const_iterator b = line.begin();
        std::string::const_iterator e = line.end();

        if (std::search(b, e, commb.begin(), commb.end()) != e) {
            while (std::search(b, e, comme.begin(), comme.end()) == e && !DataFile.eof()){
                std::getline(DataFile, line);
                b = line.begin();
                e = line.end();
                if (DataFile.eof()){
                    if (Sys.inf.id == 0)
                        cout <<  "Looks like you forgot to put the closing comment" << std::endl;
                    if (Sys.inf.id == 0)
                        cout <<  "Press any key to continue ...";
                    std::cin.ignore();
                }
            }
            std::getline(DataFile, line);
        }

        if (line[0]!= '#' && line[0]!= '/')
            ReadPairs(res.begin(), res.end(), Sys);
        
    }
    
    DataFile.close();
}


void Simulation::ReadPairs(vector<string>::iterator begin, vector<string>::iterator end, System &Sys) {
    std::string unit;
    std::string value;
    ValueUnit ValUn;

    while (begin != end) {

        if (*begin == "print" || *begin == "PRINT" || *begin == "Print") {
            std::fstream OFile, TFile;
            ++begin;

            if (Sys.inf.id == 0) {
                OFile.open(Sys.files.FileOut.c_str(),   std::fstream::in | std::fstream::out | std::fstream::app);
                TFile.open(Sys.files.TableFile.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

                while (begin != end) {
                    OFile << *begin << " ";
                    TFile << *begin << " ";
                    cout << *begin << " ";
                   ++begin;
                }

                OFile << std::endl;
                TFile << std::endl;
                cout << std::endl;

                OFile.close();
                TFile.close();
            }
            continue;
        }


        if (*begin == "AtomN" || *begin == "AtomN" || *begin == "AtomN" || *begin == "AtomN") {
            ++begin;
            Sys.At.size  = funcs::ToInt(*begin);
            Sys.At.rsize = 1.0 / ((double) Sys.At.size);
            Sys.At.Resize(Sys.At.size);
        }

        if (*begin == "Coulomb") {
            ++begin;
            Sys.control.coulomb_b = true;

        }
        
        if (*begin == "DSP" || *begin == "dsp" || *begin == "Dsp"){
            Sys.control.coulomb_b = true;
        }


        if (*begin == "Polarization" || *begin == "Polariz" || *begin == "POLARIZATION"){
            Sys.control.polariz_b = true;
        }

        if (*begin == "MEAM" || *begin == "Meam" || *begin == "meam"){
            Sys.control.meam_b = true;
        }

        if (*begin == "Morse" || *begin == "MORSE" || *begin == "morse"){
            Sys.control.morse_b = true;
        }

        if (*begin == "Buc" || *begin == "BUC" || *begin == "buc" ||
        	*begin == "Buckingham" || *begin == "BUCKINGHAM" || *begin == "buckingham") {
            Sys.control.buk_b = true;
        }

        if (*begin == "Boundary") {
            ++begin;
            while (begin < end) {
                Sys.BoundaryFor.push_back(*begin);
                ++begin;
            }
        }


        if (*begin == "info" || *begin == "Information" || *begin == "Info"){
            ++begin;
            if (*begin == "Medium" || *begin == "medium" || *begin == "MEDIUM")
                Sys.InfLevel = "Medium";

            if (*begin == "Low" || *begin == "low" || *begin == "LOW")
                Sys.InfLevel = "Low";

            if (*begin == "Detailed" || *begin == "detailed" || *begin == "DETAILED")
                Sys.InfLevel = "Detailed";
        }


        if (*begin == "NeuralNet" || *begin == "neuralnet" || *begin == "NNFile" || *begin == "nnfile" || *begin == "NetFile"){
            ++begin;;
            Sys.files.NNinFile = Sys.files.Path + *begin;
            Dat.ReadNNinFile(Sys);
        }

        if (*begin == "NetScale" || *begin == "NETSCALE" || *begin == "netscale") {
            ++begin;
            Sys.net.setScale(funcs::ToDouble(*begin));
        }

        if (*begin == "NetIterations" || *begin == "NETITERATIONS" || *begin == "netiterations") {
            ++begin;
            Sys.net.setIterations(funcs::ToInt(*begin));
        }


        if (*begin == "NetTrainingFile" || *begin == "TrainingFile" || *begin == "trainingFile") {
            ++begin;;
            Sys.files.NetTrainingFile = Sys.files.Path + *begin;
            Dat.ReadNetTrainingFile(Sys);
        }

        if (*begin == "SampleFile" || *begin == "samplefile" || *begin == "NetSample") {
            ++begin;;
            Sys.files.SampleFile = Sys.files.Path + *begin;
            Dat.ReadSampleFile(Sys);
        }


        if (*begin == "Potential" || *begin == "Pot"){
            ++begin;;
            Sys.files.PotenFile = Sys.files.Path + *begin;
            Dat.ReadPotentialFile(Sys);
        }


        if (*begin == "FitData" || *begin == "FITDATA"  || *begin == "fitdata"){
            ++begin;;
            Sys.files.FitFile = Sys.files.Path + *begin;
            Dat.ReadFitFile(Sys);
        }

        if (*begin == "DET" || *begin == "det") {
            Sys.control.det_b = true;
            ++begin;;
            Sys.files.DETFile = Sys.files.Path + *begin;

            Dat.ReadDETFile(Sys);
        }

        if (*begin == "Iterations" || *begin == "iterations" || *begin == "ITERATIONS") {
            ++begin;
            Sys.fit.iterations = funcs::ToInt(*begin);
        }

        if (*begin == "DQ" || *begin == "dq" || *begin == "Dq") {
            ++begin;
            Sys.cond.dq = funcs::ToDouble(*begin);
        }

        if (*begin == "MutProb" || *begin == "mutprob" || *begin == "MUTPROB") {
            ++begin;
            Sys.fit.MutProb = funcs::ToDouble(*begin);
        }

        if (*begin == "Rmelt" || *begin == "rmelt" || *begin == "RMELT") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Rmelt = ValUn.value;
        }


        if (*begin == "HALFLIFE" || *begin == "HalfLife" || *begin == "halflife" || *begin == "Halflife") {
            ++begin;
            Sys.fit.HalfLife = funcs::ToInt(*begin);
            Sys.fit.DecayConstant = 0.693147181 / Sys.fit.HalfLife;
        }

        if (*begin == "Decay" || *begin == "DECAY" || *begin == "decay") {
            ++begin;
            Sys.fit.DecayConstant = funcs::ToDouble(*begin);
            Sys.fit.HalfLife = (int)(0.693147181 / Sys.fit.DecayConstant);
        }

        if (*begin == "Population" || *begin == "population" || *begin == "POPULATION") {
            ++begin;
            Sys.fit.population = funcs::ToInt(*begin);
        }

        if (*begin == "FitParam" || *begin == "Fitparam" || *begin == "fitparam") {
            int ind, fitp;
            ++begin;
            while (begin < end){
                Sys.fit.FitParam[*begin] = true;
                ++begin;
            }

            if (Sys.inf.id == 0)
                cout << "Done read the fit params" << std::endl;
            continue;
        }

        if (*begin == "ForElement" || *begin == "Fitfor" || *begin=="FitFor" || *begin == "fitfor") {
            Sys.fit.ForElement.clear();
            ++begin;
            while (begin < end){
                Sys.fit.ForElement.push_back(*begin);
                ++begin;
            }
            continue;
        }


        if (*begin == "Sigma" || *begin == "SIGMA" || *begin == "sigma") {
            ++begin;
            Sys.fit.sigma    = funcs::ToDouble(*begin);
            Sys.fit.sigmacur = Sys.fit.sigma;
        }

        if (*begin == "Rate" || *begin == "RATE" || *begin == "rate") {
            ++begin;
            Sys.fit.rate = funcs::ToDouble(*begin);
        }

        if (*begin == "Epsilon" || *begin == "EPSILON" || *begin == "epsilon") {
            ++begin;
            Sys.fit.epsilon = funcs::ToDouble(*begin);
        }

        if (*begin == "ParamsGauss" || *begin == "ParamsGauss") {
            ++begin;;

            if (*begin == "FILE" || *begin == "File" || *begin == "file")
            {
                ++begin;;
                Sys.files.PotenFile = Sys.files.Path + *begin;
                RunParamsGauss(Sys);
            }
        }



        if (*begin == "RUN" || *begin == "Run" || *begin == "run") {
            ++begin;

            if (*begin == "Test" || *begin == "TEST" ||  *begin == "test" ) {
                if ((end-begin)>1) {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }
                TestScript();
                continue;
            }

            if (*begin == "MD" || *begin == "md") {
                if ((end-begin)>1) {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }
                RunMD(Sys);
                continue;
            }

            if (*begin == "NN" || *begin == "nn" || *begin == "Net" || *begin == "Neural") {
                if ((end-begin)>1) {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }
                RunNeuralNet(Sys);
                continue;
            }

            if (*begin == "NetFit" || *begin == "netfit" || *begin == "NETFIT" || *begin == "NetTrain") {
                if ((end-begin)>1) {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }
                RunNeuralNetFit(Sys);
                continue;
            }

            if (*begin == "Gen" || *begin == "GEN" || *begin == "gen"){
                ++begin;
                ReadPairs(begin, end, Sys);
                RunGen(Sys);
                continue;
            }

            if (*begin == "Fit" || *begin == "FIT" || *begin == "fit"){
                ++begin;
                if (*begin == "Par" || *begin == "par" || *begin == "PAR")
                {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                    Sys.control.fitexp_b = true;
                    RunFitPar(Sys);
                } else if (*begin == "Seq" || *begin == "seq" || *begin == "SEQ") {
                    ++begin;
                    ReadPairs(begin, end, Sys);
                    Sys.control.fitexp_b = true;
                    RunFitSeq(Sys);
                } else {
                    ReadPairs(begin, end, Sys);
                    Sys.control.fitexp_b = true;
                    RunFitSeq(Sys);
                }
                continue;
            }

            if (*begin == "Grad" || *begin == "GRAD" || *begin == "grad"){
                ++begin;
                ReadPairs(begin, end, Sys);
                RunGrad(Sys);
                continue;
            }

            if (*begin == "Premelt" || *begin == "PremeltR") {
                if ((end-begin)>1) {
                    ++begin;

                    if (*begin == "each" || *begin == "Each" || *begin == "EACH") {
                        Sys.control.premelt_b = true;
                        Sys.control.premeltR_b = true;
                        if ((end-begin)>1){
                            ++begin;
                            ReadPairs(begin, end, Sys);
                        }
                    } else{
                        if ((end-begin)>1){
                            ++begin;
                            ReadPairs(begin, end, Sys);
                        }
                        RunPremelt(Sys);
                    }
                } else {
                    RunPremelt(Sys);
                }
                continue;
            }



            if (*begin == "PremeltL") {
                ++begin;

                if (*begin == "each" || *begin == "Each" || *begin == "EACH") {
                    Sys.control.premelt_b = true;
                    Sys.control.premeltL_b = true;
                    if ((end-begin)>1){
                        ++begin;
                        ReadPairs(begin, end, Sys);
                    }

                } else{
                    if ((end-begin)>1){
                        ++begin;
                        ReadPairs(begin, end, Sys);
                    }
                    RunPremelt(Sys);
                }
                continue;
            }

        }

        if (*begin == "BackUpState") {
            string state;
            ++begin;
            state = *begin;
            Sys.BackUpState(state);
            if (Sys.inf.id == 0)
                cout << "Back-Up State: " << state << std::endl;
        }

        if (*begin == "RestoreState") {
            string state;
            if ((end-begin)>1) {
                ++begin;
                if (*begin == "each" || *begin == "Each" || *begin == "EACH") {
                    Sys.control.restoreState_each_b = true;
                } else {
                        state = *begin;
                        Sys.RestoreState(state);
                        cout << "Restore State: " << state << std::endl;
                }
            } else {
                Sys.RestoreState("default");
            }
            continue;
        }

        if (*begin == "Premelt") {
            ++begin;

            if (*begin == "each" || *begin == "Each" || *begin == "EACH") {
                Sys.control.premelt_b = true;
                if ((end-begin)>1){
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }

            } else{
                if ((end-begin)>1){
                    ++begin;
                    ReadPairs(begin, end, Sys);
                }
                RunPremelt(Sys);
            }
            continue;
        }

        if (*begin == "Boundary") {
            ++begin;
            if (*begin == "Periodic" || *begin == "periodic" || *begin == "PERIODIC") {
                Sys.control.periodic_b  = true;
                Sys.control.box_b       = false;

                Sys.BoundaryFor.clear();
                std::stringstream iss;
                for (int bon = 1; bon <= 27; bon ++) {
                    iss.str("");
                    iss.clear();
                    iss << bon;
                    Sys.BoundaryFor.push_back(iss.str());
                }
            }

            if (*begin == "Box" || *begin == "box" || *begin == "BOX") {
                Sys.control.box_b       = true;
                //Sys.control.periodic_b  = false;

                Sys.BoundaryFor.clear();
                Sys.BoundaryFor.push_back("0");

                Sys.WallFor.clear();
                Sys.WallFor.push_back("-x");
                Sys.WallFor.push_back("+x");
                Sys.WallFor.push_back("-y");
                Sys.WallFor.push_back("+y");
                Sys.WallFor.push_back("-z");
                Sys.WallFor.push_back("+z");
            }

            if (*begin == "For" || *begin == "for" || *begin == "FOR") {
                Sys.BoundaryFor.clear();
                ++begin;
                while (begin < end) {
                    Sys.BoundaryFor.push_back(*begin);
                    ++begin;
                }
                continue;
            }
        }

        if (*begin == "Periodic") {
            Sys.control.periodic_b  = true;
            ++begin;

            if (*begin == "For" || *begin == "for" || *begin == "FOR") {
                Sys.BoundaryFor.clear();
                ++begin;
                while (begin < end) {
                    Sys.BoundaryFor.push_back(*begin);
                    ++begin;
                }
                continue;
            }
        }

        if (*begin == "Wall") {
            ++begin;
            Sys.control.box_b  = true;

            if (*begin == "For" || *begin == "for" || *begin == "FOR") {
                Sys.WallFor.clear();
                ++begin;
                while (begin < end) {
                    Sys.WallFor.push_back(*begin);
                    ++begin;
                }
                continue;
            }
        }


        if (*begin == "Threads") {
            ++begin;
            Sys.inf.Threads = funcs::ToInt(*begin);

            omp_set_dynamic(0);
            omp_set_num_threads(Sys.inf.Threads);
        }

        if (*begin == "Size") {
             Sys.inf.Cell.a = funcs::ToInt(*(++begin));
             Sys.inf.Cell.b = funcs::ToInt(*(++begin));
             Sys.inf.Cell.c = funcs::ToInt(*(++begin));
        }

        if (*begin == "P" || *begin == "Preg" || *begin == "Pressure" || *begin == "pressure") {
            ++begin;
            Sys.cond.Preg = funcs::ToInt(*begin);
            Sys.cond.Pstep = 0.0;
        }

        if (*begin == "T" || *begin == "Treg" || *begin == "Temperature" || *begin == "temperature") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Temperature = ValUn.value;

            Sys.cond.Tstep = 50.0;
            Sys.cond.Tfinal = Sys.cond.Temperature;
        }

        if (*begin == "ForceFile") {
            if ((end-begin)>1) {
                ++begin;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.forcefile_b = false;
                else{
                    Sys.control.forcefile_b = true;
                }
            } else{
                Sys.control.forcefile_b = true;
            }
        }

        if (*begin == "SampleFile" || *begin == "Sample"  || *begin == "samplefile") {
            if ((end-begin)>1) {
                ++begin;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.samplefile_b = false;
                else{
                    Sys.control.samplefile_b = true;
                }
            } else{
                Sys.control.samplefile_b = true;
            }
        }

        if (*begin == "FluxFile") {
            if ((end-begin)>1) {
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.fluxfile_b = false;
                else
                    Sys.control.fluxfile_b = true;
            } else{
                Sys.control.fluxfile_b = true;
            }
        }

        if (*begin == "PosFile") {
            if ((end-begin)>1) {
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No"){
                    Sys.control.posfile_b = false;
                } else if (*begin == "VTK" || *begin == "vtk" || *begin == "VTKanimation") {
                    Sys.control.vtkfile_b  = true;
                } else{
                    Sys.control.posfile_b = true;
                }
            } else{
                Sys.control.posfile_b = true;
            }
        }

        if (*begin == "VTK" || *begin == "SaveVTK" || *begin == "VTKFile"  ) {
                Sys.control.vtkfile_b  = true;
        }

        if (*begin == "iVTK" || *begin == "VTKi") {
            ++begin;
            Sys.inf.iVTK = funcs::ToInt(*begin);
        }

        if (*begin == "Melt") {
            if ((end-begin)>1) {
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.melt_b = false;
                else
                    Sys.control.melt_b = true;
            } else{
                Sys.control.melt_b = true;
            }
        }

        if (*begin == "Pstep" || *begin == "DP") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Pstep = ValUn.value;
        }

        if (*begin == "Tstep" || *begin == "DT") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Tstep = ValUn.value;
        }

        if (*begin == "Tfinal" || *begin == "TF" || *begin == "Tfin") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Tfinal = ValUn.value;
        }

        if (*begin == "T2") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Treg2 = ValUn.value;
        }

        if (*begin == "T1") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Treg1 = ValUn.value;
        }

        if (*begin == "d_tau" || *begin == "dTime" || *begin == "dtau") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.d_tau = ValUn.value;
            Sys.cond.rd_tau  = 1.0/Sys.cond.d_tau;
            Sys.cond.d_taus   = Sys.cond.d_tau;
            Sys.cond.rd_taus  = Sys.cond.rd_tau;
        }

        if (*begin == "d_taus" || *begin == "dTimes" || *begin == "dtaus") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.d_taus = ValUn.value;
            Sys.cond.rd_taus  = 1.0/Sys.cond.d_taus;
        }

        if (*begin == "Time" || *begin == "TimeCalc") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.TimeCalc = ValUn.value;
            Sys.cond.TimeStep = ValUn.value;
        }

        if (*begin == "TimePremelt" || *begin == "Timepremelt" || *begin == "PMTime" || *begin == "pmTime") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.TimePremelt = ValUn.value;
        }

        if (*begin == "TimeStep") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.TimeStep = ValUn.value;
        }

        if (*begin == "AvTime") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Av_Time = ValUn.value;
        }

        if (*begin == "PosAvTime") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Pos_Av_Time = ValUn.value;
        }


        if (*begin == "CalcDiff") {
            if ((end-begin) > 1){
                ++begin;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.diffusion_b = false;
                else
                    Sys.control.diffusion_b = true;
            } else{
                Sys.control.diffusion_b = true;
            }
        }

        if (*begin == "CalcPVT" || *begin == "CalcProperties" || *begin == "Properties" || *begin == "PVT") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.properties_b = false;
                else
                    Sys.control.properties_b = true;
            } else{
                Sys.control.properties_b = true;
            }
        }


        if (*begin == "thermc_b" || *begin == "CalcThC" || *begin == "CalcThermalConductivity" || *begin == "ThermalConductivity" || *begin == "ThermCond") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.thermc_b = false;
            else
                    Sys.control.thermc_b = true;
            } else{
                Sys.control.thermc_b = true;
            }
        }

        if (*begin == "RDFSmoothStep") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.RDFSmoothStep = ValUn.value;
        }

        if (*begin == "CalcRDF" || *begin == "RDF" || *begin == "rdf") {
            Sys.RDF.r.resize(1000);
            std::vector<dType> empyVec;
            empyVec.resize(1000);
            std::fill(empyVec.begin(), empyVec.end(), 0.0);

            rdfPair rdfp;
            Sys.control.rdf_b = true;
            Sys.RDFfor.clear();
            if ((end-begin) > 1){
                ++begin;
                if (*begin == "for") {
                    ++begin;
                    while (begin != end) {
                        std::vector<std::string> res = funcs::splitString(*begin);
                        rdfp.At1 = res[0];
                        rdfp.At2 = res[1];
                        Sys.RDFfor.push_back(rdfp);
                        Sys.RDF.n.push_back(empyVec);
                        ++begin;
                    }
                }
            }
            continue;
        }

        if (*begin == "PositionAnalysis" || *begin == "PosAn" || *begin == "PAn") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.posanalysis_b = false;
                else
                    Sys.control.posanalysis_b = true;
            } else{
                Sys.control.posanalysis_b = true;
            }
        }


        if (*begin == "InitTemp" || *begin == "TInit" || *begin == "Tinit") {

            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.inittemp_b = false;
                else{
                    Sys.control.inittemp_b = true;
                    ValUn = Sys.getValue(*begin);
                    Sys.cond.Tinitial = ValUn.value;
                }
            } else{
                Sys.control.inittemp_b = true;
            }
        }


        if (*begin == "pressure_b" || *begin == "RegPres" || *begin == "RegPress") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.pressure_b = false;
                else
                    Sys.control.pressure_b = true;
            } else{
                Sys.control.pressure_b = true;
            }
        }

        if (*begin == "UpdatePos" || *begin == "Move" || *begin == "move") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.posupdate_b = false;
                else
                    Sys.control.posupdate_b = true;
            } else{
                Sys.control.posupdate_b = true;
            }
        }

        if (*begin == "temperature_b" || *begin == "RegTemp" || *begin == "RegTemper" || *begin == "RegTemperature") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.temperature_b = false;
                else
                    Sys.control.temperature_b = true;
            } else{
                Sys.control.temperature_b = true;
            }
        }

        if (*begin == "RegPos") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.position_b = false;
                else
                    Sys.control.position_b = true;
            } else{
                Sys.control.position_b = true;
            }
        }

        if (*begin == "RegVel" || *begin == "RegVelocity") {
            if ((end-begin) > 1){
                ++begin;;
                if (*begin == "no" || *begin == "0" || *begin == "false" || *begin == "No")
                    Sys.control.velocity_b = false;
                else
                    Sys.control.velocity_b = true;
            } else{
                Sys.control.velocity_b = true;
            }
        }



        if (*begin == "Cl") {
            ++begin;
            Sys.cond.setCl( funcs::ToInt(*begin) );
        }

        if (*begin == "P_rel" || *begin == "p_rel" || *begin == "Prel" || *begin == "prel") {
            ++begin;
            Sys.cond.P_rel = funcs::ToDouble(*begin);
        }

        if (*begin == "P_rel_fin" || *begin == "p_rel_fin" || *begin == "prelfin"  || *begin == "Prelfin") {
            ++begin;
            Sys.cond.P_rel_fin = funcs::ToDouble(*begin);
        }

        if (*begin == "T_rel" || *begin == "t_rel" || *begin == "Trel" || *begin == "trel") {
            ++begin;
            Sys.cond.T_rel = funcs::ToDouble(*begin);
        }

        if (*begin == "T_rel_fin") {
            ++begin;
            Sys.cond.T_rel_fin = funcs::ToDouble(*begin);
        }

        if (*begin == "Rc_short") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Rc_short = ValUn.value;
            Sys.cond.Rc_short2   = Sys.cond.Rc_short * Sys.cond.Rc_short;
        }

        if (*begin == "Rc_meam" || *begin == "Rc_MEAM") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Rc_meam = ValUn.value;
        }

        if (*begin == "Rc_long") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.Rc_long = ValUn.value;
            Sys.cond.Rc_long2    =  Sys.cond.Rc_long *  Sys.cond.Rc_long;
            Sys.cond.rRc_long    = 1.0 /  Sys.cond.Rc_long;
            Sys.cond.Rc_long_Bohr = 1.889725989 * Sys.cond.Rc_long;
        }

        if (*begin == "RcAlpha" || *begin == "Rcalpha" || *begin == "RC_Alpha" || *begin == "Alpha") {
            ++begin;
            Sys.cond.alpha = funcs::ToDouble(*begin);
        }


        if (*begin == "StartThC") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.cond.StartThC = ValUn.value;
        }


        if (*begin == "WallX") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.inf.WallX = ValUn.value;
        }

        if (*begin == "Walls") {
            ++begin;
            ValUn = Sys.getValue(*begin);
            Sys.inf.Walls = ValUn.value;
        }


        if (*begin == "Forces") {
            ++begin;;
            if (*begin == "FILE" || *begin == "File" || *begin == "file") {
                ++begin;
                Sys.files.InForceFile = Sys.files.Path + *begin;
                Dat.ReadForceFile(Sys);
            }
        }

        if (*begin == "Positions" || *begin == "Position" || *begin == "Configuration" || *begin == "Config" ){
            string name;
            string path;
            Sys.control.initialization_b = false;
            Sys.control.state_b          = false;

            ++begin;;
            Sys.files.XYZInFile = Sys.files.Path + *begin;

            path      = string(Sys.files.XYZInFile.begin(), std::find(Sys.files.XYZInFile.begin(), Sys.files.XYZInFile.end(), '.'));
            Sys.files.StateFile = path + ".state";
            Sys.files.XYZFile   = path + "_pos.xyz";

            Dat.ReadPositionFile(Sys);

            if (Sys.inf.id == 0)
                cout << Sys.files.StateFile.c_str()<<endl;
            Dat.ClearFile(Sys.files.XYZFile);

            if (Sys.inf.id == 0)
                cout << "Done loading the Positions File ... \n";
        }

		if (*begin == "PosOut" || *begin == "Posout" || *begin == "PositionOut" || *begin == "positionsout" ){
            string name;
            string path;

            ++begin;;
            Sys.files.XYZFile = Sys.files.Path + *begin;
            Dat.ClearFile(Sys.files.XYZFile);
		}

		if (*begin == "StateOut" || *begin == "Stateout" || *begin == "StateOut" || *begin == "stateout" ){
            string name;
            string path;

            ++begin;;
            Sys.files.StateFile = Sys.files.Path + *begin;
		}

        if (*begin == "Restart" || *begin == "restart" || *begin == "State" || *begin == "state") {
            string path;
            Sys.control.initialization_b = false;
            Sys.control.state_b      = true;

            ++begin;
            Sys.files.XYZInFile = Sys.files.Path + *begin;

            path          = string(Sys.files.XYZInFile.begin(), std::find(Sys.files.XYZInFile.begin(), Sys.files.XYZInFile.end(), '.'));
            Sys.files.StateFile = path + ".state";
            Sys.files.XYZFile   = path + "_pos.xyz";

            path      = string(Sys.files.StateFile.begin(), std::find(Sys.files.StateFile.begin(), Sys.files.StateFile.end(), '.'));
            Sys.files.XYZFile   = path + "_pos.xyz";

            Dat.Restart(Sys);
            Dat.ClearFile(Sys.files.XYZFile);
            if (Sys.inf.id == 0)
                cout << "Done loadiung the Restart File ... \n";
        }

        if (*begin == "SaveState") {
            string path;
            ++begin;

            path = Sys.files.Path + *begin + ".state";
            Dat.SaveStateFile(Sys, path);
            continue;
        }
        ++begin;
    }
}
