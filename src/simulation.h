//-------------------------------------------------------------------
// File:   simulation.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#pragma once
//#include <mpi.h>
//#include <ifstream>
//#include "amody_h5hut.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <string>
#include <iostream>
#include <time.h>
#include "system.h"
#include "datamanager.h"
#include "datatypes.h"


#ifndef Simulation_INCLUDED
#define Simulation_INCLUDED

class DataManager;

using std::vector;
using std::map;

class Simulation: public System
{
public:
    Simulation();

    System Sys;
    DataManager Dat;

    void ReadTheImput(int argc, char** argv, System &Sys);
    void ReadFile(int argc, char** argv, System &Sys);
    void ReadPairs(std::vector<std::string>::iterator, std::vector<std::string>::iterator, System &Sys);

    void MDStep(System &Sys);
    void PremeltRegion(System &Sys);
    void Premelt(System &Sys);
    dType ElEnergy(Atom& Ati, Atom& Atj);
    //void EAM_FS();
    //void MEAM();
    //void BMH();
    //void Yakub();
    //void Arima();
    void Premelt05(System &Sys);
    void Premelt10(System &Sys);
    void RunMD(System &Sys);
    void RunNeuralNet(System &Sys);
    void RunNeuralNetFit(System &Sys);
    void RunPremelt(System &Sys);
    void RunGen(System &Sys);
    void RunFitSeq(System &Sys);
    void RunFitPar(System &Sys);
    void RunParamsGauss(System &Sys);
    void RunGrad(System &Sys);
    void AtEnergy(int i);
    void DETStep(System &Sys);


    struct Compare{
        bool operator()(const std::map<std::string, Params>& a, const std::map<std::string, Params>& b){
            return a.begin()->second.fitres < b.begin()->second.fitres;
        }
    };

    void Populate(System &Sys);
    double CalcFitRes(System &Sys);
    void SortCoeffs(System &Sys);
    void FitnessFunctionForceFit(System &Sys);
    void FitnessFunctionExperiment(System &Sys, const int& it);
    void FitnessFunctionExperimentCurrent(System &Sys, const int& it);
    void CopyCoef(System &Sys);
    void CloneTheBest(System &Sys);

    dType CheckStructure(System &Sys);
    dType CompareRDFs(System &Sys);

    //void Crossover();
    //void Mutation();
    void MutationDouble(System &Sys);
    void CrossoverDouble(System &Sys);
    void Damp(System &Sys);
    void StartFit(System &Sys);
    //void SortRes();
    void ParametersMutaion(System &Sys);

    void CalcGradient(System &Sys);

    void GetParams(System &Sys);
    void BCastParams(System &Sys);
    void TestScript();
    void PositionAnalysis(System &Sys);
};

#endif
