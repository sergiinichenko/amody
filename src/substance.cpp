//-------------------------------------------------------------------
// File:   substance.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include <iostream>
#include "substance.h"
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <time.h>       /* time */

void Substance::CreateFCC(std::string name1, std::string name2, int size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = (1 + 3) * size * size * size;

    stream << AtomN << "\n";
    stream << size*A_lat << "   " << size*A_lat << "   " << size*A_lat << "   " << "\n";
    File << stream.str();

    std::cout<<"Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size; ix++)
    {
        for (int iy = 0; iy < size; iy++)
        {
            for (int iz = 0; iz < size; iz++)
            {
                //CREATION OF ATOMS OF NAME1
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //CREATION OF ATOMS OF NAME2
                //1
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;
            }
        }
    }


    for (int i=0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }
    File.close();
}

void Substance::CreateSC(std::string name1, int size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = 2 * size * size * size;

    stream << AtomN << "\n";
    stream << size*A_lat << "   " << size*A_lat << "   " << size*A_lat << "   " << "\n";
    File << stream.str();

    std::cout << "Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size; ix++)
    {
        for (int iy = 0; iy < size; iy++)
        {
            for (int iz = 0; iz < size; iz++)
            {
                //CREATION OF URANIUM MOLECULES
                //1
                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;
            }
        }
    }

    for (int i = 0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }

    File.close();
}

void Substance::CreateBCC(std::string name1, int size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = 2 * size * size * size;

    stream << AtomN << "\n";
    stream << size*A_lat << "   " << size*A_lat << "   " << size*A_lat << "   " << "\n";
    File << stream.str();

    std::cout << "Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size; ix++)
    {
        for (int iy = 0; iy < size; iy++)
        {
            for (int iz = 0; iz < size; iz++)
            {
                //CREATION OF URANIUM MOLECULES
                //1
                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;
            }
        }
    }

    for (int i = 0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }

    File.close();
}

void Substance::CreateFluoride(std::string name1, std::string name2, int* size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = (4 + 4) * size[0] * size[1] * size[2];

    stream << AtomN << "\n";
    stream << size[0]*A_lat << "   " << size[1]*A_lat << "   " << size[2]*A_lat << "   " << "\n";
    File << stream.str();

    std::cout<<"Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size[0]; ix++)  {
        for (int iy = 0; iy < size[1]; iy++)  {
            for (int iz = 0; iz < size[2]; iz++)  {
                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;
            }
        }
    }


    for (int i=0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }

    File.close();
}

void Substance::CreateUF4(std::string name1, std::string name2, int* size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = (4 + 4) * size[0] * size[1] * size[2];

    stream << AtomN << "\n";
    stream << size[0]*A_lat << "   " << size[1]*A_lat << "   " << size[2]*A_lat << "   " << "\n";
    File << stream.str();

    std::cout<<"Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size[0]; ix++)  {
        for (int iy = 0; iy < size[1]; iy++)  {
            for (int iz = 0; iz < size[2]; iz++)  {
                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;
            }
        }
    }


    for (int i=0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }

    File.close();
}


void Substance::CreateNaCl(std::string name1, std::string name2, int size, double A_lat, std::string file)
{
    std::stringstream stream;

    std::fstream File;
    File.open(file.c_str(), std::ios::out);

    int AtomN = (4 + 4) * size * size * size;

    stream << AtomN << "\n";
    stream << "Results" << "\n";
    File << stream.str();

    std::cout<<"Creating the input file...\n";
    Atom At;
    At.Resize(AtomN);

    int ind = 0;

    for (int ix = 0; ix < size; ix++)
    {
        for (int iy = 0; iy < size; iy++)
        {
            for (int iz = 0; iz < size; iz++)
            {
                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0;
                At.name[ind] = name1;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name1;
                ind += 1;

                //CREATION OF URANIUM MOLECULES
                //1
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //2
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.0;
                At.name[ind] = name2;
                ind += 1;

                //3
                At.x[ind] = ix * A_lat + A_lat * 0.0;
                At.y[ind] = iy * A_lat + A_lat * 0.0;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;

                //4
                At.x[ind] = ix * A_lat + A_lat * 0.5;
                At.y[ind] = iy * A_lat + A_lat * 0.5;
                At.z[ind] = iz * A_lat + A_lat * 0.5;
                At.name[ind] = name2;
                ind += 1;
            }
        }
    }


    for (int i=0; i<AtomN; i++)
    {
        stream.str("");
        stream.clear();
        stream.fill(' ');

        stream << At.name[i] << "\t";

        stream.setf(std::ios::fixed);
        stream.width(8);
        stream.precision(3);
        stream << At.x[i] << "\t" << At.y[i] << "\t" << At.z[i] << "\t" << "\n";
        File << stream.str();

    }

    File.close();
}
