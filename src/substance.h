//-------------------------------------------------------------------
// File:   substance.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include <iostream>
#include "atom.h"
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <time.h>       /* time */

#ifndef Substance_INCLUDED
#define Substance_INCLUDED

class Substance
{
public:
    void CreateAtom(Atom& At, std::string name);
    void CreateFCC(std::string name1, std::string name2, int size, double A_lat, std::string file);
    void CreateSC(std::string name1, int size, double A_lat, std::string file);
    void CreateBCC(std::string name1, int size, double A_lat, std::string file);
    void CreateFluoride(std::string name1, std::string name2, int* size, double A_lat, std::string file);
    void CreateUF4(std::string name1, std::string name2, int* size, double A_lat, std::string file);
    void CreateNaCl(std::string name1, std::string name2, int size, double A_lat, std::string file);
};

#endif
