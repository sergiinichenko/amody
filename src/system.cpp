//-------------------------------------------------------------------
// File:   system.cpp
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#include "system.h"
#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>

#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <cmath>
#include <algorithm>

bool IsNan (double x)  { return std::isnan(x); }
bool IsInf (double x)  { return std::isinf(x); }
bool IsnFVal(double x) {return !(x < 1.0e20 && x > -1.0e20); }
bool IsLow(double x)   {return x < -1.0e8; }
bool IsHihg(double x)  {return x >  1.0e8; }
bool IsNeg (double x)  { return x < 0.0; }


System::System() {
    props.Reset();
    control.Reset();
    inf.Reset();
    fit.Reset();
    

    BoundaryFor.clear();
    std::ostringstream iss;
    for (int bon = 1; bon <= 27; bon ++) {
        iss.str("");
        iss.clear();
        iss << bon;
        BoundaryFor.push_back(iss.str());
    }

    WallFor.clear();
    WallFor.push_back("-x");
    WallFor.push_back("+x");
    WallFor.push_back("-y");
    WallFor.push_back("+y");
    WallFor.push_back("-z");
    WallFor.push_back("+z");


    InfLevel  = "Medium";


    #undef max
    #undef min

    fit.Time.clear();
}

void System::Reset() {
    // Reset the system props to default values
    props.Reset();
    control.Reset();
    inf.Reset();
    
    if (inf.id == 0)
        std::cout<<"Reset...\n";
}

void System::RestoreImages() {
    int im, j, im0, sc, nn, ImN;
    std::vector<std::string>::const_iterator iter;

    inf.ImPos[0].x = 0.0;
    inf.ImPos[0].y = 0.0;
    inf.ImPos[0].z = 0.0;
    ImN = 1;

    for (iter = BoundaryFor.begin(); iter != BoundaryFor.end(); iter++) {
        if (*iter == "1") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
	    ImN += 1;
        }

        if (*iter == "2") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,       -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,       -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,       -inf.Cell.b, -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "3") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, -inf.Cell.b, -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "4") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, 0.0,      -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, 0.0,      -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, 0.0,      -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "5") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,       0.0,      -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,       0.0,      -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,       0.0,      -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "6") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, 0.0,   -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, 0.0,   -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, 0.0,   -inf.Cell.c);
            ImN += 1;
        }


        if (*iter == "7") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "8") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,       inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,       inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,       inf.Cell.b, -inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "9") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, inf.Cell.b, -inf.Cell.c);
            ImN += 1;
        }



        if (*iter == "10") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, -inf.Cell.b, 0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, -inf.Cell.b, 0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, -inf.Cell.b, 0.0);
            ImN += 1;
        }

        if (*iter == "11") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,       -inf.Cell.b, 0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,       -inf.Cell.b, 0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,       -inf.Cell.b, 0.0);
            ImN += 1;
        }

        if (*iter == "12") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, -inf.Cell.b, 0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, -inf.Cell.b, 0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, -inf.Cell.b, 0.0);
            ImN += 1;
        }



        if (*iter == "13") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, 0.0,      0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, 0.0,      0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, 0.0,      0.0);
            ImN += 1;
        }

        if (*iter == "14") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, 0.0,       0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, 0.0,       0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, 0.0,       0.0);
            ImN += 1;
        }


        if (*iter == "15") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, inf.Cell.b,   0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, inf.Cell.b,   0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, inf.Cell.b,   0.0);
            ImN += 1;
        }

        if (*iter == "16") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,     inf.Cell.b,     0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,     inf.Cell.b,     0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,     inf.Cell.b,     0.0);
            ImN += 1;
        }

        if (*iter == "17") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, inf.Cell.b, 0.0);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, inf.Cell.b, 0.0);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, inf.Cell.b, 0.0);
            ImN += 1;
        }



        if (*iter == "18") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "19") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,     -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,     -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,     -inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "20") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, -inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }



        if (*iter == "21") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, 0.0,    inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, 0.0,    inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, 0.0,    inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "22") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,     0.0,    inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,     0.0,    inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,     0.0,    inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "23") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, 0.0,   inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, 0.0,   inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, 0.0,   inf.Cell.c);
            ImN += 1;
        }


        if (*iter == "24") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(-inf.Cell.a, inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(-inf.Cell.a, inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(-inf.Cell.a, inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "25") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(0.0,       inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(0.0,       inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(0.0,       inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }

        if (*iter == "26") {
            inf.ImPos[ImN].x = inf.Cell.tr_x(inf.Cell.a, inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].y = inf.Cell.tr_y(inf.Cell.a, inf.Cell.b, inf.Cell.c);
            inf.ImPos[ImN].z = inf.Cell.tr_z(inf.Cell.a, inf.Cell.b, inf.Cell.c);
            ImN += 1;
        }
    }

    if (inf.id == 0)
        std::cout << "Done restoring images .... \n";
}

int System::getKey(std::string& name) {
    int ret = 0;
    std::string::const_iterator it;

    for (it = name.begin(); it != name.end(); it++) {
        ret += maps.char_to_int[*it];
    }
    return ret;    
}

void System::AssignParams(std::map<std::string, Params>& params){
    int i;
    dType q, Rc_B, qq, Rc_B_eff;
    std::string name;

    //#pragma omp parallel for private(i, name)
    for (i=0; i<At.size; i++) {
        name  = At.name[i];
        q     = params[name].q;
        if (!control.state_b) At.q[i] = params[name].q;

        At.a[i]        = params[name].a;
        At.b[i]        = params[name].b;
        At.mass[i]     = params[name].mass * 103.642778314; // a.u. to (eV*fs2/A2)
        At.rmass[i]    = 1.0/At.mass[i];
        At.code[i]     = getKey(At.name[i]);
        At.rc_meam[i]  = params[name].rc_meam;

        At.ElSelf[i] = 0.0;

        if (control.coulomb_b) {
            Rc_B = 1.889725989 * cond.Rc_long;
            qq = At.q[i] * At.q[i];
            At.ElSelf[i] =  (  erfc(cond.alpha * Rc_B)  / (2.0 * Rc_B) + cond.alpha / std::sqrt(PI) ) * qq;
        }

        if (control.polariz_b) {
            At.polb[i]      = params[name].polb;
            At.polc[i]      = params[name].polc;
            At.polariz[i]   = params[name].polariz;
            if (At.polariz[i] == 0.0)
                At.rpolariz[i]  = 0.0;
            else
                At.rpolariz[i]  = 1.0/At.polariz[i];
        }
    }

    #ifdef debug 
        std::cout << "AssignParams step 1 ... " << std::endl;
     #endif

    std::map<std::string, Params>::const_iterator iteri;
    std::map<std::string, Params>::const_iterator iterj;

    int s = 50000, key, keyi, keyj;
    //int s = 50, key, keyi, keyj;
    dType d, val, rr;
    std::vector<dType> Dr(s), rDr(s);
    ForceEnergy FE, FEc;
    std:vector<dType> R, F, Fcalc, Ecalc, Rmeam;
    std::string namei, namej;

    R.resize(s);
    Rmeam.resize(s);

    std::vector<dType> Rvec;
    std::vector<dType> Fvec;
    std::vector<std::string> namestr;
    namestr.clear();

    Rvec.clear();
    Fvec.clear();

    std::vector<dType> zero(s, 0.0);
    std::vector<dType> one(s, 1.0);
    FadV<dType> XRe(s);
    FadV<dType> XReC(s);

    std::vector<dType> Rc(s);
    std::vector<dType> Re(s);

    FadV<dType> rhoaTot(s);
    FadV<dType> rhoaTotC(s);
    FadV<dType> rhoa0(s);
    FadV<dType> rhoa1(s);
    FadV<dType> rhoa2(s);
    FadV<dType> rhoa3(s);
    FadV<dType> rhoa0c(s);
    FadV<dType> rhoa1c(s);
    FadV<dType> rhoa2c(s);
    FadV<dType> rhoa3c(s);
    FadV<dType> Xc(s);
    std::vector<dType> XcD(s);
    FadV<dType> X(s);
    FadV<dType> rX(s);
    std::vector<dType> FcutD(s);

    FadV<dType> ra0(s);


    FE.Resize(s, 0);
    FEc.Resize(s, 0);

    #ifdef debug 
        std::cout << "AssignParams step 2 ... " << std::endl;
     #endif

    for (std::map<int, LUTable>::iterator it = tab.Tab_Force.table.begin(); it != tab.Tab_Force.table.end(); ++it) {
    	it->second.setSize(s);
    	it->second.setZero();
    }

    for (std::map<int, LUTable>::iterator it = tab.Tab_Energy.table.begin(); it != tab.Tab_Energy.table.end(); ++it) {
    	it->second.setSize(s);
    	it->second.setZero();
    }


    rr = 0.0;
	for (i = 0; i < s; i++) {
		d = rr + cond.Rc_long * i / s;
		R[i] = d;
	}

    FcutD = CutFuncD(R, 6.0);

    tab.FcutD.setSize(s);
    tab.FcutD.setZero();
    tab.FcutD.defineTable(R, FcutD);

    #ifdef debug 
        std::cout << "AssignParams step 3 ... " << std::endl;
     #endif

	for ( iteri = params.begin(); iteri != params.end(); ++iteri ) {
        namei = iteri->first;
        if ( params[namei].pair ) {

        	key = getKey(params[namei].ni) + 10 * getKey(params[namei].nj);// int( params[params[namei].ni].mass * 103.642778314 + 10 * params[params[namei].nj].mass * 103.642778314 );

			FEc = GetFERPair( namei, R );

			FE.F = FEc.F + tab.Tab_Force.getValue(key, R);
			FE.E = FEc.E + tab.Tab_Energy.getValue(key, R);

			tab.Tab_Force.table[key].defineTable(R, FE.F);
			tab.Tab_Energy.table[key].defineTable(R, FE.E);
        }
    }

    #ifdef debug 
        std::cout << "AssignParams step 4 ... " << std::endl;
     #endif

    for ( iteri = params.begin(); iteri != params.end(); ++iteri ) {

        namei = iteri->first;

		for ( iterj = params.begin(); iterj != params.end(); ++iterj ) {

			namej = iterj->first;

            #ifdef debug 
                std::cout << "AssignParams step 4 1 ... " << std::endl;
            #endif
			if ( !params[namei].pair && !params[namej].pair ) {

				key = getKey(namei) + 10 * getKey(namej); //int( params[namei].mass * 103.642778314 + 10 * params[namej].mass * 103.642778314 );

				FEc = GetFER(namei, namej, R);

                #ifdef debug 
                    std::cout << "AssignParams step 4 1 1 ... " << std::endl;
                #endif

				FE.F = FEc.F + tab.Tab_Force.getValue(key, R);
				FE.E = FEc.E + tab.Tab_Energy.getValue(key, R);

                #ifdef debug 
                    std::cout << "AssignParams step 4 1 2 ... " << std::endl;
                #endif

				tab.Tab_Force.table[key].defineTable(R, FE.F);
				tab.Tab_Energy.table[key].defineTable(R, FE.E);

                #ifdef debug 
                    std::cout << "AssignParams step 4 2 ... " << std::endl;
                #endif

            }
		}


		rr = 0.0;

        key = getKey(namei); //int( params[namei].mass * 103.642778314 + 10 * params[namej].mass * 103.642778314 );

        for (i = 0; i < s; i++) {
            Rmeam[i] = 1.2 * params[namei].rc_meam * i / s;;
        }

        FcutD = CutFuncD(Rmeam, params[namei].rc_meam);

        /*
        for (int i = 0; i < Rmeam.size(); i++) {
            std::cout << Rmeam[i] << ":" << FcutD[i] << "  ";
        }
        std::cin.ignore();
        */

        std::fill(Re.begin(), Re.end(), params[namei].re );
        std::fill(Rc.begin(), Rc.end(), params[namei].rc_meam );
        #ifdef debug 
            std::cout << "AssignParams step 4 3 ... " << std::endl;
        #endif

        X._val  = Rmeam;
        X._dx   = one;
        Xc._val = Rc;
        Xc._dx  = one;

        XRe    = ( X / Re - 1.0 );
        XReC   = (Xc / Re - 1.0 );


        #ifdef debug 
            std::cout << "AssignParams step 4 4 ... " << std::endl;
        #endif

        rhoa0 = params[namei].rho0 * exp( - params[namei].beta0 * XRe);
        rhoa1 = params[namei].rho0 * exp( - params[namei].beta1 * XRe);
        rhoa2 = params[namei].rho0 * exp( - params[namei].beta2 * XRe);
        rhoa3 = params[namei].rho0 * exp( - params[namei].beta3 * XRe);
        rhoaTot = rhoa0 + rhoa1 + rhoa2 + rhoa3;

        rhoa0c = params[namei].rho0 * exp( - params[namei].beta0 * XReC);
        rhoa1c = params[namei].rho0 * exp( - params[namei].beta1 * XReC);
        rhoa2c = params[namei].rho0 * exp( - params[namei].beta2 * XReC);
        rhoa3c = params[namei].rho0 * exp( - params[namei].beta3 * XReC);
        rhoaTotC = rhoa0c + rhoa1c + rhoa2c + rhoa3c;

        #ifdef debug 
            std::cout << "AssignParams step 4 5 ... " << std::endl;
        #endif

        rhoa0._val = (rhoa0._val - rhoa0c._val) * FcutD;
        rhoa0._dx  = (rhoa0._dx  - rhoa0c._dx)  * FcutD;

        rhoa1._val = (rhoa1._val - rhoa1c._val) * FcutD;
        rhoa1._dx  = (rhoa1._dx  - rhoa1c._dx)  * FcutD;

        rhoa2._val = (rhoa2._val - rhoa2c._val) * FcutD;
        rhoa2._dx  = (rhoa2._dx  - rhoa2c._dx)  * FcutD;

        rhoa3._val = (rhoa3._val - rhoa3c._val) * FcutD;
        rhoa3._dx  = (rhoa3._dx  - rhoa3c._dx)  * FcutD;

        rhoaTot._val = (rhoaTot._val - rhoaTotC._val) * FcutD;
        rhoaTot._dx  = (rhoaTot._dx - rhoaTotC._dx) * FcutD;


        #ifdef debug 
            std::cout << "AssignParams step 4 6 ... " << std::endl;
        #endif


        tab.Tab_rho0_val.table[key].defineTable(Rmeam, rhoa0._val);
        tab.Tab_rho0_dx.table[key].defineTable( Rmeam, rhoa0._dx);

        tab.Tab_rho1_val.table[key].defineTable(Rmeam, rhoa1._val);
        tab.Tab_rho1_dx.table[key].defineTable( Rmeam, rhoa1._dx);

        tab.Tab_rho2_val.table[key].defineTable(Rmeam, rhoa2._val);
        tab.Tab_rho2_dx.table[key].defineTable( Rmeam, rhoa2._dx);

        tab.Tab_rho3_val.table[key].defineTable(Rmeam, rhoa3._val);
        tab.Tab_rho3_dx.table[key].defineTable( Rmeam, rhoa3._dx);

        tab.Tab_rho_val.table[key].defineTable(Rmeam, rhoaTot._val);
        tab.Tab_rho_dx.table[key].defineTable(Rmeam, rhoaTot._dx);

        #ifdef debug 
            std::cout << "AssignParams step 4 7 ... " << std::endl;
        #endif
    }
    #ifdef debug 
        std::cout << "AssignParams step 5 ... " << std::endl;
     #endif


}

ForceEnergy System::GetFER(std::string& namei, std::string& namej, dType& R) {
    ForceEnergy res;
    std::vector<dType> X(1);
    X[0] = R;

    res = GetFER(namei, namej, X);
    return res;
}

ForceEnergy System::GetFER(std::string& namei, std::string& namej, std::vector<dType>& R) {
    ForceEnergy res;
    std::string pair;
    int s = R.size();
    dType d, Dab, alphaab, rab,  qq, rRc_long_Bohr, ExpRc, erfcRC;
    dType Aab, Cab;

    std:vector<dType> F(s);

    FadV<dType> ExpVal(s);
    FadV<dType> Energy(s);
    FadV<dType> erfFunc(s);
    FadV<dType> XBohr(s);
    FadV<dType> XCBohr(s);
    FadV<dType> X(s);
    std::vector<dType> erfcR_Sc;
    std::vector<dType> R_sc_Bohr;
    std::vector<dType> rR_sc_Bohr;

    X = assignValV(R);

    std::fill(F.begin(), F.end(), 0.0);

    res.Resize(s);
    res.Fill(0.0);


    if (control.buk_b) {
		Aab    = params[namei].buc_Aab * params[namej].buc_Aab;
		rab    = params[namei].buc_rab + params[namej].buc_rab;
		Cab    = params[namei].buc_Cab * params[namej].buc_Cab;
		Dab    = params[namei].buc_Dab * params[namej].buc_Dab;

		if (Aab > 0.0 && rab > 0.0) {
			ExpVal = Aab * exp( - X / rab) - Cab / pow(X, 6.0);
			ExpRc  = Aab * exp( - cond.Rc_short / rab) - Cab / std::pow(cond.Rc_short, 6.0) - Dab / std::pow(cond.Rc_short, 8.0);

			Energy = ExpVal - ExpRc;
			res.F  = res.F + (-Energy._dx );
			res.E  = res.E + ( Energy._val);
		}
    }

    if (control.morse_b)  {

		Dab     = params[namei].DM * params[namej].DM;
		alphaab = params[namei].alphaM + params[namej].alphaM;
		rab     = params[namei].r + params[namej].r;

		if (Dab > 0.0 && rab > 0.0) {
			ExpVal = exp( - alphaab * (X - rab));
			ExpRc  = exp( - alphaab * (cond.Rc_short - rab));

			Energy = Dab * ((ExpVal * ExpVal - 2.0 * ExpVal) - ((ExpRc * ExpRc - 2.0 * ExpRc)));
			res.F  = res.F + (-Energy._dx);
			res.E  = res.E + (Energy._val);
		}
    }

    if (control.coulomb_b)  {
        qq = params[namei].q * params[namej].q;

        R_sc_Bohr  = 1.889725989 * R;
        std::vector<dType>  Rc_Bohr(s, cond.Rc_long_Bohr);

        XBohr  = assignValV(R_sc_Bohr);
        XCBohr = assignValV(Rc_Bohr);

        Energy = qq * erfc(cond.alpha * XBohr) / XBohr - qq * erfc(cond.alpha * XCBohr)  /XCBohr;

        res.F  = res.F + (- 51.422082 * Energy._dx);
        res.E  = res.E + (  27.211396 * Energy._val);
    }
    return res;
}


ForceEnergy System::GetFERPair(std::string& pair, std::vector<dType>& R) {
    ForceEnergy res;
    int s = R.size();
    dType d, Dab, alphaab, rab,  qq, rRc_long_Bohr, ExpRc, erfcRC;
    dType Aab, Cab;

    FadV<dType> ExpVal(s);
    FadV<dType> Energy(s);
    FadV<dType> erfFunc(s);
    FadV<dType> XBohr(s);
    FadV<dType> XCBohr(s);
    FadV<dType> X(s);
    std::vector<dType> erfcR_Sc;
    std::vector<dType> R_sc_Bohr;
    std::vector<dType> rR_sc_Bohr;

    X = assignValV(R);

    res.Resize(s);
    res.Fill(0.0);

    if (control.buk_b) {
		Aab     = params[pair].buc_Aab;
		rab     = params[pair].buc_rab;
		Cab     = params[pair].buc_Cab;
		Dab 	= params[pair].buc_Dab;

		if (Aab > 0.0 && rab > 0.0) {
			ExpVal = Aab * exp( - X / rab) - Cab / pow(X, 6.0);
			ExpRc  = Aab * exp( - cond.Rc_short / rab) - Cab / std::pow(cond.Rc_short, 6.0) - Dab / std::pow(cond.Rc_short, 8.0);

			Energy = ExpVal - ExpRc;
			res.F  = res.F + (-Energy._dx );
			res.E  = res.E + ( Energy._val);
		}
    }

    if (control.morse_b)  {
		Dab     = params[pair].DM;
		alphaab = params[pair].alphaM;
		rab     = params[pair].r;

		if (Dab > 0.0 && rab > 0.0) {
			ExpVal = exp( - alphaab * (X - rab));
			ExpRc  = exp( - alphaab * (cond.Rc_short - rab));

			Energy = Dab * ((ExpVal * ExpVal - 2.0 * ExpVal) - ((ExpRc * ExpRc - 2.0 * ExpRc)));

			res.F  = res.F + (-Energy._dx);
			res.E  = res.E + ( Energy._val);
		}
    }

    return res;
}

void System::BackUpState(std::string state) {
    States[state].At = At;
    States[state].ImPos = inf.ImPos;
    States[state].Cell = inf.Cell;
    States[state].Wall = inf.Wall;
}

void System::RestoreState(std::string state) {
    At = States[state].At;
    inf.ImPos = States[state].ImPos;
    inf.Cell = States[state].Cell;
    inf.Wall = States[state].Wall;
}

void System::SetInitPositions() {
    int i;
    At.x0 = At.x;
    At.y0 = At.y;
    At.z0 = At.z;

    At.xav = At.x;
    At.yav = At.y;
    At.zav = At.z;

    At.xd = At.x;
    At.yd = At.y;
    At.zd = At.z;

    At.xp = At.x;
    At.yp = At.y;
    At.zp = At.z;
}

void System::ResetPositions() {
    At.x = At.x0;
    At.y = At.y0;
    At.z = At.z0;

    At.xav = At.x;
    At.yav = At.y;
    At.zav = At.z;

    At.xd = At.x;
    At.yd = At.y;
    At.zd = At.z;

    At.xp = At.x;
    At.yp = At.y;
    At.zp = At.z;
}

void System::Initialization() {
    std::string name;
    srand(static_cast<int>(time(0)));
    Spline spline;
    bool updatespline = true;
    int range = 0;

    inf.Reset();
    inf.dID = inf.id;
    inf.dNP = inf.num_procs;


    if (!control.initialization_b) {
        int qi;

        dType q, en1, en2, ion, aff;
        dType a1, a2, a3, a0, i0, i1, i2, i3, i4, i5, i6, i7;
        std::vector<dType> X(6), Y(6);

        cond.lusc_q = 1.0 / cond.dq;
        cond.lusz_q = (int)(11.0 * cond.lusc_q);

        std::map<std::string, DETparams>::const_iterator iter;

        for (iter = detpar.begin(); iter != detpar.end(); iter++) {
            name = iter->first;

            lup_dEq[name].aff.resize(cond.lusz_q);
            lup_dEq[name].ion.resize(cond.lusz_q);

            a0 = iter->second.Eaffin[0];
            a1 = iter->second.Eaffin[1];
            a2 = iter->second.Eaffin[2];
            a3 = iter->second.Eaffin[3];

            i0 = iter->second.Eioniz[0];
            i1 = iter->second.Eioniz[1];
            i2 = iter->second.Eioniz[2];
            i3 = iter->second.Eioniz[3];
            i4 = iter->second.Eioniz[4];
            i5 = iter->second.Eioniz[5];
            i6 = iter->second.Eioniz[6];
            i7 = iter->second.Eioniz[7];
            /*
            X[0] = -3.0;
            X[1] = -2.0;
            X[2] = -1.0;
            X[3] =  0.0;
            X[4] =  1.0;
            X[5] =  2.0;
            X[6] =  3.0;
            X[7] =  4.0;
            X[8] =  5.0;
            X[9] =  6.0;
            X[10] =  7.0;

            Y[0] = a3;
            Y[1] = a2;
            Y[2] = a1;
            Y[3] = a0;
            Y[4] = i1;
            Y[5] = i2;
            Y[6] = i3;
            Y[7] = i4;
            Y[8] = i5;
            Y[9] = i6;
            Y[10] = i7;
            spline.defineSpline(X, Y);
            */

            for (int i = 0; i != cond.lusz_q; i++) {
                q = - 3.0 + i / cond.lusc_q;

                if (q >= -3.0 && q < 0.0) {
                    if (range != 1) {
                        range = 1;
                        updatespline = true;
                    }
                    X[0] = -3.0;
                    X[1] = -2.0;
                    X[2] = -1.0;
                    X[3] =  0.0;
                    X[4] =  1.0;
                    X[5] =  2.0;
                    Y[0] = a3;
                    Y[1] = a2;
                    Y[2] = a1;
                    Y[3] = a0;
                    Y[4] = i1;
                    Y[5] = i2;
                } else if (q >= 0.0 && q < 1.0) {
                    if (range != 2) {
                        range = 2;
                        updatespline = true;
                    }
                    X[0] = -2.0;
                    X[1] = -1.0;
                    X[2] =  0.0;
                    X[3] =  1.0;
                    X[4] =  2.0;
                    X[5] =  3.0;
                    Y[0] = a2;
                    Y[1] = a1;
                    Y[2] = a0;
                    Y[3] = i1;
                    Y[4] = i2;
                    Y[5] = i3;
                } else if (q >= 1.0 && q < 2.0) {
                    if (range != 3) {
                        range = 3;
                        updatespline = true;
                    }
                    X[0] = -1.0;
                    X[1] =  0.0;
                    X[2] =  1.0;
                    X[3] =  2.0;
                    X[4] =  3.0;
                    X[5] =  4.0;
                    Y[0] = a1;
                    Y[1] = a0;
                    Y[2] = i1;
                    Y[3] = i2;
                    Y[4] = i3;
                    Y[5] = i4;
                } else if (q >= 2.0 && q < 3.0) {
                    if (range != 4) {
                        range = 4;
                        updatespline = true;
                    }
                    X[0] =  0.0;
                    X[1] =  1.0;
                    X[2] =  2.0;
                    X[3] =  3.0;
                    X[4] =  4.0;
                    X[5] =  5.0;
                    Y[0] = a0;
                    Y[1] = i1;
                    Y[2] = i2;
                    Y[3] = i3;
                    Y[4] = i4;
                    Y[5] = i5;
                } else if (q >= 3.0 && q < 4.0) {
                    if (range != 5) {
                        range = 5;
                        updatespline = true;
                    }
                    X[0] =  1.0;
                    X[1] =  2.0;
                    X[2] =  3.0;
                    X[3] =  4.0;
                    X[4] =  5.0;
                    X[5] =  6.0;
                    Y[0] = i1;
                    Y[1] = i2;
                    Y[2] = i3;
                    Y[3] = i4;
                    Y[4] = i5;
                    Y[5] = i6;
                } else if (q >= 4.0) {
                    if (range != 6) {
                        range = 6;
                        updatespline = true;
                    }
                    X[0] =  2.0;
                    X[1] =  3.0;
                    X[2] =  4.0;
                    X[3] =  5.0;
                    X[4] =  6.0;
                    X[5] =  7.0;
                    Y[0] = i2;
                    Y[1] = i3;
                    Y[2] = i4;
                    Y[3] = i5;
                    Y[4] = i6;
                    Y[5] = i7;
                }

                if (updatespline) {
                    spline.defineSpline(X, Y);
                    updatespline = false;
                }

                ion =  spline.getValue(q+cond.dq) - spline.getValue(q);
                aff  =  spline.getValue(q-cond.dq) - spline.getValue(q);
                qi = (int)((q+3.0)/cond.dq+0.5);
                lup_dEq[name].ion[qi] = ion;
                lup_dEq[name].aff[qi] = aff;
            }
        }

        inf.Wall.xmin = 0;
        inf.Wall.ymin = 0;
        inf.Wall.zmin = 0;

        inf.Wall.xmax =  inf.Cell.a;
        inf.Wall.ymax =  inf.Cell.b;
        inf.Wall.zmax =  inf.Cell.c;

        Pairs.resize(cond.ClTot);
        Cell.resize(cond.ClTot);

        for (int i = 0; i < cond.ClTot; i++) {
            Pairs[i].clear();
            Cell[i].Clear();

            At.pairs[i].clear();
        }

        for (int i=0; i< At.size; i++) {
            At.x0[i] = At.x[i];
            At.y0[i] = At.y[i];
            At.z0[i] = At.z[i];
        }
        inf.MassTot = 0.0;

        // Define the total mass of the whole System (for the density calculation)
        inf.MassTot = 0.0;
        for (int i = 0; i <  At.size; i++) {
        	inf.MassTot += At.mass[i] * 0.0096485256018;    //0.009771106 = 1./103.642778314
        }
        inf.MassTot *= 1660.538782;

        control.savetext_b = true;

        int SliceSize = (int)(At.size)/inf.dNP + 1;

        AtData.resize(SliceSize * 6, 0.0);

        BackUpState("default");

        Velocity_Vec.Clear();
        J_current_Vec.Clear();
        RJJ.Clear();
        control.initialization_b = true;
        if (inf.id == 0)
            std::cout << "Done Initialization .... \n";
    }
    if (control.inittemp_b) InitialTemperature();
}

void System::InitializeData() {
    //inf.Reset();
	inf.Time = 0.0;
    SetInitPositions();
    Velocity_Vec.Clear();
    J_current_Vec.Clear();
    RJJ.Clear();
    Rvv.Clear();
}


void System::InitialTemperature() {
    unsigned in, i;
    double Vel, dx, dy, dz, len;

    //for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
    //i = Domain[inf.dID].atomI[in];
    #pragma omp parallel for private(i, Vel, dx, dy, dz, len)
    for (i = 0; i < At.size; i++) {
        Vel = std::sqrt(3.0 * cond.Tinitial * kb / At.mass[i]);

        dx = 2.0 * ( (double)rand() / (double)RAND_MAX ) - 1.0;
        dy = 2.0 * ( (double)rand() / (double)RAND_MAX ) - 1.0;
        dz = 2.0 * ( (double)rand() / (double)RAND_MAX ) - 1.0;
        len = std::sqrt(dx * dx + dy * dy + dz * dz);

        dx /= len;
        dy /= len;
        dz /= len;

        At.vx[i] = Vel * dx;
        At.vy[i] = Vel * dy;
        At.vz[i] = Vel * dz;
    }
}

void System::NewPositions(std::string type = "n") {
    int i, in, size, icl, cl;
    dType c1, c2, dt;

    if (type == "n") {
        dt = cond.d_tau;
    } else if (type == "s") {
        dt = cond.d_taus;
    } else {
        dt = cond.d_tau;
    }

    c1 =  2.0/3.0 * dt * dt;
    c2 = -1.0/6.0 * dt * dt;

    At.x = At.x + At.vx * dt + c1 * At.ax + c2 * At.axo;
    At.y = At.y + At.vy * dt + c1 * At.ay + c2 * At.ayo;
    At.z = At.z + At.vz * dt + c1 * At.az + c2 * At.azo;

    /*
    At.vx_mid = At.vx + 0.5 * At.ax * dt;
    At.vy_mid = At.vy + 0.5 * At.ay * dt;
    At.vz_mid = At.vz + 0.5 * At.az * dt;

    At.x = At.x + At.vx_mid * dt;
    At.y = At.y + At.vy_mid * dt;
    At.z = At.z + At.vz_mid * dt;
    */
}

void System::UpdateVelocity(std::string type = "n") {
    dType dt, c1, c2, c3;

    if (type == "n") {
        dt = cond.d_tau;
    } else if (type == "s") {
        dt = cond.d_taus;
    } else {
        dt = cond.d_tau;
    }

    c1 =  1.0 / 3.0 * dt;
    c2 =  5.0 / 6.0 * dt;
    c3 = -1.0 / 6.0 * dt;

    At.axn = At.Fx * At.rmass;
    At.ayn = At.Fy * At.rmass;
    At.azn = At.Fz * At.rmass;

    At.vx = At.vx + c1 * At.axn + c2 * At.ax + c3 * At.axo;
    At.vy = At.vy + c1 * At.ayn + c2 * At.ay + c3 * At.ayo;
    At.vz = At.vz + c1 * At.azn + c2 * At.az + c3 * At.azo;

    At.axo = At.ax;
    At.ayo = At.ay;
    At.azo = At.az;

    At.ax = At.axn;
    At.ay = At.ayn;
    At.az = At.azn;

    if (control.gravity_b) At.az = At.az - aGrav; //
    props.Summ_of_PotEnergy = sum(At.FscRsc);
}

void System::adjustTimeStep() {
    double Rmin;
    double Vmax;
    //std::vector<Atom>::iterator Viter = std::max_element(At.begin(), At.end(), maxVcompare());
    //std::vector<Atom>::iterator Riter = std::min_element(At.begin(), At.end(), minRcompare());

    //Vmax = (*Viter).Vel;
    //Rmin = (*Riter).rmin;

    //cond.d_tau = d_pos / Vmax;

    //if (Rmin < 1.0) {
    //    cond.d_tau *= Rmin;
    //}
}

void System::BoxConditions() {
    double Dx, Dy, Dz;
    int i, in, size, icl, cl;
    bool condxp, condxm, condyp, condym, condzp, condzm;

    condxm = (std::find(WallFor.begin(), WallFor.end(), "-x") != WallFor.end());
    condxp = (std::find(WallFor.begin(), WallFor.end(), "+x") != WallFor.end());

    condym = (std::find(WallFor.begin(), WallFor.end(), "-y") != WallFor.end());
    condyp = (std::find(WallFor.begin(), WallFor.end(), "+y") != WallFor.end());

    condzm = (std::find(WallFor.begin(), WallFor.end(), "-z") != WallFor.end());
    condzp = (std::find(WallFor.begin(), WallFor.end(), "+z") != WallFor.end());

    #pragma omp parallel for private(i, in, Dx, Dy, Dz, icl, cl)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        Dx = At.x[i] -  inf.Wall.xmin;
        if (Dx < inf.WallX && condxm){
            At.Fx[i] += 0.0434 * (std::exp((2.5 - Dx) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }

        Dx =  inf.Wall.xmax - At.x[i];
        if (Dx < inf.WallX && condxp){
            At.Fx[i] -= 0.0434 * (std::exp((2.5 - Dx) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }

        Dy = At.y[i] -  inf.Wall.ymin;
        if (Dy < inf.WallX && condym) {
            At.Fy[i] += 0.0434 * (std::exp((2.5 - Dy) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }

        Dy =  inf.Wall.ymax - At.y[i];
        if (Dy < inf.WallX && condyp){
            At.Fy[i] -= 0.0434 * (std::exp((2.5 - Dy) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }


        Dz = At.z[i] -  inf.Wall.zmin;
        if (Dz < inf.WallX && condzm){
            At.Fz[i] += 0.0434 * (std::exp((2.5 - Dz) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }

        Dz =  inf.Wall.zmax - At.z[i];
        if (Dz < inf.WallX && condzp){
            At.Fz[i] -= 0.0434 * (std::exp((2.5 - Dz) * 8.0) - std::exp((2.5 - inf.WallX) * 8.0));
        }
    }
}

void System::PeriodicConditions() {
    int i, in, icl, cl;
    double Dx, Dy, Dz, trX, trY, trZ;;

    Dx =  inf.Wall.xmax - inf.Wall.xmin;
    Dy =  inf.Wall.ymax - inf.Wall.ymin;
    Dz =  inf.Wall.zmax - inf.Wall.zmin;

    #pragma omp parallel for private(i, in, icl, cl, trX, trY, trZ)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        assert(!IsNan(At.x[i]));
        assert(!IsNan(At.y[i]));
        assert(!IsNan(At.z[i]));
        assert(!IsInf(At.x[i]));
        assert(!IsInf(At.y[i]));
        assert(!IsInf(At.z[i]));

        trX = 0.0;
        trY = 0.0;
        trZ = 0.0;
        if (inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]) <  inf.Wall.xmin) {
	        trX  += inf.Cell.tr_x(Dx, 0.0, 0.0);
	        trY  += inf.Cell.tr_y(Dx, 0.0, 0.0);
	        trZ  += inf.Cell.tr_z(Dx, 0.0, 0.0);
        }

        if (inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]) >  inf.Wall.xmax) {
	        trX += inf.Cell.tr_x(-Dx, 0.0, 0.0);
	        trY += inf.Cell.tr_y(-Dx, 0.0, 0.0);
	        trZ += inf.Cell.tr_z(-Dx, 0.0, 0.0);
        }

        // Periodic conditions for Y direction

        if (inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]) <  inf.Wall.ymin) {
	        trX  += inf.Cell.tr_x(0.0, Dy, 0.0);
	        trY  += inf.Cell.tr_y(0.0, Dy, 0.0);
	        trZ  += inf.Cell.tr_z(0.0, Dy, 0.0);
        }

        if (inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]) >  inf.Wall.ymax) {
	        trX += inf.Cell.tr_x(0.0, -Dy, 0.0);
	        trY += inf.Cell.tr_y(0.0, -Dy, 0.0);
	        trZ += inf.Cell.tr_z(0.0, -Dy, 0.0);
        }

        // Periodic conditions for Z direction

        if (inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) <  inf.Wall.zmin) {
	        trX  += inf.Cell.tr_x(0.0, 0.0, Dz);
	        trY  += inf.Cell.tr_y(0.0, 0.0, Dz);
	        trZ  += inf.Cell.tr_z(0.0, 0.0, Dz);
        }

        if (inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) >  inf.Wall.zmax) {
	        trX += inf.Cell.tr_x(0.0, 0.0, -Dz);
	        trY += inf.Cell.tr_y(0.0, 0.0, -Dz);
	        trZ += inf.Cell.tr_z(0.0, 0.0, -Dz);
        }

        //if (trX != 0.0 || trY != 0.0 || trZ != 0.0) {
        //   std::cout << "trans " << i << "  " << trX << "  " << trY << "  " << trZ << std::endl; 
        //}

	    At.xav[i] += trX;
        At.xp[i]  += trX;
        At.xd[i]  += trX;
        At.x[i]   += trX;

        At.yav[i] += trY;
        At.yp[i]  += trY;
        At.yd[i]  += trY;
        At.y[i]   += trY;

        At.zav[i] += trZ;
        At.zp[i]  += trZ;
        At.zd[i]  += trZ;
        At.z[i]   += trZ;
    }
    //if (!CheckBoundary()) PeriodicConditions();
}


bool System::CheckBoundary() {
  int i, in, icl, cl;
  double Dx, Dy, Dz;

  Dx =  inf.Wall.xmax - inf.Wall.xmin;
  Dy =  inf.Wall.ymax - inf.Wall.ymin;
  Dz =  inf.Wall.zmax - inf.Wall.zmin;

  #pragma omp parallel for private(i, in, icl, cl)
  for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
      i = Domain[inf.dID].atomI[in];
      At.withinboundary[i] = 0;

      // Periodic conditions for Y direction
      if (At.x[i] <  inf.Wall.xmin)
        At.withinboundary[i] = 1;

      if (At.x[i] >  inf.Wall.xmax)
        At.withinboundary[i] = 1;

      // Periodic conditions for Y direction
      if (At.y[i] <  inf.Wall.ymin)
        At.withinboundary[i] = 1;

      if (At.y[i] >  inf.Wall.ymax)
        At.withinboundary[i] = 1;

      // Periodic conditions for Z direction
      if (At.z[i] <  inf.Wall.zmin)
        At.withinboundary[i] = 1;

      if (At.z[i] >  inf.Wall.zmax)
        At.withinboundary[i] = 1;
  }
  if (sum(At.withinboundary) > 0) {
      std::cout << inf.id << "  CheckBoundary " << std::endl;
      return false;
  }
  return true;
}

void System::ResetForces(std::string type = "n") {
    int i, in, icl, cl;

    std::fill(At.done.begin(), At.done.end(), false);

    std::fill(At.rmin.begin(), At.rmin.end(), 1.0e5);

    std::fill(At.Fx.begin(), At.Fx.end(), 0);
    std::fill(At.Fy.begin(), At.Fy.end(), 0);
    std::fill(At.Fz.begin(), At.Fz.end(), 0);

    if (type == "n") {
        std::fill(At.Fxp.begin(), At.Fxp.end(), 0);
        std::fill(At.Fyp.begin(), At.Fyp.end(), 0);
        std::fill(At.Fzp.begin(), At.Fzp.end(), 0);

        std::fill(At.Fxs.begin(), At.Fxs.end(), 0);
        std::fill(At.Fys.begin(), At.Fys.end(), 0);
        std::fill(At.Fzs.begin(), At.Fzs.end(), 0);

        std::fill(At.Ep.begin(), At.Ep.end(), 0);
        std::fill(At.Em.begin(), At.Em.end(), 0);
        std::fill(At.ElstEn_tau.begin(), At.ElstEn_tau.end(), 0);

        std::fill(At.Eqx.begin(), At.Eqx.end(), 0);
        std::fill(At.Eqy.begin(), At.Eqy.end(), 0);
        std::fill(At.Eqz.begin(), At.Eqz.end(), 0);

        std::fill(At.Epx.begin(), At.Epx.end(), 0);
        std::fill(At.Epy.begin(), At.Epy.end(), 0);
        std::fill(At.Epz.begin(), At.Epz.end(), 0);

        std::fill(At.FscRP.begin(), At.FscRP.end(), 0);
        std::fill(At.FscRS.begin(), At.FscRS.end(), 0);
        std::fill(At.EscP.begin(), At.EscP.end(), 0);
        std::fill(At.EscS.begin(), At.EscS.end(), 0);

        std::fill(At.rxfy.begin(), At.rxfy.end(), 0);
        std::fill(At.ryfz.begin(), At.ryfz.end(), 0);
        std::fill(At.rzfx.begin(), At.rzfx.end(), 0);

        control.calc_Fp = true;
        control.calc_Fs = true;

    } else if (type == "s") {
        std::fill(At.Fxs.begin(), At.Fxs.end(), 0);
        std::fill(At.Fys.begin(), At.Fys.end(), 0);
        std::fill(At.Fzs.begin(), At.Fzs.end(), 0);

        std::fill(At.FscRS.begin(), At.FscRS.end(), 0);
        std::fill(At.EscS.begin(), At.EscS.end(), 0);

        control.calc_Fs = true;

    } else if (type == "p") {
        std::fill(At.Fxp.begin(), At.Fxp.end(), 0);
        std::fill(At.Fyp.begin(), At.Fyp.end(), 0);
        std::fill(At.Fzp.begin(), At.Fzp.end(), 0);

        std::fill(At.Ep.begin(), At.Ep.end(), 0);
        std::fill(At.Em.begin(), At.Em.end(), 0);
        std::fill(At.ElstEn_tau.begin(), At.ElstEn_tau.end(), 0);

        std::fill(At.Eqx.begin(), At.Eqx.end(), 0);
        std::fill(At.Eqy.begin(), At.Eqy.end(), 0);
        std::fill(At.Eqz.begin(), At.Eqz.end(), 0);

        std::fill(At.Epx.begin(), At.Epx.end(), 0);
        std::fill(At.Epy.begin(), At.Epy.end(), 0);
        std::fill(At.Epz.begin(), At.Epz.end(), 0);

        std::fill(At.FscRP.begin(), At.FscRP.end(), 0);
        std::fill(At.EscP.begin(), At.EscP.end(), 0);
        std::fill(At.rxfy.begin(), At.rxfy.end(), 0);
        std::fill(At.ryfz.begin(), At.ryfz.end(), 0);
        std::fill(At.rzfx.begin(), At.rzfx.end(), 0);

        control.calc_Fp = true;

    } else {
        std::fill(At.Fxp.begin(), At.Fxp.end(), 0);
        std::fill(At.Fyp.begin(), At.Fyp.end(), 0);
        std::fill(At.Fzp.begin(), At.Fzp.end(), 0);

        std::fill(At.Fxs.begin(), At.Fxs.end(), 0);
        std::fill(At.Fys.begin(), At.Fys.end(), 0);
        std::fill(At.Fzs.begin(), At.Fzs.end(), 0);

        std::fill(At.Ep.begin(), At.Ep.end(), 0);
        std::fill(At.Em.begin(), At.Em.end(), 0);
        std::fill(At.ElstEn_tau.begin(), At.ElstEn_tau.end(), 0);

        std::fill(At.Eqx.begin(), At.Eqx.end(), 0);
        std::fill(At.Eqy.begin(), At.Eqy.end(), 0);
        std::fill(At.Eqz.begin(), At.Eqz.end(), 0);

        std::fill(At.Epx.begin(), At.Epx.end(), 0);
        std::fill(At.Epy.begin(), At.Epy.end(), 0);
        std::fill(At.Epz.begin(), At.Epz.end(), 0);

        std::fill(At.FscRP.begin(), At.FscRP.end(), 0);
        std::fill(At.FscRS.begin(), At.FscRS.end(), 0);
        std::fill(At.EscP.begin(), At.EscP.end(), 0);
        std::fill(At.EscS.begin(), At.EscS.end(), 0);
        std::fill(At.rxfy.begin(), At.rxfy.end(), 0);
        std::fill(At.ryfz.begin(), At.ryfz.end(), 0);
        std::fill(At.rzfx.begin(), At.rzfx.end(), 0);

        control.calc_Fp = true;
        control.calc_Fs = true;
    }

    std::fill(At.PotEnergy_tau.begin(), At.PotEnergy_tau.end(), 0);

    std::fill(At.FscRsc.begin(), At.FscRsc.end(), 0);

    std::fill(At.condx.begin(), At.condx.end(), 0);
    std::fill(At.condy.begin(), At.condy.end(), 0);
    std::fill(At.condz.begin(), At.condz.end(), 0);

    At.muxpr = At.mux;
    At.muypr = At.muy;
    At.muzpr = At.muz;
}

void System::FindPairs() {
    double Dx, Dy, Dz, rDx, rDy, rDz, DR, R_sc, R_sc2, Rcut, Rcut2, Rx, Ry, Rz;
    double X, Y, Z, Xi, Yi, Zi, X2, Y2, Z2, Xi2, Yi2, Zi2, Xc, Yc, Zc;
    int ind, cl, NCl2, n, NofMovAt, c, l, i, j, im, Im, iXc, iYc, iZc, cl2, clin, Xfrom, Xto, Yfrom, Yto, Zfrom, Zto;
    double CellX, CellY, CellZ, DCelX, DCelY, DCelZ;
    // Displacement
    Dx = (inf.Wall.xmax - inf.Wall.xmax0) * (inf.Wall.xmax - inf.Wall.xmax0) + (inf.Wall.xmin - inf.Wall.xmin0) * (inf.Wall.xmin - inf.Wall.xmin0);
    Dy = (inf.Wall.ymax - inf.Wall.ymax0) * (inf.Wall.ymax - inf.Wall.ymax0) + (inf.Wall.ymin - inf.Wall.ymin0) * (inf.Wall.ymin - inf.Wall.ymin0);
    Dz = (inf.Wall.zmax - inf.Wall.zmax0) * (inf.Wall.zmax - inf.Wall.zmax0) + (inf.Wall.zmin - inf.Wall.zmin0) * (inf.Wall.zmin - inf.Wall.zmin0);

    R_sc = std::sqrt(Dx + Dy + Dz) / ((inf.Wall.xmax - inf.Wall.xmin) + (inf.Wall.ymax - inf.Wall.ymin) + (inf.Wall.zmax - inf.Wall.zmin)) ;

    if (R_sc > 0.05) {
        control.findpairs_b = true;

        inf.Wall.xmax0 = inf.Wall.xmax;
        inf.Wall.ymax0 = inf.Wall.ymax;
        inf.Wall.zmax0 = inf.Wall.zmax;

        inf.Wall.xmin0 = inf.Wall.xmin;
        inf.Wall.ymin0 = inf.Wall.ymin;
        inf.Wall.zmin0 = inf.Wall.zmin;
    }

    if (control.findpairs_b || inf.Time == 0.0) {

        std::vector<int> MovAt;
        std::stringstream stream;
        pair p;

        // Cell size
        Dx = (inf.Wall.xmax - inf.Wall.xmin) * cond.rCl;
        Dy = (inf.Wall.ymax - inf.Wall.ymin) * cond.rCl;
        Dz = (inf.Wall.zmax - inf.Wall.zmin) * cond.rCl;

        //Reverse cell size
        rDx = 1.0 / Dx;
        rDy = 1.0 / Dy;
        rDz = 1.0 / Dz;

        assert( cond.ClTot == Pairs.size());
        assert( cond.ClTot == Cell.size());
        #pragma omp parallel for private(c)
        for (c = 0; c < cond.ClTot; c++) {
            Pairs[c].clear();
            Cell[c].Clear();
            Cell[c].dx = Dx;
            Cell[c].dy = Dy;
            Cell[c].dz = Dz;

            // Cell coordinates
            Zi = (int)(c * cond.rCl * cond.rCl);
            Yi = (int)((c - Zi * cond.Cl * cond.Cl) * cond.rCl);
            Xi = (int)(c - Zi * cond.Cl * cond.Cl - Yi * cond.Cl);
            Cell[c].x = inf.Cell.tr_x(Xi * Dx + 0.5 * Dx, Yi * Dy + 0.5 * Dy, Zi * Dz + 0.5 * Dz);
            Cell[c].y = inf.Cell.tr_y(Xi * Dx + 0.5 * Dx, Yi * Dy + 0.5 * Dy, Zi * Dz + 0.5 * Dz);
            Cell[c].z = inf.Cell.tr_z(Xi * Dx + 0.5 * Dx, Yi * Dy + 0.5 * Dy, Zi * Dz + 0.5 * Dz);
            //Cell[c].x = Xi * Dx + 0.5 * Dx;
            //Cell[c].y = Yi * Dy + 0.5 * Dy;
            //Cell[c].z = Zi * Dz + 0.5 * Dz;
        }

        //Number of cells in each direction
        DCelX = (int) ( cond.Rc_long * rDx + 1);
        DCelY = (int) ( cond.Rc_long * rDy + 1);
        DCelZ = (int) ( cond.Rc_long * rDz + 1);

        DR = 0.75 * std::sqrt(Dx * Dx + Dy * Dy + Dz * Dz);
        Rcut  = ( cond.Rc_long + DR);
        Rcut2 = Rcut * Rcut;

        CellX = DCelX * 2 + 1;
        CellY = DCelY * 2 + 1;
        CellZ = DCelZ * 2 + 1;

        NCl2  = CellX * CellY * CellZ;

        std::vector<pair> pp;

        #pragma omp parallel for private(im, Im, cl, Xi, Yi, Zi, X, Y, Z, cl2, Xi2, Yi2, Zi2, X2, Y2, Z2, R_sc, p, pp) \
                                 private(Xfrom, Xto, Yfrom, Yto, Zfrom, Zto, Xc, Yc, Zc, iXc, iYc, iZc, clin)
        for (cl = 0; cl < cond.ClTot; cl++) {
            pp.clear();

            X = Cell[cl].x;
            Y = Cell[cl].y;
            Z = Cell[cl].z;
            for (cl2 = 0; cl2 < cond.ClTot; cl2++)  {
                for (im = 0; im < inf.ImPos.size(); im ++)  {
                    X2 = Cell[cl2].x + inf.ImPos[im].x;
                    Y2 = Cell[cl2].y + inf.ImPos[im].y;
                    Z2 = Cell[cl2].z + inf.ImPos[im].z;

                    R_sc = ((X - X2) * (X - X2) +
                            (Y - Y2) * (Y - Y2) +
                            (Z - Z2) * (Z - Z2));

                    if (R_sc <= Rcut2) {
                        p.im = im;
                        p.cl = cl2;
                        p.r = std::sqrt(R_sc);
                        pp.push_back(p);
                    }
                }
            }
            assert( cl < Pairs.size() );
            Pairs[cl] = pp;
        }

        //#pragma omp parallel for private(i, X, Y, Z, cl)
        for (i = 0; i <  At.size; ++i) {
            X = (int)((inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]) - inf.Wall.xmin) * rDx);                //int((Pos[i*3+0] - xmin)/Dx);
            Y = (int)((inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]) - inf.Wall.ymin) * rDy);                //int((Pos[i*3+1] - ymin)/Dy);
            Z = (int)((inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) - inf.Wall.zmin) * rDz);                //int((Pos[i*3+2] - zmin)/Dz);
            cl = (int)(Z * cond.Cl * cond.Cl + Y * cond.Cl + X);

			assert( cl < Cell.size() );
			Cell[cl].atomI.push_back(i);
			Cell[cl].rx.push_back(At.x[i]);
			Cell[cl].ry.push_back(At.y[i]);
			Cell[cl].rz.push_back(At.z[i]);

			assert( i < At.Cell.size() );
            At.Cell[i] = cl;
            At.pairs[i] = Pairs[cl];
        }

        control.findpairs_b = false;
        DefineDomains();
    }
}

void System::DefineDomains() {
    const int root=0;

    int cl, np, dm, dmi, dmj, in, icl, k, i, cl2, Ncl, Ncl2, cli;
    double NX, NY, NZ, NXi, NYi, NZi, NI, rNX, rNY, rNZ, X, Y, Z, Xi, Yi, Zi, Dx, Dy, Dz, Rcutmin, Rcutmax;
    double Xmin, Xmax, Ymin, Ymax, Zmin, Zmax, Rx, Ry, Rz;

    Domain.resize(inf.dNP);

    //#pragma omp parallel for private(np)
    for (np = 0; np < inf.dNP; np++) {
        Domain[np].cellI.clear();
        Domain[np].atomI.clear();
        Domain[np].intcellI.clear();
    }

    NZ = int(std::pow(inf.dNP, 1.0/3.0));
    NY = int(std::sqrt(inf.dNP / NZ));
    NX = int(inf.dNP / (NZ * NY));

    rNX = 1.0 / NX;
    rNY = 1.0 / NY;
    rNZ = 1.0 / NZ;

    Dx = (inf.Wall.xmax - inf.Wall.xmin) * cond.rCl;
    Dy = (inf.Wall.ymax - inf.Wall.ymin) * cond.rCl;
    Dz = (inf.Wall.zmax - inf.Wall.zmin) * cond.rCl;

    // Define which cells belong to which domain
    #pragma omp parallel for private(cl, Xi, Yi, Zi, X, Y, Z, np, Xmin, Xmax, Ymin, Ymax, Zmin, Zmax)
    for (cl = 0; cl < cond.ClTot; cl++) {

        Zi = (int)(cl * cond.rCl * cond.rCl);
        Yi = (int)((cl - Zi * cond.Cl * cond.Cl) * cond.rCl);
        Xi = (int)(cl - Zi * cond.Cl * cond.Cl - Yi * cond.Cl);

        X = (int)((Xi) * cond.rCl * NX);
        Y = (int)((Yi) * cond.rCl * NY);
        Z = (int)((Zi) * cond.rCl * NZ);
        np = (int)(Z * NZ * NZ + Y * NY + X);

        #pragma omp critical
        {
            assert( np < Domain.size() );
            Domain[np].cellI.push_back(cl);
            Cell[cl].domain = np;
            //Domain[np].atomI.push_back(Cell[cl].atomI);
            Domain[np].atomI.insert(Domain[np].atomI.begin(), Cell[cl].atomI.begin(), Cell[cl].atomI.end());
            Domain[np].rx.insert(Domain[np].rx.begin(), Cell[cl].rx.begin(), Cell[cl].rx.end());
            Domain[np].ry.insert(Domain[np].ry.begin(), Cell[cl].ry.begin(), Cell[cl].ry.end());
            Domain[np].rz.insert(Domain[np].rz.begin(), Cell[cl].rz.begin(), Cell[cl].rz.end());
        }
    }


    //Loop over each cell in the system
    //#pragma omp parallel for private(in,  X, Y, Z, cl, icl, np, dm, Ncl2)
    for (cl = 0; cl < cond.ClTot; cl++) {

        assert( cl < Pairs.size() );
        assert( cl < Cell.size() );
        dmi = Cell[cl].domain;
        Ncl = Pairs[cl].size();
        // Loop over all the pair cells that are at the interactiton distance from the cl
        for (k = 0; k < Ncl; k++) {

            cl2     = Pairs[cl][k].cl;
            dmj     = Cell[cl2].domain;

            // If the cells are not within the same domain, means that the cell cl has an interaction with a cell from another domain
            if (dmi != dmj) {
                assert( dmi < Domain.size() );
                assert( dmj < Domain.size() );

                /// Define the overlap cells which belong to the current domain but share information with adjoint domains
                if (std::find(Domain[dmi].intcellI.begin(), Domain[dmi].intcellI.end(), cl) == Domain[dmi].intcellI.end()) {
                    Domain[dmi].intcellI.push_back(cl);
                }

                /// Define the adjoint cells to current domain
                if (std::find(Domain[dmi].extcellI.begin(), Domain[dmi].extcellI.end(), cl2) == Domain[dmi].extcellI.end()) {
                    Domain[dmi].extcellI.push_back(cl2);
                }
            }
        }
    }
}

void System::UpdateCells() {
    double xmax, ymax, zmax, xmin, ymin, zmin;
    double Dx, Dy, Dz, rDx, rDy, rDz, DR, R_sc, Rcut2;
    double X, Y, Z, X2, Y2, Z2, Xc, Yc, Zc, FPRlim2, Xi, Yi, Zi;
    int ind, cl, n, np, ncl, ndm, dm, c, l, i, j, Im, iXc, iYc, iZc, cl2, Xfrom, Xto, Yfrom, Yto, Zfrom, Zto;
    int cli, clj, dmi, dmj, icl, in;
    std::stringstream stream;
    pair p;

    Dx = (inf.Wall.xmax - inf.Wall.xmin) * cond.rCl;
    Dy = (inf.Wall.ymax - inf.Wall.ymin) * cond.rCl;
    Dz = (inf.Wall.zmax - inf.Wall.zmin) * cond.rCl;
    rDx = 1.0 / Dx;
    rDy = 1.0 / Dy;
    rDz = 1.0 / Dz;


    //#pragma omp parallel for private(icl, cli, in, i, X, Y, Z, clj, n, np, dm, dmj)
    for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
        cli = Domain[inf.dID].cellI[icl];
        for (in = 0; in < Cell[cli].atomI.size();) {
            i = Cell[cli].atomI[in];

            assert( i < At.x.size() );
            assert( i < At.y.size() );
            assert( i < At.z.size() );

            X   = (int)((inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]) - inf.Wall.xmin) * rDx);
            Y   = (int)((inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]) - inf.Wall.ymin) * rDy);
            Z   = (int)((inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) - inf.Wall.zmin) * rDz);
            clj = (int)(Z * cond.Cl * cond.Cl + Y * cond.Cl + X);

            assert( cli < Cell.size() );
            //assert( clj < Cell.size() );
	    if (clj > Cell.size() || clj < 0) {
	    	std::cout << "Original  : x " << At.x[i] << "  y " << At.y[i] << "  z " << At.z[i] << std::endl;
	    	std::cout << "Translate : ";
                std::cout << "x " << inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]);
                std::cout << "y " << inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]);
                std::cout << "z " << inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) << std::endl;
		std::cout << "X : " << X << "  Y : " << Y << "  Z : " << Z << std::endl;
		std::cout << "Cl : " << Cell.size() << "  rCl : " << cond.rCl << "  rDx : " << rDx << "  rDy : " << rDy << "  rDz : " << rDz << std::endl;
		std::cout << "cli : " << cli << "   clj : " << clj << std::endl;
		std::cout << std::endl;
		assert( clj < Cell.size() );
	    }
            assert( i < At.Cell.size() );

            if (cli != clj) {
                //#pragma omp critical
                {
                    //Cell[cli].atomI.erase(std::remove(Cell[cli].atomI.begin(), Cell[cli].atomI.end(), i), Cell[cli].atomI.end());
                    //Cell[clj].atomI.push_back(i);
                    Cell[cli].Erase(i);
                    Cell[clj].Push(i, At.x[i], At.y[i], At.z[i]);
                    At.Cell[i] = clj;
                    At.pairs[i] = Pairs[clj];

                    dmj = Cell[clj].domain;
                    assert( dmj < Domain.size() );
                    assert( inf.dID < Domain.size() );
                    if (dmj != inf.dID) {
                        //Domain[inf.dID].atomI.erase(std::remove(Domain[inf.dID].atomI.begin(), Domain[inf.dID].atomI.end(), i), Domain[inf.dID].atomI.end());
                        //Domain[dmj].atomI.push_back(i);
                    	Domain[inf.dID].Erase(i);
                    	Domain[dmj].Push(i, At.x[i], At.y[i], At.z[i]);
                    }
                }
            } else {
                ++in;
            }
        }
    }


    //#pragma omp parallel for private(icl, cli, in, i, X, Y, Z, clj, n, np, dm, dmj)
    for (icl = 0; icl < Domain[inf.dID].extcellI.size(); icl++) {
        cli = Domain[inf.dID].extcellI[icl];
        for (in = 0; in < Cell[cli].atomI.size();) {
            i = Cell[cli].atomI[in];

            X   = (int)((inf.Cell.itr_x(At.x[i], At.y[i], At.z[i]) - inf.Wall.xmin) * rDx);
            Y   = (int)((inf.Cell.itr_y(At.x[i], At.y[i], At.z[i]) - inf.Wall.ymin) * rDy);
            Z   = (int)((inf.Cell.itr_z(At.x[i], At.y[i], At.z[i]) - inf.Wall.zmin) * rDz);
            clj = (int)(Z * cond.Cl * cond.Cl + Y * cond.Cl + X);

            assert( cli < Cell.size() );
            assert( clj < Cell.size() );

            if (cli != clj) {
                //#pragma omp critical
                {
                    //Cell[cli].atomI.erase(std::remove(Cell[cli].atomI.begin(), Cell[cli].atomI.end(), i), Cell[cli].atomI.end());
                    //Cell[clj].atomI.push_back(i);
                    Cell[cli].Erase(i);
                    Cell[clj].Push(i, At.x[i], At.y[i], At.z[i]);

                    At.Cell[i] = clj;

                    dmj = Cell[clj].domain;
                    assert( dmj < Domain.size() );
                    assert( inf.dID < Domain.size() );
                    if (dmj != inf.dID) {
                        //Domain[inf.dID].atomI.erase(std::remove(Domain[inf.dID].atomI.begin(), Domain[inf.dID].atomI.end(), i), Domain[inf.dID].atomI.end());
                        //Domain[dmj].atomI.push_back(i);
                    	Domain[inf.dID].Erase(i);
                    	Domain[dmj].Push(i, At.x[i], At.y[i], At.z[i]);
                    }
                }
            } else {
                ++in;
            }
        }
    }

    ExchangeCellsData();

    //#pragma omp parallel for private(np)
    for (np = 0; np < inf.dNP; np++) {
        //Domain[np].atomI.clear();
        Domain[np].atomClear();
    }

    //#pragma omp parallel for private(cl, Xi, Yi, Zi, X, Y, Z, np, Xmin, Xmax, Ymin, Ymax, Zmin, Zmax)
    for (cl = 0; cl < cond.ClTot; cl++) {
        dm = Cell[cl].domain;
        //Domain[dm].atomI.insert(Domain[dm].atomI.begin(), Cell[cl].atomI.begin(), Cell[cl].atomI.end());
        Domain[dm].Insert(Cell[cl].atomI, Cell[cl].rx, Cell[cl].ry, Cell[cl].rz);
    }
}

void System::ExchangeCellsData() {
    const int root=0;
    int from, size, to, j, j8, i, in, d, cl, icl, js, dm;

    if (inf.num_procs > 1 & control.mpi_b) {

        for (cl = 0; cl < cond.ClTot; cl++) {
            dm = Cell[cl].domain;

            if (dm == inf.id) {
                size = Cell[cl].atomI.size();
            }

            MPI_Bcast(&size, 1, MPI_INT, dm, MPI_COMM_WORLD);

            if (dm != inf.id) {
                Cell[cl].Resize(size);
            }
        }

        for (cl = 0; cl < cond.ClTot; cl++) {
            dm   = Cell[cl].domain;
            size = Cell[cl].atomI.size();
            AtData.resize(size*4, 0.0);

            if (dm == inf.id) {
                #pragma omp parallel for private(in, i)
                for (in = 0; in < size; in++) {
                    AtData[in*4 + 0] = Cell[cl].atomI[in];
                    AtData[in*4 + 1] = Cell[cl].rx[in];
                    AtData[in*4 + 2] = Cell[cl].ry[in];
                    AtData[in*4 + 3] = Cell[cl].rz[in];
                }
            }

            MPI_Bcast(&AtData.front(), (size*4), mpiType, dm, MPI_COMM_WORLD);

            if (dm != inf.id) {
                #pragma omp parallel for private(in, i)
                for (in = 0; in < size; in++) {
                    Cell[cl].atomI[in] = AtData[in*4 + 0];
                    Cell[cl].rx[in] = AtData[in*4 + 1];
                    Cell[cl].ry[in] = AtData[in*4 + 2];
                    Cell[cl].rz[in] = AtData[in*4 + 3];
                }
            }
        }





        for (dm = 0; dm < inf.dNP; dm++) {
            if (dm == inf.id) {
                size = Domain[dm].atomI.size();
            }

            MPI_Bcast(&size, 1, MPI_INT, dm, MPI_COMM_WORLD);

            if (dm != inf.id) {
                Domain[dm].atomI.resize(size);
            }
        }

    }
}

void System::ExchangeData() {
    const int root=0;
    int from, size, to, j, j8, i, in, d, cl, icl, js, dm;

    //GetPosData(Sys);
    //BCastPosData(Sys);


    if (inf.num_procs > 1 & control.mpi_b) {

        for (cl = 0; cl < cond.ClTot; cl++) {
            dm = Cell[cl].domain;
            assert( dm < Domain.size() );

            if (std::find(Domain[dm].intcellI.begin(), Domain[dm].intcellI.end(), cl) != Domain[dm].intcellI.end()) {

                size = Cell[cl].atomI.size() * 9;
                AtData.resize(size, 0.0);

                if (dm == inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {
                        i = Cell[cl].atomI[in];

                        assert( i < At.x.size() );
                        assert( in*9 < AtData.size() );

                        AtData[in*9 + 0]	    = i;

                        AtData[in*9 + 1]	    = At.x[i];
                        AtData[in*9 + 2]	    = At.y[i];
                        AtData[in*9 + 3]	    = At.z[i];

                        AtData[in*9 + 4]	    = At.vx_mid[i];
                        AtData[in*9 + 5]	    = At.vy_mid[i];
                        AtData[in*9 + 6]	    = At.vz_mid[i];

                        AtData[in*9 + 7]	    = At.FscRsc[i];
                        AtData[in*9 + 8]	    = At.PotEnergy_tau[i];
                    }
                }

                MPI_Bcast(&AtData.front(), size, mpiType, dm, MPI_COMM_WORLD);

                if (dm != inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {
                        i   = AtData[in * 9 + 0];
                        Cell[cl].atomI[in]  = i;
                        Cell[cl].rx[in]      = At.x[i];
                        Cell[cl].ry[in]      = At.y[i];
                        Cell[cl].rz[in]      = At.z[i];
                        assert( i < At.x.size() );
                        assert( in*9 < AtData.size() );

                        At.x[i]		        = AtData[in*9 + 1];
                        At.y[i]		        = AtData[in*9 + 2];
                        At.z[i]		        = AtData[in*9 + 3];

                        At.vx_mid[i]	    = AtData[in*9 + 4];
                        At.vy_mid[i]	    = AtData[in*9 + 5];
                        At.vz_mid[i]	    = AtData[in*9 + 6];

                        At.FscRsc[i]	    = AtData[in*9 + 7];
                        At.PotEnergy_tau[i]	= AtData[in*9 + 8];
                    }
                }
            }
        }
    }
}

void System::ExchangeThCondData() {
    const int root=0;
    int from, size, to, j, j8, i, in, d, cl, icl, js, dm;

    if (inf.num_procs > 1 & control.mpi_b) {

        for (cl = 0; cl < cond.ClTot; cl++) {
            dm = Cell[cl].domain;
            if (std::find(Domain[dm].intcellI.begin(), Domain[dm].intcellI.end(), cl) != Domain[dm].intcellI.end()) {

                size = Cell[cl].atomI.size() * 6;
                AtData.resize(size, 0.0);

                if (dm == inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {
                        i = Cell[cl].atomI[in];

                        AtData[in*6 + 0]	    = i;
                        AtData[in*6 + 1]	    = At.condx[i];
                        AtData[in*6 + 2]	    = At.condy[i];
                        AtData[in*6 + 3]	    = At.condz[i];

                        AtData[in*6 + 4]	    = At.KinEnergy_tau[i];
                        AtData[in*6 + 5]	    = At.PotEnergy_tau[i];
                    }
                }

                MPI_Bcast(&AtData.front(), size, mpiType, dm, MPI_COMM_WORLD);

                if (dm != inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {

                        i = AtData[in * 6 + 0];
                        Cell[cl].atomI[in]      = i;
                        Cell[cl].rx[in]      = At.x[i];
                        Cell[cl].ry[in]      = At.y[i];
                        Cell[cl].rz[in]      = At.z[i];
                        At.condx[i]		        = AtData[in*6 + 1];
                        At.condy[i]		        = AtData[in*6 + 2];
                        At.condz[i]		        = AtData[in*6 + 3];

                        At.KinEnergy_tau[i]     = AtData[in*6 + 4];
                        At.PotEnergy_tau[i]     = AtData[in*6 + 5];
                    }
                }
            }
        }
    }
}

void System::ExchangeVelocities() {
    const int root=0;
    int from, size, to, j, j8, i, in, d, cl, icl, js, dm;

    if (inf.num_procs > 1 & control.mpi_b) {

        for (cl = 0; cl < cond.ClTot; cl++) {
            dm = Cell[cl].domain;
            if (std::find(Domain[dm].intcellI.begin(), Domain[dm].intcellI.end(), cl) != Domain[dm].intcellI.end()) {

                size = Cell[cl].atomI.size() * 4;
                AtData.resize(size, 0.0);

                if (dm == inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {
                        i = Cell[cl].atomI[in];

                        AtData[in*4 + 0]	    = i;
                        AtData[in*4 + 1]	    = At.vx[i];
                        AtData[in*4 + 2]	    = At.vy[i];
                        AtData[in*4 + 3]	    = At.vz[i];
                    }
                }

                MPI_Bcast(&AtData.front(), size, mpiType, dm, MPI_COMM_WORLD);

                if (dm != inf.id) {
                    #pragma omp parallel for private(in, i)
                    for (in = 0; in < Cell[cl].atomI.size(); in++) {

                        i = AtData[in * 4 + 0];
                        Cell[cl].atomI[in]      = i;
                        Cell[cl].rx[in]         = At.x[i];
                        Cell[cl].ry[in]         = At.y[i];
                        Cell[cl].rz[in]         = At.z[i];
                        At.vx[i]		        = AtData[in*4 + 1];
                        At.vy[i]		        = AtData[in*4 + 2];
                        At.vz[i]		        = AtData[in*4 + 3];
                    }
                }
            }
        }
    }
}

void System::RegVelocity() {
    double TVelX, TVelY, TVelZ;
    double dt_t, sc;
    int i, in, icl, cl;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    TVelX = 0.0;
    TVelY = 0.0;
    TVelZ = 0.0;

    #pragma omp parallel for private(i, in) reduction(+: TVelX, TVelY, TVelZ)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        assert( i < At.vx.size());
        TVelX += At.vx[i];
        TVelY += At.vy[i];
        TVelZ += At.vz[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&TVelX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelX = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&TVelY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelY = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&TVelZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        TVelZ = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    sc = inf.Time < 0.25e3 ? 1.0 / (inf.Time) : 1.0 / (0.25 * 1000);
    sc = sc > 1.0 ? 0.1 : sc;

    inf.VelX += sc * (TVelX * At.rsize - inf.VelX);
    inf.VelY += sc * (TVelY * At.rsize - inf.VelY);
    inf.VelZ += sc * (TVelZ * At.rsize - inf.VelZ);

    dt_t =  inf.Time < 50.0e3 ? 50.0 / ( inf.Time) : 1.0 / (50.0 * 1000);
    dt_t = dt_t > 1.0 ? 0.1 : std::sqrt(dt_t);

    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        At.vx[i] -=  inf.VelX * dt_t;
        At.vy[i] -=  inf.VelY * dt_t;
        At.vz[i] -=  inf.VelZ * dt_t;
    }
}

void System::RegPosition() {
    double TotPosX, TotPosY, TotPosZ;
    double Dx, Dy, Dz, sc;
    int i, in, icl, cl;

    TotPosX = 0.0;
    TotPosY = 0.0;
    TotPosZ = 0.0;

    #pragma omp parallel for private(i, in) reduction(+: TotPosX, TotPosY, TotPosZ)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        TotPosX += At.x[i];
        TotPosY += At.y[i];
        TotPosZ += At.z[i];
    }

    TotPosX *= At.rsize;
    TotPosY *= At.rsize;
    TotPosZ *= At.rsize;

    sc = 0.002 * (0.25 + 0.75 * std::exp(- 1.0e-3 * inf.Time * 0.05));

    inf.PosX = 0.5 * ( inf.Wall.xmax -  inf.Wall.xmin);
    inf.PosY = 0.5 * ( inf.Wall.ymax -  inf.Wall.ymin);
    inf.PosZ = 0.5 * ( inf.Wall.zmax -  inf.Wall.zmin);

    Dx = (TotPosX -  inf.PosX) * sc;
    Dy = (TotPosY -  inf.PosY) * sc;
    Dz = (TotPosZ -  inf.PosZ) * sc;


    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        At.x[i] -= Dx;
        At.y[i] -= Dy;
        At.z[i] -= Dz;

        At.x0[i] -= Dx;
        At.y0[i] -= Dy;
        At.z0[i] -= Dz;

        At.xp[i] -= Dx;
        At.yp[i] -= Dy;
        At.zp[i] -= Dz;

        At.xd[i] -= Dx;
        At.yd[i] -= Dy;
        At.zd[i] -= Dz;

        At.xav[i] -= Dx;
        At.yav[i] -= Dy;
        At.zav[i] -= Dz;
    }
}

void System::RegTemperature() {
    double dt_tT, ScT, E_kin;
    std::vector<dType> vel;
    E_kin = 0;

    E_kin = 0;

    vel = At.vx * At.vx + At.vy * At.vy + At.vz * At.vz;
    E_kin = 0.5 * sum((At.mass * vel));
    props.Temperature_tau = 2.0 * E_kin / (kb * 3 *  At.size);

    // --------------------   Temperature regulation   --------------
    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Temperature / props.Temperature_tau - 1.0));

    At.vx = At.vx * ScT;
    At.vy = At.vy * ScT;
    At.vz = At.vz * ScT;
}

void System::RegPressure() {
    double dt_tP, ScP, Dfin;
    int im, i, in, icl, cl;

    // --------------------   props.Pressure regulation   --------------
    dt_tP = cond.P_rel + 1.0  / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.P_rel_fin - cond.P_rel) - (1.0 / (1.0 + 148.41316) * (cond.P_rel_fin - cond.P_rel));
    ScP = std::pow(1.0 - 1.0 / (dt_tP * 100000) * (cond.Preg - props.Pressure_tau), 1.0 / 3.0);

    if (IsNan(ScP)) ScP = 1.0;
    while (ScP > 1.05 || ScP < 0.95) {
        ScP = std::pow(ScP, 0.95);
    }
    assert( ScP < 1.05 && ScP > 0.95 );

    #pragma omp parallel for private(im)
    for (im = 0; im < inf.ImPos.size(); im++) {
        inf.ImPos[im].x *= ScP;
        inf.ImPos[im].y *= ScP;
        inf.ImPos[im].z *= ScP;
    }

     inf.Cell.a *= ScP;
     inf.Cell.b *= ScP;
     inf.Cell.c *= ScP;

     inf.Wall.xmin *= ScP;
     inf.Wall.ymin *= ScP;
     inf.Wall.zmin *= ScP;

     inf.Wall.xmax *= ScP;
     inf.Wall.ymax *= ScP;
     inf.Wall.zmax *= ScP;

     inf.PosX *= ScP;
     inf.PosY *= ScP;
     inf.PosZ *= ScP;

     At.xp = At.xp * ScP;
     At.yp = At.yp * ScP;
     At.zp = At.zp * ScP;

     At.xd = At.xd * ScP;
     At.yd = At.yd * ScP;
     At.zd = At.zd * ScP;

     At.xav = At.xav * ScP;
     At.yav = At.yav * ScP;
     At.zav = At.zav * ScP;

     At.x = At.x * ScP;
     At.y = At.y * ScP;
     At.z = At.z * ScP;

     At.x0 = At.x0 * ScP;
     At.y0 = At.y0 * ScP;
     At.z0 = At.z0 * ScP;
}

void System::PremeltR() {
    double dt_tT, ScT1, ScT2;
    double Rlim2, Rsc2, zmin, zmax;
    double Z, Y, X;
    int i, in;

    zmin = 0.0;
    zmax =  inf.Cell.c;
    X = 0.5 * ( inf.Wall.xmax +  inf.Wall.xmin);
    Y = 0.5 * ( inf.Wall.ymax +  inf.Wall.ymin);
    Z = 0.5 * ( inf.Wall.zmax +  inf.Wall.zmin);

    Rlim2  = cond.Rmelt * cond.Rmelt;

    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT1 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg1 / props.Temperature_tau - 1.0));
    ScT2 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg2 / props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i, in, Rsc2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        Rsc2 = (At.x[i] - X) * (At.x[i] - X) + (At.y[i] - Y) * (At.y[i] - Y) + (At.z[i] - Z) * (At.z[i] - Z);
        if (Rsc2 <= Rlim2) {
            At.vx[i] *= ScT2;
            At.vy[i] *= ScT2;
            At.vz[i] *= ScT2;
        } else {
            At.vx[i] *= ScT1;
            At.vy[i] *= ScT1;
            At.vz[i] *= ScT1;
        }
    }
}

void System::PremeltL() {
    double dt_tT, ScT1, ScT2;
    double Rlim2, Rsc2, zmin, zmax;
    double Z, Y, X;
    int i, in;

    zmin = 0.0;
    zmax =  inf.Cell.c;
    X = 0.5 * ( inf.Wall.xmax +  inf.Wall.xmin);
    Y = 0.5 * ( inf.Wall.ymax +  inf.Wall.ymin);
    Z = 0.5 * ( inf.Wall.zmax +  inf.Wall.zmin);

    Rlim2  = cond.Rmelt * cond.Rmelt;

    dt_tT = cond.T_rel + 1.0 / (1.0 + std::exp(-0.2e-3 *  inf.Time + 5.0)) * (cond.T_rel_fin - cond.T_rel) - (1.0 / (1.0 + 148.41316) * (cond.T_rel_fin - cond.T_rel));
    ScT1 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg1 / props.Temperature_tau - 1.0));
    ScT2 = std::sqrt(1.0 + 1.0 / dt_tT * (cond.Treg2 / props.Temperature_tau - 1.0));

    #pragma omp parallel for private(i, in, Rsc2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        if (At.x[i] < X) {
            At.vx[i] *= ScT2;
            At.vy[i] *= ScT2;
            At.vz[i] *= ScT2;
        } else {
            At.vx[i] *= ScT1;
            At.vy[i] *= ScT1;
            At.vz[i] *= ScT1;
        }
    }
}

void System::GetProperties() {
    double R, R_sc, R_sc2, masstot;
    double RminAv, val, sum, X, Y, Z;
    double xmax, ymax, zmax, xmin, ymin, zmin;
    double PTerm1, PTerm2, IntEn, E_pot;
    double E_kin, vel;
    double aver, b, sc;
    int i, j, in, icl, cl;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    double tv, tt, tp, ten, td, tsh;
    tv = 0.0;
    tt = 0.0;
    tp = 0.0;
    ten = 0.0;
    td = 0.0;
    tsh = 0.0;

    //start_t(tv1);
    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    // SIMPLE CUBIC VOLUME CALCULATION
    props.Volume_tau = ( inf.Wall.xmax -  inf.Wall.xmin) * ( inf.Wall.ymax -  inf.Wall.ymin) * ( inf.Wall.zmax -  inf.Wall.zmin) * inf.Cell.sin_b;


    //--------------------------   props.Density mesurement   ---------------------------
    props.Density_tau = inf.MassTot / props.Volume_tau;



    //-------------------------  Internal Kinetic temperature #1   --------------------
    E_kin = 0;

    #pragma omp parallel for private(i, in, vel) reduction(+:E_kin)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        vel = (At.vx[i] * At.vx[i] + At.vy[i] * At.vy[i] + At.vz[i] * At.vz[i]);
        At.KinEnergy_tau[i] = 0.5 * At.mass[i] * vel;
        E_kin += At.KinEnergy_tau[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&E_kin,    1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_kin = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.Temperature_tau = 2.0 * E_kin / (kb * 3 *  At.size);

    //------------------------   props.Pressure mesurement   -------------------------
    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&props.Summ_of_PotEnergy, 1, mpiType,
                                  &idData.front(),    1, mpiType, MPI_COMM_WORLD);
        props.Summ_of_PotEnergy = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    PTerm1 = kb * props.Temperature_tau *  At.size / props.Volume_tau;
    PTerm2 = 0.1666667 / props.Volume_tau * props.Summ_of_PotEnergy;
    props.Pressure_tau = (PTerm1 + PTerm2) * 160218.0; // MPa


    //----------------------   Internal energy mesurement   -----------------------
    IntEn = 0.0;
    E_pot = 0.0;
    E_kin = 0.0;

    #pragma omp parallel for private(i, in) reduction(+:E_pot, E_kin)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        E_kin = E_kin + At.KinEnergy_tau[i];
        E_pot = E_pot + At.PotEnergy_tau[i];
        At.Energy_tau[i] = At.KinEnergy_tau[i] + 0.5 * At.PotEnergy_tau[i];
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&E_kin,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_kin = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&E_pot,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        E_pot = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.KinEnergy_tau = E_kin;
    props.PotEnergy_tau = 0.5 * E_pot;

    props.Energy_tau = (props.KinEnergy_tau + props.PotEnergy_tau) *  At.rsize;

    props.IntEnergy_tau = 0.5 * E_pot;

    props.Enthalpy_tau = props.Energy_tau + cond.Preg / 160218.0 * props.Volume;//props.Pressure / 160218.0 * props.Volume;
    props.CohesiveEnergy_tau = (E_pot + E_kin) * At.rsize;

    // ---------------------------   DIFFUSION ----------------------------------------
    double R_sc2_sum;
    R_sc2       = 0;
    R_sc2_sum   = 0;
    //props.Diffusion_tau =  MSD_tau * 1.0/(2.0*3.0) * cond.rd_tau;

    #pragma omp parallel for private(i, in, R_sc2) reduction(+: R_sc2_sum)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        R_sc2 = ((At.xd[i] - At.x[i]) * (At.xd[i] - At.x[i]) +
                 (At.yd[i] - At.y[i]) * (At.yd[i] - At.y[i]) +
                 (At.zd[i] - At.z[i]) * (At.zd[i] - At.z[i]));
        R_sc2_sum += R_sc2;
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&R_sc2_sum,       1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        R_sc2_sum = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    props.MSD_tau = R_sc2_sum *  At.rsize;
    props.Diffusion_tau       = CalcDiffusion();
    props.ThermCond_tau  	  = CalcThermalConductivity();
    props.ICompress_tau    	  = CalcIsotermalCompressibility();
    props.SpecificHeat_tau 	  = CalcSpecificHeat();
    props.Viscosity_tau       = CalcViscosity();

}

void System::AverageProperties() {
    double sc, b;
    int i, in;

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;


    props.Temperature += sc * (props.Temperature_tau - props.Temperature);
    props.Pressure += sc * (props.Pressure_tau - props.Pressure);
    props.Density += sc * (props.Density_tau - props.Density);
    props.IntEnergy += sc * (props.IntEnergy_tau - props.IntEnergy);
    props.Energy += sc * (props.Energy_tau - props.Energy);
    props.Diffusion += sc * (props.Diffusion_tau - props.Diffusion);
    props.Volume += sc *  (props.Volume_tau - props.Volume);
    props.MSD    += sc * (props.MSD_tau - props.MSD);
    props.SpecificHeat += sc * (props.SpecificHeat_tau - props.SpecificHeat);
    props.PotEnergy += sc * (props.PotEnergy_tau - props.PotEnergy);
    props.KinEnergy += sc * (props.KinEnergy_tau - props.KinEnergy);

    props.Enthalpy += sc * (props.Enthalpy_tau - props.Enthalpy);
    props.GibbsEnergy += sc * (props.GibbsEnergy_tau - props.GibbsEnergy);
    props.ThermCond += sc * (props.ThermCond_tau - props.ThermCond);
    props.ICompress  += sc * (props.ICompress_tau - props.ICompress);
    props.Viscosity     += sc * (props.Viscosity_tau - props.Viscosity);
    props.CohesiveEnergy += sc * (props.CohesiveEnergy_tau - props.CohesiveEnergy);

    #pragma omp parallel for private(i)
    for (i=0; i< At.size; i++)
    {
        At.KinEnergy[i] += sc * (At.KinEnergy_tau[i] - At.KinEnergy[i]);
        At.PotEnergy[i] += sc * (At.PotEnergy_tau[i] - At.PotEnergy[i]);
        At.Energy[i]       += sc * (At.Energy_tau[i] - At.Energy[i]);
    }

    b = -std::log(1.0 / (cond.Pos_Av_Time))/(cond.Pos_Av_Time);
    sc =  inf.Time < cond.Pos_Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Pos_Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    #pragma omp parallel for private(i, in)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        At.xav[i]       += sc * (At.x[i] - At.xav[i]);
        At.yav[i]       += sc * (At.y[i] - At.yav[i]);
        At.zav[i]       += sc * (At.z[i] - At.zav[i]);
    }
}

void System::RadialDistFunc() {
    int nj, Ncl, j, cl, i, n, in, k, cl2, Ncl2, im, nint, atNum, pairNum;
    double DR = RDF.RDFRmax / RDF.r.size();
    double R_sc, R_sc2, R, Rmax2, Rx, Ry, Rz;
    Rmax2 = RDF.RDFRmax * RDF.RDFRmax;

    std::string namePair;
    std::vector<rdfPair>::const_iterator rdfp;
    std::vector<std::string> RDFpairs;
    std::vector<dType> RDFuni;

    RDFuni.resize(RDF.r.size());

    for (i = 0; i < RDF.r.size(); i++) {
        RDF.r[i] = i * DR + 0.5 * DR;
        for (j = 0; j < RDFfor.size(); j++) {
            RDF.n[j][i] = 0.0;
        }
    }

    pairNum = 0;


    RDFpairs.clear();
    for (rdfp = RDFfor.begin(); rdfp != RDFfor.end(); ++rdfp) {
        RDFpairs.push_back((*rdfp).At1 + (*rdfp).At2);
        RDFpairs.push_back((*rdfp).At2 + (*rdfp).At1);
    }


    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        cl = At.Cell[i];
        Ncl = Pairs[cl].size();
        for (k = 0; k < Ncl; k++)   {

            im      = Pairs[cl][k].im;
            cl2     = Pairs[cl][k].cl;
            Ncl2    = Cell[cl2].atomI.size();

            for (n = 0; n < Ncl2; n++) {

                j = Cell[cl2].atomI[n];
                if (j == i) continue;

                namePair = At.name[i] + At.name[j];
                // Check if the name of atom exists in the DRFfor vector
                if (std::find(RDFpairs.begin(), RDFpairs.end(), namePair) == RDFpairs.end()) continue;
                pairNum = int((find(RDFpairs.begin(), RDFpairs.end(), namePair) - RDFpairs.begin()) / 2.0);

                Rx = (At.x[i] - (At.x[j] + inf.ImPos[im].x));
                if (Rx >  cond.Rc_long) continue;
                Ry = (At.y[i] - (At.y[j] + inf.ImPos[im].y));
                if (Ry >  cond.Rc_long) continue;
                Rz = (At.z[i] - (At.z[j] + inf.ImPos[im].z));
                if (Rz >  cond.Rc_long) continue;

                R_sc2 = (Rx * Rx + Ry * Ry + Rz * Rz);

                if (R_sc2 <=  Rmax2 && R_sc2 > 0.0) {
                    R_sc    = std::sqrt(R_sc2);
                    nint = (int) (R_sc / DR);
                    RDF.n[pairNum][nint] += 1.0;//funcs.Dirac(0 * DR);
                }
            }
        }
    }

    for (pairNum = 0; pairNum != RDFfor.size(); ++pairNum) {
        atNum = std::accumulate(RDF.n[pairNum].begin(), RDF.n[pairNum].end(), 0.0);

        for (i = 0; i < RDFuni.size(); i++) {
            RDFuni[i] = atNum / (4.0/3.0 * PI * RDF.RDFRmax * RDF.RDFRmax * RDF.RDFRmax) * 4 * PI *RDF.r[i] * RDF.r[i] * DR;
        }

        for (i = 0; i < RDF.r.size(); i++) {
            RDF.n[pairNum][i] = RDF.n[pairNum][i] / RDFuni[i];
        }
    }

    vector<double> data;
    vector<double> dataout;
    data.resize(RDF.r.size());
    dataout.resize(RDF.r.size());

    pairNum = 0;
    for (rdfp = RDFfor.begin(); rdfp != RDFfor.end(); ++rdfp) {
        for (i = 0; i < RDF.r.size(); i++) {
            data[i] = RDF.n[pairNum][i];
        }

        dataout = funcs::Regress2(data, cond.RDFSmoothStep);

        for (int i = 0; i < RDF.r.size(); i++)
            if (dataout[i] > 0.0)
                RDF.n[pairNum][i] = dataout[i];
            else
                RDF.n[pairNum][i] = 0.0;

        pairNum++;
    }
}

dType System::CalcSpecificHeat() {
    dType res, EnFl, En, En2, b, sc;
    iType in, i;
    dType DEn, DEn_tau, DEn2, DEn2_tau;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    En = 0.0;
    En2 = 0.0;

    #pragma omp parallel for private(i, in) reduction(+:En, En2)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];

        En   = En + (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume);
        En2 = En2 + (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume) * (At.Energy_tau[i] + cond.Preg / 160218.0 * props.Volume);
    }

    if (inf.num_procs > 1 & control.mpi_b) {
        MPI_Allgather(&En,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        En = std::accumulate(idData.begin(), idData.end(), 0.0);

        MPI_Allgather(&En2,           1, mpiType,
                                  &idData.front(),  1, mpiType, MPI_COMM_WORLD);
        En2 = std::accumulate(idData.begin(), idData.end(), 0.0);
    }

    DEn_tau = En * At.rsize;
    DEn2_tau = En2 * At.rsize;

    DEn += sc * (DEn_tau - DEn);
    DEn2 += sc * (DEn2_tau - DEn2);

    EnFl = (DEn2  - DEn * DEn);

    res = 1.0 / (kb * props.Temperature * props.Temperature)  *  EnFl * 96.48535 * 1000.0;
    return res;
}

dType System::CalcViscosity() {
    
    double Term1XY, Term2XY, TermXY, Term1YZ, Term2YZ, TermYZ, Term1ZX, Term2ZX, TermZX, coef;
    double w1, val, E, sum, res;
    int ind, i, m, in, icl, cli, cl;
    int n, k, t, size;
    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    res = 0.0;

    if (( inf.Time >= 2.0)) {
        coef = cond.d_tau / ( 3.0 * props.Volume * kb * props.Temperature ) * 1.60218e-4;
        //coef = cond.d_tau / ( props.Volume * kb * props.Temperature ) * 1.60218e-4;

        Term1XY = 0.0;
        Term2XY = 0.0;
        Term1YZ = 0.0;
        Term2YZ = 0.0;
        Term1ZX = 0.0;
        Term2ZX = 0.0;

        #pragma omp parallel for private(i, in, icl, cl, E)  \
                                 reduction(+:Term1XY, Term2XY, Term1YZ, Term2YZ, Term1ZX, Term2ZX)
        for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
            cl = Domain[inf.dID].cellI[icl];
            for (in = 0; in < Cell[cl].atomI.size(); in++) {
                i = Cell[cl].atomI[in];

                Term1XY += At.mass[i]*At.vx[i]*At.vy[i];
                Term1YZ += At.mass[i]*At.vy[i]*At.vz[i];
                Term1ZX += At.mass[i]*At.vz[i]*At.vx[i];
                Term2XY += 0.5 * At.rxfy[i];
                Term2YZ += 0.5 * At.ryfz[i];
                Term2ZX += 0.5 * At.rzfx[i];
            }
        }

        TermXY = Term1XY + Term2XY;
        TermYZ = Term1YZ + Term2YZ;
        TermZX = Term1ZX + Term2ZX;

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermXY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermXY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermYZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermYZ = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZX = std::accumulate(idData.begin(), idData.end(), 0.0);
        }


        if ( P_vec.xy.size() < (int)(100000/cond.d_tau) ) {
            P_vec.xy.push_back(TermXY);
            P_vec.yz.push_back(TermYZ);
            P_vec.zx.push_back(TermZX);
            Corr_P_vec.Push_back(0.0);

            size = P_vec.xy.size();
            t = size - 1;

            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                Corr_P_vec.xy[k] += (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] += (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] += (P_vec.zx[n] * P_vec.zx[t]);
                Corr_P_vec.nxy[k] = Corr_P_vec.xy[k]  / (size - k);
                Corr_P_vec.nyz[k] = Corr_P_vec.yz[k]  / (size - k);
                Corr_P_vec.nzx[k] = Corr_P_vec.zx[k]  / (size - k);
            }
        } else {
            size = P_vec.xy.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                Corr_P_vec.xy[k] -= (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] -= (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] -= (P_vec.zx[n] * P_vec.zx[t]);
            }

            P_vec.xy.erase(P_vec.xy.begin());
            P_vec.yz.erase(P_vec.yz.begin());
            P_vec.zx.erase(P_vec.zx.begin());
            P_vec.xy.push_back(TermXY);
            P_vec.yz.push_back(TermYZ);
            P_vec.zx.push_back(TermZX);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                Corr_P_vec.xy[k] += (P_vec.xy[n] * P_vec.xy[t]);// +
                Corr_P_vec.yz[k] += (P_vec.yz[n] * P_vec.yz[t]);// + 
                Corr_P_vec.zx[k] += (P_vec.zx[n] * P_vec.zx[t]);
                Corr_P_vec.nxy[k] = Corr_P_vec.xy[k]  / (size - k);
                Corr_P_vec.nyz[k] = Corr_P_vec.yz[k]  / (size - k);
                Corr_P_vec.nzx[k] = Corr_P_vec.zx[k]  / (size - k);
            }
        }

        if (Corr_P_vec.nxy.size() < 10) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(J_current_Vec, RJJ, J_current_Vec.x.size());

            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            //RJJ.njj = FFFilter(RJJ.njj, FFstart);
            if (Corr_P_vec.nxy.size() > (500+25*25)) {
                for (int j=0; j<25; j++)
                    res += coef * 1.0/25 * (std::accumulate(Corr_P_vec.nxy.begin(), Corr_P_vec.nxy.begin()+(500+25*j), 0.0) + 
                                            std::accumulate(Corr_P_vec.nyz.begin(), Corr_P_vec.nyz.begin()+(500+25*j), 0.0) + 
                                            std::accumulate(Corr_P_vec.nzx.begin(), Corr_P_vec.nzx.begin()+(500+25*j), 0.0) );
            } else {
                res = coef * ( std::accumulate(Corr_P_vec.nxy.begin(), Corr_P_vec.nxy.end(), 0.0) +
                               std::accumulate(Corr_P_vec.nyz.begin(), Corr_P_vec.nyz.end(), 0.0) +
                               std::accumulate(Corr_P_vec.nzx.begin(), Corr_P_vec.nzx.end(), 0.0) );
            }
        }
    }

    return res;

    /*
//Attention!! The correction term below is not correct for grand canonical ensembles, see Annu. Rev. Phys. Chem. 1965. 16:67-102. p.  92
//Jab_av = props.Volume_tau*(props.Pressure_tau +
//compute tensors
//At.jxx = At.vx*At.vx*At.mass + At.x*At.Fx;
//std::cout<< kb <<" kb"<<"\n";
//jxx = sum(At.jxx);
At.jxy = At.vx*At.vy*At.mass + At.x*At.Fy;
jxy = sum(At.jxy);
At.jxz = At.vx*At.vz*At.mass + At.x*At.Fz;
jxz = sum(At.jxz);
//At.jyy = At.vy*At.vy*At.mass + At.y*At.Fy;
//jyy = sum(At.jyy);
//At.jyx = At.vy*At.vx*At.mass + At.y*At.Fx;
//jyx = sum(At.jyx);
At.jyz = At.vy*At.vz*At.mass + At.y*At.Fz;
jyz = sum(At.jyz);
//At.jzz = At.vz*At.vz*At.mass + At.z*At.Fz;
//jzz = sum(At.jzz);
//At.jzx = At.vz*At.vx*At.mass + At.z*At.Fx;
//jzx = sum(At.jzx);
//At.jzy = At.vz*At.vy*At.mass + At.z*At.Fy;
//jzy = sum(At.jzy);
//At.rf_y = At.y*At.Fy;
//At.rf_z = At.z*At.Fz;




        //different autocorrelation functions
        //bulk viscosity
        //shear viscosity
        //Cxyxy.push_back(0.0);
        //Cxzxz.push_back(0.0);
        //Cyzyz.push_back(0.0);

    //} else {
        //J_vec.xx.erase(J_vec.xx.begin());
        //J_vec.xy.erase(J_vec.xy.begin());
        //J_vec.xz.erase(J_vec.xz.begin());
        //J_vec.yx.erase(J_vec.yx.begin());
        //J_vec.yy.erase(J_vec.yy.begin());
        //J_vec.yz.erase(J_vec.yz.begin());
        //J_vec.zx.erase(J_vec.zx.begin());
        //J_vec.zy.erase(J_vec.zy.begin());
        //J_vec.zz.erase(J_vec.zz.begin());


        //J_vec.xx.push_back(jxx);
        //J_vec.xy.push_back(jxy);
        //J_vec.xz.push_back(jxz);
        //J_vec.yx.push_back(jyx);
        //J_vec.yy.push_back(jyy);
        //J_vec.yz.push_back(jyz);
        //J_vec.zx.push_back(jzx);
        //J_vec.zy.push_back(jzy);
        //J_vec.zz.push_back(jzz);
    //}

    //clean the autocorr functions
    //start summing up terms
    double tau_max;
    //std::cout<< tau_max<< "  !"<<"\n";


    //dType bulk_vis = 0; //bulk viscosity
    double res = 0.0;
    int n,k,t;
    int size;
    //dType count = 0;
    //dType vbulk_running_integral;
    //std::cout << vbulk_running_integral << "old nu" <<"\n";

if ( J_vec.xy.size() < (int)(100000/cond.d_tau))
    {
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            J_vec.yz.push_back(jyz);
            Corr_J_vec.Push_back(0.0);
            Cxyxy.push_back(0.0);
            Cxzxz.push_back(0.0);
            Cyzyz.push_back(0.0);

            size = J_vec.xy.size();


    }

 else //erase first member, add a new last member
     {
    J_vec.xy.erase(J_vec.xy.begin());
    J_vec.xz.erase(J_vec.xz.begin());
    J_vec.yz.erase(J_vec.yz.begin());


    J_vec.xy.push_back(jxy);
    J_vec.xz.push_back(jxz);
    J_vec.yz.push_back(jyz);
    }


    std::fill(Cxyxy.begin(), Cxyxy.end(), 0.0);
    std::fill(Cxzxz.begin(), Cxzxz.end(), 0.0);
    std::fill(Cyzyz.begin(), Cyzyz.end(), 0.0);

    //calculate the autocorr functn
    #pragma omp parallel for private(n, k, t)
    for (k = 0; k < size; k++)
     {

        for (t = 0; t < (size - k); t++)
         {
            n = t + k;
            Cxyxy[k]+= (J_vec.xy[n] * J_vec.xy[t]);
            Cxzxz[k]+= (J_vec.xz[n] * J_vec.xz[t]);
            Cyzyz[k] += (J_vec.yz[n] * J_vec.yz[t]);
         }
        //normalize
        Cxyxy[k] = Cxyxy[k] / (size - k);
        Cxzxz[k] = Cxzxz[k] / (size - k);
        Cyzyz[k] = Cyzyz[k] / (size - k);
     }

    std::ofstream write_output("correl_out.dat");
    assert(write_output.is_open());
    for (int n=0; n<J_vec.xy.size(); n=n+10)
    {
        write_output << n << " "<< Cxyxy[n] <<" " <<Cxzxz[n]<<" "<<Cyzyz[n]<<"\n";
    }
    write_output.close();
*/



    //this viscosity script is based on the perturbation method, see J. Chem. Phys. 116, 209 (2002) Berk Hess.
    /*
    //dType jxx;
    dType jxy;
    dType jxz;
    //dType jyy;
    //dType jyx;
    dType jyz;
    //dType jzz;
    //dType jzx;
    //dType jzy;

    //add the perturbation acceleration to each of the particles: a_{x}(z) = Acos(kz) ; A denoted as Amp below:
    dType Amp = 0.1; //nm ps^-2, can be chosen

    //At.ax = At.ax + Amp*cos()

    std::cout<< cos(2*pi)<<"\n";
    At.jxy = At.vx*At.vy*At.mass + At.x*At.Fy;
    jxy = sum(At.jxy);
    At.jxz = At.vx*At.vz*At.mass + At.x*At.Fz;
    jxz = sum(At.jxz);
    //At.jyy = At.vy*At.vy*At.mass + At.y*At.Fy;
    //jyy = sum(At.jyy);
    //At.jyx = At.vy*At.vx*At.mass + At.y*At.Fx;
    //jyx = sum(At.jyx);
    At.jyz = At.vy*At.vz*At.mass + At.y*At.Fz;
    jyz = sum(At.jyz);
    //At.jzz = At.vz*At.vz*At.mass + At.z*At.Fz;
    //jzz = sum(At.jzz);
    //At.jzx = At.vz*At.vx*At.mass + At.z*At.Fx;
    //jzx = sum(At.jzx);
    //At.jzy = At.vz*At.vy*At.mass + At.z*At.Fy;
    //jzy = sum(At.jzy);
    //At.rf_y = At.y*At.Fy;
    //At.rf_z = At.z*At.Fz;

        if ( J_vec.xy.size() < (int)(100000/cond.d_tau)) {
            //J_vec.xx.push_back(jxx);
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            //J_vec.yx.push_back(jyx);
            //J_vec.yy.push_back(jyy);
            J_vec.yz.push_back(jyz);
            //J_vec.zx.push_back(jzx);
            //J_vec.zy.push_back(jzy);
            //J_vec.zz.push_back(jzz);
            Corr_J_vec.Push_back(0.0);

            //different autocorrelation functions
            //bulk viscosity

            //shear viscosity
            Cxyxy.push_back(0.0);
            Cxzxz.push_back(0.0);
            Cyzyz.push_back(0.0);

        } else {
            //J_vec.xx.erase(J_vec.xx.begin());
            J_vec.xy.erase(J_vec.xy.begin());
            J_vec.xz.erase(J_vec.xz.begin());
            //J_vec.yx.erase(J_vec.yx.begin());
            //J_vec.yy.erase(J_vec.yy.begin());
            J_vec.yz.erase(J_vec.yz.begin());
            //J_vec.zx.erase(J_vec.zx.begin());
            //J_vec.zy.erase(J_vec.zy.begin());
            //J_vec.zz.erase(J_vec.zz.begin());


            //J_vec.xx.push_back(jxx);
            J_vec.xy.push_back(jxy);
            J_vec.xz.push_back(jxz);
            //J_vec.yx.push_back(jyx);
            //J_vec.yy.push_back(jyy);
            J_vec.yz.push_back(jyz);
            //J_vec.zx.push_back(jzx);
            //J_vec.zy.push_back(jzy);
            //J_vec.zz.push_back(jzz);
        }

        //clean the autocorr functions

        //same for shear viscosity autocorr fnctns.
        std::fill(Cxyxy.begin(), Cxyxy.end(), 0.0);
        std::fill(Cxzxz.begin(), Cxzxz.end(), 0.0);
        std::fill(Cyzyz.begin(), Cyzyz.end(), 0.0);


        //start summing up terms
        double tau_max;
        //std::cout<< tau_max<< "  !"<<"\n";


        //dType bulk_vis = 0; //bulk viscosity
        double res = 0.0;
        //dType count = 0;
        //dType vbulk_running_integral;
        //std::cout << vbulk_running_integral << "old nu" <<"\n";
        for (int n=0; n<J_vec.xy.size() ; n++)
        {
            tau_max = 0.0;
            for (int i=0; i<(J_vec.xy.size()-n) ; i++)
            {
                //bulk autocoor. fnctns.

                //shear autocoor. fnctns.
                Cxyxy[n] += J_vec.xy[i]*J_vec.xy[i+n];
                Cxzxz[n] += J_vec.xz[i]*J_vec.xz[i+n];
                Cyzyz[n] += J_vec.yz[i]*J_vec.yz[i+n];
                tau_max++;
            }

            Cxyxy[n] /= tau_max;
            Cxzxz[n] /= tau_max;
            Cyzyz[n] /= tau_max;
        }

        //correl_intgrnd = cond.d_tau*(Cxxxx + Cxxyy + Cxxzz + Cyyxx + Cyyyy + Cyyzz + Czzxx + Czzyy + Czzzz); //value of integrand at time tau_max
        shear_intgrnd = cond.d_tau*(Cxyxy + Cxzxz + Cyzyz);

        for (int n=0; n< shear_intgrnd.size(); n++)
        {
            //bulk_vis += correl_intgrnd[n]; // ev^2/fs
            res += shear_intgrnd[n];

        }
        //final integrals:
        //bulk_vis = 1.0/(9.0*kb*props.Volume_tau*props.Temperature)*bulk_vis;
        res = 1.0/(3.0*kb*props.Volume_tau*props.Temperature)*res;
        //switch units
        //bulk_vis = bulk_vis*1.60218e-4;
        res = res*1.60218e-4;

        //J_vec.bv.push_back(bulk_vis);
        //vbulk_running_integral = sum(J_vec.zz);
        //std:: cout << shear_vis << "\n";
        //std:: cout << Czzzz.size() << "\n";
        */



/*
    dType jxx;
    dType jxy;
    dType jxz;
    dType jyy;
    dType jyx;
    dType jyz;
    dType jzz;
    dType jzx;
    dType jzy;

    //Einstein relation approach
    At.jxy = At.x*At.vy*At.mass;
    jxy = sum(At.jxy);
    At.jxz = At.x*At.vz*At.mass;
    jxz = sum(At.jxz);
    At.jyz = At.y*At.vz*At.mass;
    jyz = sum(At.jyz);

    int lag=50000;

    if ( P_vec.xy.size() < lag ) {
        P_vec.xy.push_back(jxy);
        P_vec.xz.push_back(jxz);
        P_vec.yz.push_back(jyz);
        //Corr_P_vec.Push_back(0.0);
        P_vec.Cxyxy.push_back(0.0);
        P_vec.Cxzxz.push_back(0.0);
        P_vec.Cyzyz.push_back(0.0);
    } else {
        P_vec.xy.erase(P_vec.xy.begin());
        P_vec.xz.erase(P_vec.xz.begin());
        P_vec.yz.erase(P_vec.yz.begin());
        P_vec.xy.push_back(jxy);
        P_vec.xz.push_back(jxz);
        P_vec.yz.push_back(jyz);
    }

    //clean vector that will hold Pxy(t) values
    std::fill(P_vec.Cxyxy.begin(), P_vec.Cxyxy.end(), 0.0);
    std::fill(P_vec.Cxzxz.begin(), P_vec.Cxzxz.end(), 0.0);
    std::fill(P_vec.Cyzyz.begin(), P_vec.Cyzyz.end(), 0.0);
    P_vec.sum =0.0;
    double obs_time = P_vec.xy.size()*cond.d_tau;
    //if (J_vec.xy.size() >= (int)(1000/cond.d_tau)){ //make sure enough time origins are collected
    if (P_vec.xy.size() >= 20) { //make sure enough time origins are collected
        P_vec.Cxyxy = pow2d(P_vec.xy - P_vec.xy[0]);
        P_vec.Cxzxz = pow2d(P_vec.xz - P_vec.xz[0]);
        P_vec.Cyzyz = pow2d(P_vec.yz - P_vec.yz[0]);
        P_vec.sum = sum(P_vec.Cxyxy + P_vec.Cxzxz + P_vec.Cyzyz);

        //res=res/P_vec.xy.size(); //final average over nt0 time steps
        P_vec.sum = P_vec.sum/At.size; //final average over nt0 time steps
        P_vec.sum = 1.0/(6.0 * kb * props.Volume_tau * props.Temperature * obs_time) * P_vec.sum;
        //switch units
        P_vec.sum = P_vec.sum*1.60218e-4;
    }
    return P_vec.sum;
    */
}

dType System::CalcIsotermalCompressibility() {
    dType res, VFl, Vol2tau, b, sc, Volume2;
    iType in, i;
    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);

    b = -std::log(1.0 / (cond.Av_Time))/(cond.Av_Time);
    sc =  inf.Time < cond.Av_Time ? std::exp(-( inf.Time)*b) : 1.0 / (cond.Av_Time);
    sc =  inf.Time == 0.0 ? 1.0 : sc;

    Vol2tau = props.Volume_tau * props.Volume_tau;
    Volume2 += sc * (Vol2tau - Volume2);

    VFl = (Volume2 - props.Volume * props.Volume);

    res =  - 1.0 / (kb * props.Temperature * props.Temperature * props.Volume)  *  VFl;
    return res;
}

/*
dType System::CalcThermalConductivity() {
    double Cond_TermX, Cond_TermY, Cond_TermZ, Conv_TermX, Conv_TermY, Conv_TermZ;
    double wn, res, TermX, TermY, TermZ;

    double w1, val, coef, E, sum;
    int J_i, Nm, ind, i, m, in, icl, cli, cl;
    int n, k, t, size, FFcount = 25;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);
    res = 0.0;

    if (control.thermc_b) {
        coef = cond.d_tau / (3 * props.Volume * kb * props.Temperature * props.Temperature) * 1602176;

        Conv_TermX = 0.0;
        Conv_TermY = 0.0;
        Conv_TermZ = 0.0;
        Cond_TermX = 0.0;
        Cond_TermY = 0.0;
        Cond_TermZ = 0.0;

        #pragma omp parallel for private(i, in, icl, cl, E)  \
                                 reduction(+:Conv_TermX, Conv_TermY, Conv_TermZ) \
                                 reduction(+:Cond_TermX, Cond_TermY, Cond_TermZ)
        for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
            cl = Domain[inf.dID].cellI[icl];
            for (in = 0; in < Cell[cl].atomI.size(); in++) {
                i = Cell[cl].atomI[in];

                Cond_TermX += At.condx[i];
                Cond_TermY += At.condy[i];
                Cond_TermZ += At.condz[i];

                E = At.KinEnergy_tau[i] + 0.5 * At.PotEnergy_tau[i];

                Conv_TermX += E * At.vx[i];
                Conv_TermY += E * At.vy[i];
                Conv_TermZ += E * At.vz[i];
            }
        }

        TermX = Conv_TermX + 0.5 * Cond_TermX;
        TermY = Conv_TermY + 0.5 * Cond_TermY;
        TermZ = Conv_TermZ + 0.5 * Cond_TermZ;

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermX = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZ = std::accumulate(idData.begin(), idData.end(), 0.0);
        }

        if ( J_current_Vec.x.size() < (int)(100000/cond.d_tau)) {
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);
            RJJ.Push_back(0.0);

            size = J_current_Vec.x.size();
            t = size - 1;

            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        } else {
            size = J_current_Vec.x.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                RJJ.jj[k] -= (J_current_Vec.x[n] * J_current_Vec.x[t] +
                                    J_current_Vec.y[n] * J_current_Vec.y[t] +
                                    J_current_Vec.z[n] * J_current_Vec.z[t]);
            }

            J_current_Vec.x.erase(J_current_Vec.x.begin());
            J_current_Vec.y.erase(J_current_Vec.y.begin());
            J_current_Vec.z.erase(J_current_Vec.z.begin());
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                                     J_current_Vec.y[n] * J_current_Vec.y[t] +
                                     J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        }

        if (RJJ.njj.size() < 10) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(J_current_Vec, RJJ, J_current_Vec.x.size());
            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            if (RJJ.njj.size() > (500+25*FFcount)) {
                for (int j=0; j<FFcount; j++)
                    res += coef * 1.0/FFcount * std::accumulate(RJJ.njj.begin(), RJJ.njj.begin()+(500+25*j), 0.0);
            } else {
                res = coef * std::accumulate(RJJ.njj.begin(), RJJ.njj.end(), 0.0);
            }
        }
    }
    return res;
}
*/

dType System::CalcThermalConductivity() {
    double Cond_TermX, Cond_TermY, Cond_TermZ, Conv_TermX, Conv_TermY, Conv_TermZ;
    double wn, res, TermX, TermY, TermZ;

    double w1, val, coef, E, sum;
    int J_i, Nm, ind, i, m, in, icl, cli, cl;
    int n, k, t, size;

    std::vector<dType> idData, njj;
    idData.resize(inf.dNP, 0);

    res = 0.0;

    if (( inf.Time >= 5.0) && control.thermc_b) {
        coef = cond.d_tau / (3 * props.Volume * kb * props.Temperature * props.Temperature) * 1602176;

        Conv_TermX = 0.0;
        Conv_TermY = 0.0;
        Conv_TermZ = 0.0;
        Cond_TermX = 0.0;
        Cond_TermY = 0.0;
        Cond_TermZ = 0.0;

        #pragma omp parallel for private(i, in, icl, cl, E)  \
                                 reduction(+:Conv_TermX, Conv_TermY, Conv_TermZ) \
                                 reduction(+:Cond_TermX, Cond_TermY, Cond_TermZ)
        for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
            cl = Domain[inf.dID].cellI[icl];
            for (in = 0; in < Cell[cl].atomI.size(); in++) {
                i = Cell[cl].atomI[in];

                Cond_TermX += At.condx[i];
                Cond_TermY += At.condy[i];
                Cond_TermZ += At.condz[i];

                E = At.KinEnergy_tau[i] + 0.5 * At.PotEnergy_tau[i];

                Conv_TermX += E * At.vx[i];
                Conv_TermY += E * At.vy[i];
                Conv_TermZ += E * At.vz[i];
            }
        }

        TermX = Conv_TermX + 0.5 * Cond_TermX;
        TermY = Conv_TermY + 0.5 * Cond_TermY;
        TermZ = Conv_TermZ + 0.5 * Cond_TermZ;

        /*
        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermX = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZ = std::accumulate(idData.begin(), idData.end(), 0.0);
        }
        */

        if ( J_current_Vec.x.size() < (int)(100000/cond.d_tau) ) {
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);
            RJJ.Push_back(0.0);

            size = J_current_Vec.x.size();
            t = size - 1;

            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        } else {
            size = J_current_Vec.x.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                RJJ.jj[k] -= (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
            }

            J_current_Vec.x.erase(J_current_Vec.x.begin());
            J_current_Vec.y.erase(J_current_Vec.y.begin());
            J_current_Vec.z.erase(J_current_Vec.z.begin());
            J_current_Vec.x.push_back(TermX);
            J_current_Vec.y.push_back(TermY);
            J_current_Vec.z.push_back(TermZ);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                RJJ.jj[k] += (J_current_Vec.x[n] * J_current_Vec.x[t] +
                              J_current_Vec.y[n] * J_current_Vec.y[t] +
                              J_current_Vec.z[n] * J_current_Vec.z[t]);
                RJJ.njj[k] = RJJ.jj[k]  / (size - k);
            }
        }

        if (RJJ.njj.size() > 200) {
            //AutocorrelationCumm(J_current_Vec, RJJ, J_current_Vec.x.size());
            /*
            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            //njj = FFFilter(RJJ.njj, FFstart);
            if (RJJ.njj.size() > (500+25*25)) {
                for (int j=0; j<25; j++)
                    res += coef * 1.0/25 * std::accumulate(RJJ.njj.begin(), RJJ.njj.begin()+(500+25*j), 0.0);
            } else {
                res = coef * std::accumulate(RJJ.njj.begin(), RJJ.njj.end(), 0.0);
            }
            */
            //RJJ.njj = coef * Autocorrelation(J_current_Vec, J_current_Vec.x.size());
            njj  = RJJ.njj; //FFFilter(RJJ.njj, 1.0);
            size = njj.size();

            int size_lim = 5000;

            int from = int(0.25 * size_lim);
            int to   = int(0.50 * size_lim);
            if (size < size_lim) {
                from = int(0.25 * size);
                to   = int(0.50 * size);
            }
            int step = int((to - from) / 25);
            
            res = 0.0;
            for (int j=0; j<25; j++)
                res += coef * 1.0/25 * std::accumulate(njj.begin(), njj.begin()+(from+step*j), 0.0);
        }
    }
    return res;
}


dType System::CalcDiffusion() {
    double TermX, TermY, TermZ;
    double wn;
    double res;

    double w1, val, coef, E, sum;
    int J_i, Nm, ind, i, m, in, icl, cli, cl;
    int n, k, t, size, FFcount = 25;

    std::vector<dType> idData;
    idData.resize(inf.dNP, 0);
    res = 0.0;

    // 1.0e-1 is a convertion coefficient from A2/fs to cm2/s
    coef = 1.0e-1; //kb * props.Temperature cond.d_tau / (3 * props.Volume * kb * props.Temperature * props.Temperature) * 1602176;
    if (( inf.Time >= cond.StartThC) && (control.thermc_b)) {

        TermX = std::accumulate(At.vx.begin(), At.vx.end(), 0.0 );
        TermY = std::accumulate(At.vy.begin(), At.vy.end(), 0.0 );
        TermZ = std::accumulate(At.vz.begin(), At.vz.end(), 0.0 );

        if (inf.num_procs > 1 & control.mpi_b) {
            MPI_Allgather(&TermX, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermX = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermY, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermY = std::accumulate(idData.begin(), idData.end(), 0.0);

            MPI_Allgather(&TermZ, 1, mpiType,   &idData.front(),  1, mpiType, MPI_COMM_WORLD);
            TermZ = std::accumulate(idData.begin(), idData.end(), 0.0);
        }

        if ( Velocity_Vec.x.size() < (int)(250000/cond.d_tau)) {
            Velocity_Vec.x.push_back(TermX);
            Velocity_Vec.y.push_back(TermY);
            Velocity_Vec.z.push_back(TermZ);
            Rvv.Push_back(0.0);

            size = Velocity_Vec.x.size();

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = (size - 1) - k;
                Rvv.jj[k] += (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                     Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                     Velocity_Vec.z[n] * Velocity_Vec.z[t]);
                Rvv.njj[k] = Rvv.jj[k]  / (size - k);
            }
        } else {

            size = Velocity_Vec.x.size();

            t = 0;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = k;
                Rvv.jj[k] -= (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                    Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                    Velocity_Vec.z[n] * Velocity_Vec.z[t]);
            }

            Velocity_Vec.x.erase(Velocity_Vec.x.begin());
            Velocity_Vec.y.erase(Velocity_Vec.y.begin());
            Velocity_Vec.z.erase(Velocity_Vec.z.begin());
            Velocity_Vec.x.push_back(TermX);
            Velocity_Vec.y.push_back(TermY);
            Velocity_Vec.z.push_back(TermZ);

            t = size - 1;
            #pragma omp parallel for private(n, k)
            for (k = 0; k < size; k++) {
                n = size - k - 1;
                Rvv.jj[k] += (Velocity_Vec.x[n] * Velocity_Vec.x[t] +
                                      Velocity_Vec.y[n] * Velocity_Vec.y[t] +
                                      Velocity_Vec.z[n] * Velocity_Vec.z[t]);
                Rvv.njj[k] = Rvv.jj[k]  / (size - k);
            }
        }

        if (Rvv.njj.size() < 10) {
            res = 0.0;
        } else {
            //AutocorrelationCumm(Velocity_Vec, Rvv, Velocity_Vec.x.size());
            if (Rvv.njj.size() > (500+25*FFcount)) {
                for (int j=0; j<FFcount; j++)
                    res += coef * 1.0/FFcount * std::accumulate(Rvv.njj.begin(), Rvv.njj.begin()+(500+25*j), 0.0);
            } else {
                res = coef * std::accumulate(Rvv.njj.begin(), Rvv.njj.end(), 0.0);
            }
        }
    }
    return res;
}

std::vector<dType> System::Autocorrelation(XYZ_vec& jvec, int size) {
    int n, m, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    #pragma omp parallel for private(n, m, k, t, val)
    for (k = 0; k < size; k++) {
        val = 0.0;

        for (t = 0; t < (size - k); t++) {
            n = t + k;
            val += (jvec.x[t] * jvec.x[n] +
                         jvec.y[t] * jvec.y[n] +
                         jvec.z[t] * jvec.z[n]);
        }
        res[k] = val / (size - k);
    }
    return res;
}

std::vector<dType> System::FFFilter(std::vector<dType>& InVec, double freq) {
    int i, Nm;
    vector<dType> data;
    int nn = 1;
    int nn2 = nn * 2;

    nn2 = 65536;
    nn  = 32768;
    vector<dType> dataout(nn);

    data.resize(nn2);
    //#pragma omp parallel for private(i)
    for ( i=0; i<nn; i++ ) {
        if (i < InVec.size()) {
            data[i * 2]     = InVec[i];
            data[i * 2 + 1] = 0.0;
        } else {
            data[i * 2] = 0.0;
            data[i * 2 + 1] = 0.0;
        }
    }

    funcs::FFT( data );

    Nm = (int)(freq * (nn * cond.d_tau) + 1);
    for (i = 0; i < nn; i++)  {
        if ((i >= (Nm + 1)) && (i <= (nn - Nm))) {
            data[i * 2] = 0.0;
            data[i * 2 + 1] = 0.0;
        }
    }

    funcs::rFFT( data );

    for ( i = 0; i < nn; i++ ) {
        dataout[i]  = data[i * 2] / nn;
    }
    return dataout;
}

void System::AutocorrelationCumm(XYZ_vec& jvec, Corr_struct& RJJ, int size) {
    int n, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    t = size - 1;

    #pragma omp parallel for private(n, k)
    for (k = 0; k < size; k++) {
        n = size - k - 1;
        RJJ.jj[k] += (jvec.x[n] * jvec.x[t] +
                             jvec.y[n] * jvec.y[t] +
                             jvec.z[n] * jvec.z[t]);
        RJJ.njj[k] = RJJ.jj[k]  / (size - k);
    }
}

void System::AutocorrelationMov(XYZ_vec& jvec, Corr_struct& RJJ, int size) {
    int n, k, t;
    std::vector<dType> res(size);
    std::vector<dType> multx;
    std::vector<dType> multy;
    std::vector<dType> multz;
    dType val;

    std::fill(res.begin(), res.end(), 0);

    t = size - 1;

    #pragma omp parallel for private(n, k)
    for (k = 0; k < size; k++) {
        n = size - k - 1;
        RJJ.jj[k] += (jvec.x[n] * jvec.x[t] +
                             jvec.y[n] * jvec.y[t] +
                             jvec.z[n] * jvec.z[t]);
        RJJ.njj[k] = RJJ.jj[k]  / (size - k);
    }
}

void System::FFPSI(int i, PairDistV& PairR) {
    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  FFPSI : " << std::endl;
    #endif
    dType Fx, Fy, Fz, FscRsc, qsum, Rc_B;

    std::vector<dType> Esc;
    std::vector<dType> Fsc;

    Fsc = tab.Tab_Force.getValue(PairR.keyi, PairR.R);
    Esc = tab.Tab_Energy.getValue(PairR.keyi, PairR.R);

    std::replace_if(Esc.begin(), Esc.end(), IsNan, 0.0);
    std::replace_if(Esc.begin(), Esc.end(), IsInf, 0.0);
    std::replace_if(Fsc.begin(), Fsc.end(), IsNan, 0.0);
    std::replace_if(Fsc.begin(), Fsc.end(), IsInf, 0.0);

    Fx = sum(Fsc * PairR.R1x);
    Fy = sum(Fsc * PairR.R1y);
    Fz = sum(Fsc * PairR.R1z);

    At.FscRP[i] += sum(Fsc * PairR.R);
    At.EscP[i]   += sum(Esc);

    At.Fxp[i] += Fx;
    At.Fyp[i] += Fy;
    At.Fzp[i] += Fz;

    At.ElSelf[i] = At.q[i] * At.qs[i] / cond.Rc_long_Bohr;
    PairR.Fsc = PairR.Fsc + Fsc;
    At.rxfy[i] += sum(PairR.Rx * PairR.R1y * Fsc);
    At.ryfz[i] += sum(PairR.Ry * PairR.R1z * Fsc);
    At.rzfx[i] += sum(PairR.Rz * PairR.R1x * Fsc);
    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  FFPSI : " << std::endl;
    #endif
}



void System::FFMEAMdens(int i, PairDistV& PairR) {
    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  FFMEAMdens : " << std::endl;
    #endif
    int size = PairR.R.size(), j;
    //assert(size > 0);
    if (!(size > 0)) return;
    assert(size > 0);
    std::vector<dType> X(size), rX(size), x(size), y(size), z(size), rhoa0i(size);
    dType ra1i = 0.0, ra2i = 0.0, ra3i = 0.0;

    X = PairR.R;
    //rX  = 1.0 / X;
    //x = PairR.Rx * rX;
    //y = PairR.Ry * rX;
    //z = PairR.Rz * rX;


    //The first spherical electron density orbit
    rhoa0i     = tab.Tab_rho0_val.getValue(PairR.keyj, PairR.R);
    At.rho0[i] = sum(rhoa0i);

    /*
    //The second-fourth spherical electron density orbits
    At.rho1[i] = 0.0;
    At.rho2[i] = 0.0;
    At.rho3[i] = 0.0;


    std::vector<dType> rhoa1i(size), rhoa2i(size), rhoa3i(size);
    rhoa1i = tab.Tab_rho1_val.getValue(PairR.keyj, PairR.R);
    rhoa2i = tab.Tab_rho2_val.getValue(PairR.keyj, PairR.R);
    rhoa3i = tab.Tab_rho3_val.getValue(PairR.keyj, PairR.R);

    if (sum(rhoa1i) > 0.0) {
        ra1i = sum((x * rhoa1i) * (x * rhoa1i))
             + sum((y * rhoa1i) * (y * rhoa1i))
             + sum((z * rhoa1i) * (z * rhoa1i));
        At.rho1[i] = ra1i;
    } 

    if (sum(rhoa2i) > 0.0) {
        std::vector<dType> rhoa2xi(size);
        std::vector<dType> rhoa2yi(size);
        std::vector<dType> rhoa2zi(size);
        dType ra2i;

        rhoa2xi = rhoa2i * x;
        rhoa2yi = rhoa2i * y;
        rhoa2zi = rhoa2i * z;


        ra2i = sum((x * rhoa2xi) * (x * rhoa2xi))
            + sum((y * rhoa2xi) * (y * rhoa2xi))
            + sum((z * rhoa2xi) * (z * rhoa2xi))

            + sum((x * rhoa2yi) * (x * rhoa2yi))
            + sum((y * rhoa2yi) * (y * rhoa2yi))
            + sum((z * rhoa2yi) * (z * rhoa2yi))

            + sum((x * rhoa2zi) * (x * rhoa2zi))
            + sum((y * rhoa2zi) * (y * rhoa2zi))
            + sum((z * rhoa2zi) * (z * rhoa2zi))
            - 1.0/3.0 * sum(rhoa2i * rhoa2i);
        At.rho2[i] = ra2i;
    }

    if (sum(rhoa3i) > 0.0) {
        std::vector<dType> rhoa3i(size);
        std::vector<dType> rhoa3xi(size);
        std::vector<dType> rhoa3yi(size);
        std::vector<dType> rhoa3zi(size);
        dType ra3i;

        rhoa3i = tab.Tab_rho3_val.getValue(PairR.keyj, PairR.R);

        rhoa3xi = rhoa3i * x;
        rhoa3yi = rhoa3i * y;
        rhoa3zi = rhoa3i * z;

        ra3i= sum((x * x * rhoa3xi) * (x * x * rhoa3xi))
            + sum((x * y * rhoa3xi) * (x * y * rhoa3xi))
            + sum((x * z * rhoa3xi) * (x * z * rhoa3xi))

            + sum((y * x * rhoa3xi) * (y * x * rhoa3xi))
            + sum((y * y * rhoa3xi) * (y * y * rhoa3xi))
            + sum((y * z * rhoa3xi) * (y * z * rhoa3xi))

            + sum((z * x * rhoa3xi) * (z * x * rhoa3xi))
            + sum((z * y * rhoa3xi) * (z * y * rhoa3xi))
            + sum((z * z * rhoa3xi) * (z * z * rhoa3xi))


            + sum((x * x * rhoa3yi) * (x * x * rhoa3yi))
            + sum((x * y * rhoa3yi) * (x * y * rhoa3yi))
            + sum((x * z * rhoa3yi) * (x * z * rhoa3yi))

            + sum((y * x * rhoa3yi) * (y * x * rhoa3yi))
            + sum((y * y * rhoa3yi) * (y * y * rhoa3yi))
            + sum((y * z * rhoa3yi) * (y * z * rhoa3yi))

            + sum((z * x * rhoa3yi) * (z * x * rhoa3yi))
            + sum((z * y * rhoa3yi) * (z * y * rhoa3yi))
            + sum((z * z * rhoa3yi) * (z * z * rhoa3yi))


            + sum((x * x * rhoa3zi) * (x * x * rhoa3zi))
            + sum((x * y * rhoa3zi) * (x * y * rhoa3zi))
            + sum((x * z * rhoa3zi) * (x * z * rhoa3zi))

            + sum((y * x * rhoa3zi) * (y * x * rhoa3zi))
            + sum((y * y * rhoa3zi) * (y * y * rhoa3zi))
            + sum((y * z * rhoa3zi) * (y * z * rhoa3zi))

            + sum((z * x * rhoa3zi) * (z * x * rhoa3zi))
            + sum((z * y * rhoa3zi) * (z * y * rhoa3zi))
            + sum((z * z * rhoa3zi) * (z * z * rhoa3zi))

            -3.0/5.0 * (sum((x * rhoa3i) * (x * rhoa3i))
                    + sum((y * rhoa3i) * (y * rhoa3i))
                    + sum((z * rhoa3i) * (z * rhoa3i)));

        At.rho3[i] = ra3i;
    }
    */

    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  FFMEAMdens : " << std::endl;
    #endif

}


void System::FFMEAM(int i, PairDistV& PairR) {
    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  FFMEAM : " << std::endl;
    #endif
    bool calcG;
    int size = PairR.R.size();

    //assert(size > 0);
    if (!(size > 0)) return;
    assert(size > 0);

    dType Fx, Fy, Fz;
    dType Ai, Ei, rho0i, t0, t1, t2, t3, beta0, beta1, beta2, beta3, Nz, beta0i;
    FadV<dType> X(size), rX(size), x(size), y(size), z(size);
    FadV<dType> Gi(size), Gj(size);
    std::vector<dType> zero(PairR.R.size(), 0.0), one(PairR.R.size(), 1.0);
    std::vector<dType> Aj(size), Ej(size), rhj0(size), rhj1(size), rhj2(size), rhj3(size), t1j(size), t2j(size), t3j(size), Nzj(size);
    std::string namei, namej;

    namei = At.name[i];

	//FcutD = tab.FcutD.getValue(PairR.R);//CutFuncD(PairR.R, Rc);

    Ai         = params[namei].A;
    Ei         = params[namei].E;
    rho0i      = params[namei].rho0;
    beta0i	   = params[namei].beta0;

    int j;
    for (int n = 0; n < size; n++) {
    	j = PairR.j[n];
    	namej      = At.name[j];
		Aj[n]    = params[namej].A;
		Ej[n]    = params[namej].E;
        t1j[n]   = params[namej].t1;
        t2j[n]   = params[namej].t2;
        t3j[n]   = params[namej].t3;
        Nzj[n]   = params[namej].Nz;
		rhj0[n]  = At.rho0[j];
		rhj1[n]  = At.rho1[j];
		rhj2[n]  = At.rho2[j];
		rhj3[n]  = At.rho3[j];
    }

    t0 = params[namei].t0;
    t1 = params[namei].t1;
    t2 = params[namei].t2;
    t3 = params[namei].t3;
    beta0 = params[namei].beta0;
    beta1 = params[namei].beta1;
    beta2 = params[namei].beta2;
    beta3 = params[namei].beta3;
    Nz    = params[namei].Nz;

    Gi._val = zero;
    Gi._dx  = zero;
    Gj = Gi;
    X._val = PairR.R;
    X._dx  = one;

    //FCut = tab.FcutD.getValue(PairR.R);

    rX  = 1.0 / X;
    x = PairR.Rx * rX;
    y = PairR.Ry * rX;
    z = PairR.Rz * rX;

    FadV<dType> rra0i(size);
    FadV<dType> rra0j(size);


    FadV<dType> rhoa0i(size);
    FadV<dType> rhoa0j(size);

    FadV<dType> ra0i(size);
    FadV<dType> ra0j(size);

    ra0i._val = fill(size, At.rho0[i]);
    ra0i._dx  = tab.Tab_rho0_dx.getValue(PairR.keyj,  PairR.R);
    rra0i     = 1.0 / ra0i;

    ra0j._val = rhj0;
    ra0j._dx  = tab.Tab_rho0_dx.getValue(PairR.keyi,  PairR.R);
    rra0j     = 1.0 / ra0j;

    calcG = false;

	if (beta1 != 0.0 && t1 != 0.0) {
	    FadV<dType> rhoa1i(size);
	    FadV<dType> rhoa1j(size);
	    FadV<dType> ra1i(size);
	    FadV<dType> ra1j(size);

	    rhoa1i._val = tab.Tab_rho1_val.getValue(PairR.keyi, PairR.R);
        rhoa1i._dx  = tab.Tab_rho1_dx.getValue(PairR.keyi, PairR.R);

		rhoa1j._val = tab.Tab_rho1_val.getValue(PairR.keyj, PairR.R);
        rhoa1j._dx  = tab.Tab_rho1_dx.getValue(PairR.keyj, PairR.R);

        ra1i._val = fill(size,At.rho1[i] );
        ra1j._val = rhj1;

        ra1i._dx = pow2sumVdx(x * rhoa1i)
                 + pow2sumVdx(y * rhoa1i)
                 + pow2sumVdx(z * rhoa1i);

        ra1j._dx = pow2sumVdx(x * rhoa1j)
            	 + pow2sumVdx(y * rhoa1j)
                 + pow2sumVdx(z * rhoa1j);

        Gi = Gi + t1 * ra1i * rra0i * rra0i;
        Gj = Gj + t1j* ra1j * rra0j * rra0j;

        calcG = true;
    } else {
	    FadV<dType> ra1i(size);
	    FadV<dType> ra1j(size);

	    ra1i._val = zero;
        ra1i._dx = one;
        ra1j = ra1i;
    }

    if (beta2 != 0.0 && t2 != 0.0) {
        FadV<dType> rhoa2i(size);
        FadV<dType> rhoa2xi(size);
        FadV<dType> rhoa2yi(size);
        FadV<dType> rhoa2zi(size);
        FadV<dType> ra2i(size);

        FadV<dType> rhoa2j(size);
        FadV<dType> rhoa2xj(size);
        FadV<dType> rhoa2yj(size);
        FadV<dType> rhoa2zj(size);
        FadV<dType> ra2j(size);

    	rhoa2i._val = tab.Tab_rho2_val.getValue(PairR.keyi, PairR.R);
        rhoa2i._dx  = tab.Tab_rho2_dx.getValue(PairR.keyi, PairR.R);

    	rhoa2j._val = tab.Tab_rho2_val.getValue(PairR.keyj, PairR.R);
        rhoa2j._dx  = tab.Tab_rho2_dx.getValue(PairR.keyj, PairR.R);

        rhoa2xi = rhoa2i * x;
        rhoa2yi = rhoa2i * y;
        rhoa2zi = rhoa2i * z;

        rhoa2xj = rhoa2j * x;
        rhoa2yj = rhoa2j * y;
        rhoa2zj = rhoa2j * z;

        ra2i._val = fill(size,At.rho2[i] );
        ra2j._val = rhj2;

        ra2i._dx = pow2sumVdx(x * rhoa2xi)
                 + pow2sumVdx(y * rhoa2xi)
                 + pow2sumVdx(z * rhoa2xi)

                 + pow2sumVdx(x * rhoa2yi)
                 + pow2sumVdx(y * rhoa2yi)
                 + pow2sumVdx(z * rhoa2yi)

                 + pow2sumVdx(x * rhoa2zi)
                 + pow2sumVdx(y * rhoa2zi)
                 + pow2sumVdx(z * rhoa2zi)
                 - 1.0/3.0 * pow2sumVdx(rhoa2i);

        ra2j._dx = pow2sumVdx(x * rhoa2xj)
                 + pow2sumVdx(y * rhoa2xj)
                 + pow2sumVdx(z * rhoa2xj)

                 + pow2sumVdx(x * rhoa2yj)
                 + pow2sumVdx(y * rhoa2yj)
                 + pow2sumVdx(z * rhoa2yj)

                 + pow2sumVdx(x * rhoa2zj)
                 + pow2sumVdx(y * rhoa2zj)
                 + pow2sumVdx(z * rhoa2zj)
                 - 1.0/3.0 * pow2sumVdx(rhoa2j);

         Gi = Gi + t2 * ra2i * rra0i * rra0i;
         Gj = Gj + t2j* ra2j * rra0j * rra0j;
         calcG = true;
    } else {
        FadV<dType> ra2i(size);
        FadV<dType> ra2j(size);

        ra2i._val = zero;
        ra2i._dx = one;
        ra2j = ra2i;
    }

    if (beta3 != 0.0 && t3 != 0.0) {
        FadV<dType> rhoa3i(size);
        FadV<dType> rhoa3xi(size);
        FadV<dType> rhoa3yi(size);
        FadV<dType> rhoa3zi(size);
        FadV<dType> ra3i(size);

        FadV<dType> rhoa3j(size);
        FadV<dType> rhoa3xj(size);
        FadV<dType> rhoa3yj(size);
        FadV<dType> rhoa3zj(size);
        FadV<dType> ra3j(size);

        rhoa3i._val = tab.Tab_rho3_val.getValue(PairR.keyi, PairR.R);
        rhoa3i._dx = tab.Tab_rho3_dx.getValue(PairR.keyi, PairR.R);

        rhoa3j._val = tab.Tab_rho3_val.getValue(PairR.keyj, PairR.R);
        rhoa3j._dx = tab.Tab_rho3_dx.getValue(PairR.keyj, PairR.R);

        rhoa3xi = rhoa3i * x;
        rhoa3yi = rhoa3i * y;
        rhoa3zi = rhoa3i * z;

        rhoa3xj = rhoa3j * x;
        rhoa3yj = rhoa3j * y;
        rhoa3zj = rhoa3j * z;

        ra3i._val = fill(size,At.rho3[i] );
        ra3j._val = rhj3;

        ra3i._dx = pow2sumVdx(x * x * rhoa3xi)
                 + pow2sumVdx(x * y * rhoa3xi)
                 + pow2sumVdx(x * z * rhoa3xi)

                 + pow2sumVdx(y * x * rhoa3xi)
                 + pow2sumVdx(y * y * rhoa3xi)
                 + pow2sumVdx(y * z * rhoa3xi)

                 + pow2sumVdx(z * x * rhoa3xi)
                 + pow2sumVdx(z * y * rhoa3xi)
                 + pow2sumVdx(z * z * rhoa3xi)


                 + pow2sumVdx(x * x * rhoa3yi)
                 + pow2sumVdx(x * y * rhoa3yi)
                 + pow2sumVdx(x * z * rhoa3yi)

                 + pow2sumVdx(y * x * rhoa3yi)
                 + pow2sumVdx(y * y * rhoa3yi)
                 + pow2sumVdx(y * z * rhoa3yi)

                 + pow2sumVdx(z * x * rhoa3yi)
                 + pow2sumVdx(z * y * rhoa3yi)
                 + pow2sumVdx(z * z * rhoa3yi)


                 + pow2sumVdx(x * x * rhoa3zi)
                 + pow2sumVdx(x * y * rhoa3zi)
                 + pow2sumVdx(x * z * rhoa3zi)

                 + pow2sumVdx(y * x * rhoa3zi)
                 + pow2sumVdx(y * y * rhoa3zi)
                 + pow2sumVdx(y * z * rhoa3zi)

                 + pow2sumVdx(z * x * rhoa3zi)
                 + pow2sumVdx(z * y * rhoa3zi)
                 + pow2sumVdx(z * z * rhoa3zi)

                 -3.0/5.0 * (pow2sumVdx(x * rhoa3i)
                           + pow2sumVdx(y * rhoa3i)
                           + pow2sumVdx(z * rhoa3i));

        ra3j._dx = pow2sumVdx(x * x * rhoa3xj)
                 + pow2sumVdx(x * y * rhoa3xj)
                 + pow2sumVdx(x * z * rhoa3xj)

                 + pow2sumVdx(y * x * rhoa3xj)
                 + pow2sumVdx(y * y * rhoa3xj)
                 + pow2sumVdx(y * z * rhoa3xj)

                 + pow2sumVdx(z * x * rhoa3xj)
                 + pow2sumVdx(z * y * rhoa3xj)
                 + pow2sumVdx(z * z * rhoa3xj)


                 + pow2sumVdx(x * x * rhoa3yj)
                 + pow2sumVdx(x * y * rhoa3yj)
                 + pow2sumVdx(x * z * rhoa3yj)

                 + pow2sumVdx(y * x * rhoa3yj)
                 + pow2sumVdx(y * y * rhoa3yj)
                 + pow2sumVdx(y * z * rhoa3yj)

                 + pow2sumVdx(z * x * rhoa3yj)
                 + pow2sumVdx(z * y * rhoa3yj)
                 + pow2sumVdx(z * z * rhoa3yj)


                 + pow2sumVdx(x * x * rhoa3zj)
                 + pow2sumVdx(x * y * rhoa3zj)
                 + pow2sumVdx(x * z * rhoa3zj)

                 + pow2sumVdx(y * x * rhoa3zj)
                 + pow2sumVdx(y * y * rhoa3zj)
                 + pow2sumVdx(y * z * rhoa3zj)

                 + pow2sumVdx(z * x * rhoa3zj)
                 + pow2sumVdx(z * y * rhoa3zj)
                 + pow2sumVdx(z * z * rhoa3zj)

                 -3.0/5.0 * (pow2sumVdx(x * rhoa3j)
                           + pow2sumVdx(y * rhoa3j)
                           + pow2sumVdx(z * rhoa3j));

        Gi = Gi + t3 * ra3i * rra0i * rra0i;
        Gj = Gj + t3j* ra3j * rra0j * rra0j;

        calcG = true;
    } else {
        FadV<dType> ra3i(size);
        FadV<dType> ra3j(size);

        ra3i._val = zero;
        ra3i._dx = one;

        ra3j = ra3i;
    }

    FadV<dType> fGi(size);
    FadV<dType> fGj(size);
    FadV<dType> Frhoi(size);
    FadV<dType> Frhoj(size);

    fGi    = 2.0 / (1.0 + exp(-Gi));//sqrt(1.0 + Gi);
    fGj    = 2.0 / (1.0 + exp(-Gj));//sqrt(1.0 + Gj);

    FadV<dType> rhoi(size);
    FadV<dType> Energyi(size);
    FadV<dType> rhoj(size);
    FadV<dType> Energyj(size);
    std::vector<dType> Esc;
    std::vector<dType> Fsc;

    rhoi  = ra0i * fGi;
    rhoi  = rhoi / Nz;

    rhoj  = ra0j * fGj;
    rhoj  = rhoj / Nzj;

    std::vector<dType> dxi = rhoi._dx;
    std::vector<dType> dxj = rhoj._dx;
    rhoi._dx = one;
    rhoj._dx = one;
    Frhoi = Ai * Ei * rhoi * log(rhoi);
    Frhoj = Aj * Ej * rhoj * log(rhoj);

    Energyi._val = Frhoi._val;// * FcutD;
    Energyj._val = Frhoj._val;// * FcutD;

    Energyi._dx = Frhoi._dx * dxi;// * FcutD;
    Energyj._dx = Frhoj._dx * dxj;// * FcutD;

    Esc    =  Energyi._val;// * FcutD * FcutD;
    Fsc    = -(Energyi._dx + Energyj._dx); // * FcutD * FcutD;

    std::replace_if(Esc.begin(), Esc.end(), IsNan, 0.0);
    std::replace_if(Esc.begin(), Esc.end(), IsInf, 0.0);
    std::replace_if(Fsc.begin(), Fsc.end(), IsNan, 0.0);
    std::replace_if(Fsc.begin(), Fsc.end(), IsInf, 0.0);


    At.FscRS[i] += sum(Fsc * PairR.R);
    At.EscS[i]  += Esc[0];

    Fx = sum(Fsc * PairR.R1x);
    Fy = sum(Fsc * PairR.R1y);
    Fz = sum(Fsc * PairR.R1z);

    At.Fxs[i] += Fx;
    At.Fys[i] += Fy;
    At.Fzs[i] += Fz;

    At.rxfy[i] += 0.5 * sum(PairR.Rx * PairR.R1y * Fsc);
    At.ryfz[i] += 0.5 * sum(PairR.Ry * PairR.R1z * Fsc);
    At.rzfx[i] += 0.5 * sum(PairR.Rz * PairR.R1x * Fsc);
    PairR.Fsc = PairR.Fsc + Fsc;
    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  FFMEAM : " << std::endl;
    #endif
}


void System::FFPolarization(int i, PairDistV& PairR) {
    int j, p;
    int size = PairR.R.size();
    dType s0, s3;//, Fqx, Fqy, Fqz, Fpx, Fpy, Fpz;
    dType FscRsc, PotEnergy_tau, Fx, Fy, Fz;

    std::vector<dType> Esc;
    std::vector<dType> Fsc;
    std::vector<dType> pjr;
    std::vector<dType> R;

    FadV<dType> Eqx(size), Eqy(size), Eqz(size), Epx(size), Epy(size), Epz(size), Eix(size), Eiy(size), Eiz(size);

    FadV<dType> Energy(size);
    FadV<dType> X(size);
    FadV<dType> rX(size);
    FadV<dType> R_sc(size);
    FadV<dType> R_sc2(size);
    FadV<dType> rR_sc(size);
    FadV<dType> rR_sc3(size);
    FadV<dType> rR_sc5(size);
    FadV<dType> rR_sc7(size);
    FadV<dType> Rx(size);
    FadV<dType> Ry(size);
    FadV<dType> Rz(size);

    FadV<dType> Tx(size);
    FadV<dType> Ty(size);
    FadV<dType> Tz(size);

    R     = 1.889725989 * PairR.R;
    R_sc  = assignValV(R);
    rR_sc = 1.0 / R_sc;

    R_sc2 = R_sc * R_sc;

    rR_sc3 = rR_sc  * rR_sc * rR_sc;
    rR_sc5 = rR_sc3 * rR_sc * rR_sc;
    rR_sc7 = rR_sc5 * rR_sc * rR_sc;

    Tx  = PairR.Rx * rR_sc3;
    Ty  = PairR.Ry * rR_sc3;
    Tz  = PairR.Rz * rR_sc3;

    // The electric field acting on the point charge due to other point charges (Trullas2003, eq:2.4)
    Eqx  = sumV(PairR.q * Tx);   // 5.2164543514 *
    Eqy  = sumV(PairR.q * Ty);
    Eqz  = sumV(PairR.q * Tz);

    pjr  = (PairR.muxpr * PairR.Rx + PairR.muypr * PairR.Ry + PairR.muzpr * PairR.Rz);

    Epx  = sumV(3.0 * pjr * PairR.Rx * rR_sc5 - PairR.muxpr * rR_sc3);
    Epy  = sumV(3.0 * pjr * PairR.Ry * rR_sc5 - PairR.muypr * rR_sc3);
    Epz  = sumV(3.0 * pjr * PairR.Rz * rR_sc5 - PairR.muzpr * rR_sc3);

    Eix   = Eqx + Epx;
    Eiy   = Eqy + Epy;
    Eiz   = Eqz + Epz;

    At.Eex[i] = Eix._val[0];
    At.Eey[i] = Eiy._val[0];
    At.Eez[i] = Eiz._val[0];

    At.mux[i] = (6.7483330371 * At.polariz[i]) * At.Eex[i];
    At.muy[i] = (6.7483330371 * At.polariz[i]) * At.Eey[i];
    At.muz[i] = (6.7483330371 * At.polariz[i]) * At.Eez[i];

    Energy =  27.211396 *
            (- (At.mux[i] * Eqx + At.muy[i] * Eqy + At.muz[i] * Eqz)
             - 0.5*(At.mux[i] * Epx + At.muy[i] * Epy + At.muz[i] * Epz)
             + 0.5*(0.1481847435 * At.rpolariz[i]) * (At.mux[i] * At.mux[i] + At.muy[i] * At.muy[i] + At.muz[i] * At.muz[i]));

    Esc   =  Energy._val;
    Fsc    = -Energy._dx;

    std::replace_if (Esc.begin(), Esc.end(), IsNan, 0.0);
    std::replace_if (Esc.begin(), Esc.end(), IsInf, 0.0);
    std::replace_if (Fsc.begin(), Fsc.end(), IsNan, 0.0);
    std::replace_if (Fsc.begin(), Fsc.end(), IsInf, 0.0);

    FscRsc = sum(Fsc * PairR.R);

    Fx = sum(Fsc * PairR.R1x);
    Fy = sum(Fsc * PairR.R1y);
    Fz = sum(Fsc * PairR.R1z);

    At.Fxp[i] += Fx;
    At.Fyp[i] += Fy;
    At.Fzp[i] += Fz;
    At.FscRsc[i] += FscRsc;
    At.PotEnergy_tau[i] += Esc[0];
    PairR.Fsc = PairR.Fsc + Fsc;
}

void System::FFThCond(int i, PairDistV& PairRM, PairDistV& PairRL) {
    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  FFThCond : " << std::endl;
    #endif
    int j, p;
    dType R_sc2;
    dType Fx, Fy, Fz, FscRsc, PotEnergy_tau;

    std::vector<dType> A0;

    A0 = ((At.vx_mid[i] + PairRM.vx_mid) * PairRM.Fsc * PairRM.R1x +
              (At.vy_mid[i] + PairRM.vy_mid) * PairRM.Fsc * PairRM.R1y +
              (At.vz_mid[i] + PairRM.vz_mid) * PairRM.Fsc * PairRM.R1z);

    At.condx[i] = 0.5 * sum(A0 * PairRM.Rx);
    At.condy[i] = 0.5 * sum(A0 * PairRM.Ry);
    At.condz[i] = 0.5 * sum(A0 * PairRM.Rz);

    A0 = ((At.vx_mid[i] + PairRL.vx_mid) * PairRL.Fsc * PairRL.R1x +
          (At.vy_mid[i] + PairRL.vy_mid) * PairRL.Fsc * PairRL.R1y +
          (At.vz_mid[i] + PairRL.vz_mid) * PairRL.Fsc * PairRL.R1z);

    At.condx[i] += 0.5 * sum((A0 * PairRL.Rx));
    At.condy[i] += 0.5 * sum((A0 * PairRL.Ry));
    At.condz[i] += 0.5 * sum((A0 * PairRL.Rz));

    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  FFThCond : " << std::endl;
    #endif
}

/*
void System::ElectronTransfer(int i, PairDistV& PairR) {
    int j, p, jp, jm;
    int size = PairR.q.size();

    dType R_sc2;
    dType Rx, Ry, Rz, Fx, Fy, Fz, FscRsc, props.PotEnergy_tau, cond.Rc_long_Bohr, rlimits.Rc_long_Bohr, erfcRC;
    dType Eia, Eii, DEip, DEim;

    std::vector<dType> R_sc_Bohr;
    std::vector<dType> rR_sc_Bohr;
    std::vector<dType> erfcR_Sc;

    std::vector<dType> Esc;
    std::vector<dType> Fsc;

    std::vector<dType> Ep;
    std::vector<dType> Em;
    std::vector<dType> qq, DEijpm, DEijmp, DEcorrmp, DEcorrpm;

    std::vector<dType> Eja(size), Eji(size), DEcp, DEcm, DEjm, DEjp, DEtotp, DEtotm, R_sc(size), rR_sc(size);
    std::vector<bool> totP, totM;

    R_sc  = 1.889725989 * PairR.R;
    rR_sc = 1.0 / R_sc;
    qq = At.q[i] * PairR.q;

    Eia  = At.aff[i];
    Eii  = At.ion[i];

    Eja  = PairR.aff;
    Eji   = PairR.ion;

    DEip = At.Ep[i];
    DEim = At.Em[i];

    DEjm = PairR.Em;
    DEjp = PairR.Ep;

    DEijpm   =  27.211396 * rR_sc * ((At.q[i] + dq) * (PairR.q - dq) - qq);
    DEijmp   =  27.211396 * rR_sc * ((At.q[i] - dq) * (PairR.q + dq) - qq);

    DEcorrpm = -27.211396 * rR_sc * ((At.q[i] + dq) * qq + (PairR.q - dq) * qq);
    DEcorrmp = -27.211396 * rR_sc * ((At.q[i] - dq) * qq + (PairR.q + dq) * qq);

    DEtotp = 0.5 * (DEip + DEjm) + DEijpm + Eii + Eja + DEcorrpm;
    DEtotm = 0.5 * (DEim + DEjp) + DEijmp + Eia + Eji + DEcorrmp;

    jp = std::distance(DEtotp.begin(), std::min_element(DEtotp.begin(), DEtotp.end()));
    jm = std::distance(DEtotm.begin(), std::min_element(DEtotm.begin(), DEtotm.end()));

    At.dq[i] = 0.0;

    if (DEtotp[jp] < DEtotm[jm] && DEtotp[jp] < 0.0) {
        At.dq[i]   = dq;
        At.indj[i] = jp;
    }

    if (DEtotm[jm] < DEtotp[jp] && DEtotm[jm] < 0.0) {
        At.dq[i]   = -dq;
        At.indj[i] = jm;
    }
}
*/

void System::ElectronTransfer(int i, PairDistV& PairR) {
    int j, p, jp, jm;
    int size = PairR.q.size();

    dType R_sc2;
    dType Rx, Ry, Rz, Fx, Fy, Fz, FscRsc, PotEnergy_tau, rRc_long_Bohr, erfcRC;
    dType Eia, Eii, DEip, DEim;

    std::vector<dType> R_sc_Bohr;
    std::vector<dType> rR_sc_Bohr;
    std::vector<dType> erfcR_Sc;

    std::vector<dType> Esc;
    std::vector<dType> Fsc;

    std::vector<dType> Ep;
    std::vector<dType> Em;
    std::vector<dType> qq, DEijpm, DEijmp, DEcorrmp, DEcorrpm;

    std::vector<dType> Eja(size), Eji(size), DEjm, DEjp, DEtotp, DEtotm, R_sc(size), rR_sc(size);
    std::vector<bool> totP, totM;

    R_sc  = 1.889725989 * PairR.R;
    rR_sc = 1.0 / R_sc;
    qq = At.q[i] * PairR.q;

    Eia  = At.aff[i];
    Eii  = At.ion[i];

    Eja  = PairR.aff;
    Eji   = PairR.ion;

    DEip = At.Ep[i];
    DEim = At.Em[i];

    DEjm = PairR.Em;
    DEjp = PairR.Ep;

    DEijpm   =  27.211396 * rR_sc * ((At.q[i] + cond.dq) * (PairR.q - cond.dq) - qq);
    DEijmp   =  27.211396 * rR_sc * ((At.q[i] - cond.dq) * (PairR.q + cond.dq) - qq);

    DEcorrpm = -27.211396 * rR_sc * ((At.q[i] + cond.dq) * qq + (PairR.q - cond.dq) * qq);
    DEcorrmp = -27.211396 * rR_sc * ((At.q[i] - cond.dq) * qq + (PairR.q + cond.dq) * qq);

    // The BASIC ONE
    DEtotp  = 0.5 * (DEip + DEjm) + Eii + Eja + DEijpm + DEcorrpm;
    DEtotm = 0.5 * (DEim + DEjp) + Eia + Eji + DEijmp + DEcorrmp;

    jp  = std::distance(DEtotp.begin(), std::min_element(DEtotp.begin(), DEtotp.end()));
    jm = std::distance(DEtotm.begin(), std::min_element(DEtotm.begin(), DEtotm.end()));

    At.dq[i] = 0.0;

    if (DEtotp[jp] < DEtotm[jm] && DEtotp[jp] < 0.0) {
        At.dq[i]   = -cond.dq;
        At.indj[i] = PairR.j[jp];
    }

    if (DEtotm[jm] < DEtotp[jp] && DEtotm[jm] < 0.0) {
        At.dq[i]   = cond.dq;
        At.indj[i] = PairR.j[jm];
    }
}


/*
void System::ForceField() {
    struct timeval t1, t2;

    int cl, i, in, icl;
    PairDistV PairL;
    PairDistV PairDET;
    PairDistV PairM;
    PairL.Clear();
    PairM.Clear();
    std::vector<dType> R;
    std::vector<dType> Rx;
    std::vector<dType> Ry;
    std::vector<dType> Rz;

    if (!control.calc_Fp) {
        cond.di += 1.0;
    } else {
    	cond.di = 0;
    }

    double v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11;
    v1 = 0.0;
    v2 = 0.0;
    v3 = 0.0;
    v4 = 0.0;
    v5 = 0.0;
    v6 = 0.0;
    v7 = 0.0;
    v8 = 0.0;
    v9 = 0.0;
    v10 = 0.0;
    v11 = 0.0;

	t1 = start_t();
	#pragma omp parallel for private(i, in, icl, PairM) schedule(dynamic)
	for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
		for (in = 0; in < Cell[icl].atomI.size(); in++) {
			i = Cell[icl].atomI[in];
			PairM.Clear();
			PairListMEAM(i, PairM);

			PairM.R1x = PairM.Rx * PairM.rR;
			PairM.R1y = PairM.Ry * PairM.rR;
			PairM.R1z = PairM.Rz * PairM.rR;
			FFMEAMdens(i,  PairM);
		}
	}
	v1 +=  elapsed_t(t1);

    #pragma omp parallel for private(i, in, cl, icl, PairL, PairM, PairDET, R, Rx, Ry, Rz) schedule(dynamic)
	for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
		for (in = 0; in < Cell[icl].atomI.size(); in++) {
			#if(timing)
				v2 +=  elapsed_t(t1);
			#endif
			i = Cell[icl].atomI[in];
			PairDET.Clear();
			PairL.Clear();

			#if(timing)
				v3 +=  elapsed_t(t1);
			#endif
			if ((control.morse_b || control.coulomb_b) && control.calc_Fp) {
				PairList(i, cond.Rc_long, PairL);
				#if(timing)
					v4 +=  elapsed_t(t1);
				#endif

				if (PairL.j.size() == 0) continue;

				PairL.Fsc.resize(PairL.j.size());
				std::fill(PairL.Fsc.begin(), PairL.Fsc.end(), 0.0);
				PairL.R1x = PairL.Rx * PairL.rR;
				PairL.R1y = PairL.Ry * PairL.rR;
				PairL.R1z = PairL.Rz * PairL.rR;
			} 
			#if(timing)
				v5 +=  elapsed_t(t1);
			#endif

			if ((control.morse_b || control.coulomb_b) && control.calc_Fp) {
				FFPSI(i, PairL);
			}
			#if(timing)
				v6 +=  elapsed_t(t1);
			#endif

			if (control.polariz_b && control.calc_Fp) {
				PairL.q     = subset(At.q,     PairL.j);
				PairL.polb  = subset(At.polb,  PairL.j);
				PairL.polc  = subset(At.polc,  PairL.j);
				PairL.muxpr = subset(At.muxpr, PairL.j);
				PairL.muypr = subset(At.muypr, PairL.j);
				PairL.muzpr = subset(At.muzpr, PairL.j);
				FFPolarization(i,   PairL);
			}
			#if(timing)
				v7 +=  elapsed_t(t1);
			#endif

			if (control.meam_b  && control.calc_Fs) {
                PairListMEAM(i, PairM);
                PairM.Fsc.resize(PairM.j.size());
				std::fill(PairM.Fsc.begin(), PairM.Fsc.end(), 0.0);

				PairM.R1x = PairM.Rx * PairM.rR;
				PairM.R1y = PairM.Ry * PairM.rR;
				PairM.R1z = PairM.Rz * PairM.rR;
				FFMEAM(i,  PairM);
			}
			#if(timing)
				v8 +=  elapsed_t(t1);
			#endif

			if (control.det_b && control.calc_Fp) {
				PairDET.R = PairL.R;
				PairDET.j = PairL.j;
				PairDET.q     = subset(At.q,    PairL.j);
				PairDET.aff   = subset(At.aff,  PairL.j);
				PairDET.ion   = subset(At.ion,  PairL.j);
				PairDET.Em    = subset(At.Em,   PairL.j);
				PairDET.Ep    = subset(At.Ep,   PairL.j);
				ElectronTransfer(i, PairDET);
			}
			#if(timing)
				v9 +=  elapsed_t(t1);
			#endif

			if (control.calc_Fp) {
				if (At.vFx[i].size() < 3) {
					At.vFx[i].push_back(At.Fxp[i]);
					At.vFy[i].push_back(At.Fyp[i]);
					At.vFz[i].push_back(At.Fzp[i]);
				} else {
					At.vFx[i].erase(At.vFx[i].begin());
					At.vFy[i].erase(At.vFy[i].begin());
					At.vFz[i].erase(At.vFz[i].begin());
					At.vFx[i].push_back(At.Fxp[i]);
					At.vFy[i].push_back(At.Fyp[i]);
					At.vFz[i].push_back(At.Fzp[i]);
				}
			} else {
				At.Fxp[i] = At.vFx[i][2] + (At.vFx[i][2] - At.vFx[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFx[i][2] - 2.0 * At.vFx[i][1] + At.vFx[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
				At.Fyp[i] = At.vFy[i][2] + (At.vFy[i][2] - At.vFy[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFy[i][2] - 2.0 * At.vFy[i][1] + At.vFy[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
				At.Fzp[i] = At.vFz[i][2] + (At.vFz[i][2] - At.vFz[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFz[i][2] - 2.0 * At.vFz[i][1] + At.vFz[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
			}
			#if(timing)
				v10 +=  elapsed_t(t1);
			#endif

			At.Fx[i] = At.Fxp[i] + At.Fxs[i];
			At.Fy[i] = At.Fyp[i] + At.Fys[i];
			At.Fz[i] = At.Fzp[i] + At.Fzs[i];

			At.FscRsc[i] = At.FscRS[i] + At.FscRP[i];
			At.PotEnergy_tau[i] = At.EscS[i] + At.EscP[i];

			if (control.thermc_b && control.calc_Fp) {
				PairL.vx_mid    = subset(At.vx_mid, PairL.j);
				PairL.vy_mid    = subset(At.vy_mid, PairL.j);
				PairL.vz_mid    = subset(At.vz_mid, PairL.j);
				FFThCond(i, PairM, PairL);
			}
			#if(timing)
				v11 +=  elapsed_t(t1);
			#endif

		  }
	}
	#if(timing)

		std::cout << "MEAMDens         " << v1 << std::endl;
		std::cout << "Start            " << v2 << std::endl;
		std::cout << "Clear            " << v3 << std::endl;
		std::cout << "PairListL        " << v4 << std::endl;
		std::cout << "PairListMEAM     " << v5 << std::endl;
		std::cout << "FFPSI            " << v6 << std::endl;
		std::cout << "FFPolarization   " << v7 << std::endl;
		std::cout << "EEMEAM           " << v8 << std::endl;
		std::cout << "ElectronTransfer " << v9 << std::endl;
		std::cout << "At.Fzp           " << v10 << std::endl;
		std::cout << "FFThCond         " << v11 << std::endl;
	#endif

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsLow, -1.0e5);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsLow, -1.0e5);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsLow, -1.0e5);

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fx.begin(), At.Fx.end(), IsNan, 0.0);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsNan, 0.0);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsNan, 0.0);

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsInf, 0.0);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsInf, 0.0);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsInf, 0.0);

	control.calc_Fp = false;
	control.calc_Fs = false;

    //for (int i = 0; i < At.size ; i++) {
    //    std::cout << "  F["<<i << "] : " << At.Fx[i] << "  " << At.Fy[i] << "  " << At.Fz[i] << std::endl;
    //    std::cout << "  E["<<i << "] : " << At.PotEnergy_tau[i] << std::endl;
    //}
}
*/

void System::ForceField() {
    struct timeval t1, t2;

    int cl, i, in, icl;
    PairDistV PairL;
    PairDistV PairDET;
    PairDistV PairM;
    PairL.Clear();
    PairM.Clear();
    std::vector<dType> R;
    std::vector<dType> Rx;
    std::vector<dType> Ry;
    std::vector<dType> Rz;

    if (!control.calc_Fp) {
        cond.di += 1.0;
    } else {
    	cond.di = 0;
    }

    double v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11;
    v1 = 0.0;
    v2 = 0.0;
    v3 = 0.0;
    v4 = 0.0;
    v5 = 0.0;
    v6 = 0.0;
    v7 = 0.0;
    v8 = 0.0;
    v9 = 0.0;
    v10 = 0.0;
    v11 = 0.0;

	t1 = start_t();
	#pragma omp parallel for private(i, in, icl, PairM) schedule(dynamic)
	for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
		for (in = 0; in < Cell[icl].atomI.size(); in++) {
			i = Cell[icl].atomI[in];
			PairListMEAM(i, PairM);
			FFMEAMdens(i,  PairM);
		}
	}
	v1 +=  elapsed_t(t1);

    #pragma omp parallel for private(i, in, cl, icl, PairL, PairM, PairDET, R, Rx, Ry, Rz) schedule(dynamic)
	for (icl = 0; icl < Domain[inf.dID].cellI.size(); icl++) {
		for (in = 0; in < Cell[icl].atomI.size(); in++) {
			#if(timing)
				v2 +=  elapsed_t(t1);
			#endif
			i = Cell[icl].atomI[in];
			PairDET.Clear();
			PairL.Clear();

			#if(timing)
				v3 +=  elapsed_t(t1);
			#endif
			if ((control.morse_b || control.coulomb_b) && control.calc_Fp) {
				PairList(i, cond.Rc_long, PairL);
				#if(timing)
					v4 +=  elapsed_t(t1);
				#endif

				if (PairL.j.size() == 0) continue;

				PairL.Fsc.resize(PairL.j.size());
				std::fill(PairL.Fsc.begin(), PairL.Fsc.end(), 0.0);
				PairL.R1x = PairL.Rx * PairL.rR;
				PairL.R1y = PairL.Ry * PairL.rR;
				PairL.R1z = PairL.Rz * PairL.rR;
				FFPSI(i, PairL);
			} 
			#if(timing)
				v5 +=  elapsed_t(t1);
			#endif

			if (control.polariz_b && control.calc_Fp) {
				PairL.q     = subset(At.q,     PairL.j);
				PairL.polb  = subset(At.polb,  PairL.j);
				PairL.polc  = subset(At.polc,  PairL.j);
				PairL.muxpr = subset(At.muxpr, PairL.j);
				PairL.muypr = subset(At.muypr, PairL.j);
				PairL.muzpr = subset(At.muzpr, PairL.j);
				FFPolarization(i,   PairL);
			}
			#if(timing)
				v7 +=  elapsed_t(t1);
			#endif

			if (control.meam_b  && control.calc_Fs) {
                PairListMEAM(i, PairM);
				FFMEAM(i,  PairM);
			}
			#if(timing)
				v8 +=  elapsed_t(t1);
			#endif

			if (control.det_b && control.calc_Fp) {
				PairDET.R = PairL.R;
				PairDET.j = PairL.j;
				PairDET.q     = subset(At.q,    PairL.j);
				PairDET.aff   = subset(At.aff,  PairL.j);
				PairDET.ion   = subset(At.ion,  PairL.j);
				PairDET.Em    = subset(At.Em,   PairL.j);
				PairDET.Ep    = subset(At.Ep,   PairL.j);
				ElectronTransfer(i, PairDET);
			}
			#if(timing)
				v9 +=  elapsed_t(t1);
			#endif

			if (control.calc_Fp) {
				if (At.vFx[i].size() < 3) {
					At.vFx[i].push_back(At.Fxp[i]);
					At.vFy[i].push_back(At.Fyp[i]);
					At.vFz[i].push_back(At.Fzp[i]);
				} else {
					At.vFx[i].erase(At.vFx[i].begin());
					At.vFy[i].erase(At.vFy[i].begin());
					At.vFz[i].erase(At.vFz[i].begin());
					At.vFx[i].push_back(At.Fxp[i]);
					At.vFy[i].push_back(At.Fyp[i]);
					At.vFz[i].push_back(At.Fzp[i]);
				}
			} else {
				At.Fxp[i] = At.vFx[i][2] + (At.vFx[i][2] - At.vFx[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFx[i][2] - 2.0 * At.vFx[i][1] + At.vFx[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
				At.Fyp[i] = At.vFy[i][2] + (At.vFy[i][2] - At.vFy[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFy[i][2] - 2.0 * At.vFy[i][1] + At.vFy[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
				At.Fzp[i] = At.vFz[i][2] + (At.vFz[i][2] - At.vFz[i][1]) * cond.rd_tau * cond.di * cond.d_taus + 0.5 * (At.vFz[i][2] - 2.0 * At.vFz[i][1] + At.vFz[i][0]) * cond.rd_tau * cond.rd_tau * cond.d_taus * cond.d_taus * cond.di * cond.di;
			}
			#if(timing)
				v10 +=  elapsed_t(t1);
			#endif

			At.Fx[i] = At.Fxp[i] + At.Fxs[i];
			At.Fy[i] = At.Fyp[i] + At.Fys[i];
			At.Fz[i] = At.Fzp[i] + At.Fzs[i];

			At.FscRsc[i] = At.FscRS[i] + At.FscRP[i];
			At.PotEnergy_tau[i] = At.EscS[i] + At.EscP[i];

			if (control.thermc_b && control.calc_Fp) {
				PairL.vx_mid    = subset(At.vx_mid, PairL.j);
				PairL.vy_mid    = subset(At.vy_mid, PairL.j);
				PairL.vz_mid    = subset(At.vz_mid, PairL.j);
				FFThCond(i, PairM, PairL);
			}
			#if(timing)
				v11 +=  elapsed_t(t1);
			#endif

		  }
	}
	#if(timing)

		std::cout << "MEAMDens         " << v1 << std::endl;
		std::cout << "Start            " << v2 << std::endl;
		std::cout << "Clear            " << v3 << std::endl;
		std::cout << "PairListL        " << v4 << std::endl;
		std::cout << "PairListMEAM     " << v5 << std::endl;
		std::cout << "FFPSI            " << v6 << std::endl;
		std::cout << "FFPolarization   " << v7 << std::endl;
		std::cout << "EEMEAM           " << v8 << std::endl;
		std::cout << "ElectronTransfer " << v9 << std::endl;
		std::cout << "At.Fzp           " << v10 << std::endl;
		std::cout << "FFThCond         " << v11 << std::endl;
	#endif

/*
	std::replace_if (At.Fx.begin(), At.Fx.end(), IsnFVal, 0.0);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsnFVal, 0.0);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsnFVal, 0.0);
*/

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsLow, -1.0e5);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsLow, -1.0e5);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsLow, -1.0e5);

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsHihg, 1.0e5);
	std::replace_if (At.Fx.begin(), At.Fx.end(), IsNan, 0.0);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsNan, 0.0);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsNan, 0.0);

	std::replace_if (At.Fx.begin(), At.Fx.end(), IsInf, 0.0);
	std::replace_if (At.Fy.begin(), At.Fy.end(), IsInf, 0.0);
	std::replace_if (At.Fz.begin(), At.Fz.end(), IsInf, 0.0);

	control.calc_Fp = false;
	control.calc_Fs = false;

    //for (int i = 0; i < At.size ; i++) {
    //    std::cout << "  F["<<i << "] : " << At.Fx[i] << "  " << At.Fy[i] << "  " << At.Fz[i] << std::endl;
    //    std::cout << "  E["<<i << "] : " << At.PotEnergy_tau[i] << std::endl;
    //}
}


void System::DETStep() {
    int icl, cl, in, i, qi;
    int j, jp, jm;
    bool condition;
    dType q;
    std::string name;

    #pragma omp parallel for private(in, i, j) schedule(dynamic)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        j = At.indj[i];

        condition = ( j > i) ;
        if ( condition ) {
            #pragma omp critical
                At.q[i]    -= At.dq[i];
            #pragma omp critical
                At.q[j]    += At.dq[i];
        }
    }

    #pragma omp parallel for private(in, i, name, q, qi) schedule(dynamic)
    for (in = 0; in < Domain[inf.dID].atomI.size(); in++) {
        i = Domain[inf.dID].atomI[in];
        name = At.name[i];
        q    = At.q[i];
        qi = (int)((q+3.0)/cond.dq+0.5);
        At.ion[i]   = lup_dEq[name].ion[qi];
        At.aff[i]    = lup_dEq[name].aff[qi];
    }
}


template<class T>
struct IsZero {
    bool operator()(T& L) const
    {
        return L == 0;
    }
};

void System::PairList(int& i, dType& Rcut, PairDistV& Pair) {
    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  PairList : " << std::endl;
    #endif
    int cl, k, im, cl2, n, j, in, Ncl, Ncl2, icl, count;
    dType Esc, Fsc, R_sc, R1x, R1y, R1z, Rx, Ry, Rz, Rcut2, val;
    Rcut2 = Rcut * Rcut;

    count = 0;
    At.qs[i] = 0.0;

    std::vector<int> index, *sub;
    std::vector<bool> cond;
    index.clear();

    PairDistV res;
    res.Clear();
    res.Reserve(At.size);

    std::vector<dType> valx, valy, valz;

    for ( k = 0; k < At.pairs[i].size(); k++ ) {

    	im      = At.pairs[i][k].im;
        cl2     = At.pairs[i][k].cl;
        sub     = &Cell[cl2].atomI;
    	index.insert(index.end(), (*sub).begin(), (*sub).end());

    	//valx = At.x[i] - (Cell[cl2].rx + inf.ImPos[im].x);
    	//valy = At.y[i] - (Cell[cl2].ry + inf.ImPos[im].y);
    	//valz = At.z[i] - (Cell[cl2].rz + inf.ImPos[im].z);

    	//std::cout << Cell[cl2].atomI.size() << "  " << Cell[cl2].rx.size() << std::endl;

        //res.Rx.insert(res.Rx.end(), valx.begin(), valx.end());
        //res.Ry.insert(res.Ry.end(), valy.begin(), valy.end());
        //res.Rz.insert(res.Rz.end(), valz.begin(), valz.end());

        for (n = 0; n < (*sub).size(); n++) {
            j = (*sub)[n];
            res.Rx.push_back(At.x[i] - (At.x[j] + inf.ImPos[im].x));
            res.Ry.push_back(At.y[i] - (At.y[j] + inf.ImPos[im].y));
            res.Rz.push_back(At.z[i] - (At.z[j] + inf.ImPos[im].z));
        	res.keyi.push_back(At.code[i] + 10 * At.code[j]);
        	res.keyj.push_back(10 * At.code[i] + At.code[j]);
        }
    }

    res.Resize(index.size());
    res.R2 = res.Rx * res.Rx + res.Ry * res.Ry + res.Rz * res.Rz;

    Pair.R2.clear();
    Pair.Rx.clear();
    Pair.Ry.clear();
    Pair.Rz.clear();
    Pair.j.clear();
    Pair.keyi.clear();
    Pair.keyj.clear();
    Pair.R.clear();
    Pair.rR.clear();

    for (n = 0; n < index.size(); n++) {
        if (res.R2[n] <=  Rcut2 && res.R2[n] > 0.0) {
            At.qs[i] += At.q[j];

            Pair.j.push_back(index[n]);
            Pair.keyi.push_back(res.keyi[n]);
            Pair.keyj.push_back(res.keyj[n]);

            Pair.R2.push_back(res.R2[n]);
            Pair.Rx.push_back(res.Rx[n]);
            Pair.Ry.push_back(res.Ry[n]);
            Pair.Rz.push_back(res.Rz[n]);
        }
    }
    Pair.R = sqrt(Pair.R2);
    Pair.rR = 1.0 / Pair.R;
    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  PairList : " << std::endl;
    #endif
}

void System::PairListMEAM(int& i, PairDistV& Pair) {
    int cl, k, im, cl2, n, j, in, Ncl, Ncl2, icl, count;
    dType R_sc2, Esc, Fsc, R_sc, rR_sc, R1x, R1y, R1z, Rx, Ry, Rz, Rcut2, val, Rc;

    #if(debug)
        std::cout << "id : " << inf.id << "    Enter  PairListMEAM : " << std::endl;
    #endif

    count = 0;
    At.qs[i] = 0.0;

    cl  = At.Cell[i];
    Ncl = Pairs[cl].size();
    for (k = 0; k < Ncl; k++)   {
        im      = Pairs[cl][k].im;
        cl2     = Pairs[cl][k].cl;
        Ncl2    = Cell[cl2].atomI.size();

        for (n = 0; n < Ncl2; n++) {

            j = Cell[cl2].atomI[n];
            if (j == i) continue;

            Rc = At.rc_meam[j]; //7.0; //params[At.name[j]].rc_meam;

            Rx = (At.x[i] - (At.x[j] + inf.ImPos[im].x));
            if (Rx >  Rc) continue;
            Ry = (At.y[i] - (At.y[j] + inf.ImPos[im].y));
            if (Ry >  Rc) continue;
            Rz = (At.z[i] - (At.z[j] + inf.ImPos[im].z));
            if (Rz >  Rc) continue;

            R_sc2 = (Rx * Rx + Ry * Ry + Rz * Rz);
            Rcut2 = Rc * Rc;

            if (R_sc2 <=  Rcut2 && R_sc2 > 0.0) {
                R_sc    = std::sqrt(R_sc2);
                rR_sc   = 1.0 / R_sc;
                At.qs[i] += At.q[j];

                if (Pair.j.size() <= count) {
                    Pair.j.push_back(j);
                    Pair.keyi.push_back(At.code[i]);
                    Pair.keyj.push_back(At.code[j]);
                    Pair.R.push_back(R_sc);
                    Pair.rR.push_back(rR_sc);
                    Pair.Rx.push_back(Rx);
                    Pair.Ry.push_back(Ry);
                    Pair.Rz.push_back(Rz);
                    Pair.Fsc.push_back(0.0);
                } else {
                    Pair.j[count]    = j;
                    Pair.keyi[count] = At.code[i];
                    Pair.keyj[count] = At.code[j];
                    Pair.R[count]    = R_sc;
                    Pair.rR[count]   = rR_sc;
                    Pair.Rx[count]   = Rx;
                    Pair.Ry[count]   = Ry;
                    Pair.Rz[count]   = Rz;
                    Pair.Fsc[count]  = 0.0;
                }
                count++;
            }
        }
    }

    if (Pair.j.size() > count) {
        Pair.j.resize(count);
        Pair.keyi.resize(count);
        Pair.keyj.resize(count);
        Pair.R.resize(count);
        Pair.rR.resize(count);
        Pair.Rx.resize(count);
        Pair.Ry.resize(count);
        Pair.Rz.resize(count);
        Pair.Fsc.resize(count);
    }

    Pair.R1x = Pair.Rx * Pair.rR;
    Pair.R1y = Pair.Ry * Pair.rR;
    Pair.R1z = Pair.Rz * Pair.rR;

    #if(debug)
        std::cout << "id : " << inf.id << "    Exit  PairListMEAM : " << std::endl;
    #endif

}

void System::PairList(int& i, PairDistV& Subset, dType& Rcut, PairDistV& Pair) {
    int j, in, count;

    count = 0;

    for (in = 0; in < Subset.j.size(); in++)   {
        if (Subset.R[in] <=  Rcut && Subset.R[in] > 0.0) {
            if (Pair.j.size() <= count) {
                Pair.R.push_back(Subset.R[in]);
                Pair.keyi.push_back(Subset.keyi[in]);
                Pair.keyj.push_back(Subset.keyj[in]);
                Pair.rR.push_back(Subset.rR[in]);
                Pair.j.push_back(Subset.j[in]);
                Pair.Rx.push_back(Subset.Rx[in]);
                Pair.Ry.push_back(Subset.Ry[in]);
                Pair.Rz.push_back(Subset.Rz[in]);
            } else {
                Pair.j[count] = Subset.j[in];
                Pair.keyi[count] = Subset.keyi[in];
                Pair.keyj[count] = Subset.keyj[in];
                Pair.R[count] = Subset.R[in];
                Pair.rR[count] = Subset.rR[in];
                Pair.Rx[count] = Subset.Rx[in];
                Pair.Ry[count] = Subset.Ry[in];
                Pair.Rz[count] = Subset.Rz[in];
            }
            count++;
        }
    }

    if (Pair.j.size() > count) {
        Pair.j.resize(count);
        Pair.keyi.resize(count);
        Pair.keyj.resize(count);
        Pair.R.resize(count);
        Pair.rR.resize(count);
        Pair.Rx.resize(count);
        Pair.Ry.resize(count);
        Pair.Rz.resize(count);
    }
}

void System::PairList(int& i, PairDistV& Subset, std::string type, PairDistV& Pair) {
    int j, in, count;
    dType Rc;

    count = 0;

    if (type == "meam") {
        for (in = 0; in < Subset.j.size(); in++)   {
            j = Subset.j[in];
            Rc = params[At.name[j]].rc_meam;
            if (Subset.R[in] <=  Rc && Subset.R[in] > 0.0) {
                if (Pair.j.size() <= count) {
                    Pair.j.push_back(Subset.j[in]);
                    Pair.keyi.push_back(Subset.keyi[in]);
                    Pair.keyj.push_back(Subset.keyj[in]);
                    Pair.R.push_back(Subset.R[in]);
                    Pair.rR.push_back(Subset.rR[in]);
                    Pair.Rx.push_back(Subset.Rx[in]);
                    Pair.Ry.push_back(Subset.Ry[in]);
                    Pair.Rz.push_back(Subset.Rz[in]);
                } else {
                    Pair.j[count] = Subset.j[in];
                    Pair.keyi[count] = Subset.keyi[in];
                    Pair.keyj[count] = Subset.keyj[in];
                    Pair.R[count] = Subset.R[in];
                    Pair.rR[count] = Subset.rR[in];
                    Pair.Rx[count] = Subset.Rx[in];
                    Pair.Ry[count] = Subset.Ry[in];
                    Pair.Rz[count] = Subset.Rz[in];
                }
                count++;
            }
        }
    }

    if (Pair.j.size() > count) {
        Pair.j.resize(count);
        Pair.keyi.resize(count);
        Pair.keyj.resize(count);
        Pair.R.resize(count);
        Pair.rR.resize(count);
        Pair.Rx.resize(count);
        Pair.Ry.resize(count);
        Pair.Rz.resize(count);
    }
}

FadV<dType> System::CutFunc(FadV<dType> x) {
    FadV<dType> res(x._val.size());
    Fad<dType> val;
    Fad<dType> r;

    for (int i = 0; i < x._val.size(); i++) {
        if (x._val[i] >= 1.0) {
            res._val[i] = 1.0;
            res._dx[i] = 1.0;
        } else if (x._val[i] <= 0.0) {
            res._val[i] = 0.0;
            res._dx[i] = 0.0;
        } else {
            r._val = x._val[i];
            r._dx  = x._dx[i];
            val = 0.5 * erfc(2.75 * (1.0 - 2.0 * r));
            res._val[i] = val._val;
            res._dx[i]  = val._dx;
        }
    }
    return res;
}

std::vector<dType> System::CutFuncD(std::vector<dType> x, std::vector<dType> Rc) {
    std::vector<dType> res(x.size()), Dr(x.size()), XcD(x.size()), XcDc(x.size());

	Dr  = Rc / 1.85;
	XcD  = pow( x / (Rc - Dr), 5.0);
    XcDc = pow(Rc / (Rc - Dr), 5.0);
    res = exp( - 0.69315 * XcD) - exp( - 0.69315 * XcDc);
    std::replace_if(res.begin(), res.end(), IsNeg, 0.0);
    return pow(res, 1.0);

}

std::vector<dType> System::CutFuncD(std::vector<dType> x, dType Rc) {
    std::vector<dType> res(x.size()), XcD(x.size()), exppart(x.size()), polpart(x.size());
    dType Dr, XcDc;

	Dr      = Rc / 2.25;
	XcD     = pow( x / (Rc - Dr), 6.0);
    XcDc    = pow(Rc / (Rc - Dr), 6.0);
    exppart = exp( - 0.69315 * XcD) - exp( - 0.69315 * XcDc);

    polpart = 1.0 - (4.0 * pow( (x - 0.75 * Rc) / (Rc - 0.75 * Rc), 3.0) -  3.0 * pow( (x - 0.75 * Rc) / (Rc - 0.75 * Rc), 4.0));
    for (int i = 0.0; i < polpart.size(); i++) {
        if (x[i] >= Rc)        polpart[i] = 0.0;
        //if (x[i] <= (0.75*Rc)) polpart[i] = 1.0;
    }

    for (int i = 0.0; i < exppart.size(); i++) {
        if (x[i] >= Rc)        exppart[i] = 0.0;
        //if (x[i] <= (0.75*Rc)) polpart[i] = 1.0;
    }

    res = exppart;// * polpart;
    //std::replace_if(res.begin(), res.end(), IsNeg, 0.0);
    return res;
}


std::string System::timeStr() {
    struct timeval tv;
    time_t timev = time(0);
    std::stringstream buffer;

    struct tm * ltm = localtime( & timev);
    gettimeofday(&tv, NULL);

    buffer.setf(std::ios::fixed);
    buffer.width(6);
    buffer << ltm->tm_hour << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_min << ":";
    buffer.width(2);
    buffer.fill('0');
    buffer << ltm->tm_sec<<".";
    buffer.width(3);
    buffer.fill('0');
    buffer << tv.tv_usec;
    return buffer.str();
};

ValueUnit System::getValue(std::string& str) {

    ValueUnit res;

    std::string::iterator c;
	std::string val = "";
	std::string unit = "";

	for (c = str.begin(); c < str.end(); ++c) {

		if (isdigit(*c) || (*c) == '.' || (*c) == ',' || (*c) == '-' || (*c) == '+') {
			val += *c;
		} else if (isalpha(*c)) {
			unit += (*c);
		} else {
		}
	}


    res.scale = 1.0;
    res.shift = 0.0;

    if (unit.size() > 0) {
        res.unut  = unit;
        res.value = funcs::ToDouble(val);

        if (res.unut == "fs")
            res.scale = 1.0;
        if (res.unut == "ps")
            res.scale = 1.0e3;
        if (res.unut == "ns")
            res.scale = 1.0e6;

        if (res.unut == "A")
            res.scale = 1.0;
        if (res.unut == "nm")
            res.scale = 1.0e1;
        if (res.unut == "pm")
            res.scale = 1.0e-2;

        if (res.unut == "K")
            res.shift = 0.0;
        if (res.unut == "C")
            res.shift = +273.15;

        if (res.unut == "MPa")
            res.scale = 1.0;
        if (res.unut == "kPa")
            res.scale = 1.0e-3;
        if (res.unut == "GPa")
            res.scale = 1.0e3;
        if (res.unut == "Bar" || res.unut == "bar")
            res.scale = 1.0e-1;
    } else {
        res.scale = 1.0;
        res.shift = 0.0;
        res.value = funcs::ToDouble(val);
    }

    res.value = (res.value + res.shift) * res.scale;

	return res;

}
