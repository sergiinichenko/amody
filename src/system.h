//-------------------------------------------------------------------
// File:   system.h
// Author: sergii.nichenko
//
// AMoDy is a framework for a genera-purpose molecular dynamics modeling
//
// Copyright (C) 2013-2018 Sergii Nichenko
// <AMoDy, mailto:sergii.nichenko@psi.ch>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------

#ifndef SYSTEM_H
#define SYSTEM_H

#include <ctime>
#include <stdexcept>
#include <iostream>
#include "substance.h"
#include <fstream>
#include <string>
#include <omp.h>
#include <map>
#include <stdio.h>              /* I/O lib         ISOC  */
#include <stdlib.h>             /* Standard Lib    ISOC  */
#include <string.h>             /* Strings         ISOC  */
#include <cmath>
#include <algorithm>
#include <numeric>
#include "mpi.h"
#include <valarray>
#include "math/neural.h"
#include "math/fad.h"
#include "math/funcs.h"
#include "math/vector.h"
#include "datatypes.h"
#include "constants.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/time.h>
 #endif

class System {
public:

    // -----------------  Neural Network data set        --------------------
    Fit fit;
    Properties props;
    Controls control;
    Conditions cond;
    Files files;
    Tables tab;
    Information inf;
    Net net;
    Maps maps;
    std::map<std::string, Params> params;
    std::map<std::string, DETparams> detpar;

    //Diffusion Data
    Corr_struct Rvv;
    XYZ_vec Velocity_Vec;


    // Thermal Conductivity Data
    Corr_struct RJJ;
    XYZ_vec J_current_Vec;

    // Viscosity calculation
    Visc_Struct P_vec;
    Corr_struct Corr_P_vec;


    std::vector<XYZstruct> Force;

    std::vector<std::map<std::string, Params> > Coeffs;

    std::vector<std::string> BoundaryFor;
    std::vector<std::string> WallFor;

    std::vector<rdfPair> RDFfor;
    
    std::string InfLevel;

    Atom At;
    std::vector<dType> AtData;
    std::vector<dType> ParData;

    std::vector<CellStruct> Cell;
    std::vector<DomainStruct> Domain;

    std::vector< std::vector<pair> > Pairs;
    std::vector<PairDist> PairR;

    rdfstruct RDF;
    rdfstruct RDFref;

    std::map<std::string, dEq > lup_dEq;
    std::map< std::string, StateStruct > States;
    

    System();
    void ForceField();
    ForceEnergy GetFER(std::string& namei, std::string& namej, std::vector<dType>& R);
    ForceEnergy GetFERPair(std::string& pair, std::vector<dType>& R);
	ForceEnergy GetFER(std::string& namei, std::string& namej, dType& R);

    void PairList(int& i, dType& Rcut, PairDistV& Pair);
    void PairListMEAM(int& i, PairDistV& Pair);
    void PairList(int& i, PairDistV& Subset, dType& Rcut, PairDistV& Pair);
    void PairList(int& i, PairDistV& Subset, std::string, PairDistV& Pair);
    void FFPSI(int Ati, PairDistV& PairR);
    void FFMEAM(int Ati, PairDistV& PairR);
    void FFMEAMdens(int Ati, PairDistV& PairR);
    FadV<dType> CutFunc(FadV<dType> x);
    std::vector<dType> CutFuncD(std::vector<dType> x, std::vector<dType> Rc);
    std::vector<dType> CutFuncD(std::vector<dType> x, dType Rc);
    void FFPolarization2(int Ati, std::vector<PairDist> PairR);
    void FFPolarization(int Ati, PairDistV& PairR);
    void FFThCond(int Ati, PairDistV& PairRM, PairDistV& PairRL);
    void ElectronTransfer(int Ati, PairDistV& PairR);
    void DETStep();

    void BackUpState(std::string);
    void RestoreState(std::string);
    void FindPairs();
    void DefineDomains();
    void UpdateCells();
    void ExchangeCellsData();
    void ExchangeData();
    void ExchangeThCondData();
    void ExchangeVelocities();
    void UpdateDomains();
    void Reset();
    void RestoreImages();
    void SetInitPositions();
    void ResetPositions();
    void AssignParams(std::map<std::string, Params>&);
    void Initialization();
    void InitializeData();
    void InitialTemperature();
    void ions();
    void UpdateVelocity(std::string);
    void NewPositions(std::string);
    void adjustTimeStep();
    void BoxConditions();
    void PeriodicConditions();
    bool CheckBoundary();
    void ResetForces(std::string);
    void RegVelocity();
    void RegPosition();
    void RegTemperature();
    void RegPressure();
    void GetProperties();
    void RadialDistFunc();
    int  getKey(std::string& name);
    dType CalcViscosity();
    dType CalcThermalConductivity();
    dType CalcDiffusion();
    dType CalcSpecificHeat();
    dType CalcIsotermalCompressibility();
    void CalcThCData();
    void PremeltR();
    void PremeltL();
    void PhonoDensityOfSate();
    void AverageProperties();
    std::vector<dType> Autocorrelation(XYZ_vec& jvec, int end);
    void AutocorrelationCumm(XYZ_vec& jvec, Corr_struct& RJJ, int size);
    void AutocorrelationMov(XYZ_vec& jvec, Corr_struct& RJJ, int size);
    std::vector<dType> FFFilter(std::vector<dType>& jvec, double freq);

    #ifdef _WIN32
        LARGE_INTEGER t1, t2;           // ticks
    #else
        struct timeval t1, t2;
    #endif

    std::string timeStr();
    ValueUnit getValue(std::string& str);
};

#endif
